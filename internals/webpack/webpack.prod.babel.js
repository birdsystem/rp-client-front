// Important modules this config uses
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const OfflinePlugin = require('offline-plugin');

module.exports = require('./webpack.base.babel')({
  devtool: 'source-map',
  // In production, we skip all hot-reloading stuff
  entry: [
    path.join(process.cwd(), 'app/app.js'),
    path.join(process.cwd(), 'node_modules/antd-mobile'),
  ],

  // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },

  // We use ExtractTextPlugin so we get a separate CSS file instead
  // of the CSS being in the JS and injected as a style tag
  cssLoaders: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    // use: 'css-loader?modules&-autoprefixer&importLoaders=1!postcss-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: true,
          autoprefixer: false,
          importLoaders: 1,
        },
      },
      'postcss-loader',
    ],
  }),
  lessLoaders: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    // use: 'css-loader?modules&-autoprefixer&importLoaders=1!postcss-loader!less-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: true,
          autoprefixer: false,
          importLoaders: 1,
        },
      },
      'postcss-loader',
      {
        loader: 'less-loader',
        options: {
          javascriptEnabled: true,
        },
      },
    ],
  }),

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      children: true,
      minChunks: 2,
      async: true,
    }),

    // OccurrenceOrderPlugin is needed for long-term caching to work properly.
    // See http://mxs.is/googmv
    // new webpack.optimize.OccurrenceOrderPlugin(true),

    // Merge all duplicate modules
    // new webpack.optimize.DedupePlugin(),

    // Minify and optimize the JavaScript
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      minChunks: (module) => {
        const userRequest = module.userRequest;
        if (typeof userRequest !== 'string') {
          return false;
        }
        return /antd-mobile/.test(userRequest);
      },
    }),

    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     warnings: false, // ...but do not show warnings in the console (there is a lot of them)
    //   },
    // }),

    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'app/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
      inject: true,
    }),

    // Extract the CSS into a separate file
    new ExtractTextPlugin('[name].[contenthash].css'),

    // Put it in the end to capture all the HtmlWebpackPlugin's
    // assets manipulations and do leak its manipulations to HtmlWebpackPlugin
    // new OfflinePlugin({
    //   relativePaths: false,
    //   publicPath: '/',

    //   // No need to cache .htaccess. See http://mxs.is/googmp,
    //   // this is applied before any match in `caches` section
    //   excludes: ['.htaccess'],

    //   caches: {
    //     main: [':rest:'],

    //     // All chunks marked as `additional`, loaded after main section
    //     // and do not prevent SW to install. Change to `optional` if
    //     // do not want them to be preloaded at all (cached only when first loaded)
    //     additional: ['*.chunk.js'],
    //   },

    //   // Removes warning for about `additional` section usage
    //   safeToUseOptionalCaches: true,

    //   AppCache: false,
    // }),
  ],
});
