/* For production debug purpose, usage:
npm run debug https://www.tuantuanxia.com/0.615af446af70011763ae.chunk.js:1:42496
*/
const request = require('request');
const SourceMap = require('source-map');
const { SourceMapConsumer } = SourceMap;

const argv = require('minimist')(process.argv.slice(2))._;

const fullUrl = argv[0]; // 错误文件的 url（含行号列号），如 https://www.tuantuanxia.com/0.615af446af70011763ae.chunk.js:1:42496

const regex = /(?<url>.+\.js):(?<line>\d+):(?<col>\d+)$/;

const groups = fullUrl.match(regex).groups;

const { url, line, col } = groups;

console.log(`Line: ${line}, Col: ${col}, URL: ${url}`); // eslint-disable-line

request(`${url}.map`, (error, response, body) => {
  const rawSourceMap = JSON.parse(body);
  SourceMapConsumer.with(rawSourceMap, null, (consumer) => {
    const pos = consumer.originalPositionFor({
      line: +line,
      column: +col,
    });

    console.log(pos); // eslint-disable-line
  });
});
