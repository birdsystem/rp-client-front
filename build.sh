#!/usr/bin/env bash
set -e

trap "echo Fail unexpectedly on line \$LINENO! >&2" ERR

server_host="${1:-/api}"

if [[ "$1" == local ]]; then
  server_host="http://localhost:8888/api"
fi

cat <<EOF > ./app/config/index.js
export default {
  serverHost: '$server_host',
};
EOF

yarn
rm -rf build
npm run build

cp -r static/* build/
