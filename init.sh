#!/usr/bin/env bash
docker run --rm  \
           -v "$PWD":/usr/src/app \
           -w /usr/src/app \
           node:8-slim \
           ./build.sh
