/* eslint-disable no-unused-vars */
import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import message from 'components/message';
import {
  selectLatestCollectCouponTime,
  selectCurrentCoupon,
  selectCurrentUser,
} from 'containers/App/selectors';
import {
  setShowRedPacketAfterLogin as _setShowRedPacketAfterLogin,
  collectACoupon as _collectACoupon,
  setShowRedPacketModal,
} from 'containers/App/actions';
import { browserHistory } from 'react-router';
import styles from './styles.css';
import redpacket from './redpacket.jpg';
import stop from './stop.jpg';

const NewYearChangeBanner = ({ t, latestCollectCouponTime, currentCoupon, currentUser, collectACoupon, setShowRedPacketAfterLogin }) => {
  const showCollectBanner = ((new Date()).getTime() - latestCollectCouponTime) >
    86400000 && !_.isEmpty(currentCoupon) && !_.isEmpty(currentUser);
  return (
    <div className={styles.bannerWrap}>
      { showCollectBanner ? <div
        onClick={() => {
          if (_.isEmpty(currentUser)) {
            setShowRedPacketAfterLogin(true);
            message.info(
                this.props.t('common.collect_after_login_or_register'));
            const path = encodeURIComponent(window.location.pathname);
            browserHistory.push(`/login?service=${path}`);
            return;
          }
          const coupon = currentCoupon;
          if (_.isEmpty(coupon)) {
            return;
          }
          collectACoupon({
            coupon_id: coupon.id,
            showRedPacketModal: true,
          });
        }}
      >
        <img
          src={redpacket}
          className={styles.bannerImg}
          alt={''}
        />
      </div>
        :
      <div
        onClick={() => {
          browserHistory.push('/notice');
        }}
      >
        <img
          src={stop}
          className={styles.bannerImg}
          alt={''}
        />
      </div>
      }
    </div>
  );
};

NewYearChangeBanner.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  latestCollectCouponTime: PropTypes.number,
  currentCoupon: PropTypes.object,
  collectACoupon: PropTypes.func,
  setShowRedPacketAfterLogin: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectLatestCollectCouponTime(),
  selectCurrentCoupon(),
  selectCurrentUser(),
  (latestCollectCouponTime, currentCoupon, currentUser) => ({
    latestCollectCouponTime,
    currentCoupon: currentCoupon && currentCoupon.toJS(),
    currentUser: currentUser && currentUser.toJS(),
  }),
);

const mapDispatchToProps = (dispatch) => ({
  setShowRedPacketAfterLogin: (show) => dispatch(
    _setShowRedPacketAfterLogin(show)),
  collectACoupon: (data) => dispatch(_collectACoupon(data)),
  setShowRedPacketModal: (show) => dispatch(setShowRedPacketModal(show)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(NewYearChangeBanner));
