import React from 'react';
import { browserHistory } from 'react-router';
import { Icon } from 'antd-mobile';
import styles from './styles.css';

const BackIcon = () => (
  <div
    className={styles.backIcon}
    onClick={() => {
      if (browserHistory.length > 0) {
        browserHistory.goBack();
      } else {
        browserHistory.push('/');
      }
    }}
  >
    <Icon type="left" size="md" />
  </div>
);

export default BackIcon;
