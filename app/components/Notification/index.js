import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Icon } from 'antd-mobile';
import { translate } from 'react-i18next';
import { selectFollowGroupNotification, selectStartGroupNotification } from 'containers/App/selectors';
import { loadGroupNotification } from 'containers/App/actions';
import _ from 'lodash';
import styles from './styles.css';

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const targetData = this.props.type === 1 ? this.props.startGroupNotification : this.props.followGroupNotification;
    if (_.isEmpty(targetData)) {
      this.props.loadGroupNotification(this.props.type);
    }
  }


  render() {
    const targetData = this.props.type === 1 ? this.props.startGroupNotification : this.props.followGroupNotification;
    return (
      <div className={styles.main}>
        {
          _.isEmpty(targetData) ?
            <div className={styles.loadingWrap}>
              <Icon type={'loading'} size={'md'} />
            </div>
            :
            <div className={styles.notificationWrap}>
              <p className={styles.title}>{targetData.title}</p>
              {
                targetData.content.map((c, index) => (
                  <p className={styles.content} key={`${index}_`}>{c}</p>
                ))
                }
            </div>
        }
      </div>
    );
  }
}

Notification.propTypes = {
  loadGroupNotification: PropTypes.func,
  startGroupNotification: PropTypes.object,
  followGroupNotification: PropTypes.object,
  type: PropTypes.number,
};

const mapStateToProps = createSelector(
  selectStartGroupNotification(),
  selectFollowGroupNotification(),
  (startGroupNotification, followGroupNotification) => ({
    startGroupNotification: startGroupNotification.toJS(),
    followGroupNotification: followGroupNotification.toJS(),
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadGroupNotification: (nType) => dispatch(loadGroupNotification(nType)),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Notification));
