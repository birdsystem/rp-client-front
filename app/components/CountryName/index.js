import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { createSelector } from 'reselect';
import CountryIcon from 'components/CountryIcon';
// import { loadCountries } from 'containers/App/actions';
import { selectCountryIso } from 'containers/App/selectors';
import styles from './styles.css';

const countries = [{
  iso: 'GB',
  printable_name: '英国',
}, {
  iso: 'DE',
  printable_name: '德国',
}, {
  iso: 'FR',
  printable_name: '法国',
}, {
  iso: 'US',
  printable_name: '美国',
}, {
  iso: 'AU',
  printable_name: '澳大利亚',
}];

export class CountryName extends React.Component {
  componentDidMount() {
    // this.props.loadCountries();
  }

  render() {
    const { iso = '', flag = false, countryIso, className } = this.props;
    const formattedIso = iso.toUpperCase() || countryIso;
    const countryName = (_.find(countries, { iso: formattedIso }) || {}).printable_name || formattedIso;

    return (
      <div className={`${styles.wrapper} ${className}`}>
        <div className={styles.content}>
          {flag && (
            <div>
              <CountryIcon iso={formattedIso} />
            </div>
          )}
          <div>
            {countryName}
          </div>
        </div>
      </div>
    );
  }
}

CountryName.propTypes = {
  iso: PropTypes.string,
  flag: PropTypes.bool,
  countryIso: PropTypes.string,
  className: PropTypes.string,
};

const mapStateToProps = createSelector(
  // selectCountries(),
  // selectLoadingCountries(),
  selectCountryIso(),
  (
    // countries,
    // countryLoading,
    countryIso,
  ) => ({
    // countries: countries || [],
    // countryLoading,
    countryIso,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    // loadCountries: () => dispatch(loadCountries()),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CountryName);
