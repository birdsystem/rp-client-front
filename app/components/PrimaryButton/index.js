import React from 'react';
import { Button } from 'antd-mobile';
import PropTypes from 'prop-types';
import styles from './styles.css';

const PrimaryButton = ({ children, ...rest }) => (
  <div className={styles.buttonWrapper}><Button type="primary" className={styles.button} {...rest}>
    { children }
  </Button></div>
);

PrimaryButton.propTypes = {
  children: PropTypes.node,
};

PrimaryButton.defaultProps = {
  children: null,
};

export default PrimaryButton;
