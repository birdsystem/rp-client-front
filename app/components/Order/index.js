/* eslint-disable camelcase,no-shadow */
import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { browserHistory } from 'react-router';
import orderHelper from 'utils/orderHelper';
import { Modal } from 'antd-mobile';
import {
  getTimeLeftDisplay,
} from 'utils/commonHelper';
import Money from 'components/Money';
import { SINGLE_BUYING, ORDER_STATUS_PAYMENT_PENDING } from 'utils/constants';
import styles from './styles.css';
import orderIcon from './order.png';
const alert = Modal.alert;

const Order = ({ order, inx, isChild = false, currency, t, cancelOrder, confirmReceipt }) => {
  const {
          id, quantity, total, type, group_info, product_props, discount,
          product_price, cross_border_shipping_subtotal, product_info, service_charge,
        } = order;
  const waitPayment = orderHelper.canPay(order);
  const key = isChild ? `cOrder${inx}` : `order${inx}`;
  const isPendingParentOrder = isChild && order.status === ORDER_STATUS_PAYMENT_PENDING;
  return (
    <div
      onClick={(e) => {
        if (isChild && isPendingParentOrder) return;
        e.preventDefault();
        browserHistory.push(`/orderdetail/${order.id}`);
      }}
      className={styles.itemWrap}
      key={key}
    >
      <div className={styles.itemStatusWrap}>
        <p
          className={styles.itemTitle}
        >
          <img src={orderIcon} className={styles.orderIcon} alt={''} />
          { product_info.name && product_info.name.substr(0, 13) }</p>
        <span className={styles.itemStatus}>{ t(`order.status_${order.status}`) }</span>
      </div>
      <div className={styles.itemInnerWrap}>
        <div
          className={styles.itemImageDiv}
          style={{ backgroundImage: `url("${product_info.image_url}")` }}
        >
        </div>
        <div className={styles.itemInfo}>
          <div className={styles.props}>{ product_props.length > 0
            ? product_props.join(' ')
            : '' }</div>
          { type === SINGLE_BUYING ? (
            <div className={styles.singleBuy}>{ t(
              'common.buy_single') }</div>
          ) : (
            <div className={styles.peopleInfoWrap}>
              <div className={styles.itemCurrentPeople}>{ t(
                'common.current_join_count') }: { group_info.current_user_count }
              </div>
              <div className={styles.itemNeedPeople}>
                {
                  group_info.min_user_count -
                  group_info.current_user_count > 0 ? t(
                    'common.people_left', {
                      count: group_info.min_user_count -
                      group_info.current_user_count,
                    })
                    : t('common.finish_group')
                }
              </div>
            </div>
          ) }
          { order.remark &&
          <div className={styles.remarkWrap}>
            <span>{ t('common.remark') }: { order.remark }</span>
          </div>
          }

          <div className={styles.orderId}>{ t(
            'common.order_id') }: { order.id }</div>
          <div className={styles.orderId}>{ t(
            'common.delivery_service') }: { order.delivery_service ? order.delivery_service.name : ''}</div>
          { group_info.left_time > 0 &&
          group_info.min_user_count !== group_info.current_user_count && (
            <div className={styles.itemTime}>
              { type === SINGLE_BUYING ? t('common.left_pay_time') : t(
                'common.left_time') }：{ getTimeLeftDisplay(
              group_info.left_time) }
            </div>
          ) }
        </div>
        <div className={styles.itemPrice}>
          <Money
            amount={product_price}
            currencyCode={currency}
          />
          <span className={styles.quantity}>&times;{ quantity }</span>
        </div>
      </div>
      <div className={styles.totalPriceWrap}>
        <span className={styles.totalPriceText}>
          {/* { t('common.total_product_quantity', { quantity }) } */}
          { t('common.cross_border_shipping_subtotal') }
          <Money amount={cross_border_shipping_subtotal} />

          { type === SINGLE_BUYING && (
            <span>
              { t('common.order_service_charge') }
              <Money amount={service_charge} />
            </span>
          )}

          {parseFloat(discount) > 0 && (
            <span>, {t('common.discount')}<Money amount={discount} /></span>
          )}

          { t('common.total') }:&nbsp;
            </span>
        <Money
          className={styles.totalPrice}
          amount={total}
          currencyCode={currency}
        />
      </div>
      { !isPendingParentOrder && <div className={styles.itemAction}>
        { orderHelper.canCancel(order) && (
          <span
            className={styles.cancelButton}
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              alert('', t('common.confirm_cancel_order'), [
                { text: t('common.dont_cancel_order') },
                {
                  text: t('common.cancel_order'),
                  onPress: () => cancelOrder(id,
                    !(order.status === ORDER_STATUS_PAYMENT_PENDING)),
                },
              ]);
            }}
          >
            { t('order.action_ORDER_ACTION_CANCEL_ORDER') }
          </span>
        ) }
        {/* <span
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            window.location = 'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515';
          }}
          className={styles.contactClient}
        >
          { t('common.contact_client') }</span> */}
        <span
          className={styles.viewDetail}
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            browserHistory.push(`/productdetail/${product_info.id}`);
          }}
        >{ t(
          'common.view_product_detail') }</span>
        { orderHelper.canTrack(order) && (
          <span
            className={styles.viewDetail}
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              browserHistory.push(`/tracking/${order.id}`);
            }}
          >
            { t('common.trace_order') }
          </span>
        )}
        { waitPayment && <span
          className={styles.makePayment}
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            browserHistory.push(
              `/payment/${id}`);
          }}
        >{ t(
          'common.pay_at_once') }</span>
        }
        { orderHelper.canConfirmReceipt(order) && (
          <span
            className={styles.makePayment}
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              confirmReceipt(id);
            }}
          >
            { t('order.action_CONFIRM_RECEIPT') }
          </span>
        ) }
      </div>
      }
    </div>
  );
};

Order.propTypes = {
  t: PropTypes.func,
  order: PropTypes.object,
  currency: PropTypes.string,
  inx: PropTypes.string,
  cancelOrder: PropTypes.func,
  confirmReceipt: PropTypes.func,
  isChild: PropTypes.bool,
};

export default translate()(Order);
