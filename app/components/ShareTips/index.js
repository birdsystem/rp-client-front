import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import message from 'components/message';

class ShareTips extends React.Component {

  state = {
    visibleStatus: 'visible',
    hasShow: false,
  };

  componentDidMount() {
    document.addEventListener('visibilitychange',
      this.visibleStatusChange.bind(this));
  }

  componentWillUnmount() {
    document.removeEventListener('visibilitychange', this.visibleStatusChange);
  }

  visibleStatusChange() {
    return;
    const current = this.state.visibleStatus;
    if (current === 'hidden' && document.visibilityState === 'visible' &&
      !this.state.hasShow) {
      message.success(this.props.t('common.share_success'));
      this.setState({
        hasShow: true,
      });
    }
    this.setState({
      visibleStatus: document.visibilityState,
    });
  }

  render() {
    return null;
  }
}

ShareTips.propTypes = {
  t: PropTypes.func,
};

export default translate()(ShareTips);
