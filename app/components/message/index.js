import { Toast } from 'antd-mobile';
import MessageQueue from './MessageQueue';

const errorMessageQueue = new MessageQueue();

const message = {
  success: (content, duration, onClose, mask) => Toast.success(content, duration || 2, onClose, mask),
  error: (content, duration, onClose = () => {}, mask = false) => {
    // new errors would be ignored until the first and only error is considered
    // outdated and cleared by queue
    if (errorMessageQueue.list().length) return false;
    const alertId = errorMessageQueue.add({ type: 'error', content }, duration);
    Toast.fail(
      content,
      duration || 3,
      () => { errorMessageQueue.remove(alertId); onClose(); },
      mask,
    );
    return alertId;
  },
  info: (content, duration, onClose, mask) => Toast.info(content, duration, onClose, mask),
  warning: (content, duration, onClose, mask) => Toast.info(content, duration, onClose, mask),
  offline: (content, duration, onClose, mask) => Toast.offline(content, duration, onClose, mask),
};

export default message;
