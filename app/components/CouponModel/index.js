import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { Modal, Icon } from 'antd-mobile';
import { selectCouponList, selectCouponListLoading, selectRegisterFrom } from 'containers/App/selectors';
import Coupon from 'components/Coupon';
import CommonButton from 'components/CommonButton';
import wechatHelper from 'utils/wechatHelper';
import styles from './styles.css';

const CouponModelComponent = ({ show, onTapStart, t, loadingCouponList, couponList, registerFrom }) => (
  <Modal
    transparent
    maskClosable
    visible={show}
    className={styles.wrap}
    onClose={() => {
      if (onTapStart) {
        onTapStart();
      }
    }}
  >
    <div className={styles.inner}>
      <div className={styles.title}>
        <div>{t('common.register_success_and_get_coupon_package')}</div>
        <div className={styles.packageName}>
          {t(`coupon_package_type.${registerFrom}`)}
        </div>
      </div>
      <div className={styles.list}>
        { (couponList && couponList.length === 0) ? <div className={styles.loadingWrap}>
          { !loadingCouponList && t('coupon.empty_coupon') }
          { loadingCouponList && <Icon type="loading" size={'medium'} /> }
        </div>
        : <ul className={styles.couponListWrap}>
          { couponList.map((coupon) => (
            <li className={styles.couponWrap} key={coupon.id}>
              <Coupon
                showToUser={false}
                coupon={coupon}
              />
            </li>
          )) }
        </ul>
      }
      </div>
      <div className={styles.footer}>
        <div className={styles.tips}>{t('common.follow_official_account_text')}</div>
        <div className={styles.footerAction}>
          <div className={styles.footerItem}>
            <CommonButton
              onClick={() => {
                if (onTapStart) {
                  onTapStart();
                }
              }}
              className={styles.followButton}
            >{ t('common.shop_at_once') }</CommonButton>
          </div>
          <div className={styles.footerItem}>
            <CommonButton
              onClick={() => {
                window.location = wechatHelper.genWechatServiceUrl();
              }}
              type={'dark'}
              className={styles.followButton}
            >{ t('pageTitles.follow_official_account') }</CommonButton>
          </div>
        </div>
      </div>
    </div>
  </Modal>
);

CouponModelComponent.propTypes = {
  t: PropTypes.func,
  show: PropTypes.bool,
  onTapStart: PropTypes.func,
  loadingCouponList: PropTypes.bool,
  couponList: PropTypes.array,
  registerFrom: PropTypes.string,
};

const mapStateToProps = createSelector(
  selectCouponList(),
  selectCouponListLoading(),
  selectRegisterFrom(),
  (couponList, loadingCouponList, registerFrom) => ({
    couponList: couponList.toJS(),
    loadingCouponList,
    registerFrom,
  }),
);

export default translate()(
  connect(mapStateToProps, null)(CouponModelComponent));
