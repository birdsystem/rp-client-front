/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import styles from './styles.css';

const Timeline = ({ data }) => (
  <div className={styles.wrapper}>
    {data.map((item, index) => (
      <div key={index} className={styles.item}>
        <div className={styles.time}>
          <p>{item.time.split(' ')[0]}</p>
          <p>{item.time.split(' ')[1]}</p>
        </div>
        {index !== data.length - 1 && (
          <div className={styles.tail}></div>
        )}
        <div className={styles.icon}></div>
        <div className={styles.content}>
          <div className={`${styles.description} ${index === 0 && styles.recent}`}>{item.message}</div>
        </div>
      </div>
    ))}
  </div>
);

Timeline.propTypes = {
  data: PropTypes.array,
};

export default translate()(Timeline);
