/* eslint-disable import/no-duplicates */
import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { browserHistory } from 'react-router';
import {
  eventCategory,
  record,
  actions,
} from 'utils/gaDIRecordHelper';
import styles from './styles.css';
import homeIcon from './home.png';
import homeFocusIcon from './home-focus.png';
import recommendIcon from './recommend.png';
import recommendFocusIcon from './recommend-focus.png';
import orderIcon from './order.png';
import orderFocusIcon from './order-focus.png';
import meIcon from './account.png';
import meFocusIcon from './account-focus.png';

const HomeBottomAction = ({ index = 0, t }) => (
  <div className={styles.homeIconWrap}>
    {
      index === 0 ? <div className={styles.tabFocus}>
        <img
          className={styles.homeIcon}
          alt={'home'}
          src={homeFocusIcon}
        ></img>
        <p>{ t('nav.home') }</p>
      </div>
        : <div
          onClick={() => {
            record(actions.TAB_HOME, {}, eventCategory.USER_ACTION);
            browserHistory.push('/');
          }}
          className={styles.tab}
        >
          <img className={styles.homeIcon} alt={'home'} src={homeIcon}></img>
          <p>{ t('nav.home') }</p>
        </div>
    }

    {
      index === 1 ? <div className={styles.tabFocus}>
        <img
          className={styles.icon}
          alt={'recommend'}
          src={recommendFocusIcon}
        ></img>
        {/* <p>{ t('nav.recommend') }</p> */}
        <p>{ t('nav.submit_product') }</p>
      </div>
        : <div
          onClick={() => {
            // browserHistory.push('/productlist');
            record(actions.TAB_SUBMIT_PRODUCT, {}, eventCategory.USER_ACTION);
            browserHistory.push('/submit-product');
          }}
          className={styles.tab}
        >
          <img className={styles.icon} alt={'recommend'} src={recommendIcon}></img>
          {/* <p>{ t('nav.recommend') }</p> */}
          <p>{ t('nav.submit_product') }</p>
        </div>
    }

    {
      index === 2 ? <div className={styles.tabFocus}>
        <img
          className={styles.icon}
          alt={'order'}
          src={orderFocusIcon}
        ></img>
        <p>{ t('nav.order') }</p>
      </div>
        : <div
          onClick={() => {
            record(actions.TAB_ORDER, {}, eventCategory.USER_ACTION);
            browserHistory.push('/myorderlist');
          }}
          className={styles.tab}
        >
          <img className={styles.icon} alt={'order'} src={orderIcon}></img>
          <p>{ t('nav.order') }</p>
        </div>
    }

    {
      index === 3 ?
        <div className={styles.tabFocus}>
          <img
            className={styles.icon}
            alt={'me'}
            src={meFocusIcon}
          ></img>
          <p>{ t('nav.me') }</p>
        </div>
        : <div
          onClick={() => {
            record(actions.TAB_ME, {}, eventCategory.USER_ACTION);
            browserHistory.push('/myinfo');
          }}
          className={styles.tab}
        >
          <img className={styles.icon} alt={'me'} src={meIcon}></img>
          <p>{ t('nav.me') }</p>
        </div>
    }
  </div>
);

HomeBottomAction.propTypes = {
  index: PropTypes.number,
  t: PropTypes.func,
};

export default translate()(HomeBottomAction);
