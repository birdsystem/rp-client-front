/*
 *
 * Money
 *
 */

import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import styles from './styles.css';

const currencyList = [{
  code: 'CNY',
  symbol: '¥',
  zhName: '元',
}, {
  code: 'GBP',
  symbol: '£',
  zhName: '镑',
}, {
  code: 'EUR',
  symbol: '€',
  zhName: '欧',
}, {
  code: 'USD',
  symbol: '$',
  zhName: '美元',
}, {
  code: 'AUD',
  symbol: 'A$',
  zhName: '澳元',
}];

const defaultCurrency = currencyList[0];

const Money = ({ currencySymbol, currencyCode, amount, withSign = false, originalAmount, withoutSign, currencySuffix = false, className, symbolClassName, displaySymbol = true }) => {
  let symbol = null;
  if (currencyCode && !currencySuffix) {
    const currencyItem = _.find(currencyList, { code: currencyCode });
    symbol = currencyItem ? currencyItem.symbol : '';
  } else {
    symbol = currencySymbol || defaultCurrency.symbol;
  }

  let amountWithSymbol = symbol;
  let amountWithSuffix = '';
  let sign = '';
  let originalPrice = null;
  let originalPriceWithSuffx = null;

  const formattedAmount = amount && amount.length > 0 ? amount.replace(/,/g, '') : amount; // replace , thousands separator

  const formattedOriginalAmount = originalAmount && originalAmount.length > 0 ? originalAmount.replace(/,/g, '') : originalAmount; // replace , thousands separator

  if (!Number.isNaN(parseFloat(formattedAmount))) {
    if (withSign) {
      sign = Math.sign(formattedAmount) < 0 ? '-  ' : (Math.sign(formattedAmount) === 0 ? '   ' : '+ '); // eslint-disable-line
    } else if (withoutSign) {
      sign = '';
    } else {
      sign = Math.sign(formattedAmount) < 0 ? '- ' : ''; // eslint-disable-line
    }

    const suffix = currencyList[0].zhName;

    amountWithSymbol = (
      <span>{sign}{displaySymbol && <span className={symbolClassName}>{symbol}</span>}<span className={styles.smallSpace}></span>{Math.abs(parseFloat(formattedAmount)).toFixed(2)}</span>
    );
    amountWithSuffix = sign + Math.abs(parseFloat(formattedAmount)).toFixed(2) + suffix;  // eslint-disable-line


    if (!Number.isNaN(parseFloat(formattedOriginalAmount))
      && (parseFloat(formattedOriginalAmount) > parseFloat(formattedAmount))) {
      originalPrice = (
        <span><span className={symbolClassName}>{symbol}</span><span className={styles.smallSpace}></span>{parseFloat(formattedOriginalAmount).toFixed(2)}</span>
      );
      originalPriceWithSuffx = parseFloat(formattedOriginalAmount).toFixed(2) + suffix;  // eslint-disable-line
    }
  }

  return (
    <span className={`${styles.money} ${className}`}>
      {originalPrice && (
        <span className={styles.originalPrice}>{currencySuffix ? originalPriceWithSuffx : originalPrice}</span>
      )}
      <span className={originalPrice ? styles.discountPrice : ''}>{currencySuffix ? amountWithSuffix : amountWithSymbol}</span>
    </span>
  );
};

Money.propTypes = {
  currencySymbol: PropTypes.string,
  currencyCode: PropTypes.string,
  amount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  originalAmount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  withSign: PropTypes.bool,
  withoutSign: PropTypes.bool,
  currencySuffix: PropTypes.bool,
  className: PropTypes.string,
  symbolClassName: PropTypes.string,
  displaySymbol: PropTypes.bool,
};

export default translate()(Money);
