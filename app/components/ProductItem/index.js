import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { selectListLayoutType } from 'containers/App/selectors';
import { getDisplayPrice } from 'utils/productHelper';
import { isBackendYes } from 'utils/backendHelper';
import { Link } from 'react-router';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import { LIST_LAYOUT_TYPE } from 'containers/App/constants';
import styles from './styles.css';

const ProductItemComponent = ({ product, listLayoutType, t, withoutOutMargin = false, onClickItem }) => {
  let outWidth = document.body.clientWidth;
  if (outWidth > 600) {
    outWidth = 600;
  }
  const twoColumnImageHeight = (outWidth * 0.46);
  let itemClassName = styles.item;
  if (listLayoutType === LIST_LAYOUT_TYPE.TWO_COLUMN) {
    itemClassName = `${styles.item} ${styles.itemTwoColumn}`;
    if (withoutOutMargin) {
      itemClassName = `${styles.item} ${styles.itemTwoColumn} ${styles.itemTwoColumnWithoutOutMargin}`;
    }
  }
  return (
    <Link
      className={itemClassName}
      to={`/productdetail/${product.id}`}
      onClick={() => {
        if (onClickItem) {
          onClickItem();
        }
      }}
    >
      <div
        className={styles.itemImageDiv}
        style={{ height: listLayoutType === LIST_LAYOUT_TYPE.TWO_COLUMN ? `${twoColumnImageHeight}px` : '120px' }}
      >
        <LazyLoad height={listLayoutType === LIST_LAYOUT_TYPE.TWO_COLUMN ? twoColumnImageHeight : '120'}>
          <img alt={'pImg'} src={product.thumbnail_url || product.image_url} className={styles.productImage} />
        </LazyLoad>
        {/* {isBackendYes(product.is_recommended) && (
          <span className={styles.recommendedTag}>
            <span className={`${styles.recommendedText} ${listLayoutType === LIST_LAYOUT_TYPE.TWO_COLUMN ? styles.bigRecommendedText : ''}`}>
              {t('common.weekly_recommended')}
            </span>
          </span>
        )} */}
      </div>
      <div className={styles.itemInfo}>
        <p className={styles.itemTitle}>{ product.name }</p>
        <div className={styles.itemPriceWrap}>
          <div className={styles.itemPrice}>
            <MoneyInLocalCurrency symbolClassName={styles.symbol} cnyAmount={getDisplayPrice(product)} />
            {/* <span className={styles.cnyPrice}><Money amount={getDisplayPrice(product)} /></span> */}
          </div>
          {
            (product.deal_count && (parseInt(product.deal_count, 10) > 0)) ? (
              <div className={styles.successDealWrap}>
                {t('common.deal_count', { count: product.deal_count })}
              </div>
            ) : null
          }
          <div className={styles.buyButton}>
            {t('common.buy')}
          </div>
        </div>
      </div>
    </Link>
  );
};

ProductItemComponent.propTypes = {
  t: PropTypes.func,
  product: PropTypes.object,
  listLayoutType: PropTypes.string,
  withoutOutMargin: PropTypes.bool,
  onClickItem: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectListLayoutType(),
  (listLayoutType) => ({
    listLayoutType,
  }),
);

export default translate()(
  connect(mapStateToProps, null)(ProductItemComponent));
