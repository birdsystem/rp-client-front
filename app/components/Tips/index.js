/**
*
* Tips
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import styles from './styles.css';

function Tips(props) {
  const { children, fontSize = '1rem' } = props;
  return (
    <p className={styles.tips} style={{ fontSize }}>
      {children}
    </p>
  );
}

Tips.propTypes = {
  children: PropTypes.node.isRequired,
  fontSize: PropTypes.string,
};
export default translate()(Tips);
