import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { Helmet } from 'react-helmet';

function WindowTitle(props) {
  const { t, title, defaultTitle = false } = props;
  return (
    <Helmet>
      {defaultTitle ? (
        <title>{t('pageTitles.default_page_title')}</title>
      ) : (
        <title>{title}</title>
      )}
    </Helmet>
  );
}

WindowTitle.propTypes = {
  title: PropTypes.string,
  t: PropTypes.func,
  defaultTitle: PropTypes.bool,
};

export default translate()(WindowTitle);
