import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import styles from './styles.css';
import loading from './loading.gif';

const Empty = ({ title = '', className = '', t }) => {
  const url = loading;
  return (
    <div className={`${styles.emptyWrap} ${className}`}>
      <img src={url} alt={'icon'} className={styles.emptyIcon} />
      <p className={styles.emptyText}>{title || t('empty.common')}</p>
    </div>
  );
};

Empty.propTypes = {
  title: PropTypes.string,
  t: PropTypes.func,
  className: PropTypes.string,
};

export default translate()(Empty);
