/*
 *
 * MoneyInLocalCurrency
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { createSelector } from 'reselect';
import { selectExchangeRateTable, selectLocalCurrencyCode } from 'containers/App/selectors';
import Money from 'components/Money';
import { DEFAULT_LOCAL_CURRENCY_CODE } from 'utils/constants';

const MoneyInLocalCurrency = ({ cnyAmount, exchangeRateTable, localCurrencyCode, symbolClassName, ...rest }) => {
  let amountInLocalCurrency = null;
  const exchangeRateForLocalCurrency = exchangeRateTable[`${(localCurrencyCode || DEFAULT_LOCAL_CURRENCY_CODE).toLowerCase()}_to_cny`];
  // console.log('local currency code', exchangeRateForLocalCurrency);
  if (!Number.isNaN(parseFloat(cnyAmount)) && exchangeRateForLocalCurrency > 0) {
    amountInLocalCurrency = parseFloat(cnyAmount) / exchangeRateForLocalCurrency;
  }
  return (
    <Money currencyCode={localCurrencyCode || DEFAULT_LOCAL_CURRENCY_CODE} symbolClassName={symbolClassName} amount={amountInLocalCurrency} {...rest} />
  );
};

MoneyInLocalCurrency.propTypes = {
  cnyAmount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  exchangeRateTable: PropTypes.object,
  localCurrencyCode: PropTypes.string,
  symbolClassName: PropTypes.string,
};

const mapStateToProps = createSelector(
  selectExchangeRateTable(),
  selectLocalCurrencyCode(),
  (exchangeRateTable, localCurrencyCode) => ({
    exchangeRateTable: exchangeRateTable ? exchangeRateTable.toJS() : {},
    localCurrencyCode,
  })
);

export default translate()(connect(mapStateToProps)(MoneyInLocalCurrency));
