/* eslint-disable no-mixed-operators,radix */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Slider } from 'antd-mobile';
import { translate } from 'react-i18next';
import Tips from 'components/Tips';
// import Money from 'components/Money';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import styles from './styles.css';

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement; // eslint-disable-line
  }
  return null;
}

class DeliveryFeeCalculator extends Component {
  constructor(props) {
    super(props);

    const { feeTable = [] } = props;

    const minWeight = feeTable.length > 0 ? feeTable.reduce((min, item) => Math.min(min, item.weight), feeTable[0].weight) : 300;

    this.state = {
      estimatedWeight: minWeight,
    };
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  }

  render() {
    const { t, visible, hide, feeTable = [] } = this.props;

    const { estimatedWeight } = this.state;

    const minWeight = feeTable.length > 0 ? feeTable.reduce((min, item) => Math.min(min, item.weight), feeTable[0].weight) : 300;
    const maxWeight = feeTable.length > 0 ? feeTable.reduce((max, item) => Math.max(max, item.weight), feeTable[0].weight) : 1000;

    const fee = (feeTable.find((i) => parseFloat(i.weight) === estimatedWeight) || {}).fee || 0;
    const step = feeTable.length > 1 ? Math.abs(feeTable[0].weight - feeTable[1].weight) : 25;

    return (
      <Modal
        visible={visible}
        transparent
        maskClosable
        onClose={hide}
        wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        className={styles.modalContainer}
      >
        <Tips>{t('common.tips_calculator')}</Tips>
        <div className={styles.rowWrapper}>
          <div className={styles.label}>{ t('common.weight') }</div>
          <Slider
            style={{ marginRight: 20, width: 200 }}
            value={estimatedWeight}
            step={step}
            min={minWeight}
            max={maxWeight}
            onChange={(value) => this.setState({ estimatedWeight: value })}
          />
          <div className={styles.weight}>{estimatedWeight}g</div>
        </div>
        <div className={styles.rowWrapper}>
          <div className={styles.label}>{ t('common.estimated_delivery_fee') }</div>
          <div className={styles.fee}><MoneyInLocalCurrency cnyAmount={fee} /></div>
        </div>
      </Modal>
    );
  }
}

DeliveryFeeCalculator.propTypes = {
  t: PropTypes.func,
  visible: PropTypes.bool,
  hide: PropTypes.func,
  feeTable: PropTypes.array,
};

export default translate()(DeliveryFeeCalculator);
