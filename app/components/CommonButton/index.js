import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd-mobile';
import styles from './styles.css';

const CommonButton = ({ children, className, disabled, type, loading, ...rest }) => (
  <button className={`${styles.commonButton} ${styles[type]} ${className}`} disabled={disabled} {...rest}>
    <span className={styles.contentWrap}>
      {
        loading &&
        <span className={styles.loadingWrap}>
          <Icon type={'loading'} size={'sm'} />
        </span>
      }
      { children }
    </span>
  </button>
);

CommonButton.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  type: PropTypes.string,
  loading: PropTypes.bool,
};

CommonButton.defaultProps = {
  children: null,
  className: '',
  disabled: false,
  type: 'light',
  loading: false,
};

export default CommonButton;
