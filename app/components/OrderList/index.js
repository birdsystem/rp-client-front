/* eslint-disable camelcase,no-shadow,prefer-template,react/no-unused-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory, Link } from 'react-router';
import { Modal } from 'antd-mobile';
import orderHelper from 'utils/orderHelper';
import {
  selectCurrentUser,
  selectCurrency,
  selectUserInfoLoading,
} from 'containers/App/selectors';
import Order from 'components/Order';
import Empty from 'components/Empty';
import { cancelOrder, confirmReceipt } from 'containers/App/actions';
import { ORDER_STATUS_PAYMENT_PENDING } from 'utils/constants';
import styles from './styles.css';
import questionIcon from './question.png';

const alert = Modal.alert;

const OrderList = (props) => {
  const { orderList, t, currency, confirmReceipt, cancelOrder, userInfoLoading, currentUser, isLoading, onClickShowParentOrderTips } = props;
  let orderListElement = null;
  if (orderList) {
    orderListElement = orderList.map((order, inx) => {
      if (order.isParentOrder) {
        let isPending = false;
        const orders = order.orders;
        let childOrder = {};
        let id = '';
        if (orders && orders.length > 0) {
          childOrder = orders[0];
          if (orders[0].status === ORDER_STATUS_PAYMENT_PENDING) {
            isPending = true;
          }
        }
        for (let i = orders.length - 1; i >= 0; i -= 1) {
          id += `${orders[i].id}_`;
        }
        if (id.lastIndexOf('_') === id.length - 1) {
          id = id.substr(0, id.length - 1);
        }
        return (
          <Link
            to={`/orderdetail/${id}`}
            className={styles.globalOrderWrap}
            key={`order${inx}`}
          >
            <div
              className={styles.globalOrderNumber}
              onClick={(e) => {
                e.preventDefault();
                onClickShowParentOrderTips(isPending);
              }}
            >
              { t('common.global_order_number') }: { order.order_number }
              <img src={questionIcon} className={styles.questionIcon} alt={'?'} />
            </div>
            <div className={styles.childOrderWrap}>
              {
                order.orders.map((mOrder, mInx) => (
                  <Order key={`order${mInx}`} order={mOrder} inx={`o${mInx}`} confirmReceipt={confirmReceipt} currency={currency} cancelOrder={cancelOrder} isChild />
                ))
              }
            </div>
            { isPending &&
            <div className={styles.parentActionWrap}>
              <div className={styles.itemAction}>
                { orderHelper.canCancel(childOrder) && (
                  <span
                    className={styles.cancelButton}
                    onClick={(e) => {
                      e.stopPropagation();
                      e.preventDefault();
                      alert('', t('common.confirm_cancel_order'), [
                        { text: t('common.dont_cancel_order') },
                        {
                          text: t('common.cancel_order'),
                          onPress: () => cancelOrder(id,
                            !(childOrder.status === ORDER_STATUS_PAYMENT_PENDING)),
                        },
                      ]);
                    }}
                  >
                      { t('order.action_ORDER_ACTION_CANCEL_ORDER') }
                    </span>
                ) }
                {/* <span
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    window.location = 'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515';
                  }}
                  className={styles.contactClient}
                >
                  { t('common.contact_client') }</span> */}
                <span
                  className={styles.viewDetail}
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    browserHistory.push(`/productdetail/${childOrder.product_info.id}`);
                  }}
                >{ t(
                  'common.view_product_detail') }</span>
                { orderHelper.canPay(childOrder) && <span
                  className={styles.makePayment}
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    browserHistory.push(
                      `/payment/${id}`);
                  }}
                >{ t(
                  'common.pay_at_once') }</span>
                }
                { orderHelper.canConfirmReceipt(childOrder) && (
                  <span
                    className={styles.makePayment}
                    onClick={(e) => {
                      e.stopPropagation();
                      e.preventDefault();
                      confirmReceipt(id);
                    }}
                  >
                      { t('order.action_CONFIRM_RECEIPT') }
                    </span>
                ) }
              </div>
            </div>
            }
          </Link>
        );
      }
      return (<Order key={`o${inx}`} order={order} inx={`o${inx}`} isChild={false} currency={currency} confirmReceipt={confirmReceipt} cancelOrder={cancelOrder} onClickShowParentOrderTips={onClickShowParentOrderTips} />);
    });
  }
  return (
    <div className={styles.main}>
      <div className={styles.orderOutWrap}>
        { orderListElement }
      </div>
      {
        !userInfoLoading &&
        currentUser &&
        !isLoading &&
        orderList.length === 0
        &&
        <Empty
          className={styles.orderEmpty}
          title={t('empty.empty_order')}
        />
      }
    </div>
  );
};

OrderList.propTypes = {
  t: PropTypes.func,
  orderList: PropTypes.array,
  isLoading: PropTypes.bool,
  onReachEnd: PropTypes.func,
  onRefresh: PropTypes.func,
  currentUser: PropTypes.object,
  currency: PropTypes.string,
  userInfoLoading: PropTypes.bool,
  cancelOrder: PropTypes.func,
  confirmReceipt: PropTypes.func,
  onClickShowParentOrderTips: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectCurrency(),
  selectUserInfoLoading(),
  (currentUser, currency, userInfoLoading) => ({
    currentUser: currentUser && currentUser.toJS(),
    currency,
    userInfoLoading,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  cancelOrder: (id, hasPay) => dispatch(cancelOrder(id, hasPay)),
  confirmReceipt: (id) => dispatch(confirmReceipt(id)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(OrderList));
