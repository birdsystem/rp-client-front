import React from 'react';
import PropTypes from 'prop-types';
import { NoticeBar } from 'antd-mobile';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { selectShowNewYearNotice } from 'containers/App/selectors';
import { browserHistory } from 'react-router';

const NewYearNotice = ({ t, showNewYearNotice }) => (
  <div>
    { showNewYearNotice &&
      <NoticeBar
        mode="link"
        onClick={() => {
          browserHistory.push('/notice');
        }}
      >
        {t('notice.new_year_notice')}
      </NoticeBar>
      }
  </div>
  );

NewYearNotice.propTypes = {
  t: PropTypes.func,
  showNewYearNotice: PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectShowNewYearNotice(),
  (showNewYearNotice) => ({
    showNewYearNotice,
  }),
);

export default translate()(
  connect(mapStateToProps, null)(NewYearNotice));
