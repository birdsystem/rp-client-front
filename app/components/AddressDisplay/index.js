import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { WingBlank } from 'antd-mobile';
import CountryIcon from 'components/CountryIcon';
import styles from './styles.css';

const AddressDisplay = ({ address, t }) => (
  <WingBlank className={styles.addressWrapper}>
    <div className={styles.countryIcon}>
      <CountryIcon iso={address.country_iso} />
    </div>
    <div className={styles.addressInfo}>
      <div className={styles.consigneeInfo}>
        <div>{t('common.receiver')}: {address.contact}</div>
        <div>{address.telephone}</div>
      </div>
      <div>{t('common.receiver_address')}: {address.address_line_1}, {address.city}, {address.post_code}</div>
    </div>
  </WingBlank>
);

AddressDisplay.propTypes = {
  t: PropTypes.func,
  address: PropTypes.object,
};

export default translate()(AddressDisplay);
