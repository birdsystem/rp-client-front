import React from 'react';
import { Button } from 'antd-mobile';
import PropTypes from 'prop-types';
import styles from './styles.css';

const SecondaryButton = ({ children, ...rest }) => (
  <div className={styles.buttonWrapper}><Button type="primary" className={styles.button} {...rest}>
    { children }
  </Button></div>
);

SecondaryButton.propTypes = {
  children: PropTypes.node,
};

SecondaryButton.defaultProps = {
  children: null,
};

export default SecondaryButton;
