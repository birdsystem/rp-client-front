/* eslint-disable import/no-duplicates */
import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import styles from './styles.css';
import shareIcon from './share2.png';

const WeChatShare = ({ t }) => (
  <div className={styles.shareWrap}>
    <img alt={'share'} src={shareIcon} className={styles.shareIcon} />
    <div className={styles.shareText}>
      <p>{t('share.wechat_share_tips1')}</p>
      <p>{t('share.wechat_share_tips2')}</p>
    </div>
  </div>
);

WeChatShare.propTypes = {
  t: PropTypes.func,
};

export default translate()(WeChatShare);
