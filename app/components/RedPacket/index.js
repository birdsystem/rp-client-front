/* eslint-disable no-unused-vars,no-param-reassign */
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { translate } from 'react-i18next';
import { RED_PACKET_MODAL_TYPE } from 'containers/App/constants';
import icon from './icon.png';
import styles from './styles.css';

const RedPacket = ({ t, coupon = {}, onClickOpenCouponPage, type = RED_PACKET_MODAL_TYPE.DRAW, showUse = true }) => (
  <div className={styles.wrap}>
    <div className={styles.collectContainer}>
      <div
        className={
          type === RED_PACKET_MODAL_TYPE.PRESENT
          ? (showUse ? `${styles.collectWrap} ${styles.collectWrapPresent}` : `${styles.collectWrap} ${styles.collectWrapPresentWithoutUse}`)
          : styles.collectWrap}
      >
        <div className={styles.moneyWrap}>
          <img
            src={icon}
            className={styles.moneyIcon}
            alt={'¥'}
          />
          <div className={styles.moneyText}>{ coupon && coupon.discount
            ? parseFloat(coupon.discount).toFixed(0)
            : '' }</div>
        </div>
        { showUse && <div
          onClick={() => {
            if (onClickOpenCouponPage) onClickOpenCouponPage();
            browserHistory.push('/coupons');
          }}
          className={styles.couponLink}
        />
        }
      </div>
    </div>
  </div>
);

RedPacket.propTypes = {
  t: PropTypes.func,
  coupon: PropTypes.object,
  onClickOpenCouponPage: PropTypes.func,
  type: PropTypes.string,
  showUse: PropTypes.bool,
};

export default translate()(RedPacket);
