/* eslint-disable no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

const Comment = ({ t, comment, index, onImagesClick }) => {
  const pics = [];
  if (comment && comment.pics) {
    const length = comment.pics.length > 3 ? 3 : comment.pics.length;
    for (let i = 0; i < length; i += 1) {
      pics.push(comment.pics[i]);
    }
  }
  return (
    <div className={styles.commentWrap} key={`comment_${index}`}>
      <div className={styles.nameWrap}>
        { comment.user_nick }
      </div>
      <div className={styles.contentWrap}>
        { comment.content }
      </div>
      {
        pics.length > 0 &&
        <div className={styles.imageWrap}>
          {
            pics.map((pic, imgInx) => (
              <div
                className={styles.imageItem}
                key={`image_${index}_${imgInx}`}
              >
                <div
                  style={{
                    backgroundImage: `url(${pic})`,
                  }}
                  onClick={() => {
                    if (onImagesClick) {
                      onImagesClick(pics, imgInx);
                    }
                  }}
                  className={styles.image}
                  alt={''}
                />
              </div>
            ))
          }
        </div>
      }
    </div>
  );
};

Comment.propTypes = {
  t: PropTypes.func,
  comment: PropTypes.object,
  onImagesClick: PropTypes.func,
  index: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default Comment;
