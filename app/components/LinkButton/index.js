import React from 'react';
import { Link } from 'react-router';
import { Button } from 'antd-mobile';
import PropTypes from 'prop-types';

const LinkButton = ({ to, text, blank, children, ...rest }) => (
  <Link to={to} target={blank ? '_blank' : null}>
    <Button {...rest}>
      { children || text }
    </Button>
  </Link>
);

LinkButton.propTypes = {
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.func]),
  blank: PropTypes.bool,
  text: PropTypes.string,
  children: PropTypes.node,
};

LinkButton.defaultProps = {
  blank: false,
  text: null,
  children: null,
};

export default LinkButton;
