import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';
import countryIcons from './countryIcons';

const CountryIcon = ({ iso, size }) => {
  let sizeClass;
  switch (size) {
    case 'small':
      sizeClass = 'small';
      break;
    case 'medium':
      sizeClass = 'medium';
      break;
    default:
      sizeClass = 'medium';
  }
  return (
    <img src={countryIcons[iso]} alt={iso} className={`${styles.icon} ${styles[sizeClass]}`} />
  );
};

CountryIcon.propTypes = {
  iso: PropTypes.string,
  size: PropTypes.string,
};

export default CountryIcon;
