function importAll(r) {
  const images = {};
  r.keys().map((item) => images[item.replace('./', '').replace('.png', '')] = r(item)); // eslint-disable-line
  return images;
}

// https://www.gosquared.com/resources/flag-icons/

const flags = importAll(require.context('./flags', false, /\.png$/));

export default flags;
