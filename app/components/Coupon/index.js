/* eslint-disable spaced-comment */
import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { isBackendYes, isBackendNo } from 'utils/backendHelper';
import { browserHistory } from 'react-router';
import {
  getCouponApplyScopeDisplay,
  isUpcoming,
} from 'utils/couponHelper';
import PrimaryButton from 'components/PrimaryButton';
import styles from './styles.css';
import expiredIcon from './expired.png';
import usedIcon from './used.png';

const Coupon = ({ t, coupon = {}, showToUser = true }) => {
  let discountText = '';

  if (coupon.discount_type === 'AMOUNT') {
    discountText = (
      <div className={styles.discountWrapper}><span className={styles.currencySymbol}>￥</span>{parseFloat(coupon.discount).toFixed(0)}</div>
    );
  } else if (coupon.discount_type === 'RATE') {
    discountText = (
      <div className={styles.discountWrapper}>{coupon.discount * 10}<span className={styles.rate}>{t('coupon.rate_discount')}</span></div>
    );
  }

  const applyScope = getCouponApplyScopeDisplay(t, coupon);
  const isUpcomingCoupon = isUpcoming(coupon);

  let showUsed = false;
  if (isBackendYes(coupon.is_used)) {
    showUsed = true;
  }

  let showExpired = false;
  if (isBackendNo(coupon.is_valid) && !showUsed && !isUpcomingCoupon) {
    showExpired = true;
  }
  return (
    <div className={styles.couponItem}>
      <div className={styles.header}>
        <div className={showToUser ? styles.leftHeader : styles.leftHeaderWide}>
          <div className={styles.title}>
            <div className={(showUsed || showExpired) ? styles.grayDiscountText : ''}>{discountText}</div>
            <div className={styles.name}>{coupon.name}</div>
          </div>
        </div>

        { showToUser && <div className={styles.rightHeader}>
          { isBackendYes(coupon.is_valid) && (
            <PrimaryButton
              size="small"
              onClick={() => browserHistory.push('/')}
            >{ t('coupon.use_it_now') }</PrimaryButton>
          ) }
        </div>
        }
        {isUpcomingCoupon && (
          <span className={styles.deadline}><span className={styles.deadlineText}>{t('coupon.upcoming')}</span></span>
        )}
      </div>
      <div className={styles.infoWrap}>
        <div className={styles.infoLabel}>
          {t('coupon.coupon_number')}:
        </div>
        <div className={styles.infoDetail}>
          {coupon.id}
        </div>
        <div className={styles.infoLabel}>
          {t('coupon.apply_scope')}:
        </div>
        <div className={styles.infoDetail}>
          {applyScope}
        </div>
        <div className={styles.infoLabel}>
          {t('coupon.validity_period')}:
        </div>
        <div className={styles.infoDetail}>
          {coupon.start_time} {t('coupon.to')} {coupon.end_time}
        </div>
      </div>
      {showUsed && (
        <img src={usedIcon} alt={t('coupon.used')} className={styles.used}></img>
      )}
      {showExpired && (
        <img src={expiredIcon} alt={t('coupon.expired')} className={styles.used}></img>
      )}
    </div>
  );
};


Coupon.propTypes = {
  t: PropTypes.func.isRequired,
  coupon: PropTypes.object,
  showToUser: PropTypes.bool,
};

export default translate()(Coupon);
