import React from 'react';
import PropTypes from 'prop-types';
import { WhiteSpace } from 'antd-mobile';
import styles from './styles.css';

const GraySpace = ({ size = 'md' }) => (
  <div
    className={styles.graySpace}
  >
    <WhiteSpace size={size === 'xxl' ? 'xl' : size} />
    {size === 'xxl' && (
      <WhiteSpace size="xl" />
    )}
  </div>
);

GraySpace.propTypes = {
  size: PropTypes.string,
};

export default GraySpace;
