/**
*
* HorizontalLine
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

function HorizontalLine(props) {
  return (
    <div className={styles.horizontalLine} style={{ borderColor: props.color, borderStyle: props.lineStyle }}>
    </div>
  );
}

HorizontalLine.propTypes = {
  color: PropTypes.string,
  lineStyle: PropTypes.string,
};
export default HorizontalLine;
