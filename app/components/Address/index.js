import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import Tips from 'components/Tips';
// import { browserHistory } from 'react-router';
import {
  Button,
  InputItem,
  TextareaItem,
  Icon,
  // Modal,
} from 'antd-mobile';
import message from 'components/message';
// import WindowTitle from 'components/WindowTitle';
import CountryIcon from 'components/CountryIcon';
import Empty from 'components/Empty';
import {
  selectCurrentUser,
  selectAddressListLoading,
  selectUserInfoLoading,
  selectAddressList,
} from 'containers/App/selectors';
// import {
//   getScrollTop,
//   getScrollHeight,
//   getWindowHeight,
//   getTimeLeftDisplay,
// } from 'utils/commonHelper';
import {
  DEFAULT_COUNTRY_ISO,
} from 'utils/constants';
import {
  loadAddressList,
  addAddress,
  updateAddress,
  removeAddress,
} from 'containers/App/actions';
import styles from './styles.css';
// const alert = Modal.alert;

class AddressComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 1, // 1-list , 2-add, 3-update
      countryIso: DEFAULT_COUNTRY_ISO,
      contact: '',
      telephone: '',
      address_line: '',
      postcode: '',
      city: '',
      hasSelect: false,
      selectId: -1,
    };
  }

  componentDidMount() {
    // this.props.loadAddressList();
  }

  componentWillUnmount() {

  }

  onTapAddConfirm() {
    const data = {
      contact: this.state.contact,
      telephone: this.state.telephone,
      address_line: this.state.address_line,
      postcode: this.state.postcode,
      city: this.state.city,
      country_iso: this.state.countryIso,
    };
    if (!this.state.contact || !this.state.telephone ||
      !this.state.address_line || !this.state.postcode || !this.state.city) {
      message.error(this.props.t('common.please_input_all_address_info'), 2);
      return;
    }
    this.props.addAddress(data);
    this.setState({
      type: 1,
      countryIso: DEFAULT_COUNTRY_ISO,
      contact: '',
      telephone: '',
      address_line: '',
      postcode: '',
      city: '',
      hasSelect: false,
      selectId: -1,
    });
  }

  onTapEditConfirm() {
    const data = {
      contact: this.state.contact,
      telephone: this.state.telephone,
      address_line: this.state.address_line,
      postcode: this.state.postcode,
      city: this.state.city,
      country_iso: this.state.countryIso,
      id: this.state.selectId,
    };
    if (!this.state.contact || !this.state.telephone ||
      !this.state.address_line || !this.state.postcode || !this.state.city) {
      message.error(this.props.t('common.please_input_all_address_info'), 2);
      return;
    }
    this.props.updateAddress(data);
    this.setState({
      type: 1,
      countryIso: DEFAULT_COUNTRY_ISO,
      contact: '',
      telephone: '',
      address_line: '',
      postcode: '',
      city: '',
      hasSelect: false,
      selectId: -1,
    });
  }

  onTapDeleteConfirm() {
    // const id = this.state.selectId;
    // const { addressList, t } = this.props;
    // const address = _.find(addressList, { id });
    // const alertInstance = alert(t('common.delete'), t('common.confirm_delete_address', { name: address.contact }), [
    //   {
    //     text: t('common.cancel'),
    //     onPress: () => {
    //       console.log('cancel');
    //     },
    //   },
    //   {
    //     text: t('common.confirm'),
    //     onPress: () => {
    //       this.props.removeAddress(id);
    //     },
    //   },
    // ]);
  }

  onTapSelectConfirm() {
    const id = this.state.selectId;
    const { addressList } = this.props;
    const address = _.find(addressList, { id });
    if (this.props.onSelectAddress) {
      this.props.onSelectAddress(address);
    }
  }

  onTapUpdate() {
    const id = this.state.selectId;
    const { addressList } = this.props;
    const address = _.find(addressList, { id });
    this.setState({
      contact: address.contact,
      telephone: address.telephone,
      address_line: address.address_line,
      postcode: address.postcode,
      city: address.city,
      country_iso: address.countryIso,
      type: 3,
    });
  }

  onTapCancel() {
    this.setState({
      type: 1,
      countryIso: DEFAULT_COUNTRY_ISO,
      contact: '',
      telephone: '',
      address_line: '',
      postcode: '',
      city: '',
      hasSelect: false,
      selectId: -1,
    });
  }

  render() {
    const { addressList, t, addressListLoading, userInfoLoading, isModal = false } = this.props;
    const { type } = this.state;
    return (
      <div className={styles.main}>
        <div className={this.state.type === 1 ? styles.addressListWrap : styles.addressListEditWrap}>
          { this.state.type === 1 &&
          <div className={styles.addressListContent}>
            {
              (addressListLoading || userInfoLoading) &&
                <div className={styles.loadingWrap}>
                  <Icon type="loading" size={'md'} />
                </div>
            }
            {
              !addressListLoading && !userInfoLoading && addressList.length === 0 ?
                <Empty title={t('empty.address')} />
                : <div className={styles.addressList}>
                  {
                    addressList.map((address) => (
                      <div
                        key={address.id}
                        onClick={() => {
                          if (isModal) {
                            this.props.onSelectAddress(address);
                            return;
                          }
                          this.setState({
                            selectId: address.id,
                            hasSelect: true,
                          });
                        }}
                        className={this.state.selectId === address.id
                          ? `${styles.addressItem} ${styles.addressItemHasSelect}`
                          : styles.addressItem}
                      >
                        <div className={styles.addAddressItemUser}>
                          <span className={styles.addressContact}>{ address.contact}</span>
                          <span className={styles.addressTelephone}>{ address.telephone}</span>
                        </div>
                        <div className={styles.addressDetail}>
                          { address.isDefault &&
                          <span className={styles.isDefault}>{ t(
                            'common.default') }</span> }
                          <span className={styles.addressCity}>{address.city}</span>
                          <span className={styles.addressLine}>{address.address_line}</span>
                          <span className={styles.addressPostcode}>{address.postcode}</span>
                        </div>
                      </div>
                    ))
                  }
                </div>
            }
          </div>
          }
          { (this.state.type === 2 || this.state.type === 3) &&
          <div className={styles.addAddress}>
            <div className={styles.countryWrap}>
              <CountryIcon
                iso={this.state.countryIso}
                size={'medium'}
                className={styles.countryIcon}
              />
              <span className={styles.countryName}>{ t(
                `country.${this.state.countryIso}`) }</span>
            </div>
            <div className={styles.item}>
              <div className={styles.item}>
                <InputItem
                  type={type}
                  clear
                  placeholder={t('common.postcode')}
                  onChange={(v) => { this.setState({ postcode: v }); }}
                  onBlur={() => {}}
                  value={this.state.postcode}
                ></InputItem>
              </div>
              <InputItem
                type={type}
                clear
                placeholder={t('common.receiver')}
                onChange={(v) => { this.setState({ contact: v }); }}
                onBlur={() => {}}
                value={this.state.contact}
              ></InputItem>
            </div>

            <div className={`${styles.item} ${styles.itemAddress}`}>
              <TextareaItem
                type={type}
                autoHeight
                clear
                rows={3}
                placeholder={t('common.receiver_address')}
                onChange={(v) => { this.setState({ address_line: v }); }}
                onBlur={() => {}}
                value={this.state.address_line}
              ></TextareaItem>
            </div>

            <div className={styles.item}>
              <InputItem
                type={type}
                clear
                placeholder={t('common.city')}
                onChange={(v) => { this.setState({ city: v }); }}
                onBlur={() => {}}
                value={this.state.city}
              ></InputItem>
            </div>

            <div className={styles.item}>
              <InputItem
                type={type}
                clear
                placeholder={t('common.contact_number')}
                onChange={(v) => { this.setState({ telephone: v }); }}
                onBlur={() => {}}
                value={this.state.telephone}
              ></InputItem>
            </div>
          </div>
          }
        </div>
        { this.state.type === 1
        &&
          !isModal
            &&
            <div className={styles.addressActionsWrap}>
              <Button
                type={'primary'}
                onClick={() => this.setState({ type: 2 })}
                inline
              >
                { t('common.add_address') }
              </Button>
              {
            this.state.hasSelect &&
            <Button
              type={'primary'}
              onClick={() => this.onTapUpdate()}
              inline
            >
              { t('common.update_address') }
            </Button>
          }
              {
            !isModal && this.state.hasSelect &&
            <Button
              type={'warning'}
              onClick={() => this.onTapDeleteConfirm()}
              inline
            >
              { t('common.delete_address') }
            </Button>
          }
              {
            isModal && this.state.hasSelect &&
              <Button
                type={'primary'}
                onClick={() => this.onTapSelectConfirm()}
                inline
              >
                { t('common.confirm') }
              </Button>
          }
            </div>
        }
        { this.state.type === 2
        &&
        <div className={styles.addressActionsWrap}>
          <Button
            className={styles.cancel}
            onClick={() => this.onTapCancel()}
            inline
          >
            { t('common.cancel') }
          </Button>
          <Button
            type={'primary'}
            onClick={() => this.onTapAddConfirm()}
            inline
          >
            { t('common.confirm') }
          </Button>
        </div>
        }
        { this.state.type === 3
        &&
        <div className={styles.addressActionsWrap}>
          <Button
            className={styles.cancel}
            onClick={() => this.onTapCancel()}
            inline
          >{t('common.cancel')}</Button>
          <Button
            type={'primary'}
            onClick={() => this.onTapEditConfirm()}
            inline
          >
            { t('common.confirm') }
          </Button>
        </div>
        }
        {
          isModal &&
            <div className={styles.tipsWrap}>
              <Tips>{t('common.update_address_tips')}</Tips>
            </div>
        }
      </div>
    );
  }
}

AddressComponent.propTypes = {
  t: PropTypes.func,
  addressList: PropTypes.array,
  addressListLoading: PropTypes.bool,
  addAddress: PropTypes.func,
  updateAddress: PropTypes.func,
  // removeAddress: PropTypes.func,
  isModal: PropTypes.bool,
  onSelectAddress: PropTypes.func,
  userInfoLoading: PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectAddressList(),
  selectAddressListLoading(),
  selectUserInfoLoading(),
  (currentUser, addressList, addressListLoading, userInfoLoading) => ({
    currentUser: currentUser && currentUser.toJS(),
    addressList: (addressList && addressList.toJS()) || [],
    addressListLoading,
    userInfoLoading,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadAddressList: () => dispatch(loadAddressList()),
  addAddress: (data) => dispatch(addAddress(data)),
  updateAddress: (data) => dispatch(updateAddress(data)),
  removeAddress: (id) => dispatch(removeAddress(id)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(AddressComponent));
