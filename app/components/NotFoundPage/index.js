/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Icon, WingBlank } from 'antd-mobile';
import LinkButton from 'components/LinkButton';
import { translate } from 'react-i18next';
import NotFoundImage from './404.png';
import styles from './styles.css';

function NotFound(props) {
  const { t } = props;

  const homeUrl = '/';

  return (
    <WingBlank className={styles.notFound}>
      <img alt="404" className={styles.image} src={NotFoundImage} />
      <LinkButton
        to={homeUrl}
        type="primary"
        size="large"
        className={styles.button}
      >
        {t('common.page_not_found')}{t('common.back_to_home_page')}
        <Icon type="right" />
      </LinkButton>
    </WingBlank>
  );
}

NotFound.propTypes = {
  t: PropTypes.func.isRequired,
};

export default translate()(NotFound);
