/* eslint-disable import/no-duplicates */
import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import styles from './styles.css';
import qrcode from './qrcode.jpg';

const WechatServiceFocus = ({ t }) => (
  <div className={styles.focusWrap}>
    <div className={styles.focusText}>
      <p>{t('common.focus_wechat_service_tips')}</p>
    </div>
    <img alt={'share'} src={qrcode} className={styles.focusIcon} />
  </div>
);

WechatServiceFocus.propTypes = {
  t: PropTypes.func,
};

export default translate()(WechatServiceFocus);
