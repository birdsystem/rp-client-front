/* eslint-disable no-unused-vars,radix */
/*
 *
 * PaymentResult
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { WingBlank, Icon, WhiteSpace, Modal } from 'antd-mobile';
import { browserHistory } from 'react-router';
import CopyToClipboard from 'react-copy-to-clipboard';
import message from 'components/message';
import Money from 'components/Money';
import WindowTitle from 'components/WindowTitle';
import WeChatShare from 'components/WeChatShare';
import WechatServiceFocus from 'components/WechatServiceFocus';
import Tips from 'components/Tips';
import ShareTips from 'components/ShareTips';
import _ from 'lodash';
import wechatHelper from 'utils/wechatHelper';
import { translate } from 'react-i18next';
import { SINGLE_BUYING, GROUP_LEADER_BUYING } from 'utils/constants';
import CommonButton from 'components/CommonButton';
import { record, eventCategory, actions, eventParams } from 'utils/gaDIRecordHelper';
import styles from '../styles.css';


class PaymentResult extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showWeChatShareTips: false,
      showFocusDialog: false,
    };
  }

  componentDidMount() {
    const { location = {} } = this.props;
    const { order = {} } = location.state || {};
    if (!_.isEmpty(order) && order.product_info) {
      if (order.type === SINGLE_BUYING) {
        wechatHelper.configProductShare(order.product_info.name, order.product_price, order.product_info.id, order.product_info.image_url);
      } else {
        // const isStart = (order.type === GROUP_LEADER_BUYING);
        // wechatHelper.configGroupShare(order.product_info.name, order.product_price, order.product_info.id, order.group_id, order.product_info.image_url, isStart);
        wechatHelper.configFollowGroupShare(order.product_info.name, order.product_price, order.product_info.id, order.group_id, order.product_info.image_url);
      }
    } else {
      wechatHelper.configShareCommon();
    }
    const data = {};
    if (!_.isEmpty(order)) {
      data[eventParams.ID] = order.id;
    }
    record(actions.ENTER_PAYMENT_RESULT_PAGE, data, eventCategory.PAGE_ENTER);
  }

  onTapFocus() {
    if (wechatHelper.isOpenInWechat()) {
      window.location = wechatHelper.genWechatServiceUrl();
    } else {
      this.setState({
        showFocusDialog: true,
      });
    }
  }

  render() {
    const { t } = this.props;
    const copySuccess = wechatHelper.isOpenInWechat() ? t('common.copy_success_wechat') : t('common.copy_success');
    const { location = {} } = this.props;
    const { order = {}, balance, used_balance } = location.state || {};
    const quantity = order.quantity;
    let url = `${window.location.protocol}//${window.location.host}`;
    if (!_.isEmpty(order) && order.product_info) {
      if (order.type === SINGLE_BUYING) {
        url = `${window.location.protocol}//${window.location.host}/productdetail/${order.product_info.id}`;
      } else {
        url = `${window.location.protocol}//${window.location.host}/productdetail/${order.product_info.id}?gid=${order.group_id}`;
      }
    }
    return (
      <div className={styles.resultWrapper}>
        <WindowTitle title={t('pageTitles.payment_result')} />

        <WingBlank>
          <Icon type="check-circle" className={styles.resultIcon} style={{ fill: '#F8873B' }} />
          <WhiteSpace />

          <div className={styles.resultTitle}>{t('payment.pay_success')}</div>
          <WhiteSpace size="xl" />
          {!isNaN(balance) && parseFloat(used_balance) !== 0 && (
            <p className={styles.balanceWrap}>{t('payment.updated_balance')}: <Money className={styles.updatedBalance} amount={balance} /></p>
          )}
          <WhiteSpace size="xl" />
          <div className={styles.deliveryTips}>{t('payment.delivery_tips')}</div>
          <WhiteSpace size="xl" />
          {parseInt(quantity) > 1 &&
          <div>
            <div className={styles.multipleOrderTips}>({t('payment.multiple_order_tips', { count: quantity })})</div>
            <WhiteSpace size="xl" />
          </div>

          }
          {/* <Tips>{t('common.share_to_your_friends_on_wechat')}</Tips>
          <CopyToClipboard
            text={url}
            onCopy={() => {
              if (wechatHelper.isOpenInWechat()) {
                this.setState({
                  showWeChatShareTips: true,
                });
              } else {
                message.success(copySuccess);
              }
            }}
          >
            <CommonButton className={styles.actionButton} type="primary">{t('share.at_once')}</CommonButton>
          </CopyToClipboard>
          <WhiteSpace size="xl" />

          <CommonButton
            className={styles.actionButton}
            onClick={() => { this.onTapFocus(); }}
          >
            {t('pageTitles.follow_official_account')}
          </CommonButton> */}

          <WhiteSpace size="xl" />

          <CommonButton
            className={styles.actionButton}
            onClick={() => {
              browserHistory.push('/');
            }}
          >
            {t('common.back_to_products')}
          </CommonButton>
        </WingBlank>
        { this.state.showWeChatShareTips &&
        <div
          onClick={
            () => {
              this.setState({
                showWeChatShareTips: false,
              });
            }
          }
        ><WeChatShare />
        </div>
        }
        <Modal
          transparent
          maskClosable
          visible={this.state.showFocusDialog}
          onClose={() => {
            this.setState({
              showFocusDialog: false,
            });
          }}
        >
          <WechatServiceFocus />
        </Modal>
        <ShareTips />
      </div>
    );
  }
}

PaymentResult.propTypes = {
  t: PropTypes.func,
  location: PropTypes.object,
};

export default translate()(PaymentResult);
