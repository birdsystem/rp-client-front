import { createSelector } from 'reselect';

/**
 * Direct selector to the Payment state domain
 */
const selectPaymentDomain = () => (state) => state.get('payment');

/**
 * Other specific selectors
 */
const selectWechatQrCodeLoading = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('qrCodeLoading'),
);

const selectWechatQrCodeImageUrl = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('qrCodeImageUrl'),
);

const selectWechatOutTradeNo = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('outTradeNo'),
);

const selectWechatPaymentResultLoading = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('wechatPaymentResultLoading'),
);

const selectWechatPaymentResult = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('wechatPaymentResult'),
);

const selectLoadWechatPaymentResultTimeout = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('loadWechatPaymentResultTimeout'),
);

const selectWechatPaymentFailReason = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('wechatPaymentFailReason'),
);

const selectLoadingOrderDetail = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('loadingOrderDetail'),
);

const selectOrderDetail = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('orderDetail'),
);

const selectNeedPayOrders = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('needPayOrders'),
);

const selectBalancePaymentEnable = () => createSelector(
  selectPaymentDomain(),
  (substate) => substate.get('balancePaymentEnable'),
);

export {
  selectPaymentDomain,
  selectWechatQrCodeLoading,
  selectWechatQrCodeImageUrl,
  selectWechatOutTradeNo,
  selectWechatPaymentResult,
  selectWechatPaymentResultLoading,
  selectWechatPaymentFailReason,
  selectLoadWechatPaymentResultTimeout,
  selectLoadingOrderDetail,
  selectOrderDetail,
  selectNeedPayOrders,
  selectBalancePaymentEnable,
};
