/*
 *
 * Payment reducer
 *
 */

import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { WECHAT_PAYMENT_RESULT_PENDING } from 'utils/constants';
import wechatPaymentHelper from 'utils/wechatPaymentHelper';
import {
  LOAD_WECHAT_SCAN_QR_CODE,
  LOAD_WECHAT_SCAN_QR_CODE_SUCCESS,
  LOAD_WECHAT_SCAN_QR_CODE_ERROR,
  LOAD_WECHAT_PAYMENT_RESULT,
  LOAD_WECHAT_PAYMENT_RESULT_ERROR,
  LOAD_WECHAT_PAYMENT_RESULT_SUCCESS,
  SET_LOAD_WECHAT_PAYMENT_RESULT_TIMEOUT,
  WECHAT_PREPAY,
  WECHAT_PREPAY_ERROR,
  WECHAT_PREPAY_SUCCESS,
  LOAD_ORDER_DETAIL,
  LOAD_ORDER_DETAIL_ERROR,
  LOAD_ORDER_DETAIL_SUCCESS,
  LOAD_NEED_PAY_ORDERS,
  LOAD_NEED_PAY_ORDERS_SUCCESS,
  LOAD_NEED_PAY_ORDERS_ERROR,
  UPDATE_BALANCE,
  DISABLE_BALANCE_PAYMENT,
} from './constants';

const initialState = fromJS({
  qrCodeLoading: false,
  qrCodeImageUrl: '',
  outTradeNo: '',
  wechatPaymentResult: WECHAT_PAYMENT_RESULT_PENDING,
  wechatPaymentFailReason: '',
  wechatPaymentResultLoading: false,
  loadWechatPaymentResultTimeout: false,
  wechatPaymentData: fromJS({}),
  orderDetail: fromJS({}),
  loadingOrderDetail: false,
  needPayOrders: fromJS({}),
  loadingNeedPayOrders: false,
  balancePaymentEnable: false,
});

function paymentReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case LOAD_ORDER_DETAIL:
      return state
        .set('loadingOrderDetail', true)
        .set('orderDetail', fromJS({}));
    case LOAD_ORDER_DETAIL_SUCCESS:
      return state
        .set('orderDetail', fromJS(action.order))
        .set('loadingOrderDetail', false);
    case LOAD_ORDER_DETAIL_ERROR:
      return state
        .set('orderDetail', fromJS({}))
        .set('loadingOrderDetail', false);
    case WECHAT_PREPAY:
      return state
        .set('wechatPaymentData', fromJS({}));
    case WECHAT_PREPAY_SUCCESS:
      return state
        .set('wechatPaymentData', fromJS(action.paymentData));
    case WECHAT_PREPAY_ERROR:
      return state
        .set('wechatPaymentData', fromJS({}));
    case SET_LOAD_WECHAT_PAYMENT_RESULT_TIMEOUT:
      if (action.isTimeout) {
        return state
          .set('wechatPaymentResultLoading', false)
          .set('loadWechatPaymentResultTimeout', action.isTimeout);
      }
      return state.set('loadWechatPaymentResultTimeout', action.isTimeout);
    case LOAD_WECHAT_SCAN_QR_CODE:
      return state
        .set('qrCodeLoading', true)
        .set('qrCodeImageUrl', '')
        .set('outTradeNo', '');
    case LOAD_WECHAT_SCAN_QR_CODE_SUCCESS:
      return state
        .set('qrCodeLoading', false)
        .set('qrCodeImageUrl', action.qrCodeImageUrl)
        .set('outTradeNo', action.outTradeNo);
    case LOAD_WECHAT_SCAN_QR_CODE_ERROR:
      return state
        .set('qrCodeLoading', false)
        .set('qrCodeImageUrl', '')
        .set('outTradeNo', '');
    case LOAD_WECHAT_PAYMENT_RESULT:
      return state
        .set('wechatPaymentResultLoading', true)
        .set('wechatPaymentResult', WECHAT_PAYMENT_RESULT_PENDING)
        .set('wechatPaymentFailReason', '');
    case LOAD_WECHAT_PAYMENT_RESULT_SUCCESS:
      return state
        .set('wechatPaymentResultLoading', wechatPaymentHelper.isPaymentPending(action.result))
        .set('wechatPaymentResult', action.result.toString())
        .set('wechatPaymentFailReason', action.failReason);
    case LOAD_WECHAT_PAYMENT_RESULT_ERROR:
      return state
        .set('wechatPaymentResultLoading', false)
        .set('wechatPaymentResult', WECHAT_PAYMENT_RESULT_PENDING)
        .set('wechatPaymentFailReason', '');
    case LOAD_NEED_PAY_ORDERS:
      return state
      .set('loadingNeedPayOrders', true)
      .set('needPayOrders', fromJS({}));
    case LOAD_NEED_PAY_ORDERS_SUCCESS:
      return state
      .set('needPayOrders', fromJS(action.needPayOrders))
      .set('loadingNeedPayOrders', false)
      .set('balancePaymentEnable', !!action.needPayOrders.can_pay_with_balance);
    case LOAD_NEED_PAY_ORDERS_ERROR:
      return state
      .set('needPayOrders', fromJS({}))
      .set('loadingNeedPayOrders', false);
    case DISABLE_BALANCE_PAYMENT:
      return state.set('balancePaymentEnable', false);
    case UPDATE_BALANCE:
      return state.setIn(['needPayOrders', 'balance'], action.balance);
    default:
      return state;
  }
}

export default paymentReducer;
