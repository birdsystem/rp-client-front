import { take, call, race, put, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import i18n from 'i18next';
import { LOCATION_CHANGE } from 'react-router-redux';
import { browserHistory } from 'react-router';
import request from 'utils/request';
import wechatPaymentHelper from 'utils/wechatPaymentHelper';
import message from 'components/message';
import uaHelper from 'utils/uaHelper';
import { WECHAT_PAYMENT_RESULT_PENDING, localStorageKeys } from 'utils/constants';
import { weChatClearOpenId, setPayPreShipmentRedirectUrl } from 'containers/App/actions';
import { record, actions } from 'utils/gaDIRecordHelper';
import { selectNeedPayOrders } from './selectors';
import {
  LOAD_WECHAT_SCAN_QR_CODE,
  LOAD_WECHAT_PAYMENT_RESULT,
  WECHAT_PREPAY,
  LOAD_ORDER_DETAIL,
  LOAD_NEED_PAY_ORDERS,
  PAY_WITH_BALANCE,
} from './constants';
import {
  loadWechatScanQrCodeSuccess,
  loadWechatScanQrCodeError,
  loadWechatPaymentResultSuccess,
  loadWechatPaymentResultError,
  setLoadWechatPaymentResultTimeout,
  wechatPrepaySuccess,
  wechatPrepayError,
  loadWechatPaymentResult,
  loadOrderDetailSuccess,
  loadOrderDetailError,
  loadNeedPayOrdersError,
  loadNeedPayOrdersSuccess,
  payWithBalanceSuccess,
  payWithBalanceError,
  disableBalancePayment,
  updateBalance,
} from './actions';

// Bootstrap sagas
export default [
  getWechatScanQrCode,
  getWechatPaymentResultWatcher,
  wechatPrepay,
  getOrder,
  getNeedPayOrders,
  payWithBalance,
];

// Individual exports for testing
export function* getWechatScanQrCode() {
  while (true) {
    const watcher = yield race({
      payment: take(LOAD_WECHAT_SCAN_QR_CODE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.payment.formData;
    const requestURL = '/payment/we-chat-pay/scan-pay';
    const requestConfig = {
      method: 'POST',
      body: formData,
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      const { qr_code_image_url, out_trade_no } = result.data;
      yield put(loadWechatScanQrCodeSuccess(qr_code_image_url, out_trade_no));
    } else {
      yield put(loadWechatScanQrCodeError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* getWechatPaymentResultWatcher() {
  while (true) {
    const watcher = yield race({
      loadResult: take(LOAD_WECHAT_PAYMENT_RESULT),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const maxRetryTimes = 20;
    const retryCount = 0;

    yield put(setLoadWechatPaymentResultTimeout(false));

    yield call(getWechatPaymentResult, watcher.loadResult.outTradeNo, maxRetryTimes, retryCount);
  }
}

export function* getWechatPaymentResult(outTradeNo, maxRetryTimes, retryCount) {
  const requestURL = `/payment/we-chat-pay/payment-result?out_trade_no=${outTradeNo}`;

  // const gid = (yield select(selectOrderDetail()) || {}).toJS().group_id;

  const result = yield call(request, requestURL);
  // const result = {
  //   success: true,
  //   data: {
  //     payment_result: 0,
  //     fail_reason: '没有钱啦',
  //   },
  // };

  retryCount += 1; // eslint-disable-line

  // console.log('fetch count: ', retryCount);

  if (result.success) {
    localStorage.removeItem(localStorageKeys.LATEST_OUT_ORDER);
    const { payment_result = WECHAT_PAYMENT_RESULT_PENDING, fail_reason = '' } = result.data; // eslint-disable-line
    console.log('sagas payment result: ', payment_result);

    yield put(loadWechatPaymentResultSuccess(payment_result, fail_reason));

    if (wechatPaymentHelper.isPaymentSuccess(payment_result)) {
      const needPayOrders = (yield select(selectNeedPayOrders()) || {}).toJS();
      const order = needPayOrders.current_order || {};
      console.log('payment success');
      if (order && order.product_info) {
        record(actions.PURCHASE, {
          transaction_id: `${order.id}`,
          affiliation: 'tuantuanxia',
          value: order.total,
          currency: 'CNY',
          tax: 0,
          shipping: order.cross_border_shipping_subtotal,
          items: [
            {
              id: order.product_info.id,
              name: order.product_info.name,
              list_name: '',
              brand: '',
              category: '',
              variant: '',
              list_position: order.id,
              quantity: order.quantity,
              price: order.product_price,
            },
          ],
        });
        const fullMsg = `🎉🎉🎉 NEW ORDER 🎉🎉🎉\n[ORDER ID] ${order.id}\n[PRODUCT] ${order.product_info.name}`;
        fetch(`https://api.telegram.org/bot569739033:AAHtHuoIAYgcLxHz8CZoc-s-0Mfmo2NO5yk/sendMessage?text=${encodeURI(fullMsg)}&chat_id=-1001123482008`);
      }
      browserHistory.push({ pathname: '/payment/success', state: { order, balance: result.data.updated_balance, used_balance: result.data.used_balance } });
    } else if (wechatPaymentHelper.isPaymentFail(payment_result)) {
      console.log('payment fail');
      message.error(`${i18n.t('payment.pay_fail')}: ${fail_reason}`); // eslint-disable-line
    } else if ((retryCount < maxRetryTimes) && wechatPaymentHelper.isPaymentPending(payment_result)) {
      console.log('payment pending');
      yield call(delay, 5000);
      yield call(getWechatPaymentResult, outTradeNo, maxRetryTimes, retryCount);
    } else {
      console.log('fetch timeout');
      if (uaHelper.isWechatBrowser()) {
        message.error(i18n.t('payment.tips_load_wechat_result_timeout'));
      }
      yield put(setLoadWechatPaymentResultTimeout(true));
    }
  } else {
    yield put(loadWechatPaymentResultError(result.error));
  }
}

export function* wechatPrepay() {
  while (true) {
    const watcher = yield race({
      prepay: take(WECHAT_PREPAY),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/payment/we-chat-pay/wap-pay';

    const formData = watcher.prepay.formData;

    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (`${result.errCode}` === '1205') {
      message.error(i18n.t('error.need_pay_pre_shipment_fee'));
      const url = location.pathname;
      yield put(setPayPreShipmentRedirectUrl(url));
    }

    if (result.success) {
      if (typeof window.WeixinJSBridge == 'undefined') { // eslint-disable-line
        if (document.addEventListener) {
          document.addEventListener('WeixinJSBridgeReady', () => onWechatBridgeReady(result).next(), false);
        } else if (document.attachEvent) {
          document.attachEvent('WeixinJSBridgeReady', () => onWechatBridgeReady(result).next());
          document.attachEvent('onWeixinJSBridgeReady', () => onWechatBridgeReady(result).next());
        }
      } else {
        yield call(onWechatBridgeReady, result);
      }
      yield put(wechatPrepaySuccess(result.data));
    } else {
      // if already pay , direct to other page
      if (`${result.errCode}` === '1206') {
        // payment is being processed
        const outTradeNO = localStorage.getItem(localStorageKeys.LATEST_OUT_ORDER);
        if (outTradeNO) {
          yield put(loadWechatPaymentResult(outTradeNO));
        }
      } else if (`${result.errCode}` === '1207') { // already paid
        message.info(i18n.t('payment.you_have_already_pay_pre_shipment'));
        window.location.reload();
      } else if (`${result.errCode}` === '1209') { // 余额足够支付所有订单费用，不需要微信支付，data 里面返回最新余额
        message.info(i18n.t('payment.balance_updated'));
        yield put(updateBalance(result.data.balance));
      } else if (`${result.errCode}` === '1211') { // 有余额被锁定，暂时无法使用余额支付
        yield put(disableBalancePayment());
      } else if (`${result.errCode}` === '1212') { // 所传的余额数值有误，即客户端展示给客户的余额不是最新的值，此时 data 里面返回最新余额
        message.info(i18n.t('payment.balance_updated'));
        yield put(updateBalance(result.data.balance));
      }
      yield put(wechatPrepayError(result.error));
    }
  }
}

function* onWechatBridgeReady(result) {
  const { timeStamp, nonceStr, signType, appId, paySign, out_trade_no } = result.data;
  localStorage.setItem(localStorageKeys.LATEST_OUT_ORDER, out_trade_no);
  const requestWxPay = () => new Promise((resolve) => {
    window.WeixinJSBridge.invoke(
      'getBrandWCPayRequest', {
        appId: appId || 'wx863aa621ffeaddc3',
        // appId,
        timeStamp,
        nonceStr,
        package: result.data.package,
        signType,
        paySign,
      },
      (res) => {
        resolve(res);
      }
    );
  });

  const res = yield call(requestWxPay);

  if (res.err_msg === 'get_brand_wcpay_request:ok') {
    // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回 ok，但并不保证它绝对可靠
    yield put(loadWechatPaymentResult(out_trade_no));
  } else if (res.err_msg === 'get_brand_wcpay_request:cancel' || res.err_msg === 'get_brand_wcpay_request:fail') {
    message.info(i18n.t('payment.message_payment_cancel'));
    // 触发一次微信支付结果查询，把余额解锁
    const checkResultURL = `/payment/we-chat-pay/payment-result?out_trade_no=${out_trade_no}`;
    yield call(request, checkResultURL);
  } else {
    yield put(weChatClearOpenId());
    message.error(i18n.t('payment.message_invalid_payment_param'));
  }
}

export function* getOrder() {
  while (true) {
    const watcher = yield race({
      loadOrder: take(LOAD_ORDER_DETAIL),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = `/group/order/${watcher.loadOrder.id}`;

    const requestConfig = {
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadOrderDetailSuccess(result.data));
    } else {
      yield put(loadOrderDetailError(watcher.loadOrder.id));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* getNeedPayOrders() {
  while (true) {
    const watcher = yield race({
      loadOrder: take(LOAD_NEED_PAY_ORDERS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/group/order/need-pay-orders';

    const requestConfig = {
      feedback: { progress: { mask: true } },
      urlParams: {
        current_order_id: watcher.loadOrder.id,
      },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadNeedPayOrdersSuccess(result.data));
    } else {
      yield put(loadNeedPayOrdersError(watcher.loadOrder.id));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* payWithBalance() {
  while (true) {
    const watcher = yield race({
      pay: take(PAY_WITH_BALANCE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/payment/pay-with-balance';

    const formData = watcher.pay.formData;

    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(payWithBalanceSuccess(result.data));
      localStorage.removeItem(localStorageKeys.LATEST_OUT_ORDER);
      const needPayOrders = (yield select(selectNeedPayOrders()) || {}).toJS();
      const order = needPayOrders.current_order || {};
      console.log('balance payment success');
      if (order && order.product_info) {
        record(actions.PURCHASE, {
          transaction_id: `${order.id}`,
          affiliation: 'tuantuanxia',
          value: order.total,
          currency: 'CNY',
          tax: 0,
          shipping: order.cross_border_shipping_subtotal,
          items: [
            {
              id: order.product_info.id,
              name: order.product_info.name,
              list_name: '',
              brand: '',
              category: '',
              variant: '',
              list_position: order.id,
              quantity: order.quantity,
              price: order.product_price,
            },
          ],
        });
        const fullMsg = `🎉🎉🎉 NEW ORDER 🎉🎉🎉\n[ORDER ID] ${order.id}\n[PRODUCT] ${order.product_info.name}`;
        fetch(`https://api.telegram.org/bot569739033:AAHtHuoIAYgcLxHz8CZoc-s-0Mfmo2NO5yk/sendMessage?text=${encodeURI(fullMsg)}&chat_id=-1001123482008`);
      }
      browserHistory.push({ pathname: '/payment/success', state: { order, balance: result.data.updated_balance, used_balance: result.data.used_balance } });
    } else {
      if (`${result.errCode}` === '1210') { // 余额不足（没有被锁定，但是不足以支付全部订单费用），此时 data 中返回余额，字段名为 balance
        yield put(updateBalance(result.data.balance));
      } else if (`${result.errCode}` === '1211') { // 有余额被锁定，暂时无法使用余额支付
        yield put(disableBalancePayment());
      }
      yield put(payWithBalanceError(result.error));
    }
  }
}
