/*
 *
 * Payment actions
 *
 */

import {
  LOAD_WECHAT_SCAN_QR_CODE,
  LOAD_WECHAT_SCAN_QR_CODE_SUCCESS,
  LOAD_WECHAT_SCAN_QR_CODE_ERROR,
  LOAD_WECHAT_PAYMENT_RESULT,
  LOAD_WECHAT_PAYMENT_RESULT_SUCCESS,
  LOAD_WECHAT_PAYMENT_RESULT_ERROR,
  SET_LOAD_WECHAT_PAYMENT_RESULT_TIMEOUT,
  WECHAT_PREPAY,
  WECHAT_PREPAY_SUCCESS,
  WECHAT_PREPAY_ERROR,
  LOAD_ORDER_DETAIL,
  LOAD_ORDER_DETAIL_ERROR,
  LOAD_ORDER_DETAIL_SUCCESS,
  LOAD_NEED_PAY_ORDERS,
  LOAD_NEED_PAY_ORDERS_SUCCESS,
  LOAD_NEED_PAY_ORDERS_ERROR,
  PAY_WITH_BALANCE,
  PAY_WITH_BALANCE_ERROR,
  PAY_WITH_BALANCE_SUCCESS,
  UPDATE_BALANCE,
  DISABLE_BALANCE_PAYMENT,
} from './constants';

export function loadWechatScanQrCode(formData) {
  return {
    type: LOAD_WECHAT_SCAN_QR_CODE,
    formData,
  };
}

export function loadWechatScanQrCodeError() {
  return {
    type: LOAD_WECHAT_SCAN_QR_CODE_ERROR,
  };
}

export function loadWechatScanQrCodeSuccess(qrCodeImageUrl, outTradeNo) {
  return {
    type: LOAD_WECHAT_SCAN_QR_CODE_SUCCESS,
    qrCodeImageUrl,
    outTradeNo,
  };
}

export function wechatPrepay(formData) {
  return {
    type: WECHAT_PREPAY,
    formData,
  };
}

export function wechatPrepayError() {
  return {
    type: WECHAT_PREPAY_ERROR,
  };
}

export function wechatPrepaySuccess(paymentData) {
  return {
    type: WECHAT_PREPAY_SUCCESS,
    paymentData,
  };
}

export function loadWechatPaymentResult(outTradeNo) {
  return {
    type: LOAD_WECHAT_PAYMENT_RESULT,
    outTradeNo,
  };
}

export function loadWechatPaymentResultSuccess(result, failReason) {
  return {
    type: LOAD_WECHAT_PAYMENT_RESULT_SUCCESS,
    result,
    failReason,
  };
}

export function loadWechatPaymentResultError() {
  return {
    type: LOAD_WECHAT_PAYMENT_RESULT_ERROR,
  };
}

export function setLoadWechatPaymentResultTimeout(isTimeout) {
  return {
    type: SET_LOAD_WECHAT_PAYMENT_RESULT_TIMEOUT,
    isTimeout,
  };
}

export function loadOrderDetail(id) {
  return {
    type: LOAD_ORDER_DETAIL,
    id,
  };
}

export function loadOrderDetailSuccess(order) {
  return {
    type: LOAD_ORDER_DETAIL_SUCCESS,
    order,
  };
}

export function loadOrderDetailError() {
  return {
    type: LOAD_ORDER_DETAIL_ERROR,
  };
}

export function loadNeedPayOrders(id) {
  return {
    type: LOAD_NEED_PAY_ORDERS,
    id,
  };
}

export function loadNeedPayOrdersSuccess(needPayOrders) {
  return {
    type: LOAD_NEED_PAY_ORDERS_SUCCESS,
    needPayOrders,
  };
}

export function loadNeedPayOrdersError() {
  return {
    type: LOAD_NEED_PAY_ORDERS_ERROR,
  };
}

export function payWithBalance(formData) {
  return {
    type: PAY_WITH_BALANCE,
    formData,
  };
}

export function payWithBalanceSuccess(data) {
  return {
    type: PAY_WITH_BALANCE_SUCCESS,
    data,
  };
}

export function payWithBalanceError() {
  return {
    type: PAY_WITH_BALANCE_ERROR,
  };
}

export function updateBalance(balance) {
  return {
    type: UPDATE_BALANCE,
    balance,
  };
}

export function disableBalancePayment() {
  return {
    type: DISABLE_BALANCE_PAYMENT,
  };
}
