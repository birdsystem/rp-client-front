/* eslint-disable camelcase, no-param-reassign */
/*
 *
 * Payment
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { createSelector } from 'reselect';
import {
  record,
  eventCategory,
  actions,
  eventParams,
} from 'utils/gaDIRecordHelper';
// import config from 'config';
import {
  Button,
  Modal,
  Icon,
  WingBlank,
  WhiteSpace,
  Flex,
  List,
  Radio,
  Switch,
} from 'antd-mobile';
import message from 'components/message';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import GraySpace from 'components/GraySpace';
import { translate } from 'react-i18next';
import WindowTitle from 'components/WindowTitle';
import uaHelper from 'utils/uaHelper';
import AddressDisplay from 'components/AddressDisplay';
import Money from 'components/Money';
import {
  SINGLE_BUYING,
  GROUP_BUYING,
  GROUP_LEADER_BUYING,
  WECHAT_PAYMENT_RESULT_SUCCESS,
  localStorageKeys,
} from 'utils/constants';

import wechatHelper from 'utils/wechatHelper';
import orderHelper from 'utils/orderHelper';
import {
  selectOpenId,
  selectCouponList,
  selectCurrentCouponMapOrderId,
} from 'containers/App/selectors';
import {
  setWeChatAuthDirectUrl,
  hidePurchaseModal,
  loadCouponList,
} from 'containers/App/actions';
import CommonButton from 'components/CommonButton';
import {
  selectWechatQrCodeImageUrl,
  selectWechatQrCodeLoading,
  selectWechatOutTradeNo,
  selectWechatPaymentResultLoading,
  selectLoadWechatPaymentResultTimeout,
  selectWechatPaymentResult,
  selectNeedPayOrders,
  selectBalancePaymentEnable,
} from './selectors';
import {
  loadWechatScanQrCode,
  loadWechatPaymentResult,
  wechatPrepay,
  setLoadWechatPaymentResultTimeout,
  loadNeedPayOrders,
  payWithBalance,
} from './actions';
import styles from './styles.css';


// const httpPrefix = config.serverHost;
const RadioItem = Radio.RadioItem;

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector ||
    el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement;
  }
  return null;
}

class Payment extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      wechatScanCodeModalOpen: false,
      hasSetGaTrack: false,
      couponModalOpen: false,
      selectedCoupon: {},
      hasInitCoupon: false,
      useBalance: true,
    };
  }

  componentDidMount() {
    const { params } = this.props;
    this.props.loadNeedPayOrders(params.orderId);
    this.props.hidePurchaseModal();
    // this.props.loadCouponList(params.orderId);

    // if (wechatHelper.isOpenInWechat() && !this.props.openId) {
    //   this.props.setWeChatAuthDirectUrl(window.location.pathname);
    //   const url = wechatHelper.genInWechatAuthUrl();
    //   window.location = url;
    // }
    // const data = {};
    // data[eventParams.ID] = params.orderId;
    // record(actions.ENTER_PAYMENT_PAGE, data, eventCategory.PAGE_ENTER);
  }

  componentWillReceiveProps(nextProps) {
    const order = _.isEmpty(nextProps.needPayOrders)
      ? {}
      : nextProps.needPayOrders.current_order;
    if (!_.isEmpty(order) && !orderHelper.canPay(order)) {
      message.error(this.props.t('payment.order_cant_pay'));
      browserHistory.push('/myorderlist');
    }

    if (!_.isEmpty(nextProps.needPayOrders) && !this.state.hasSetGaTrack) {
      // browserHistory.push({ pathname: '/payment/success', state: { order: nextProps.order } });
      if (!_.isEmpty(order) && !_.isEmpty(order.product_info)) {
        wechatHelper.configProductShare(
          order.product_info.name,
          order.product_price,
          order.product_info.id,
          order.product_info.image_url
        );
      }
      this.setState({
        hasSetGaTrack: true,
      });
      record(actions.SET_CHECKOUT_OPTION, {
        checkout_step: 1,
        checkout_option: 'wechat',
        value: '',
      });
    }

    if (nextProps.wechatPaymentResult === WECHAT_PAYMENT_RESULT_SUCCESS) {
      this.setState({
        wechatScanCodeModalOpen: false,
      });
    }
    const { params } = this.props;
    // if (!_.isEmpty(nextProps.coupons) && !this.state.hasInitCoupon && this.props.currentCouponMapOrderId === params.orderId) {
    //   this.setState({
    //     selectedCoupon: nextProps.coupons[0],
    //     hasInitCoupon: true,
    //   });
    // }
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  };

  callWechatPaymentPrepay = () => {
    const { needPayOrders, openId } = this.props;
    const order = _.isEmpty(needPayOrders) ? {} : needPayOrders.current_order;
    const unPaidShippingOrders = _.isEmpty(needPayOrders)
      ? null
      : needPayOrders.unpaid_shipping_orders;
    let orderId = order.id;
    if (unPaidShippingOrders && unPaidShippingOrders.length > 0) {
      for (let i = 0; i < unPaidShippingOrders.length; i += 1) {
        orderId += `_${unPaidShippingOrders[i].id}`;
      }
    }
    record(actions.CLICK_PAY, {}, eventCategory.USER_ACTION);
    if (!openId) {
      message.error(
        this.props.t('payment.message_please_get_wechat_authorization'));
      return;
    }
    if (uaHelper.getWechatVersion() < '5.0') {
      message.error(
        this.props.t('payment.message_wechat_version_not_supported'));
      return;
    }
    const formData = {
      currency_code: 'CNY',
      openid: openId,
      order_id: orderId,
      coupon_id: this.state.selectedCoupon.id,
    };
    if (this.state.useBalance) {
      formData.use_balance = needPayOrders.balance;
    }
    this.props.wechatPrepay(formData);
  };

  alert = () => {
    message.warning(this.props.t('error.open_in_wechat'));
  };

  handleWechatModalCancel = () => {
    if (this.props.loadWechatPaymentResultTimeout) {
      this.props.setLoadWechatPaymentResultTimeout(false);
    }
    this.setState({
      wechatScanCodeModalOpen: false,
    });
  };

  handleWechatModalOk = () => {
    this.props.loadWechatPaymentResult(this.props.outTradeNo);
  };

  toggleCouponModal = (show) => {
    this.setState({ couponModalOpen: show });
  };

  toggleBalanceSwitch = () => {
    this.setState({
      useBalance: !this.state.useBalance,
    });
  };

  payWithBalance = () => {
    const { needPayOrders } = this.props;
    const order = _.isEmpty(needPayOrders) ? {} : needPayOrders.current_order;
    const unPaidShippingOrders = _.isEmpty(needPayOrders)
      ? null
      : needPayOrders.unpaid_shipping_orders;
    let orderId = order.id;
    if (unPaidShippingOrders && unPaidShippingOrders.length > 0) {
      for (let i = 0; i < unPaidShippingOrders.length; i += 1) {
        orderId += `_${unPaidShippingOrders[i].id}`;
      }
    }
    const formData = {
      order_id: orderId,
      coupon_id: this.state.selectedCoupon.id,
    };
    this.props.payWithBalance(formData);
  }

  render() {
    const { t, wechatPaymentResultLoading, loadWechatPaymentResultTimeout, needPayOrders = {}, coupons = [], balancePaymentEnable } = this.props;
    const order = _.isEmpty(needPayOrders) ? {} : needPayOrders.current_order;
    const unPaidShippingOrders = _.isEmpty(needPayOrders)
      ? null
      : needPayOrders.unpaid_shipping_orders;

    let upPaidShippingOrdersElement = null;
    if (unPaidShippingOrders &&
      unPaidShippingOrders.length > 0) {
      upPaidShippingOrdersElement = unPaidShippingOrders.map((o, inx) => {
        const { product_props = [], product_info = {}, quantity, product_price } = o;
        return (
          <div key={`${inx}od`}>
            <div
              className={styles.orderIdWrap}
            >
              <span>{ t('common.order_id') }: { o.id }</span>
              <span>{o.created_at}</span>
            </div>
            <div className={styles.productDetail}>
              <div className={styles.productImageWrap}>
                <img
                  alt={'product'}
                  src={product_info.image_url}
                  className={styles.productImage}
                />
              </div>
              <div className={styles.productInfo}>
                <p
                  className={styles.productName}
                >{ product_info.name }</p>
                <div className={styles.productProps}>
                  { product_props.length > 0 ? product_props.join(' ') : '' }
                </div>
                { o.remark &&
                  <div className={styles.remarkWrap}>
                    <span>{ t('common.remark') }: { o.remark }</span>
                  </div>
                }
                <div className={styles.priceWrap}>
                  <div>
                    <MoneyInLocalCurrency
                      cnyAmount={product_price}
                    />
                    <span className={styles.cnyPrice}><Money
                      amount={product_price}
                    /></span>
                  </div>
                  <span
                    className={styles.productQuantity}
                  >&times;{ quantity }</span>
                </div>
              </div>
            </div>
            <WingBlank className={styles.feeWrapper}>
              <Flex
                direction="row"
                justify="between"
                className={styles.detailTips}
              >
                <Flex.Item>{ t('payment.un_pay_shipment_fee') }</Flex.Item>
                <Flex.Item className={styles.paymentItem}>
                  <MoneyInLocalCurrency
                    cnyAmount={o.cross_border_shipping}
                  />
                </Flex.Item>
              </Flex>
            </WingBlank>

            {inx !== unPaidShippingOrders.length - 1 && <GraySpace />}
          </div>
        );
      });
    }

    let productSubtotalLabel = '';
    switch (order.type) {
      case SINGLE_BUYING:
        productSubtotalLabel = t('payment.item_single_buying_product_subtotal');
        break;
      case GROUP_LEADER_BUYING:
      case GROUP_BUYING:
      default:
        productSubtotalLabel = t('payment.item_group_buying_product_subtotal');
    }

    const enableWechatOfficialAccountPayment = uaHelper.isWechatBrowser();
    // const enableWechatOfficialAccountPayment = true;

    const { product_props = [], product_info = {}, quantity, product_price, address_info = {}, remark = '' } = order;

    let totalAfterCoupon = order.total;

    const hasUnPaidShipping = unPaidShippingOrders && unPaidShippingOrders.length > 0;
    let totalIncludeUnPaidShipping = hasUnPaidShipping ? needPayOrders.total_amount_including_current_order : totalAfterCoupon;

    if (!_.isEmpty(this.state.selectedCoupon) && !_.isEmpty(needPayOrders)) {
      if (!_.isEmpty(this.state.selectedCoupon)) {
        totalAfterCoupon = parseFloat(
          this.state.selectedCoupon.payment_after_applied.total).toFixed(2);
      }
      totalIncludeUnPaidShipping = hasUnPaidShipping ? (parseFloat(totalAfterCoupon) + parseFloat(needPayOrders.unpaid_shipping_amount)).toFixed(2) : totalAfterCoupon;
    }

    let canUseOnlyBalance = false;
    const { balance } = needPayOrders;
    if (parseFloat(balance) >= totalIncludeUnPaidShipping) {
      canUseOnlyBalance = true;
    }

    let payButton;
    let totalPaymentText;
    if (parseFloat(totalIncludeUnPaidShipping) === 0) { // 支付0
      payButton = (
        <CommonButton
          className={hasUnPaidShipping ? `${styles.balanceButtonWrap} ${styles.buttonWrapWithUnPaid}` : styles.balanceButtonWrap}
          onClick={this.payWithBalance}
        >
          {t('payment.confirm_pay')}
        </CommonButton>
      );
      totalPaymentText = (
        <span>{t('payment.pay') }: <Money amount={totalIncludeUnPaidShipping} /></span>
      );
    } else if (canUseOnlyBalance && this.state.useBalance) { // 纯余额支付
      payButton = (
        <CommonButton
          className={hasUnPaidShipping ? `${styles.balanceButtonWrap} ${styles.buttonWrapWithUnPaid}` : styles.balanceButtonWrap}
          onClick={this.payWithBalance}
        >
          {t('payment.pay_with_balance')}
        </CommonButton>
      );
      totalPaymentText = (
        <span>{t('payment.balance_pay_amount') }: <Money amount={totalIncludeUnPaidShipping} /></span>
      );
    } else { // 余额不足
      payButton = (
        <CommonButton
          className={hasUnPaidShipping ? `${styles.balanceButtonWrap} ${styles.buttonWrapWithUnPaid}` : styles.balanceButtonWrap}
          onClick={this.payWithBalance}
          disabled
        >
          {t('payment.not_enough_balance') }
        </CommonButton>
      );
      totalPaymentText = (
        <span>{t('payment.pay') }: <Money amount={totalIncludeUnPaidShipping} /></span>
      );
    // } else {
    //   payButton = (
    //     <CommonButton
    //       className={hasUnPaidShipping ? `${styles.buttonWrap} ${styles.buttonWrapWithUnPaid}` : styles.buttonWrap}
    //       onClick={enableWechatOfficialAccountPayment
    //         ? this.callWechatPaymentPrepay
    //         : this.alert}
    //       loading={wechatPaymentResultLoading && !loadWechatPaymentResultTimeout}
    //       disabled={wechatPaymentResultLoading && !loadWechatPaymentResultTimeout}
    //     >
    //       { wechatPaymentResultLoading && !loadWechatPaymentResultTimeout
    //         ? t('payment.tips_please_wait')
    //         : t('payment.pay_with_wechat') }
    //     </CommonButton>
    //   );
    //   if (this.state.useBalance) { // 混合支付
    //     totalPaymentText = (
    //       <span>
    //         {t('payment.pay') }<Money amount={totalIncludeUnPaidShipping} />,
    //         {t('payment.balance_pay_amount') }<Money amount={needPayOrders.balance} />,
    //         {t('payment.wechat_pay_amount') }<Money amount={parseFloat(totalIncludeUnPaidShipping) - parseFloat(needPayOrders.balance)} />
    //       </span>
    //     );
    //   } else { // 纯微信支付
    //     totalPaymentText = (
    //       <span>{t('payment.wechat_pay_amount') }: <Money amount={totalIncludeUnPaidShipping} /></span>
    //     );
    //   }
    }

    return (
      <div className={styles.wrap}>
        <WindowTitle title={t('pageTitles.payment')} />

        {/* <div className={styles.tips}>
          { t('payment.tips_coupon') }<br />
          <Icon type="down" size="md" />
        </div> */}
        {/* <WhiteSpace size="lg" /> */}

        <AddressDisplay address={address_info} />

        <GraySpace />

        <div className={styles.productDetail}>
          <div className={styles.productImageWrap}>
            <img
              alt={'product'}
              src={product_info.image_url}
              className={styles.productImage}
            />
          </div>
          <div className={styles.productInfo}>
            <p
              className={styles.productName}
            >{ product_info.name }</p>
            <div className={styles.productProps}>
              { product_props.length > 0 ? product_props.join(' ') : '' }
            </div>
            { remark &&
              <div className={styles.remarkWrap}>
                <span>{ t('common.remark') }: { remark }</span>
              </div>
            }
            <div className={styles.priceWrap}>
              <div>
                <MoneyInLocalCurrency
                  cnyAmount={product_price}
                />
                <span className={styles.cnyPrice}><Money
                  amount={product_price}
                /></span>
              </div>
              <span
                className={styles.productQuantity}
              >&times;{ quantity }</span>
            </div>
          </div>
        </div>

        <GraySpace />

        <WingBlank className={styles.detailWrapper}>
          <Flex
            direction="row"
            justify="between"
            className={styles.detailTips}
          >
            <Flex.Item>{ productSubtotalLabel }</Flex.Item>
            <Flex.Item className={styles.paymentItem}><MoneyInLocalCurrency
              cnyAmount={order.product_subtotal}
            /></Flex.Item>
          </Flex>
          <Flex
            direction="row"
            justify="between"
            className={styles.detailTips}
          >
            <Flex.Item>{ t(
              'payment.item_cross_border_shipping_subtotal') }</Flex.Item>
            <Flex.Item className={styles.paymentItem}><MoneyInLocalCurrency
              cnyAmount={order.cross_border_shipping_subtotal}
            /></Flex.Item>
          </Flex>
          { order.type === SINGLE_BUYING && (
            <Flex
              direction="row"
              justify="between"
              className={styles.detailTips}
            >
              <Flex.Item>{ t('payment.service_charge') }</Flex.Item>
              <Flex.Item className={styles.paymentItem}><MoneyInLocalCurrency
                cnyAmount={order.service_charge}
              /></Flex.Item>
            </Flex>
          ) }
        </WingBlank>

        <GraySpace />

        {/* <WingBlank className={styles.couponWrapper}>
          {parseFloat(order.discount) > 0 ? (
            <Flex direction="row" align="center" justify="between" className={styles.detailTips}>
              <Flex.Item>{t('payment.discount')}</Flex.Item>
              <Flex.Item className={styles.rightArrowIcon}>
                <div>
                  <span className={styles.couponName}>
                    {!_.isEmpty(order.coupon_info) && order.coupon_info.name}
                  </span>
                  <span className={styles.discountAmount}>
                    -&nbsp;
                    <MoneyInLocalCurrency cnyAmount={order.discount} />
                  </span>
                </div>
              </Flex.Item>
            </Flex>
          ) : (
            <Flex direction="row" align="center" justify="between" className={styles.detailTips} onClick={() => coupons.length > 0 && this.toggleCouponModal(true)}>
              <Flex.Item>{t('payment.select_coupon')}</Flex.Item>
              <Flex.Item className={styles.rightArrowIcon}>
                {coupons.length > 0 ? (
                  <div>
                    {_.isEmpty(this.state.selectedCoupon) ? t('common.use_no_coupons') : this.state.selectedCoupon.name}
                    {!_.isEmpty(this.state.selectedCoupon) && this.state.selectedCoupon.discount_type === 'AMOUNT' && (
                      <span className={styles.couponDiscount}>-&nbsp;<MoneyInLocalCurrency cnyAmount={this.state.selectedCoupon.discount} /></span>
                    )}
                    <Icon type="right" size="md" />
                  </div>
                ) : (
                  <div>{t('common.no_available_coupon')}</div>
                )}
              </Flex.Item>
            </Flex>
          )}
        </WingBlank> */}

        {/* <GraySpace /> */}

        { hasUnPaidShipping &&
        <div className={styles.unPaidShipping}>
          <div className={styles.unPaidShippingTipsWrap}>
            <span className={styles.unPaidTipsIcon}>
              !
            </span>
            <span className={styles.unPaidTipsText}>
              { t('payment.tip_pre_shipment_fee') }
            </span>
          </div>

          <GraySpace />
          {
            upPaidShippingOrdersElement
          }
        </div>
        }

        <div className={styles.actionWrap}>
          {parseFloat(totalIncludeUnPaidShipping) > 0 && this.state.useBalance && (
            <List className={styles.balanceWrap}>
              <List.Item
                extra={<Switch
                  checked={this.state.useBalance}
                  onChange={this.toggleBalanceSwitch}
                  disabled
                />}
              >
                {balancePaymentEnable ? (
                  <span>{t('payment.use_balance')}: <Money amount={needPayOrders.balance} /></span>
                ) : (
                  <span>{t('payment.balance_locked')}</span>
                )}
              </List.Item>
            </List>
          )}

          <div className={styles.payButtonWrap}>
            <div className={styles.totalPrice}>
              <div className={hasUnPaidShipping ? styles.totalWrapWithUnPaid : styles.totalWrap}>{ t('common.total_product_quantity', { quantity }) }, { t(
                'common.total_price') }: <MoneyInLocalCurrency
                  className={styles.total}
                  cnyAmount={totalAfterCoupon}
                />
              </div>

              {hasUnPaidShipping && (
                <div className={hasUnPaidShipping ? styles.totalWrapWithUnPaid : styles.totalWrap}>
                  { t('common.un_paid_shipping') }:&nbsp;
                  <MoneyInLocalCurrency
                    className={styles.total}
                    cnyAmount={needPayOrders.unpaid_shipping_amount}
                  />
                </div>
              )}

              <div className={styles.about}>
                {totalPaymentText}
              </div>
            </div>

            {payButton}
          </div>
        </div>

        <Modal
          popup
          visible={this.state.couponModalOpen}
          animationType="slide-up"
          onClose={() => this.toggleCouponModal(false)}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        >
          <List
            renderHeader={() => <div>{ t('common.coupons') }</div>}
            className="popup-list"
          >
            { coupons.map((c) => (
              <RadioItem
                key={c.id}
                checked={c.id === this.state.selectedCoupon.id}
                onChange={() => {
                  this.setState({ selectedCoupon: c });
                  this.toggleCouponModal(false);
                }}
              >
                <div className={styles.couponItem}>
                  { c.name }
                  {c.discount_type === 'AMOUNT' && (
                    <span className={styles.couponAbout}>{t('common.about')} <MoneyInLocalCurrency cnyAmount={c.discount} />
                    </span>
                  )}
                </div>
              </RadioItem>
            )) }
            <RadioItem
              key={0}
              checked={_.isEmpty(this.state.selectedCoupon)}
              onChange={() => {
                this.setState({ selectedCoupon: {} });
                this.toggleCouponModal(false);
              }}
            >
              <div className={styles.couponItem}>{t('common.use_no_coupons')}</div>
            </RadioItem>
            <WhiteSpace />
            <List.Item>
              <Button
                type="primary"
                onClick={() => this.toggleCouponModal(false)}
              >{ t(
                'common.close_modal') }</Button>
            </List.Item>
          </List>
        </Modal>

        <div>{ JSON.stringify(this.state.log) }</div>
      </div>
    );
  }
}

Payment.propTypes = {
  params: PropTypes.object,
  t: PropTypes.func,
  outTradeNo: PropTypes.string,
  loadWechatPaymentResult: PropTypes.func,
  wechatPaymentResultLoading: PropTypes.bool,
  loadWechatPaymentResultTimeout: PropTypes.bool,
  wechatPrepay: PropTypes.func,
  setLoadWechatPaymentResultTimeout: PropTypes.func,
  wechatPaymentResult: PropTypes.string,
  openId: PropTypes.string,
  setWeChatAuthDirectUrl: PropTypes.func,
  hidePurchaseModal: PropTypes.func,
  loadCouponList: PropTypes.func,
  coupons: PropTypes.array,
  loadNeedPayOrders: PropTypes.func,
  needPayOrders: PropTypes.object,
  currentCouponMapOrderId: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  balancePaymentEnable: PropTypes.bool,
  payWithBalance: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectWechatQrCodeImageUrl(),
  selectWechatOutTradeNo(),
  selectWechatQrCodeLoading(),
  selectWechatPaymentResultLoading(),
  selectLoadWechatPaymentResultTimeout(),
  selectWechatPaymentResult(),
  selectOpenId(),
  selectCouponList(),
  selectNeedPayOrders(),
  selectCurrentCouponMapOrderId(),
  selectBalancePaymentEnable(),
  (qrCodeImageUrl, outTradeNo, qrCodeLoading,
   wechatPaymentResultLoading, loadWechatPaymentResultTimeout,
   wechatPaymentResult, openId,
   coupons, needPayOrders, currentCouponMapOrderId, balancePaymentEnable) => ({
     qrCodeImageUrl,
     outTradeNo,
     qrCodeLoading,
     wechatPaymentResultLoading,
     loadWechatPaymentResultTimeout,
     wechatPaymentResult,
     openId: openId || localStorage.getItem(localStorageKeys.WECHAT_OPEN_ID), // un fix issue
     coupons: coupons.toJS(),
     needPayOrders: needPayOrders.toJS(),
     currentCouponMapOrderId,
     balancePaymentEnable,
   }),
);

function mapDispatchToProps(dispatch) {
  return {
    loadWechatScanQrCode: (formData) => dispatch(
      loadWechatScanQrCode(formData)),
    loadWechatPaymentResult: (outTradeNo) => dispatch(
      loadWechatPaymentResult(outTradeNo)),
    wechatPrepay: (formData) => dispatch(wechatPrepay(formData)),
    setLoadWechatPaymentResultTimeout: (timeout) => dispatch(
      setLoadWechatPaymentResultTimeout(timeout)),
    setWeChatAuthDirectUrl: (url) => dispatch(setWeChatAuthDirectUrl(url)),
    hidePurchaseModal: () => dispatch(hidePurchaseModal()),
    loadCouponList: (orderId) => dispatch(loadCouponList({
      isValid: '1',
      orderId,
    })),
    loadNeedPayOrders: (id) => dispatch(loadNeedPayOrders(id)),
    payWithBalance: (data) => dispatch(payWithBalance(data)),
    dispatch,
  };
}

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(Payment));
