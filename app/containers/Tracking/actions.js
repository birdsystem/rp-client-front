import {
  LOAD_ORDER_TRACES,
  LOAD_ORDER_TRACES_ERROR,
  LOAD_ORDER_TRACES_SUCCESS,
} from './constants';

export function loadOrderTraces(id) {
  return {
    type: LOAD_ORDER_TRACES,
    id,
  };
}

export function loadOrderTracesSuccess(traces) {
  return {
    type: LOAD_ORDER_TRACES_SUCCESS,
    traces,
  };
}

export function loadOrderTracesError() {
  return {
    type: LOAD_ORDER_TRACES_ERROR,
  };
}
