import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { createSelector } from 'reselect';
import { Icon, WingBlank, WhiteSpace } from 'antd-mobile';
import { translate } from 'react-i18next';
import WindowTitle from 'components/WindowTitle';
import Timeline from 'components/Timeline';
import AddressDisplay from 'components/AddressDisplay';
import {
  selectLocationState,
} from 'containers/App/selectors';
import {
  selectOrderTraces,
} from './selectors';
import {
  loadOrderTraces,
} from './actions';
import styles from './styles.css';

class Tracking extends React.Component {
  componentDidMount() {
    const { params } = this.props;
    this.props.loadOrderTraces(params.orderId);
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  render() {
    const { t, trackingData = {} } = this.props;
    const { address_info = {}, traces = [] } = trackingData;
    return (
      <div>
        <WindowTitle title={t('pageTitles.tracking')} />

        <div className={styles.trackingWrapper}>
          <WingBlank>
            <div className={styles.statusWrapper}>
              <div onClick={this.goBack}>
                <Icon type="left" size="lg" />
              </div>
              <div>{t(`order.status_${trackingData.status}`)}</div>
            </div>

            {trackingData.delivery_service_name && trackingData.tracking_number && (
              <div className={styles.trackingNoWrapper}>
                {trackingData.delivery_service_name} {trackingData.tracking_number}
              </div>
            )}

            <div className={styles.tracesWrapper}>
              <AddressDisplay address={address_info} />
              <WhiteSpace />
              <div className={styles.traces}><Timeline data={traces} /></div>
            </div>
          </WingBlank>
        </div>

      </div>
    );
  }
}

Tracking.propTypes = {
  params: PropTypes.object,
  loadOrderTraces: PropTypes.func,
  t: PropTypes.func,
  locationState: PropTypes.object,
  trackingData: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectOrderTraces(),
  selectLocationState(),
  (trackingData, locationState) => ({
    trackingData: trackingData.toJS(),
    locationState,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    loadOrderTraces: (id) => dispatch(loadOrderTraces(id)),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Tracking));
