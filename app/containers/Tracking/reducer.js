import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  LOAD_ORDER_TRACES,
  LOAD_ORDER_TRACES_ERROR,
  LOAD_ORDER_TRACES_SUCCESS,
} from './constants';

const initialState = fromJS({
  orderTraces: fromJS({}),
  loadingTraces: false,
});

function trackingReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case LOAD_ORDER_TRACES:
      return state
        .set('loadingTraces', true)
        .set('orderTraces', fromJS({}));
    case LOAD_ORDER_TRACES_SUCCESS:
      return state
        .set('orderTraces', fromJS(action.traces))
        .set('loadingTraces', false);
    case LOAD_ORDER_TRACES_ERROR:
      return state
        .set('orderTraces', fromJS({}))
        .set('loadingTraces', false);
    default:
      return state;
  }
}

export default trackingReducer;
