import { take, call, race, put } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import request from 'utils/request';
import {
  LOAD_ORDER_TRACES,
} from './constants';
import {
  loadOrderTracesSuccess,
  loadOrderTracesError,
} from './actions';

// Bootstrap sagas
export default [
  getOrderTraces,
];

// Individual exports for testing
export function* getOrderTraces() {
  while (true) {
    const watcher = yield race({
      loadOrder: take(LOAD_ORDER_TRACES),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = `/group/order/tracking?id=${watcher.loadOrder.id}`;

    const requestConfig = {
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);
    // const result = {
    //   success: true,
    //   data: {
    //     id: 1,
    //     status: '运输中',
    //     delivery_service_name: '专线小包',
    //     tracking_number: 'SP1212121121',
    //     address_info: {
    //       address_line_1: '2010 San Ramon Ave',
    //       city: 'Mountain view',
    //       contact: 'lulu',
    //       post_code: '94040',
    //       telephone: '12345678901',
    //     },
    //     traces: [{ // 按照时间倒序返回
    //       message: '【跨境】清关完成，英国境内派送中',
    //       time: '01-07 09:31',
    //     }, {
    //       message: '【跨境】已到达英国，清关中',
    //       time: '01-06 09:31',
    //     }, {
    //       message: '【跨境】订单已出库，发往目的地英国',
    //       time: '01-06 09:31',
    //     }, {
    //       message: '【大陆】订单拣货中，待出库',
    //       time: '01-06 09:31',
    //     }, {
    //       message: '【大陆】仓库已收货，待拣货',
    //       time: '01-06 09:31',
    //     }, {
    //       message: '商家已发货，待仓库收货',
    //       time: '01-05 09:31',
    //     }, {
    //       message: '已采购，待商家发货',
    //       time: '01-04 09:31',
    //     }, {
    //       message: '已成团，待仓库采购',
    //       time: '01-03 09:31',
    //     }, {
    //       message: '已下单，待成团',
    //       time: '01-02 09:31',
    //     }],
    //   },
    // };

    if (result.success) {
      yield put(loadOrderTracesSuccess(result.data));
    } else {
      yield put(loadOrderTracesError(watcher.loadOrder.id));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
