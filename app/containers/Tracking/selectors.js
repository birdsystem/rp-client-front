import { createSelector } from 'reselect';

/**
 * Direct selector to the Tracking state domain
 */
const selectTrackingDomain = () => (state) => state.get('tracking');

/**
 * Other specific selectors
 */
const selectOrderTraces = () => createSelector(
  selectTrackingDomain(),
  (substate) => substate.get('orderTraces')
);

export {
  selectTrackingDomain,
  selectOrderTraces,
};
