import { createSelector } from 'reselect';

const selectSearchDomain = () => (state) => state.get('search');

const selectRecentSearch = () => createSelector(
  selectSearchDomain(),
  (substate) => substate.get('recentSearch'),
);

const selectHotSearch = () => createSelector(
  selectSearchDomain(),
  (substate) => substate.get('hotSearch'),
);

export {
  selectRecentSearch,
  selectHotSearch,
};
