/*
 *
 * Search constants
 *
 */

export const ADD_RECENT_SEARCH = 'Search/ADD_RECENT_SEARCH';
export const MAX_RECENT_SEARCH_COUNT = 10;
export const LOCAL_STORE_RECENT_SEARCH = 'TT/LOCAL_STORE_RECENT_SEARCH';

export const LOAD_HOT_SEARCH = 'Search/LOAD_HOT_SEARCH';
export const LOAD_HOT_SEARCH_SUCCESS = 'Search/LOAD_HOT_SEARCH_SUCCESS';
export const LOAD_HOT_SEARCH_FAIL = 'Search/LOAD_HOT_SEARCH_FAIL';
