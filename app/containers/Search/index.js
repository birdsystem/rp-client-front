/* eslint-disable jsx-a11y/no-autofocus */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import WindowTitle from 'components/WindowTitle';
import { Icon } from 'antd-mobile';
import { Link, browserHistory } from 'react-router';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import wechatHelper from 'utils/wechatHelper';
import CommonButton from 'components/CommonButton';
import homeIcon from './home.png';
import searchIcon from './search.png';
import { selectRecentSearch, selectHotSearch } from './selectors';
import { addRecentSearch, loadHotSearch } from './actions';
import styles from './styles.css';

class Search extends Component {
  constructor(props) {
    super(props);
    const { location = {} } = this.props;
    let keyword = location && location.query && decodeURI(location.query.q);
    if (!keyword || keyword === 'undefined') {
      keyword = '';
    }
    this.state = {
      searchInput: keyword,
    };
  }

  componentDidMount() {
    if (this.autoFocusInst && this.autoFocusInst.focus) {
      this.autoFocusInst.focus();
    }
    wechatHelper.configShareCommon();
    record(actions.ENTER_SEARCH_PAGE, {}, eventCategory.PAGE_ENTER);
    this.props.loadHotSearch();
  }


  onClickSearch() {
    if (!this.state.searchInput) {
      return;
    }
    this.props.addRecentSearch(this.state.searchInput);
    browserHistory.push(`/searchresult?q=${this.state.searchInput}`);
  }

  onClickItemSearch(search) {
    browserHistory.push(`/searchresult?q=${search}`);
  }

  handleKeyPress(e) {
    if (`${e.keyCode}` === '13' || `${e.charCode}` === '13') {
      this.onClickSearch();
    }
  }

  render() {
    const { recentSearch, t, hotSearch } = this.props;
    console.log(hotSearch);

    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.search_product')} />

        <div className={styles.top}>
          <Link
            className={styles.homeIconWrap}
            to="/"
          >
            <img src={homeIcon} className={styles.homeIcon} alt={'home'} />
          </Link>
          <span className={styles.searchInputWrap}>
            <img
              src={searchIcon}
              className={styles.searchIcon}
              alt={'search'}
            />
            <input
              autoFocus
              type={'search'}
              className={styles.searchInput}
              value={this.state.searchInput}
              ref={(el) => { this.autoFocusInst = el; }}
              onKeyPress={(e) => { this.handleKeyPress(e); }}
              onChange={(e) => {
                this.setState({ searchInput: e.target.value });
              }}
            />
            <span
              onClick={() => {
                this.setState({
                  searchInput: '',
                });
              }}
              className={styles.clearIcon}
            ><Icon type="cross" size="xs" /></span>
          </span>
          <CommonButton
            disabled={!this.state.searchInput}
            className={this.state.searchInput ? styles.searchButton : styles.searchButtonDisable}
            onClick={() => {
              this.onClickSearch();
            }}
          >
            {t('common.search')}
          </CommonButton>
        </div>
        <div className={styles.recentSearch}>
          <p className={styles.recentSearchTitle}>
            {t('common.recent_search')}
          </p>
          <div className={styles.recentSearchItemWrap}>
            {
              recentSearch.map(
                (search, index) => (
                  <span
                    key={`${index}`}
                    className={styles.recentSearchItem}
                    onClick={() => {
                      this.onClickItemSearch(search);
                    }}
                  >
                    {search}
                  </span>
                )
              )
            }
          </div>
        </div>

        { hotSearch && hotSearch.length > 0 && <div className={styles.recentSearch}>
          <p className={styles.recentSearchTitle}>
            { t('common.hot_search') }
          </p>
          <div className={styles.recentSearchItemWrap}>
            {
              hotSearch.map(
                (search, index) => (
                  <span
                    key={`${index}`}
                    className={styles.recentSearchItem}
                    onClick={() => {
                      this.onClickItemSearch(search);
                    }}
                  >
                    { search }
                  </span>
                )
              )
            }
          </div>
        </div>
        }
      </div>
    );
  }
}

Search.propTypes = {
  t: PropTypes.func,
  addRecentSearch: PropTypes.func,
  recentSearch: PropTypes.array,
  location: PropTypes.object,
  loadHotSearch: PropTypes.func,
  hotSearch: PropTypes.array,
};

const mapStateToProps = createSelector(
  selectRecentSearch(),
  selectHotSearch(),
  (recentSearch, hotSearch) => ({
    recentSearch: recentSearch.toJS(),
    hotSearch: hotSearch.toJS(),
  }),
);

const mapDispatchToProps = (dispatch) => ({
  addRecentSearch: (data) => dispatch(addRecentSearch(data)),
  loadHotSearch: () => dispatch(loadHotSearch()),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Search));
