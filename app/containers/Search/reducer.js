/*
 *
 * Search reducer
 *
 */

import { fromJS } from 'immutable';

import {
  localStorageKeys,
} from 'utils/constants';

import {
  ADD_RECENT_SEARCH,
  MAX_RECENT_SEARCH_COUNT,
  LOAD_HOT_SEARCH_SUCCESS,
} from './constants';

const initialState = fromJS({
  recentSearch: (localStorage.getItem(localStorageKeys.RECENT_SEARCH) && JSON.parse(localStorage.getItem(localStorageKeys.RECENT_SEARCH))) || [],
  hotSearch: [],
});

function searchReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_RECENT_SEARCH: {
      const recentSearch = state.get('recentSearch').toJS();
      if (recentSearch.length > MAX_RECENT_SEARCH_COUNT) {
        recentSearch.pop();
      }
      recentSearch.unshift(action.data);
      localStorage.setItem(localStorageKeys.RECENT_SEARCH, JSON.stringify(recentSearch));
      return state.set('recentSearch', fromJS(recentSearch));
    }
    case LOAD_HOT_SEARCH_SUCCESS: {
      return state.set('hotSearch', fromJS(action.data));
    }
    default:
      return state;
  }
}

export default searchReducer;
