import {
  ADD_RECENT_SEARCH,
  LOAD_HOT_SEARCH,
  LOAD_HOT_SEARCH_SUCCESS,
  LOAD_HOT_SEARCH_FAIL,
} from './constants';

export function addRecentSearch(data) {
  return {
    type: ADD_RECENT_SEARCH,
    data,
  };
}

export function loadHotSearch() {
  return {
    type: LOAD_HOT_SEARCH,
  };
}

export function loadHotSearchSuccess(data) {
  return {
    type: LOAD_HOT_SEARCH_SUCCESS,
    data,
  };
}

export function loadHotSearchFail() {
  return {
    type: LOAD_HOT_SEARCH_FAIL,
  };
}

