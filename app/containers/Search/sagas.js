import { call, put, race, take } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_HOT_SEARCH } from './constants';
import {
  loadHotSearchSuccess,
} from './actions';

export default [
  getLoadHotSearchWatcher,
];

export function* getLoadHotSearchWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadHotSearch: take(LOAD_HOT_SEARCH),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = '/product/search-keyword';
    const requestConfig = {
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      yield put(loadHotSearchSuccess(result.data));
    }
  }
}
