/* eslint-disable no-unused-vars,no-param-reassign */
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import {
  selectCurrentUser,
  selectCurrentCoupon,
  selectLocationState,
  selectLatestCollectCouponTime,
  selectShowRedPacketAfterLogin,
  selectShowRedPacketModal,
  selectRedPacketModalType,
} from 'containers/App/selectors';
import wechatHelper from 'utils/wechatHelper';
import { Modal } from 'antd-mobile';
import { loadACoupon, collectACoupon, collectACouponSuccess, setLatestCollectCouponTime, setShowRedPacketAfterLogin, setShowRedPacketModal } from 'containers/App/actions';
import message from 'components/message';
import RedPacket from 'components/RedPacket';
import { whiteList } from 'utils/constants';
import redPackage from './red_packet.png';
import close from './close.png';
import styles from './styles.css';

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector ||
    el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement;
  }
  return null;
}

class RedPacketEvent extends Component {
  constructor(props) {
    const showRedPacketAtFirst = Math.random() <= 0.3;
    // const showRedPacketAtFirst = true;
    super(props);
    const canShowPacket = !window.location.pathname.toLowerCase().match(/^\/(payment|myinfo|receiveaddress|collect-coupon|login|register)/g);
    const canShowPacketWithTimeLimit = ((new Date()).getTime() - this.props.latestCollectCouponTime) > 86400000;
    this.state = {
      showRedPacketInterval: 1000 * 180,
      showRedPacketTime: 1000 * 30,
      showRedPacketAtFirst,
      canShowPacket,
      showingPacket: true,
      showingAnimation: false,
      animationTime: 1000 * 5,
      canShowPacketWithTimeLimit,
      hasStartPacketDisplayLoop: false,
      hasLoad: false,
    };
  }

  componentDidMount() {
    this.props.loadACoupon();
    wechatHelper.configCouponCollectShare();
  }

  componentWillReceiveProps(nextProps) {
    const pathName = nextProps.locationState.locationBeforeTransitions.pathname;
    const canShowPacket = !pathName.toLowerCase().match(/^\/(payment|myinfo|receiveaddress|collect-coupon|login|register)/g);
    this.setState({
      canShowPacket,
    });

    if (!_.isEmpty(nextProps.currentCoupon) && !this.state.hasStartPacketDisplayLoop) {
      if (this.state.showRedPacketAtFirst) {
        this.hideRedPacket();
      } else {
        this.showRedPacket();
      }
      this.setState({
        hasStartPacketDisplayLoop: true,
      });
    }

    const canShowPacketWithTimeLimit = ((new Date()).getTime() - nextProps.latestCollectCouponTime) > 86400000;
    this.setState({
      canShowPacketWithTimeLimit,
    });

    if (!_.isEmpty(this.props.currentCoupon) && this.props.showRedPacketAfterLogin && !_.isEmpty(this.props.currentUser)) {
      this.props.setShowRedPacketAfterLogin(false);
      this.collectCoupon();
    }
  }

  componentWillUnmount() {
    if (this.showRedPacketTimeOut) {
      clearTimeout(this.showRedPacketTimeOut);
    }
    if (this.hideRedPacketTimeOut) {
      clearTimeout(this.hideRedPacketTimeOut);
    }
    if (this.showAnimationTimeOut) {
      clearTimeout(this.hideRedPacketTimeOut);
    }
    if (this.hideAnimationTimeOut) {
      clearTimeout(this.hideAnimation);
    }
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  }

  collectCoupon = () => {
    if (_.isEmpty(this.props.currentUser)) {
      this.props.setShowRedPacketAfterLogin(true);
      message.info(this.props.t('common.collect_after_login_or_register'));
      const path = encodeURIComponent(window.location.pathname);
      browserHistory.push(`/login?service=${path}`);
      return;
    }
    const coupon = this.props.currentCoupon;
    if (_.isEmpty(coupon)) {
      return;
    }
    this.props.collectACoupon({
      coupon_id: coupon.id,
      showRedPacketModal: true,
    });
  }

  showAnimation() {
    if (this.showAnimationTimeOut) {
      clearTimeout(this.showAnimationTimeOut);
    }
    if (this.hideAnimationTimeOut) {
      clearTimeout(this.hideAnimationTimeOut);
    }
    this.showAnimationTimeOut = setTimeout(() => {
      this.setState({
        showingAnimation: true,
      });
      this.hideAnimationTimeOut = setTimeout(() => {
        this.setState({
          showingAnimation: false,
        });
      }, this.state.animationTime);
    }, this.state.showRedPacketTime / 2);
  }

  showRedPacket() {
    if (this.showRedPacketTimeOut) {
      clearTimeout(this.showRedPacketTimeOut);
    }
    if (this.hideRedPacketTimeOut) {
      clearTimeout(this.showRedPacketTimeOut);
    }
    this.setState({
      showingPacket: false,
    });
    this.showRedPacketTimeOut = setTimeout(() => {
      this.setState({
        showingPacket: true,
      });
      this.hideRedPacket();
    }, this.state.showRedPacketInterval);
  }

  hideRedPacket() {
    if (this.showRedPacketTimeOut) {
      clearTimeout(this.showRedPacketTimeOut);
    }
    if (this.hideRedPacketTimeOut) {
      clearTimeout(this.showRedPacketTimeOut);
    }
    this.setState({
      showingPacket: true,
    });
    this.showAnimation();
    this.hideRedPacketTimeOut = setTimeout(() => {
      this.setState({
        showingPacket: false,
      });
      this.showRedPacket();
    }, this.state.showRedPacketTime);
  }

  render() {
    const { t, currentCoupon, currentUser } = this.props;
    const visible = this.state.canShowPacket
      && this.state.showingPacket && this.state.canShowPacketWithTimeLimit
      && !_.isEmpty(currentCoupon);
    return (
      <div className={styles.wrap}>
        {
          visible &&
          <div
            onClick={() => {
              this.collectCoupon();
            }}
            className={styles.packetWrap}
          >
            <img
              src={redPackage}
              className={this.state.showingAnimation ? `${styles.package} ${styles.tada}` : styles.package}
              alt={'RedPackage'}
            />
          </div>
        }
        <Modal
          className={'redPacketModal'}
          visible={this.props.showRedPacketModal}
          transparent
          maskClosable
          onClose={() => {
            this.props.setShowRedPacketModal(false);
          }}
          afterClose={() => {
            this.props.loadACoupon();
          }}
          title=""
          footer={[]}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        >
          <div className={styles.collectContainer}>
            <RedPacket
              coupon={currentCoupon}
              onClickOpenCouponPage={() => {
                this.props.setShowRedPacketModal(false);
              }}
            />
            <div
              onClick={() => {
                this.props.setShowRedPacketModal(false);
              }}
            >
              <img
                src={close}
                className={styles.close}
                alt={'X'}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

RedPacketEvent.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  currentCoupon: PropTypes.object,
  locationState: PropTypes.object,
  latestCollectCouponTime: PropTypes.number,
  loadACoupon: PropTypes.func,
  setShowRedPacketAfterLogin: PropTypes.func,
  showRedPacketAfterLogin: PropTypes.bool,
  showRedPacketModal: PropTypes.bool,
  setShowRedPacketModal: PropTypes.func,
  collectACoupon: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectCurrentCoupon(),
  selectLocationState(),
  selectLatestCollectCouponTime(),
  selectShowRedPacketAfterLogin(),
  selectShowRedPacketModal(),
  (currentUser, currentCoupon, locationState, latestCollectCouponTime, showRedPacketAfterLogin, showRedPacketModal) => ({
    currentUser: currentUser && currentUser.toJS(),
    currentCoupon: currentCoupon && currentCoupon.toJS(),
    locationState,
    latestCollectCouponTime,
    showRedPacketAfterLogin,
    showRedPacketModal,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadACoupon: () => dispatch(loadACoupon()),
  collectACoupon: (data) => dispatch(collectACoupon(data)),
  collectACouponSuccess: () => dispatch(collectACouponSuccess()),
  setLatestCollectCouponTime: (time) => dispatch(setLatestCollectCouponTime(time)),
  setShowRedPacketAfterLogin: (show) => dispatch(setShowRedPacketAfterLogin(show)),
  setShowRedPacketModal: (show) => dispatch(setShowRedPacketModal(show)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(RedPacketEvent));
