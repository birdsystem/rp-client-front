/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { browserHistory } from 'react-router';
import WindowTitle from 'components/WindowTitle';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { Icon } from 'antd-mobile';
import {
  selectLocationState,
} from 'containers/App/selectors';
import data from './data';
import styles from './styles.css';

class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCategoryId: '11549',
    };
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/' && this.props.locationState.previousPathname !== '/collect-coupon') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  toggleCategory = (id) => {
    this.setState({ activeCategoryId: id });
  }

  render() {
    const { t } = this.props;

    const { activeCategoryId } = this.state;

    const categoryContent = data.find((category) => category.id === activeCategoryId);

    return (
      <div className={styles.tabContainer}>
        <WindowTitle title={t('pageTitles.product_category')} />
        <div
          onClick={this.goBack}
          className={styles.tabHeader}
        >
          <Icon type="left" size="md" className={styles.goBack} />
        </div>
        <div className={styles.tabContent}>
          <div className={styles.tabNav}>
            <ul>
              {data.map((category) => (
                <li
                  key={category.id}
                  onClick={() => this.toggleCategory(category.id)}
                  className={activeCategoryId === category.id ? styles.activeTab : ''}
                >
                  {category.name}
                </li>
              ))}
            </ul>
          </div>
          <div className={styles.tabMenu}>
            <ul className={styles.tabUl}>
              <li className={styles.tabLi}>
                {categoryContent.children.map((subCategory, index) => (
                  <div className={styles.tabList} key={index}>
                    <h2>{subCategory.name}</h2>
                    <ul>
                      {subCategory.children.map((item, idx) => (
                        <li key={idx}>
                          <a href={`/searchresult?q=${item.keyword}`}>
                            <div className={styles.tabImg}>
                              <img src={`https://www.tuantuanxia.com/images/${item.src}`} />
                            </div>
                            <p className={styles.tabDesc}>{item.name}</p>
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                ))}
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

Categories.propTypes = {
  t: PropTypes.func,
  locationState: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectLocationState(),
  (locationState) => ({
    locationState,
  }),
);

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Categories));
