import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { WhiteSpace } from 'antd-mobile';
import { browserHistory } from 'react-router';
import LinkButton from 'components/LinkButton';
import WindowTitle from 'components/WindowTitle';
import CommonButton from 'components/CommonButton';
import Tips from 'components/Tips';
import { translate } from 'react-i18next';
import wechatHelper from 'utils/wechatHelper';
import styles from './styles.css';

class FollowOfficialAccount extends Component {
  onClickFollow = () => {
    if (wechatHelper.isOpenInWechat()) {
      window.location = wechatHelper.genWechatServiceUrl();
    }
  }

  render() {
    const { t, location = {} } = this.props;
    let pid = -1;
    if (location && location.state && location.state.product) {
      pid = parseInt(location.state.product.id, 10);
    }
    wechatHelper.configSubmitProductShare();

    return (
      <div className={styles.resultWrapper}>
        <WindowTitle title={t('pageTitles.follow_official_account')} />

        {/* <Tips fontSize="16px">{t('common.follow_official_account_text')}</Tips> */}

        {/* <CommonButton
          className={styles.followButton}
          onClick={this.onClickFollow}
        >
          {t('pageTitles.follow_official_account')}
        </CommonButton> */}

        <WhiteSpace size="xl" />

        { pid > 0 && <CommonButton
          className={styles.view}
          onClick={() => {
            browserHistory.push(`/productdetail/${pid}`);
          }}
        >
          { t('common.buy_at_once') }
        </CommonButton>
        }

        { pid > 0 && <WhiteSpace size="xl" />}

        <LinkButton type="default" to="/">
          {t('common.back_to_home_page')}
        </LinkButton>

      </div>
    );
  }
}

FollowOfficialAccount.propTypes = {
  t: PropTypes.func,
  location: PropTypes.object,
};

export default translate()(FollowOfficialAccount);
