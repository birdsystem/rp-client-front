/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { InputItem, Icon, Button, WingBlank } from 'antd-mobile';
import { Link } from 'react-router';
import message from 'components/message';
import WindowTitle from 'components/WindowTitle';
import CommonButton from 'components/CommonButton';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import { selectCurrentUser, selectOpenId } from 'containers/App/selectors';
import { resetPassword, setWeChatAuthDirectUrl } from 'containers/App/actions';
import wechatHelper from 'utils/wechatHelper';
import {
  localStorageKeys,
} from 'utils/constants';
import request from 'utils/request';
import styles from './styles.css';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countryCode: '44',
      name: '',
      password: '',
      confirmPassword: '',
      code: '',
      mobile: '',
      autoLogin: true,
      countDownSecond: 0,
      countingDown: false,
    };
  }

  componentDidMount() {
    // if (wechatHelper.isOpenInWechat() && !this.props.openId) {
    //   this.props.setWeChatAuthDirectUrl('/register');
    //   const url = wechatHelper.genInWechatAuthUrl();
    //   window.location = url;
    // }
    wechatHelper.configShareCommon();
    record(actions.ENTER_RESET_PASSWORD_PAGE, {}, eventCategory.PAGE_ENTER);
  }

  getCode = () => {
    if (this.state.countingDown) {
      return;
    }
    this.startCodeCountDown(60);
    const requestConfig = {
      method: 'POST',
      body: {
        telephone: this.state.mobile,
        dial_code: this.state.countryCode,
      },
      feedback: { progress: { mask: true } },
    };
    request('/auth/client/public/send-reset-password-verification-code', requestConfig).then((response) => {
      if (response.success) {
        message.success(this.props.t('common.verification_code_sent'));
      } else {
        this.stopCodeCountDown();
      }
    }, () => {
      this.stopCodeCountDown();
    });
  }

  stopCodeCountDown = () => {
    if (this.state.codeCountDown) {
      window.clearInterval(this.state.codeCountDown);
    }
    this.setState({
      countingDown: false,
      codeCountDown: undefined,
    });
  };

  countryCodeChange = (event) => {
    this.setState({ countryCode: event.target.value });
  };

  startCodeCountDown = (second) => {
    if (this.state.codeCountDown) {
      window.clearInterval(this.state.codeCountDown);
    }
    this.state.countDownSecond = second;
    const countDown = window.setInterval(() => {
      if (this.state.countDownSecond <= 0) {
        this.stopCodeCountDown();
        return;
      }
      this.setState({
        countDownSecond: this.state.countDownSecond - 1,
      });
    }, 1000);
    this.setState({
      countingDown: true,
      codeCountDown: countDown,
    });
  }

  reset = () => {
    if (this.state.password !== this.state.confirmPassword) {
      message.error(this.props.t('common.password_must_be_the_same'));
      return;
    }
    const data = {
      dial_code: this.state.countryCode,
      new_password: this.state.password,
      telephone: this.state.mobile,
      verification_code: this.state.code,
    };
    this.props.resetPassword(data);
  }

  render() {
    const { t } = this.props;
    const { type } = this.state;
    let getCodeText = t('common.get_code');
    if (this.state.countingDown) {
      getCodeText = `${getCodeText} ( ${this.state.countDownSecond} )`;
    }
    return (
      <WingBlank size="lg" className={styles.main}>
        <WindowTitle title={t('pageTitles.reset_password')} />

        <div className={styles.label}>
          { t('common.please_input_registration_mobile') }
        </div>
        <div className={styles.mobileWrap}>
          <select
            className={styles.countryCodeSelect}
            value={this.state.countryCode}
            onChange={this.countryCodeChange}
          >
            <option value="86">+86</option>
            <option value="44">+44</option>
          </select>
          <Icon type="down" size={'xs'} />
          <div className={styles.mobileInputWrap}>
            <InputItem
              type={type}
              className={styles.registerInput}
              clear
              onChange={(v) => { this.setState({ mobile: v }); }}
              onBlur={(v) => { }}
            ></InputItem>
          </div>
        </div>

        <div className={styles.label}>
          { t('common.please_input_code') }
        </div>
        <div className={styles.codeWrap}>
          <div className={styles.codeInputWrap}>
            <InputItem
              type={type}
              className={styles.registerInput}
              onChange={(v) => { this.setState({ code: v }); }}
              onBlur={(v) => { }}
            ></InputItem>
          </div>
          <CommonButton disabled={this.state.countingDown} onClick={() => this.getCode()} className={styles.getCode}>{getCodeText}</CommonButton>
        </div>

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_input_new_password')}
            onChange={(v) => { this.setState({ password: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_confirm_password')}
            onChange={(v) => { this.setState({ confirmPassword: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <CommonButton onClick={this.reset} className={styles.registerButton}>{t('common.reset_password')}</CommonButton>

        <div className={styles.registerWrap}>
          <Link to="/login"><p className={styles.loginTips}>{t('common.login_tips')}</p></Link>
        </div>
      </WingBlank>
    );
  }
}

ResetPassword.propTypes = {
  t: PropTypes.func,
  resetPassword: PropTypes.func,
  openId: PropTypes.string,
  setWeChatAuthDirectUrl: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectOpenId(),
  (currentUser, openId) => ({
    currentUser,
    openId: openId || localStorage.getItem(localStorageKeys.WECHAT_OPEN_ID), // un fix issue
  }),
);

const mapDispatchToProps = (dispatch) => ({
  resetPassword: (data) => dispatch(resetPassword(data)),
  setWeChatAuthDirectUrl: (url) => dispatch(setWeChatAuthDirectUrl(url)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(ResetPassword));
