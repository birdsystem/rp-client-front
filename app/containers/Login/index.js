import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { InputItem, WhiteSpace, WingBlank, Icon } from 'antd-mobile';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import message from 'components/message';
import { Link } from 'react-router';
import Tips from 'components/Tips';
import { selectCurrentUser } from 'containers/App/selectors';
import { login, setLoginCallback } from 'containers/App/actions';
import WindowTitle from 'components/WindowTitle';
import CommonButton from 'components/CommonButton';
import WechatLogo from './wechat-logo.png';
import styles from './styles.css';
import wechatHelper from '../../utils/wechatHelper';

class Login extends Component {

  constructor(props) {
    super(props);
    const { location = {} } = this.props;
    const { wechatInfo = {} } = location.state || {};
    let hasWechatInfo = false;
    if (!_.isEmpty(wechatInfo)) {
      hasWechatInfo = true;
    }
    this.state = {
      countryCode: '44',
      name: '',
      password: '',
      wechatInfo,
      hasWechatInfo,
      mobile: '',
    };
    if (location && location.query && location.query.service) {
      this.props.setLoginCallback(decodeURIComponent(location.query.service));
    }
  }

  componentDidMount() {
    // record(actions.ENTER_LOGIN_PAGE, {}, eventCategory.PAGE_ENTER);
  }

  login = () => {
    if (!this.state.mobile || !this.state.countryCode || !this.state.password) {
      message.error(this.props.t('common.input_mobile_and_password'));
      return;
    }
    const data = {
      dial_code: this.state.countryCode,
      telephone: this.state.mobile,
      password: this.state.password,
    };
    // if (this.state.hasWechatInfo) {
    //   data = Object.assign(data, this.state.wechatInfo);
    // }
    // record(actions.LOGIN, {}, eventCategory.USER_EVENT);
    this.props.login(data);
  }

  countryCodeChange = (event) => {
    this.setState({ countryCode: event.target.value });
  };

  render() {
    const { t } = this.props;
    // const url = wechatHelper.genInWechatAuthUrl('login');
    // const isOpenInWechat = wechatHelper.isOpenInWechat();

    return (
      <WingBlank size="lg" className={styles.main}>
        <WindowTitle title={t('pageTitles.login')} />

        <WhiteSpace size="xl" />

        {/* { !this.state.hasWechatInfo && isOpenInWechat && (
          <div>
            <p className={styles.welcome}>{t('common.welcome')}</p>
            <a
              href={url}
              className={styles.wechatButton}
              type="primary"
              size="large"
            >
              <img
                src={WechatLogo}
                alt="wechat logo"
                className={styles.wechatLogo}
              />
              <span>{ t('common.wechat_login') }</span>
            </a>
            <p className={styles.or}><span>{t('common.or')}</span></p>
          </div>
        )} */}

        <div className={styles.label}>
          { t('common.mobile') }
        </div>
        <div className={styles.mobileWrap}>
          <select
            className={styles.countryCodeSelect}
            value={this.state.countryCode}
            onChange={this.countryCodeChange}
          >
            <option value="86">+86</option>
            <option value="44">+44</option>
          </select>
          <Icon type="down" size={'xs'} />
          <div className={styles.mobileInputWrap}>
            <InputItem
              type={'number'}
              className={styles.loginInput}
              clear
              onChange={(v) => { this.setState({ mobile: v }); }}
            ></InputItem>
          </div>
        </div>

        <div className={styles.label}>
          { t('common.password') }
        </div>
        <div className={styles.codeWrap}>
          <InputItem
            className={styles.loginInput}
            type={'password'}
            clear
            // placeholder={t('common.please_input_password')}
            onChange={(v) => { this.setState({ password: v }); }}
          ></InputItem>
        </div>
        {/* {
          this.state.hasWechatInfo &&
          <div className={styles.infoTips}>
            <Tips>{ t('common.login_with_wechat_info_tips') }</Tips>
          </div>
        } */}

        <CommonButton className={styles.loginButton} type="primary" onClick={() => this.login()} size="large">{t('common.login')}</CommonButton>

        <div className={styles.forgotPassword}>
          <Link to="/reset-password"><p className={styles.loginTips}>{t('common.forgot_password')}</p></Link>
        </div>

        <div className={styles.registerWrap}>
          <Link to="/register"><p className={styles.registerTips}>{t('common.register_tips')}</p></Link>
        </div>
      </WingBlank>
    );
  }
}

Login.propTypes = {
  t: PropTypes.func,
  login: PropTypes.func,
  location: PropTypes.object,
  setLoginCallback: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  (currentUser) => ({
    currentUser,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  login: (data) => dispatch(login(data)),
  setLoginCallback: (url) => dispatch(setLoginCallback(url)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(Login));
