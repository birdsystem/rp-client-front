/* eslint-disable no-unused-vars */
/*
 *
 * Notice
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Icon } from 'antd-mobile';
import {
  selectLocationState,
} from 'containers/App/selectors';
import wechatHelper from 'utils/wechatHelper';
import { translate } from 'react-i18next';
import styles from './styles.css';
import icon from './icon.png';
import notice from './notice.png';

const Notice = ({ t, locationState }) => {
  wechatHelper.configShareCommon();
  return (<div className={styles.wrap}>
    <img src={notice} className={styles.noticeImg} alt={'新春快乐'} />
    <span
      className={styles.close}
      onClick={() => {
        if (locationState.previousPathname !== '/') {
          browserHistory.goBack();
        } else {
          browserHistory.push('/');
        }
      }}
    >
      <Icon type="cross" />
    </span>
  </div>);
};

const mapStateToProps = createSelector(
  selectLocationState(),
  (locationState) => ({
    locationState,
  }),
);


Notice.propTypes = {
  t: PropTypes.func,
  locationState: PropTypes.object,
};

export default translate()(connect(mapStateToProps, null)(Notice));
