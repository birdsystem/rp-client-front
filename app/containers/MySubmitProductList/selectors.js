import { createSelector } from 'reselect';

const selectMySubmitProductListDomain = () => (state) => state.get('mysubmitproductlist');

const selectProductList = () => createSelector(
  selectMySubmitProductListDomain(),
  (substate) => substate.get('productList')
);

const selectProductListLoading = () => createSelector(
  selectMySubmitProductListDomain(),
  (substate) => substate.get('productListLoading')
);

const selectTotalCount = () => createSelector(
  selectMySubmitProductListDomain(),
  (substate) => substate.get('totalCount')
);

export {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
};
