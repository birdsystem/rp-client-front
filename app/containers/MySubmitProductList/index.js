/* eslint-disable react/no-unused-prop-types,react/no-will-update-set-state,no-unused-vars,react/no-did-mount-set-state */
import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { withRouter, browserHistory } from 'react-router';
import {
  getScrollTop,
  getScrollHeight,
  getWindowHeight,
} from 'utils/commonHelper';
import WindowTitle from 'components/WindowTitle';
import {
  selectCurrentUser,
  selectUserInfoLoading,
} from 'containers/App/selectors';
import {
  clearLatestAction,
} from 'containers/App/actions';
import Empty from 'components/Empty';
import ProductItem from 'components/ProductItem';
import wechatHelper from 'utils/wechatHelper';
import { getDisplayPrice } from 'utils/productHelper';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import { DEFAULT_PAGE_SIZE } from 'utils/constants';
import {
  selectProductList,
  selectProductListLoading,
} from './selectors';
import { loadProductList } from './actions';
import styles from './styles.css';

class MySubmitProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasLoad: false,
    };
  }

  componentDidMount() {
    this.configWechatJSSdk();
    record(actions.ENTER_MY_SUBMIT_PRODUCT_LIST_PAGE, {}, eventCategory.PAGE_ENTER);
    if (!this.state.hasLoad && !_.isEmpty(this.props.currentUser)) {
      this.props.loadProductList(this.props.currentUser.id);
      this.setState({
        hasLoad: true,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.hasLoad && !_.isEmpty(nextProps.currentUser)) {
      this.props.loadProductList(nextProps.currentUser.id);
      this.setState({
        hasLoad: true,
      });
    }
  }

  configWechatJSSdk() {
    wechatHelper.configShareCommon();
  }

  render() {
    const { t } = this.props;
    const productList = this.props.productList;

    let productElement = null;
    if (productList && productList.length > 0) {
      productElement = productList.map((productsItem, inx) => (
        <div className={styles.productItemWrap} key={`pw_${inx}`}>
          <h3 className={styles.productItemDate}>{productsItem.date}</h3>
          <span></span>
          {
            productsItem.items.map((product, mInx) => (
              <ProductItem product={product} key={`p_${mInx}`} />
            ))
          }
        </div>
        ));
    }

    return (
      <div className={styles.productWrap}>
        <WindowTitle title={t('pageTitles.my_submit_product')} />
        {
          productElement
        }
        {
          !this.props.productListLoading && !this.props.userInfoLoading && this.state.hasLoad && this.props.productList.length === 0
          &&
          <Empty title={t('empty.empty_submit_product')} />
        }
      </div>
    );
  }
}

MySubmitProductList.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  loadProductList: PropTypes.func,
  productList: PropTypes.array,
  productListLoading: PropTypes.bool,
  totalCount: PropTypes.number,
  recommendScrollTop: PropTypes.number,
  userInfoLoading: PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectProductList(),
  selectProductListLoading(),
  selectCurrentUser(),
  selectUserInfoLoading(),
  (productList, productListLoading, currentUser, userInfoLoading) => ({
    productList: productList && productList.toJS(),
    productListLoading,
    currentUser: currentUser && currentUser.toJS(),
    userInfoLoading,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadProductList: (id) => dispatch(loadProductList(id)),
});

export default translate()(
  withRouter(
    connect(mapStateToProps, mapDispatchToProps)(MySubmitProductList)));
