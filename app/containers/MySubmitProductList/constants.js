/*
 *
 * MySubmitProductList constants
 *
 */

export const LOAD_PRODUCT_LIST = 'MySubmitProductList/LOAD_PRODUCT_LIST';
export const LOAD_PRODUCT_LIST_SUCCESS = 'MySubmitProductList/LOAD_PRODUCT_LIST_SUCCESS';
export const LOAD_PRODUCT_LIST_ERROR = 'MySubmitProductList/LOAD_PRODUCT_LIST_ERROR';
