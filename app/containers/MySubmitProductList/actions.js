/*
 *
 * Global actions
 *
 */

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

export function loadProductList(id) {
  return {
    type: LOAD_PRODUCT_LIST,
    id,
  };
}

export function loadProductListSuccess(list) {
  return {
    type: LOAD_PRODUCT_LIST_SUCCESS,
    list,
  };
}

export function loadProductListError() {
  return {
    type: LOAD_PRODUCT_LIST_ERROR,
  };
}
