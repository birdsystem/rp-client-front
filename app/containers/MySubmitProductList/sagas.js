/* eslint-disable no-unused-vars,no-param-reassign */
import { call, put, race, take, select } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { DEFAULT_PAGE_SIZE } from 'utils/constants';
import { LOAD_PRODUCT_LIST } from './constants';
import {
  loadProductListSuccess,
} from './actions';


export default [
  getLoadProductListWatcher,
];

export function* getLoadProductListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadProductList: take(LOAD_PRODUCT_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;
    const id = watcher.loadProductList.id;
    const requestConfig = {
      urlParams: {
        limit: -1,
        start: 0,
        user_id: id,
      },
      feedback: { progress: { mask: false } },
    };
    const requestURL = '/product';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      const newList = [];
      let items = [];
      let lastDate = '';
      for (let i = 0; i < list.length; i += 1) {
        const item = list[i];
        item.create_date = item.create_time.substr(0, item.create_time.indexOf(' '));
        if (lastDate === item.create_date) {
          items.push(item);
        } else if (items.length === 0) {
          // new
          items.push(item);
          lastDate = item.create_date;
        } else {
          const data = {
            date: lastDate,
            items: items.slice(),
          };
          newList.push(data);
          items = [];
          items.push(item);
          lastDate = item.create_date;
        }
      }
      if (items.length > 0) {
        const data = {
          date: lastDate,
          items: items.slice(),
        };
        newList.push(data);
      }
      yield put(loadProductListSuccess(newList));
    }
  }
}
