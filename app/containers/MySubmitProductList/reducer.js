/*
 *
 * mySubmitProductList reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

const initialState = fromJS({
  productList: [],

  productListLoading: false,

  currentPage: 1,

  totalCount: 0,
});

function mySubmitProductListReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_PRODUCT_LIST:
      return state.set('productListLoading', true);
    case LOAD_PRODUCT_LIST_SUCCESS: {
      return state.set('productList', fromJS(action.list))
      .set('productListLoading', false);
    }
    case LOAD_PRODUCT_LIST_ERROR:
      return state.set('productListLoading', false);
    default:
      return state;
  }
}

export default mySubmitProductListReducer;
