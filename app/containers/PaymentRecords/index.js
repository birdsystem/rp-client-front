import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { browserHistory } from 'react-router';
import WindowTitle from 'components/WindowTitle';
import moment from 'moment';
import Money from 'components/Money';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { List, Icon } from 'antd-mobile';
import {
  selectPaymentRecords,
  selectLocationState,
} from 'containers/App/selectors';
import { loadPaymentRecords } from 'containers/App/actions';
import defaultIcon from './payment.png';
import cashbackIcon from './cash_back.png';
import refundIcon from './refund.png';
import topUpIcon from './top_up.png';
import unpaidShippingIcon from './unpaid_shipping.png';
import styles from './styles.css';

const Item = List.Item;
const Brief = Item.Brief;

class PaymentRecords extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadPaymentRecords();
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/' && this.props.locationState.previousPathname !== '/collect-coupon') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  render() {
    const { paymentRecords, t } = this.props;

    return (
      <div>
        <WindowTitle title={t('pageTitles.payment_records')} />
        <div
          onClick={this.goBack}
          className={styles.tabHeader}
        >
          <Icon type="left" size="md" className={styles.goBack} />
        </div>
        <List>
          {paymentRecords.map((item) => {
            let icon = defaultIcon;
            switch (item.type) {
              case 'CASH_BACK':
                icon = cashbackIcon;
                break;
              case 'TOP_UP':
                icon = topUpIcon;
                break;
              case 'CROSS_BORDER_FREIGHT':
                icon = unpaidShippingIcon;
                break;
              case 'REFUND':
              case 'ORDER_PAYMENT_REFUND':
                icon = refundIcon;
                break;
              default:
                icon = defaultIcon;
            }
            return (
              <Item
                key={item.id}
                extra={(
                  <Money withSign amount={item.amount} className={parseFloat(item.amount) > 0 ? styles.positiveAmount : styles.negativeAmount} />
                )}
                align="top"
                thumb={(
                  <img src={icon} />
                )}
                multipleLine
                onClick={() => {
                  if (item.record_id) {
                    browserHistory.push(`/orderdetail/${item.record_id}`);
                  }
                }}
              >
                {t(`payment.type_${item.type}`)}
                {item.product_name && `-${item.product_name}`}
                <Brief>
                  <p>{item.note && `${t('common.payment_note')}: ${item.note}`}</p>
                  {moment(item.update_time).format('YYYY-MM-DD HH:mm')}
                </Brief>
              </Item>
            );
          })}
        </List>
      </div>
    );
  }
}

PaymentRecords.propTypes = {
  paymentRecords: PropTypes.array,
  loadPaymentRecords: PropTypes.func,
  t: PropTypes.func,
  locationState: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectPaymentRecords(),
  selectLocationState(),
  (paymentRecords, locationState) => ({
    paymentRecords: paymentRecords.toJS(),
    locationState,
  }),
);

function mapDispatchToProps(dispatch) {
  return {
    loadPaymentRecords: () => dispatch(loadPaymentRecords()),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(PaymentRecords));
