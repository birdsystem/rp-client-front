import { createSelector } from 'reselect';

/**
 * Direct selector to the ProductBrowseHistory state domain
 */
const selectProductBrowseHistoryDomain = () => (state) => state.get('productBrowseHistory');

/**
 * Other specific selectors
 */
const selectRecords = () => createSelector(
  selectProductBrowseHistoryDomain(),
  (substate) => substate.get('records')
);

const selectLoading = () => createSelector(
  selectProductBrowseHistoryDomain(),
  (substate) => substate.get('loading')
);

export {
  selectProductBrowseHistoryDomain,
  selectRecords,
  selectLoading,
};
