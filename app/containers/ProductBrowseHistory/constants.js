export const LOAD_RECORDS = 'Account/ProductBrowseHistory/LOAD_RECORDS';
export const LOAD_RECORDS_SUCCESS = 'Account/ProductBrowseHistory/LOAD_RECORDS_SUCCESS';
export const LOAD_RECORDS_ERROR = 'Account/ProductBrowseHistory/LOAD_RECORDS_ERROR';
