import React from 'react';
import PropTypes from 'prop-types';
import WindowTitle from 'components/WindowTitle';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import moment from 'moment';
import { Icon } from 'antd-mobile';
import wechatHelper from 'utils/wechatHelper';
import { createSelector } from 'reselect';
import {
  selectLocationState,
} from 'containers/App/selectors';
import {
  selectRecords,
  selectLoading,
} from './selectors';
import { loadRecords } from './actions';
import styles from './styles.css';

class ProductBrowseHistory extends React.Component {
  componentDidMount() {
    this.props.loadRecords();
    wechatHelper.configShareCommon();
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/' && this.props.locationState.previousPathname !== '/collect-coupon') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  render() {
    const { t, records = [], loading } = this.props;

    return (
      <div className={styles.wrapper}>
        <WindowTitle title={t('pageTitles.product_browse_history')} />
        <div className={styles.stickyHeader}>
          <div onClick={this.goBack}>
            <Icon type="left" size="lg" />
          </div>
        </div>
        <div className={styles.recordsWrapper}>
          {records.map((date) => (
            <div className={styles.dateWrapper} key={date.date}>
              <div className={styles.date}>
                {moment(date.date).format('MM-DD')}
              </div>
              {date.products.map((product, index) => (
                <div className={`${styles.productWrapper} ${(index + 1) % 3 === 2 && styles.middleProduct}`} key={product.id} onClick={() => browserHistory.push(`/productdetail/${product.id}`)}>
                  <div className={styles.productImgWrapper}>
                    <img className={styles.productImg} alt="product" src={product.image_url} />
                  </div>
                  <div className={styles.productLabel}>
                    <MoneyInLocalCurrency cnyAmount={product.price} />
                  </div>
                </div>
              ))}
            </div>
          ))}
          {records.length < 1 && !loading && (
            <div className={styles.empty}>
              {t('common.empty_browse_history')}
            </div>
          )}
        </div>
      </div>
    );
  }
}

ProductBrowseHistory.propTypes = {
  loadRecords: PropTypes.func,
  records: PropTypes.array,
  t: PropTypes.func,
  loading: PropTypes.bool,
  locationState: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectRecords(),
  selectLoading(),
  selectLocationState(),
  (records, loading, locationState) => ({ records: records ? records.toJS() : [], loading, locationState })
);

function mapDispatchToProps(dispatch) {
  return {
    loadRecords: () => dispatch(loadRecords()),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(ProductBrowseHistory));
