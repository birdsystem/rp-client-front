import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  LOAD_RECORDS_SUCCESS,
  LOAD_RECORDS_ERROR,
  LOAD_RECORDS,
} from './constants';

const initialState = fromJS({
  records: fromJS([]),
  loading: false,
});

function defaultReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_RECORDS:
      return state
        .set('loading', true)
        .set('records', fromJS([]));
    case LOAD_RECORDS_SUCCESS:
      return state
        .set('records', fromJS(action.records))
        .set('loading', false);
    case LOAD_RECORDS_ERROR:
      return state
        .set('records', fromJS([]))
        .set('loading', false);
    case LOCATION_CHANGE:
      return initialState;
    default:
      return state;
  }
}

export default defaultReducer;
