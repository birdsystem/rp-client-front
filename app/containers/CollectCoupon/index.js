/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { Icon, WingBlank, InputItem, Modal } from 'antd-mobile';
import WindowTitle from 'components/WindowTitle';
import PrimaryButton from 'components/PrimaryButton';
import { collectACoupon, loadACoupon } from 'containers/App/actions';
import { selectCurrentUser, selectCurrentCoupon } from 'containers/App/selectors';
import wechatHelper from 'utils/wechatHelper';
import message from 'components/message';
import phoneNumberHelper from 'utils/phoneNumberHelper';
import RedPacket from 'components/RedPacket';
import styles from './styles.css';
import questionIcon from './question.png';

const alert = Modal.alert;
class CollectCoupon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialCode: '44',
      mobile: '',
      showInstruction: false,
    };
  }

  componentDidMount() {
    this.props.loadACoupon(true);
    wechatHelper.configCouponCollectShare();
  }

  onCloseInstructionModal = () => {
    this.setState({
      showInstruction: false,
    });
  }

  dialCodeChange = (event) => {
    this.setState({ dialCode: event.target.value });
  }

  collect = () => {
    const { mobile, dialCode } = this.state;
    const { coupon, t } = this.props;
    if (mobile) {
      const mobileValid = this.state.dialCode === '86' ? phoneNumberHelper.isCNMobileNumber(mobile) : phoneNumberHelper.isUKMobileNumber(mobile);
      if (mobileValid) {
        this.props.collectACoupon({
          fromSinglePage: true,
          coupon_id: coupon.id,
          dial_code: dialCode,
          telephone: mobile,
        });
      } else {
        message.error(t('error.invalid_mobile_number'));
      }
    } else {
      this.props.collectACoupon({
        fromSinglePage: true,
        coupon_id: coupon.id,
      });
    }
  }

  render() {
    const { t, coupon = {}, currentUser } = this.props;

    const buttonDisable = _.isEmpty(coupon) || (!currentUser ? (!this.state.dialCode || !this.state.mobile) : false);

    return (
      <div className={styles.collectWrapper}>
        <WindowTitle title={t('pageTitles.collect_coupon')} />

        <WingBlank>
          <RedPacket coupon={coupon} />
          <div
            onClick={() => {
              this.setState({
                showInstruction: true,
              });
            }}
            className={styles.questionWrapper}
          >
            <img src={questionIcon} className={styles.questionIcon} alt={'?'} />{t('common.coupon_instructions')}
          </div>

          {!currentUser && (
            <div>
              <div className={styles.label}>
                { t('common.input_phone_to_collect_coupon') }
              </div>
              <div className={styles.mobileWrap}>
                <select
                  className={styles.dialCodeSelect}
                  value={this.state.dialCode}
                  onChange={this.dialCodeChange}
                >
                  <option value="86">+86</option>
                  <option value="44">+44</option>
                </select>
                <Icon type="down" size={'xs'} />
                <div className={styles.mobileInputWrap}>
                  <InputItem
                    className={styles.registerInput}
                    clear
                    onChange={(v) => { this.setState({ mobile: v }); }}
                    onBlur={(v) => { }}
                  ></InputItem>
                </div>
              </div>
            </div>
          )}

          <PrimaryButton
            disabled={buttonDisable}
            onClick={this.collect}
            size="large"
          >{ t(
            'common.collect_coupon') }
          </PrimaryButton>

          <div className={styles.homeButton} onClick={() => browserHistory.push('/')}>{t('common.go_to_home_page')}</div>
        </WingBlank>
        <Modal
          visible={this.state.showInstruction}
          transparent
          maskClosable
          onClose={this.onCloseInstructionModal}
          title={t('common.coupon_instructions_title')}
        >
          <div className={styles.instructionWrapper}>{t('common.coupon_instructions_content')}</div>
        </Modal>
      </div>
    );
  }
}

CollectCoupon.propTypes = {
  t: PropTypes.func,
  collectACoupon: PropTypes.func,
  currentUser: PropTypes.object,
  loadACoupon: PropTypes.func,
  coupon: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectCurrentCoupon(),
  (currentUser, coupon) => ({
    currentUser: currentUser && currentUser.toJS(),
    coupon: coupon ? coupon.toJS() : {},
  }),
);

const mapDispatchToProps = (dispatch) => ({
  collectACoupon: (data) => dispatch(collectACoupon(data)),
  loadACoupon: (showLoading) => dispatch(loadACoupon(showLoading)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(CollectCoupon));
