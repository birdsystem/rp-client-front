/* eslint-disable no-mixed-operators,radix */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import _ from 'lodash';
import Tips from 'components/Tips';
import message from 'components/message';
import HorizontalLine from 'components/HorizontalLine';
import CommonButton from 'components/CommonButton';
import { browserHistory } from 'react-router';
import { Modal, DatePicker, List, InputItem, Picker, Icon } from 'antd-mobile';
import { selectPendingProduct, selectCurrentUser, selectPendingGroup, selectDefaultGroupConfig } from 'containers/App/selectors';
import { SINGLE_BUYING, GROUP_LEADER_BUYING, GROUP_BUYING, DEFAULT_GROUP_SIZE, DEFAULT_GROUP_LASTING_DAYS, localStorageKeys } from 'utils/constants';
import { isBackendYes } from 'utils/backendHelper';
import { setLatestAction, loadSystemConfig } from 'containers/App/actions';
import Money from 'components/Money';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import moment from 'moment';
import Notification from 'components/Notification';
import { record, actions, constantsValue, eventCategory } from 'utils/gaDIRecordHelper';
import questionIcon from './question.png';
import styles from './styles.css';
import wechatHelper from '../../utils/wechatHelper';

const nowTimeStamp = Date.now();
const now = new Date(nowTimeStamp + 86400000 * 2);
const maxDate = new Date(nowTimeStamp + DEFAULT_GROUP_LASTING_DAYS * 24 * 60 * 60 * 1000);
const minDate = new Date(nowTimeStamp);

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement; // eslint-disable-line
  }
  return null;
}

class PurchaseModal extends Component {
  constructor(props) {
    super(props);
    const { params, pendingProduct } = this.props;
    const id = params && params.productId;

    let maxQuantity = 2000; // default stock

    if (pendingProduct && parseInt(pendingProduct.quantity, 10) > 0) { // if there is a non-zero total stock providing, use it as the max quantity that a user can buy
      maxQuantity = parseInt(pendingProduct.quantity, 10);
    }

    const hasCheck = isBackendYes(pendingProduct.has_check);
    let limitForPurchasing = maxQuantity;
    if (!hasCheck) {
      // maxQuantity = parseInt(pendingProduct.limit_for_purchasing, 10);
      // limitForPurchasing = parseInt(pendingProduct.limit_for_purchasing, 10);
    }

    this.state = {
      id,
      quantity: 1,
      userCount: DEFAULT_GROUP_SIZE,
      selectedProp: {},
      selectedSku: {},
      endingDate: now,
      image: null,
      maxDate,
      minUserCount: DEFAULT_GROUP_SIZE,
      maxQuantity,
      showTosModal: false,
      hasCheck,
      limitForPurchasing,
      remark: '',
      initProps: false,
      hasRecordPropChange: false,
      inFollowGroupPage: window.location.pathname.toLocaleLowerCase().indexOf('followgroup') >= 0,
      deliveryPickerVisible: false,
      deliveryPickerValue: [],
    };
  }

  componentWillMount() {
    this.props.loadSystemConfig();
  }

  componentWillReceiveProps(nextProps) {
    const { defaultGroupConfig, pendingProduct } = nextProps;
    if (defaultGroupConfig.max_lasting_days !== this.props.defaultGroupConfig.max_lasting_days) {
      this.setState({
        maxDate: new Date(nowTimeStamp + parseInt(defaultGroupConfig.max_lasting_days, 10) * 24 * 60 * 60 * 1000),
      });
    }
    if (defaultGroupConfig.min_user_count !== this.props.defaultGroupConfig.min_user_count) {
      this.setState({
        userCount: parseInt(defaultGroupConfig.min_user_count, 10),
        minUserCount: parseInt(defaultGroupConfig.min_user_count, 10),
      });
    }
    if (!this.state.initProps && pendingProduct && pendingProduct.meta && pendingProduct.meta.props && pendingProduct.meta.props.length) {
      this.setState({ initProps: true });
      const propList = pendingProduct.meta.props;
      for (let i = 0; i < propList.length; i += 1) {
        const prop = propList[i];
        if (prop.values && prop.values.length === 1) {
          const value = prop.values[0];
          this.changeProp(prop, value);
        }
      }
    }
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  }

  getGroupBuyingShippingFee(userCount) {
    const { pendingProduct = {} } = this.props;
    const feeTable = pendingProduct.cross_border_shipping_table || [];
    let previousValue = 0;
    if (feeTable.length && (userCount > feeTable[feeTable.length - 1].min)) {
      return feeTable[feeTable.length - 1].cross_border_shipping;
    }
    for (let i = 0; i < feeTable.length; i += 1) {
      const currentValue = feeTable[i].min;
      if (userCount > previousValue && userCount <= currentValue) {
        return feeTable[i].cross_border_shipping;
      }
      previousValue = currentValue;
    }
    return null;
  }

  onDeliveryPickerChange = (v) => {
    console.log(v);
    this.setState({ deliveryPickerValue: v });
  }

  addQuantity() {
    const { t } = this.props;
    let { quantity } = this.state;
    const { hasCheck, limitForPurchasing, selectedSku } = this.state;
    const { maxQuantity } = this.state;
    if (quantity < maxQuantity) {
      quantity += 1;
      this.setState({
        quantity,
      });
    } else if (quantity === parseInt(selectedSku.quantity, 10)) {
      message.info(t('common.exceed_stock_quantity'));
    // } else if (!hasCheck && quantity === limitForPurchasing) {
    //   message.info(t('common.exceed_max_quantity_limit_for_uncheck_product', { count: limitForPurchasing }));
    }
  }

  reduceQuantity() {
    let { quantity } = this.state;
    quantity -= 1;
    if (quantity <= 1) {
      quantity = 1;
    }
    this.setState({
      quantity,
    });
  }

  addUserCount() {
    let { userCount } = this.state;
    userCount += 1;
    this.setState({
      userCount,
    });
  }

  reduceUserCount() {
    let { userCount } = this.state;
    const { minUserCount } = this.state;
    userCount -= 1;
    if (userCount <= minUserCount) {
      userCount = minUserCount;
    }
    this.setState({
      userCount,
    });
  }

  singleBuy = () => {
    record(
      actions.CLICK_PRODUCT_BUY_SINGLE,
      {},
      eventCategory.USER_ACTION,
    );
    record(
      `${this.props.pendingProduct.id}_${actions.CLICK_PRODUCT_BUY_SINGLE}`,
      {},
      eventCategory.USER_ACTION,
    );
    const data = {
      type: SINGLE_BUYING,
      productId: this.props.pendingProduct.id,
      count: this.state.quantity,
      minPeople: 1,
      skuId: this.state.selectedSku.id,
      remark: this.state.remark,
      delivery_service_id: this.state.deliveryPickerValue[0],
    };
    this.props.setLatestAction(data);
    localStorage.setItem(localStorageKeys.LATEST_ACTION, JSON.stringify(data));

    const product = this.props.pendingProduct;
    let category = '';
    if (product.category && product.category.length > 0) {
      const categoryItem = product.category[0];
      category = `${categoryItem.id}|${categoryItem.name}`;
    }
    let price = this.state.selectedSku.price_including_platform_delivery_fee || product.price_including_platform_delivery_fee;
    price += product.single_buying_cross_border_shipping;
    // record(actions.BEGIN_CHECKOUT, {
    //   items: [
    //     {
    //       id: product.id,
    //       name: product.name,
    //       list_name: '',
    //       brand: '',
    //       category,
    //       variant: (this.state.selectedSku && this.state.selectedSku.id) || '',
    //       list_position: -1,
    //       quantity: this.state.quantity,
    //       price,
    //     },
    //   ],
    //   coupon: '',
    //   event_callback: () => {
        if (this.props.currentUser && this.props.currentUser.id) {
          browserHistory.push({ pathname: '/receiveaddress', state: { product, price } });
        } else {
          this.autoLogin();
        }
    //   },
    // });
  }

  groupBuy = () => {
    const action = this.state.inFollowGroupPage ? actions.CLICK_FOLLOW_GROUP_PRODUCT_BUY_FOLLOW : actions.CLICK_PRODUCT_BUY_FOLLOW;
    record(action,
      {},
      eventCategory.USER_ACTION,
    );
    record(`${this.props.pendingProduct.id}_${action}`,
      {},
      eventCategory.USER_ACTION,
    );
    const data = {
      type: GROUP_BUYING,
      productId: this.props.pendingGroup.product_detail.id,
      groupId: this.props.pendingGroup.id,
      count: this.state.quantity,
      skuId: this.state.selectedSku.id,
      remark: this.state.remark,
    };
    this.props.setLatestAction(data);
    localStorage.setItem(localStorageKeys.LATEST_ACTION, JSON.stringify(data));

    const product = this.props.pendingProduct;
    let category = '';
    if (product.category && product.category.length > 0) {
      const categoryItem = product.category[0];
      category = `${categoryItem.id}|${categoryItem.name}`;
    }
    let price = this.state.selectedSku.price_including_platform_delivery_fee || product.price_including_platform_delivery_fee;
    price += this.props.pendingGroup.cross_border_shipping;
    // record(actions.BEGIN_CHECKOUT, {
    //   items: [
    //     {
    //       id: product.id,
    //       name: product.name,
    //       list_name: '',
    //       brand: '',
    //       category,
    //       variant: (this.state.selectedSku && this.state.selectedSku.id) || '',
    //       list_position: -1,
    //       quantity: this.state.quantity,
    //       price,
    //     },
    //   ],
    //   coupon: constantsValue.COUPON_FOLLOW_GROUP,
    //   event_callback: () => {
        if (this.props.currentUser && this.props.currentUser.id) {
          browserHistory.push({ pathname: '/receiveaddress', state: { product, price } });
        } else {
          this.autoLogin();
        }
    //   },
    // });
  }

  groupLeaderBuy = () => {
    record(actions.CLICK_PRODUCT_BUY_START,
      {},
      eventCategory.USER_ACTION,
    );
    record(
      `${this.props.pendingProduct.id}_${actions.CLICK_PRODUCT_BUY_START}`,
      {},
      eventCategory.USER_ACTION,
    );
    const data = {
      type: GROUP_LEADER_BUYING,
      productId: this.props.pendingProduct.id,
      count: this.state.quantity,
      minPeople: this.state.userCount,
      skuId: this.state.selectedSku.id,
      endingDate: moment(this.state.endingDate).format('YYYY-MM-DD'),
      remark: this.state.remark,
    };
    this.props.setLatestAction(data);
    localStorage.setItem(localStorageKeys.LATEST_ACTION, JSON.stringify(data));

    const product = this.props.pendingProduct;
    let category = '';
    if (product.category && product.category.length > 0) {
      const categoryItem = product.category[0];
      category = `${categoryItem.id}|${categoryItem.name}`;
    }
    let price = this.state.selectedSku.price_including_platform_delivery_fee || product.price_including_platform_delivery_fee;
    price += this.getGroupBuyingShippingFee(this.state.userCount);
    // record(actions.BEGIN_CHECKOUT, {
    //   items: [
    //     {
    //       id: product.id,
    //       name: product.name,
    //       list_name: '',
    //       brand: '',
    //       category,
    //       variant: (this.state.selectedSku && this.state.selectedSku.id) || '',
    //       list_position: -1,
    //       quantity: this.state.quantity,
    //       price,
    //     },
    //   ],
    //   coupon: constantsValue.COUPON_START_GROUP,
    //   event_callback: () => {
        if (this.props.currentUser && this.props.currentUser.id) {
          browserHistory.push({ pathname: '/receiveaddress', state: { product, price } });
        } else {
          this.autoLogin();
        }
    //   },
    // });
  }

  autoLogin = () => {
    // if (wechatHelper.isOpenInWechat()) {
    //   const url = wechatHelper.genInWechatAuthUrl('login');
    //   window.location = url;
    // } else {
      browserHistory.push('/login');
    // }
  }

  toggleTosModal = (showTosModal) => {
    if (showTosModal) {
      this.setState({ showTosModal: true });
    } else {
      this.setState({ showTosModal: false });
    }
  }

  changeProp(prop, value) {
    const { pid } = prop;
    const { vid, image } = value;
    this.setState((prevState, props) => {
      const changedState = {};
      const newSelectedProp = Object.assign({}, prevState.selectedProp);
      newSelectedProp[pid] = vid;

      const newSelectedPropPath = _.reduce(newSelectedProp, (path, v, p) => `${path}${p}:${v};`, '');
      const newSelectedSku = _.find(props.pendingProduct.meta.sku_info, (sku) => sku.prop_path === newSelectedPropPath.slice(0, newSelectedPropPath.length - 1)) || {};

      if (image) {
        changedState.image = image;
      }

      const quantity = prevState.quantity;

      if (!_.isEmpty(newSelectedSku) && parseInt(newSelectedSku.quantity, 10) > 0) {
        let newMaxQuantity = parseInt(newSelectedSku.quantity, 10);
        if (!prevState.hasCheck && prevState.limitForPurchasing < newMaxQuantity) {
          newMaxQuantity = prevState.limitForPurchasing;
        }
        changedState.quantity = quantity > newMaxQuantity ? newMaxQuantity : quantity;
        changedState.maxQuantity = newMaxQuantity;
      } else {
        changedState.maxQuantity = 0;
      }

      return {
        selectedProp: newSelectedProp,
        selectedSku: newSelectedSku,
        ...changedState,
      };
    });
  }

  render() {
    const { t, pendingProduct = {}, visible, hide, type, pendingGroup } = this.props;

    let submitButtonText;
    let submitMethod;

    switch (type) {
      case SINGLE_BUYING:
        submitButtonText = t('common.direct_order');
        submitMethod = this.singleBuy;
        break;
      case GROUP_BUYING:
        submitButtonText = t('common.join_group');
        submitMethod = this.groupBuy;
        break;
      case GROUP_LEADER_BUYING:
      default:
        submitButtonText = t('common.confirm_start_group');
        submitMethod = this.groupLeaderBuy;
    }

    const deliveryServices = _.isEmpty(pendingProduct.delivery_services) ? [
      // {
      //   value: 1,
      //   label: '空运',
      //   cross_border_shipping: '20',
      // }, {
      //   value: 2,
      //   label: '海运',
      //   cross_border_shipping: '10',
      // },
    ] : pendingProduct.delivery_services.map((s) => ({
      value: s.id,
      label: s.name,
      cross_border_shipping: s.cross_border_shipping,
    }));

    const originDataHasSkuInfo = pendingProduct && pendingProduct.meta && pendingProduct.meta.sku_info;
    const selectedPropsHasSku = !_.isEmpty(this.state.selectedSku);
    const isPropAllSelected = _.size(this.state.selectedProp) === ((pendingProduct.meta || {}).props || []).length;

    let submitDisabled = false;

    if (_.isEmpty(this.state.deliveryPickerValue)) {
      submitButtonText = t('common.message_choose_delivery_service');
      submitDisabled = true;
    }

    if (originDataHasSkuInfo) { // sku_info and props are supposed to always show up together
      if (!selectedPropsHasSku || selectedPropsHasSku && parseInt(this.state.selectedSku.quantity, 10) < 1) { // the selected props does not have a sku_info, or its sku quantity is 0, both means no stock
        submitButtonText = t('common.sku_no_stock');
        submitDisabled = true;
      }
      if (!isPropAllSelected) {
        submitButtonText = t('common.select_all_props');
        submitDisabled = true;
      }
    } // else if origin data lack of sku_info, can still buy

    let itemProps = null;
    if (pendingProduct && pendingProduct.meta && pendingProduct.meta.props) {
      itemProps = pendingProduct.meta.props.map((prop) => (
        <div key={prop.pid}>
          <div className={styles.propName}>
            {prop.name}
          </div>
          <div className={styles.tagContainer}>
            {prop.values && prop.values.length > 0 && prop.values.map((value) => (
              <div
                className={`${styles.tag} ${this.state.selectedProp[prop.pid] === value.vid ? styles.tagActive : styles.tagNormal}`}
                key={value.vid}
                onClick={() => {
                  this.setState({
                    hasRecordPropChange: true,
                  });
                  let action = actions.CLICK_PRODUCT_PROPS_START;
                  if (type === SINGLE_BUYING) {
                    action = actions.CLICK_PRODUCT_PROPS_SINGLE;
                  } else if (type === GROUP_BUYING) {
                    action = this.state.inFollowGroupPage ? actions.CLICK_FOLLOW_GROUP_PRODUCT_PROPS_FOLLOW : actions.CLICK_PRODUCT_PROPS_FOLLOW;
                  }
                  record(action,
                    {},
                    eventCategory.USER_ACTION,
                  );
                  record(
                    `${this.props.pendingProduct.id}_${action}`,
                    {},
                    eventCategory.USER_ACTION,
                  );
                  this.changeProp(prop, value);
                }}
              >
                {value.name}
              </div>
            ))}
          </div>
        </div>
      ));
    }

    const groupLeaderBuyingUnitShippingFee = this.getGroupBuyingShippingFee(this.state.userCount);

    let serviceCharge = 0;
    if (pendingProduct && pendingProduct.service_charge) {
      serviceCharge = pendingProduct.service_charge * this.state.quantity;
    }

    return (
      <Modal
        popup
        visible={visible}
        animationType="slide-up"
        onClose={hide}
        wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        className={styles.modalContainer}
      >
        <div className={styles.headerWrapper}>
          <div
            className={styles.itemImageDiv}
            style={{ backgroundImage: this.state.image ? `url('${this.state.image}')` : `url('${pendingProduct.image_url}')` }}
          >
          </div>
          <div className={styles.productContent}>
            <div className={styles.productPrice}>
              <MoneyInLocalCurrency cnyAmount={this.state.selectedSku.price_including_platform_delivery_fee || pendingProduct.price_including_platform_delivery_fee} />
              <span className={styles.cnyPrice}><Money amount={this.state.selectedSku.price_including_platform_delivery_fee || pendingProduct.price_including_platform_delivery_fee} /></span>
            </div>

            {/* {type === SINGLE_BUYING && (
              <div className={styles.shippingPrice}>
                {t('common.cross_border_shipping_total_per_item')}:&nbsp;
                <MoneyInLocalCurrency cnyAmount={pendingProduct.single_buying_cross_border_shipping} />
              </div>
            )} */}

            {type === GROUP_BUYING && (
              <div className={styles.shippingPrice}>
                {isBackendYes(pendingProduct.is_recommended) ? (
                  <div className={styles.recommendedProductFreeShipping}>
                    {t('common.recommended_product_free_shipping_on_group_buying')}
                  </div>
                ) : (
                  <div>{t('common.cross_border_shipping_total_per_item')}:&nbsp;
                  <MoneyInLocalCurrency cnyAmount={pendingGroup.cross_border_shipping} /></div>
                )}
              </div>
            )}

            {type === GROUP_LEADER_BUYING && (
              <div className={styles.leaderShippingPrice}>
                {isBackendYes(pendingProduct.is_recommended) ? (
                  <div className={styles.recommendedProductFreeShipping}>
                    {t('common.recommended_product_free_shipping_on_group_buying')}
                  </div>
                ) : (
                  <div>{t('common.cross_border_shipping_total_per_item')}:&nbsp;
                  <MoneyInLocalCurrency cnyAmount={groupLeaderBuyingUnitShippingFee} /></div>
                )}
              </div>
            )}

            {type === GROUP_BUYING && (
              <div className={styles.sponsor}>
                {t('common.sponsor')}: {pendingGroup.owner.username}
              </div>
            )}

            <div className={styles.selectCount}>
              {isPropAllSelected && t('common.stock', { stock: parseInt(this.state.selectedSku.quantity, 10) || this.state.maxQuantity })}
              {t('common.already_select', { quantity: this.state.quantity })}
            </div>
          </div>
        </div>

        <div className={styles.bodyWrapper}>
          <div className={styles.propsWrapper}>
            {itemProps}
          </div>

          {originDataHasSkuInfo && (
            <HorizontalLine />
          )}

          <div className={styles.quantityWrap}>
            <div className={styles.quantityLabel}>{t('common.quantity')}</div>
            <div className={styles.quantityControl}>
              <div className={styles.quantityReduce} onClick={() => this.reduceQuantity()}>-</div>
              <div className={styles.quantityCount}>{this.state.quantity}</div>
              <div className={styles.quantityAdd} onClick={() => this.addQuantity()}>+</div>
            </div>
          </div>
          <div className={styles.remarkWrap}>
            <div className={styles.remarkLabel}>
              {t('common.remark')}
            </div>
            <InputItem
              maxLength={150}
              className={styles.remarkInput}
              clear
              onChange={(v) => { this.setState({ remark: v }); }}
            ></InputItem>
          </div>

          <div className={styles.serviceChargeWrap}>
            <div className={styles.serviceChargeLabel}>{t('common.choose_delivery_service')}</div>
            <div className={styles.serviceCharge}>
              <Picker
                visible={this.state.deliveryPickerVisible}
                data={deliveryServices}
                cols={1}
                value={this.state.deliveryPickerValue}
                onChange={this.onDeliveryPickerChange}
                onOk={() => this.setState({ deliveryPickerVisible: false })}
                onDismiss={() => this.setState({ deliveryPickerVisible: false })}
              >
                <div
                  className={styles.countryWrap}
                >
                  <div
                    className={styles.actionItemRight}
                    onClick={() => {
                      this.setState({ deliveryPickerVisible: true });
                    }}
                  >
                    {_.isEmpty(this.state.deliveryPickerValue) ?
                      t('common.please_choose')
                    : ((_.find(deliveryServices, (s) => this.state.deliveryPickerValue[0] === s.value) || {}).label)
                    }
                    <Icon type="right" />
                  </div>
                </div>
              </Picker>
            </div>
          </div>

          {!_.isEmpty(this.state.deliveryPickerValue) && this.state.hasCheck && (
            <div className={styles.serviceChargeWrap}>
              <div className={styles.serviceChargeLabel}>{t('common.cross_border_shipping_total_per_item')}</div>
              <div className={styles.serviceCharge}>
                <MoneyInLocalCurrency cnyAmount={(_.find(deliveryServices, (s) => this.state.deliveryPickerValue[0] === s.value) || {}).cross_border_shipping} />
              </div>
            </div>
          )}

          {type === SINGLE_BUYING && (
            <div className={styles.serviceChargeWrap}>
              <div className={styles.serviceChargeLabel}>{t('common.service_charge')}</div>
              <div className={styles.serviceCharge}>
                <MoneyInLocalCurrency cnyAmount={serviceCharge} />
                {/* <Money amount={pendingProduct.service_charge} /> */}
              </div>
            </div>
          )}

          {type === GROUP_LEADER_BUYING && (
            <div className={styles.leaderWrapper}>
              <HorizontalLine />
              <div className={styles.quantityWrap}>
                <div className={styles.quantityLabel}>{ t('common.group_people') }</div>
                <div className={styles.quantityControl}>
                  <div
                    className={styles.quantityReduce}
                    onClick={() => this.reduceUserCount()}
                  >-
                  </div>
                  <div
                    className={styles.quantityCount}
                  >{ this.state.userCount }</div>
                  <div
                    className={styles.quantityAdd}
                    onClick={() => this.addUserCount()}
                  >+
                  </div>
                </div>
              </div>
              <div className={styles.datePicker}>
                <DatePicker
                  format="YYYY-MM-DD"
                  mode="date"
                  title={t('common.select_ending_date')}
                  extra="Optional"
                  minDate={minDate}
                  maxDate={this.state.maxDate}
                  value={this.state.endingDate}
                  onChange={(endingDate) => this.setState({ endingDate })}
                >
                  <List.Item arrow="horizontal">{t('common.ending_date')}</List.Item>
                </DatePicker>
              </div>
              <div className={styles.serviceChargeWrap}>
                <div className={styles.serviceChargeLabel}>{t('common.group_buying_discount_cross_border_shipping')}</div>
                <div className={styles.serviceCharge}>
                  {isBackendYes(pendingProduct.is_recommended) ? (
                    <MoneyInLocalCurrency cnyAmount={0} />
                  ) : (
                    <MoneyInLocalCurrency cnyAmount={groupLeaderBuyingUnitShippingFee} />
                  )}
                </div>
              </div>
            </div>

          )}

          {type === GROUP_BUYING && (
            <div className={styles.toKnowWrap}>
              <Notification type={2} />
            </div>
          )}

          {type === GROUP_LEADER_BUYING && (
            <div className={styles.toKnowWrap}>
              <Notification type={1} />
            </div>
          )}

          {/* <HorizontalLine /> */}

          {/* <div className={styles.notice}><Tips fontSize="12px">{t('common.special_purchase_notice')}</Tips></div>

          <div className={styles.tosWrapper} onClick={() => this.toggleTosModal(true)}>
            <div>{t('common.agree_to_tos')}</div>
            <img alt="info" src={questionIcon} className={styles.questionIcon} />
          </div> */}
        </div>

        <div className={styles.buttonWrapper}>
          <CommonButton className={styles.modalButton} type={'dark'} disabled={submitDisabled} onClick={submitMethod}>
            {submitButtonText}
          </CommonButton>
        </div>

        <Modal
          visible={this.state.showTosModal}
          transparent
          maskClosable={false}
          onClose={() => this.toggleTosModal(false)}
          title={t('common.terms_of_service')}
          footer={[{ text: t('common.ok'), onPress: () => this.toggleTosModal(false) }]}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
          wrapClassName={styles.tosModalWrapper}
        >
          <div className={styles.tosModal}>
            {t('termsOfService.content')}
          </div>
        </Modal>
      </Modal>
    );
  }
}

PurchaseModal.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  params: PropTypes.object,
  pendingProduct: PropTypes.object,
  pendingGroup: PropTypes.object,
  setLatestAction: PropTypes.func,
  visible: PropTypes.bool,
  hide: PropTypes.func,
  type: PropTypes.string,
  defaultGroupConfig: PropTypes.object,
  loadSystemConfig: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectPendingProduct(),
  selectCurrentUser(),
  selectPendingGroup(),
  selectDefaultGroupConfig(),
  (pendingProduct, currentUser, pendingGroup, defaultGroupConfig) => ({
    pendingProduct: pendingProduct ? pendingProduct.toJS() : {},
    currentUser: currentUser && currentUser.toJS(),
    pendingGroup: pendingGroup ? pendingGroup.toJS() : {},
    defaultGroupConfig: defaultGroupConfig ? defaultGroupConfig.toJS() : {},
  }),
);

const mapDispatchToProps = (dispatch) => ({
  setLatestAction: (data) => dispatch(setLatestAction(data)),
  loadSystemConfig: () => dispatch(loadSystemConfig()),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(PurchaseModal));
