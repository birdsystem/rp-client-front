/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { InputItem, Icon, Button, WingBlank } from 'antd-mobile';
import message from 'components/message';
import WindowTitle from 'components/WindowTitle';
import CommonButton from 'components/CommonButton';
import { selectCurrentUser, selectOpenId } from 'containers/App/selectors';
import { changePassword, setWeChatAuthDirectUrl } from 'containers/App/actions';
import wechatHelper from 'utils/wechatHelper';
import {
  localStorageKeys,
} from 'utils/constants';
import styles from './styles.css';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmNewPassword: '',
    };
  }

  componentDidMount() {
    // if (wechatHelper.isOpenInWechat() && !this.props.openId) {
    //   this.props.setWeChatAuthDirectUrl('/register');
    //   const url = wechatHelper.genInWechatAuthUrl();
    //   window.location = url;
    // }
  }

  submit = () => {
    if (this.state.newPassword !== this.state.confirmNewPassword) {
      message.error(this.props.t('common.password_must_be_the_same'));
      return;
    }
    const data = {
      new_password: this.state.newPassword,
      old_password: this.state.oldPassword,
    };
    this.props.changePassword(data);
  }

  render() {
    const { t } = this.props;

    return (
      <WingBlank size="lg" className={styles.main}>
        <WindowTitle title={t('pageTitles.change_password')} />

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_input_old_password')}
            onChange={(v) => { this.setState({ oldPassword: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_input_new_password')}
            onChange={(v) => { this.setState({ newPassword: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_confirm_password')}
            onChange={(v) => { this.setState({ confirmNewPassword: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <CommonButton onClick={this.submit} size="large" className={styles.registerButton}>{t('common.change_password')}</CommonButton>

      </WingBlank>
    );
  }
}

ChangePassword.propTypes = {
  t: PropTypes.func,
  changePassword: PropTypes.func,
  openId: PropTypes.string,
  setWeChatAuthDirectUrl: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectOpenId(),
  (currentUser, openId) => ({
    currentUser,
    openId: openId || localStorage.getItem(localStorageKeys.WECHAT_OPEN_ID), // un fix issue
  }),
);

const mapDispatchToProps = (dispatch) => ({
  changePassword: (data) => dispatch(changePassword(data)),
  setWeChatAuthDirectUrl: (url) => dispatch(setWeChatAuthDirectUrl(url)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(ChangePassword));
