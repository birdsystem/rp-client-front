import { call, put, race, take } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_ORDER_DETAIL } from './constants';
import {
  loadOrderDetailSuccess,
  loadOrderDetailError,
} from './actions';

export default [
  getLoadOrderDetailWatcher,
];

export function* getLoadOrderDetailWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadOrderDetail: take(LOAD_ORDER_DETAIL),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = `/group/order/${watcher.loadOrderDetail.id}`;

    const requestConfig = {
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      yield put(loadOrderDetailSuccess(result.data));
    } else {
      yield put(loadOrderDetailError());
    }
  }
}

