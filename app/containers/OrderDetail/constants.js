/*
 *
 * OrderDetail constants
 *
 */

export const LOAD_ORDER_DETAIL = 'OrderDetail/LOAD_GROUP_DETAIL';
export const LOAD_ORDER_DETAIL_SUCCESS = 'OrderDetail/LOAD_GROUP_DETAIL_SUCCESS';
export const LOAD_ORDER_DETAIL_ERROR = 'OrderDetail/LOAD_GROUP_DETAIL_ERROR';

