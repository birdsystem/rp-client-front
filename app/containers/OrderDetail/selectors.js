import { createSelector } from 'reselect';

const selectOrderDetailDomain = () => (state) => state.get('orderdetail');

const selectOrderDetail = () => createSelector(
  selectOrderDetailDomain(),
  (substate) => substate.get('orderDetail')
);

const selectOrderDetailLoading = () => createSelector(
  selectOrderDetailDomain(),
  (substate) => substate.get('orderDetailLoading')
);

export {
  selectOrderDetail,
  selectOrderDetailLoading,
};
