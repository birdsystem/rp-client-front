/* eslint-disable no-unused-vars,react/no-will-update-set-state,radix,react/sort-comp,no-empty,react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import Money from 'components/Money';
import WeChatShare from 'components/WeChatShare';
import WindowTitle from 'components/WindowTitle';
import { browserHistory, Link } from 'react-router';
import message from 'components/message';
import {
  Icon,
  Steps,
  WhiteSpace,
} from 'antd-mobile';
import { getTimeLeftDisplay } from 'utils/commonHelper';
import GraySpace from 'components/GraySpace';
import AddressDisplay from 'components/AddressDisplay';
import wechatHelper from 'utils/wechatHelper';
import {
  getDisplayPrice,
  getStartGroupPrice,
  getGroupPrice,
  getSingleBuyPrice,
  getDisplayPriceFromProps,
  getStartGroupPriceFromProps,
  getSingleBuyPriceFromProps,
} from 'utils/productHelper';
import CopyToClipboard from 'react-copy-to-clipboard';
import _ from 'lodash';
import {
  SINGLE_BUYING,
  GROUP_BUYING,
  GROUP_LEADER_BUYING,
  GROUP_STATUS_ACTIVE,
  GROUP_STATUS_SUCCESS,
  GROUP_STATUS_FAILED,
  MY_ORDER_TYPE_MY_START,
  MY_ORDER_TYPE_MY_FOLLOW,
  MY_ORDER_TYPE_PROCESS,
  MY_ORDER_TYPE_FINISH,
  ORDER_STATUS_PAYMENT_PENDING,
  ORDER_STATUS_SUPPLEMENTARY_PAYMENT_PENDING,
  ORDER_STATUS_GROUP_ACTIVE,
  ORDER_STATUS_GROUP_SUCCESS,
  ORDER_STATUS_GROUP_FAILED,
  ORDER_STATUS_CANCELLED,
  ORDER_STATUS_REFUNDED,
  ORDER_STATUS_WAIT_WAREHOUSE_PURCHASE,
  ORDER_STATUS_WAREHOUSE_PURCHASED,
  ORDER_STATUS_WAREHOUSE_RECEIVED,
  ORDER_STATUS_ORDER_SHIPPED,
  ORDER_STATUS_USER_RECEIVED,
  ORDER_STATUS_PROBLEM,
} from 'utils/constants';
import {
  selectCurrentUser,
  selectLocationState,
  selectCurrency,
  selectPurchaseModalVisible,
  selectPurchaseModalType,
} from 'containers/App/selectors';
import {
  showPurchaseModal,
  hidePurchaseModal,
  loadGroupDetailSuccess,
  loadProductDetailSuccess,
  loadProductDetail,
  loadGroupDetail,
} from 'containers/App/actions';
import orderHelper from 'utils/orderHelper';
import PurchaseModal from '../PurchaseModal';
import { loadOrderDetail } from './actions';
import { selectOrderDetail, selectOrderDetailLoading } from './selectors';
import styles from './styles.css';

const Step = Steps.Step;

class OrderDetail extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props;
    const id = params.orderId;
    this.state = {
      id,
      hasConfigWeChatShare: false,
      showWeChatShareTips: false,
    };
    this.props.loadOrderDetail(id);
  }

  componentDidMount() {
    wechatHelper.configShareCommon();
  }

  componentWillUpdate(nextProps, nextState) {
    if (!_.isEmpty(nextProps.orderDetail) && nextState.hasConfigWeChatShare === false) {
      let canFollowGroup = nextProps.orderDetail.status === ORDER_STATUS_GROUP_ACTIVE;
      if (nextProps.orderDetail.type === SINGLE_BUYING) {
        canFollowGroup = false;
      }

      this.props.loadProductDetail(nextProps.orderDetail.product_info.id);
      this.props.loadGroupDetail(nextProps.orderDetail.group_id);

      const productDetail = nextProps.orderDetail.product_info || {};
      if (canFollowGroup) {
        wechatHelper.configFollowGroupShare(productDetail.name, nextProps.orderDetail.product_price, productDetail.id, nextProps.orderDetail.group_id, productDetail.image_url);
      } else {
        wechatHelper.configProductShare(productDetail.name, nextProps.orderDetail.product_price, productDetail.id, productDetail.image_url);
      }
      this.setState({
        hasConfigWeChatShare: true,
      });
    }
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  getStatusDisplay(item) {
    const ret = this.props.t(`order.status_${item.status}`);
    return ret;
  }

  getStepFromStatus(order) {
    if (order && order.status) {
      switch (order.status) {
        case ORDER_STATUS_PAYMENT_PENDING:
        case ORDER_STATUS_SUPPLEMENTARY_PAYMENT_PENDING:
        case ORDER_STATUS_CANCELLED:
        case ORDER_STATUS_GROUP_FAILED:
        case ORDER_STATUS_PROBLEM:
        case ORDER_STATUS_REFUNDED:
          return -1;
        case ORDER_STATUS_GROUP_ACTIVE:
          return 0;
        case ORDER_STATUS_ORDER_SHIPPED:
          return 2;
        case ORDER_STATUS_USER_RECEIVED:
          return 3;
        default:
          return 1;
      }
    }
    return null;
  }

  getDisplayImageFromGroupDetail(groupDetail) {
    const ret = groupDetail.product_detail.image_url;
    if (groupDetail.product_detail.meta &&
      groupDetail.product_detail.meta.pic_url &&
      groupDetail.buy_info) {

    }
    return ret;
  }

  render() {
    const { orderDetail, orderDetailLoading, t, purchaseModalVisible, purchaseModalType } = this.props;
    const shareUrl = `${window.location.protocol}//${window.location.host}`;
    const copySuccess = wechatHelper.isOpenInWechat() ? t(
      'common.copy_success_wechat') : t('common.copy_success');
    let isSpecialOrder = false;
    if (!_.isEmpty(orderDetail) &&
      (orderDetail.status === ORDER_STATUS_GROUP_FAILED
        || orderDetail.status === ORDER_STATUS_CANCELLED
        || orderDetail.status === ORDER_STATUS_REFUNDED
        || orderDetail.status === ORDER_STATUS_PROBLEM
      )) {
      isSpecialOrder = true;
    }

    const productStyle = [];
    if (!_.isEmpty(orderDetail) && orderDetail.product_props) {
      for (let i = 0; i < orderDetail.product_props.length; i += 1) {
        const props = orderDetail.product_props[i];
        const propsArray = props.split(':');
        if (propsArray.length === 2) {
          productStyle.push(propsArray[1]);
        }
      }
    }

    const currentStep = this.getStepFromStatus(orderDetail);

    let displayId = '';
    if (!_.isEmpty(orderDetail)) {
      displayId = orderDetail.id;
      if (displayId.indexOf('_') > 0) {
        displayId = orderDetail.order_number;
      }
    }

    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.order_detail')} />

        {(orderDetailLoading || !orderDetail) ? (
          <div className={styles.itemWrap}>
            <div className={styles.stickyHeader}>
              <div onClick={this.goBack}>
                <Icon type="left" size="lg" />
              </div>
            </div>
          </div>
        ) : (
          <div className={styles.wrapInner}>
            <div className={styles.itemWrap}>
              <div className={styles.stickyHeader}>
                <div onClick={this.goBack}>
                  <Icon type="left" size="lg" />
                </div>
                <p className={styles.orderNumber}>
                  { `${t('common.order_id')}: ${displayId}` }
                </p>
                <div className={styles.status}>
                  { this.getStatusDisplay(orderDetail) }
                </div>
              </div>
            </div>
            <div className={styles.stepWrap}>
              {currentStep > -1 ? (
                <Steps
                  current={currentStep}
                  direction="horizontal"
                  size="small"
                >
                  <Step title={t('order.step_group_active')} />
                  <Step title={t('order.step_preparing')} />
                  <Step title={t('order.step_in_transit')} />
                  <Step title={t('order.step_finished')} />
                </Steps>
              ) : (
                <div className={styles.detailedStatus}>{t(`order.status_${orderDetail.status}_description`)}</div>
              )}
            </div>

            <GraySpace />
            {!_.isEmpty(orderDetail.address_info) && (
              <AddressDisplay address={orderDetail.address_info} />
            )}
            <GraySpace />

            <div className={styles.itemTitle}>
              { t('pageTitles.product_detail') }
            </div>
            <div className={styles.productDetail} onClick={() => browserHistory.push(`/productdetail/${orderDetail.product_info.id}`)}>
              <div className={styles.productImageWrap}>
                <img
                  alt={'product'}
                  src={orderDetail.product_info.image_url}
                  className={styles.productImage}
                />
              </div>
              <div className={styles.productInfo}>
                <p
                  className={styles.productName}
                >{ orderDetail.product_info.name }</p>
                <div className={styles.tagPriceWrap}>
                  <div className={styles.styleWrap}>
                    {
                      productStyle.map((style, inx) => (
                        <span
                          key={`${inx}`}
                          className={styles.badge}
                        >
                          {style}
                        </span>
                      ))
                    }
                  </div>
                  <div className={styles.priceWrap}>
                    <Money
                      amount={orderDetail.product_price}
                      currencyCode={this.props.currency}
                    />
                    <span
                      className={styles.productQuantity}
                    >&times;{ orderDetail.quantity }</span>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.paymentWrap}>
              <div className={styles.paymentRow}>
                <div className={styles.paymentlabel}>
                  { t('payment.item_product_subtotal') }
                </div>
                <div className={styles.paymentItem}>
                  <Money
                    amount={orderDetail.product_subtotal}
                  />
                </div>
              </div>
              <div className={styles.paymentRow}>
                <div className={styles.paymentlabel}>
                  { t('payment.item_cross_border_shipping_subtotal') }
                </div>
                <div className={styles.paymentItem}>
                  <Money
                    amount={orderDetail.cross_border_shipping_subtotal}
                  />
                </div>
              </div>
              {parseFloat(orderDetail.service_charge) > 0 && (
                <div className={styles.paymentRow}>
                  <div className={styles.paymentlabel}>
                    { t('payment.service_charge') }
                  </div>
                  <div className={styles.paymentItem}>
                    <Money amount={orderDetail.service_charge} />
                  </div>
                </div>
              )}
              {parseFloat(orderDetail.discount) > 0 && (
                <div className={styles.paymentRow}>
                  <div className={styles.paymentlabel}>
                    { t('payment.discount') }
                  </div>
                  <div className={styles.paymentItem}>
                    - <Money amount={orderDetail.discount} />
                  </div>
                </div>
              )}
              <div className={styles.paymentRow}>
                <div className={styles.paymentLabelTotal}>
                  { t('payment.total') }
                </div>
                <div className={styles.paymentItem}>
                  <Money
                    className={styles.totalPrice}
                    amount={orderDetail.total}
                    currencyCode={this.props.currency}
                  />
                </div>
              </div>
            </div>
            <WhiteSpace />

            <GraySpace />
            <div className={styles.itemTitle}>
              { t('common.order_info') }
            </div>
            <div className={styles.addressWrap}>
              <div className={styles.addressItem}>
                <div className={styles.addressLabel}>
                  { t('common.order_id') }
                </div>
                <div className={styles.addressContent}>
                  { displayId }
                </div>
              </div>
              { orderDetail.remark &&
              <div className={styles.addressItem}>
                <div className={styles.addressLabel}>
                  { t('common.remark') }
                </div>
                <div className={styles.addressContent}>
                  { orderDetail.remark }
                </div>
              </div>
              }
              <div className={styles.addressItem}>
                <div className={styles.addressLabel}>
                  { t('common.delivery_service') }
                </div>
                <div className={styles.addressContent}>
                  { orderDetail.delivery_service && orderDetail.delivery_service.name }
                </div>
              </div>
              <div className={styles.addressItem}>
                <div className={styles.addressLabel}>
                  { t('common.created_at') }
                </div>
                <div className={styles.addressContent}>
                  { orderDetail.created_at }
                </div>
              </div>
              {orderDetail.paid_at && (
                <div className={styles.addressItem}>
                  <div className={styles.addressLabel}>
                    { t('common.paid_at') }
                  </div>
                  <div className={styles.addressContent}>
                    { orderDetail.paid_at }
                  </div>
                </div>
              )}
              {orderDetail.shipped_at && (
                <div className={styles.addressItem}>
                  <div className={styles.addressLabel}>
                    { t('common.shipped_at') }
                  </div>
                  <div className={styles.addressContent}>
                    { orderDetail.shipped_at }
                  </div>
                </div>
              )}
              {orderDetail.refunded_at && (
                <div className={styles.addressItem}>
                  <div className={styles.addressLabel}>
                    { t('common.refunded_at') }
                  </div>
                  <div className={styles.addressContent}>
                    { orderDetail.refunded_at }
                  </div>
                </div>
              )}
            </div>

            <div className={styles.actionWrap}>
              {/* <Link
                target="_blank"
                to={'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515'}
                className={styles.askWrap}
              >
                <div className={styles.ask_customer}>
                  <p>{ t('common.ask') }</p>
                  <p>{ t('common.custome_service') }</p>
                </div>
              </Link> */}
              {orderHelper.canTrack(orderDetail) && (
                <Link
                  to={`/tracking/${orderDetail.id}`}
                  className={styles.askWrap}
                >
                  <div className={styles.ask_customer}>
                    <p>{ t('common.trace_order_line1') }</p>
                    <p>{ t('common.trace_order_line2') }</p>
                  </div>
                </Link>
              )}
              { (orderDetail &&
              orderDetail.status === ORDER_STATUS_PAYMENT_PENDING) ?
                <div
                  onClick={() => {
                    browserHistory.push(
                    `/payment/${orderDetail.id}`);
                  }}
                  className={styles.payWrap}
                >
                  <p>{ t('common.at_once') }</p>
                  <p>{ t('common.pay') }</p>
                </div>
                :
                (orderDetail.status !== ORDER_STATUS_REFUNDED && <div className={styles.shareButton}>
                  <CopyToClipboard
                    text={shareUrl}
                    onCopy={() => {
                      if (wechatHelper.isOpenInWechat()) {
                        this.setState({
                          showWeChatShareTips: true,
                        });
                      } else {
                        message.success(copySuccess);
                      }
                    }}
                  >
                    <span>{ t('common.share') }</span>
                  </CopyToClipboard>
                </div>)
              }

              {
                orderDetail.status === ORDER_STATUS_REFUNDED &&
                <div
                  onClick={() => {
                    this.props.showPurchaseModal(SINGLE_BUYING);
                  }}
                  className={styles.payWrap}
                >
                  <p>{ t('common.buy_single') }</p>
                </div>
              }

            </div>
          </div>
        )}

        {this.state.showWeChatShareTips && (
          <div
            onClick={
              () => {
                this.setState({
                  showWeChatShareTips: false,
                });
              }
            }
          >
            <WeChatShare />
          </div>
        )}
        <PurchaseModal
          visible={purchaseModalVisible}
          type={purchaseModalType}
          hide={this.props.hidePurchaseModal}
        />
      </div>
    );
  }
}

OrderDetail.propTypes = {
  t: PropTypes.func,
  params: PropTypes.object,
  loadOrderDetail: PropTypes.func,
  orderDetailLoading: PropTypes.bool,
  orderDetail: PropTypes.object,
  currentUser: PropTypes.object,
  currency: PropTypes.string,
  locationState: PropTypes.object,
  showPurchaseModal: PropTypes.func,
  hidePurchaseModal: PropTypes.func,
  purchaseModalVisible: PropTypes.bool,
  purchaseModalType: PropTypes.string,
  loadGroupDetailSuccess: PropTypes.func,
  loadProductDetailSuccess: PropTypes.func,
  loadProductDetail: PropTypes.func,
  loadGroupDetail: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectOrderDetailLoading(),
  selectOrderDetail(),
  selectCurrentUser(),
  selectLocationState(),
  selectCurrency(),
  selectPurchaseModalType(),
  selectPurchaseModalVisible(),
  (orderDetailLoading, orderDetail, currentUser, locationState, currency, purchaseModalType, purchaseModalVisible) => ({
    orderDetailLoading,
    orderDetail: (orderDetail && orderDetail.toJS()) || null,
    currentUser: currentUser && currentUser.toJS(),
    locationState,
    currency,
    purchaseModalType,
    purchaseModalVisible,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  showPurchaseModal: (type) => dispatch(showPurchaseModal(type)),
  hidePurchaseModal: () => dispatch(hidePurchaseModal()),
  loadOrderDetail: (id) => dispatch(loadOrderDetail(id)),
  loadGroupDetailSuccess: (data) => dispatch(loadGroupDetailSuccess(data)),
  loadProductDetailSuccess: (data) => dispatch(loadProductDetailSuccess(data)),
  loadProductDetail: (id) => dispatch(loadProductDetail({
    id,
    hideLoading: true,
  })),
  loadGroupDetail: (id) => dispatch(loadGroupDetail(id, true)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(OrderDetail));
