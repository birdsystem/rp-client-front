/*
 *
 * Group detail actions
 *
 */

import {
  LOAD_ORDER_DETAIL,
  LOAD_ORDER_DETAIL_SUCCESS,
  LOAD_ORDER_DETAIL_ERROR,
} from './constants';

export function loadOrderDetail(id) {
  return {
    type: LOAD_ORDER_DETAIL,
    id,
  };
}

export function loadOrderDetailSuccess(data) {
  return {
    type: LOAD_ORDER_DETAIL_SUCCESS,
    data,
  };
}

export function loadOrderDetailError() {
  return {
    type: LOAD_ORDER_DETAIL_ERROR,
  };
}
