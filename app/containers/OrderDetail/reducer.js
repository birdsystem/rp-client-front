/*
 *
 * Group Detail reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_ORDER_DETAIL,
  LOAD_ORDER_DETAIL_SUCCESS,
  LOAD_ORDER_DETAIL_ERROR,
} from './constants';

const initialState = fromJS({
  orderDetail: null,
  orderDetailLoading: false,
});

function orderDetailReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_ORDER_DETAIL:
      return state
        .set('orderDetailLoading', true)
      .set('orderDetail', null);
    case LOAD_ORDER_DETAIL_SUCCESS:
      return state
        .set('orderDetailLoading', false)
      .set('orderDetail', fromJS(action.data));
    case LOAD_ORDER_DETAIL_ERROR:
      return state
      .set('orderDetailLoading', false);
    default:
      return state;
  }
}

export default orderDetailReducer;
