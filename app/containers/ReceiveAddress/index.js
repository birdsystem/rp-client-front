/* eslint-disable react/no-did-mount-set-state,camelcase,no-unused-vars,no-plusplus */
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory } from 'react-router';
import CommonButton from 'components/CommonButton';
import wechatHelper from 'utils/wechatHelper';
import WindowTitle from 'components/WindowTitle';
import {
  record,
  eventCategory,
  actions,
  constantsValue,
} from 'utils/gaDIRecordHelper';
import message from 'components/message';
import { InputItem, Button, TextareaItem, Picker, Icon, Modal } from 'antd-mobile';
import CountryName from 'components/CountryName';
import AddressComponent from 'components/Address';

import {
  SINGLE_BUYING,
  GROUP_BUYING,
  GROUP_LEADER_BUYING,
  DEFAULT_COUNTRY_ISO,
} from 'utils/constants';
import {
  selectLatestAction,
  selectCurrentUser,
  selectAddress,
  selectAddressList,
} from 'containers/App/selectors';
import {
  updateReceiveAddress,
  startGroup,
  followGroup,
  changeCountry,
  addAddress,
  updateAddress,
} from 'containers/App/actions';
import styles from './styles.css';

const supportedCountries = [{
  label: (<div>
    <CountryName flag iso="GB" />
  </div>),
  value: 'GB',
// }, {
//   label: (<div>
//     <CountryName flag iso="DE" />
//   </div>),
//   value: 'DE',
// }, {
//   label: (<div>
//     <CountryName flag iso="FR" />
//   </div>),
//   value: 'FR',
}];

class ReceiveAddress extends Component {
  constructor(props) {
    super(props);
    if (props.address) {
      this.state = {
        contact: props.address.contact,
        telephone: props.address.telephone,
        address_line: props.address.address_line,
        postcode: props.address.postcode,
        city: props.address.city,
        hasLoadFromStorage: true,
        countryPickerVisible: false,
        countryPickerValue: [props.address.country_iso || DEFAULT_COUNTRY_ISO],
        hasInitCountryPicker: true,
        selectAddressVisible: false,
      };
    } else if (props.currentUser) {
      this.state = {
        telephone: props.currentUser.telephone,
        contact: props.currentUser.wechat_name || props.currentUser.username,
        hasLoadFromStorage: false,
        countryPickerVisible: false,
        countryPickerValue: [props.currentUser.country_iso || DEFAULT_COUNTRY_ISO],
        hasInitCountryPicker: true,
        selectAddressVisible: false,
      };
    } else {
      this.state = {
        hasLoadFromStorage: false,
        countryPickerVisible: false,
        countryPickerValue: [DEFAULT_COUNTRY_ISO],
        hasInitCountryPicker: false,
        selectAddressVisible: false,
      };
    }
  }

  componentDidMount() {
    const { location = {}, latestAction } = this.props;
    const { product, price } = location.state || {};
    wechatHelper.configShareCommon();
    if (product && price && !_.isEmpty(latestAction)) {
      let category = '';
      if (product.category && product.category.length > 0) {
        const categoryItem = product.category[0];
        category = `${categoryItem.id}|${categoryItem.name}`;
      }
      let coupon = '';
      if (latestAction.type === GROUP_LEADER_BUYING) {
        coupon = constantsValue.COUPON_START_GROUP;
      } else if (latestAction.type === GROUP_BUYING) {
        coupon = constantsValue.COUPON_FOLLOW_GROUP;
      }
      record(actions.CHECKOUT_PROGRESS, {
        items: [
          {
            id: product.id,
            name: product.name,
            list_name: '',
            brand: '',
            category,
            variant: '',
            list_position: -1,
            quantity: latestAction.count,
            price,
          },
        ],
        coupon,
      });
    }

    record(actions.ENTER_COMPLETE_ADDRESS_PAGE, {}, eventCategory.PAGE_ENTER);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.address && !this.state.hasLoadFromStorage) {
      this.setState({
        contact: nextProps.address.contact,
        telephone: nextProps.address.telephone,
        address_line: nextProps.address.address_line,
        postcode: nextProps.address.postcode,
        city: nextProps.address.city,
        hasLoadFromStorage: true,
        countryPickerValue: [nextProps.address.country_iso],
        hasInitCountryPicker: true,
      });
    }
  }

  onCountryPickerChange = (v) => {
    this.setState({ countryPickerValue: v });
    if (!_.isEmpty(this.props.currentUser) && this.props.currentUser.country_iso !== v[0]) {
      this.props.changeCountry(v[0]);
    }
  }

  isSameAddress(address, otherAddress) {
    if (address.contact && address.telephone && address.address_line && address.postcode && address.city && address.country_iso
    && otherAddress.contact && otherAddress.telephone && otherAddress.address_line && otherAddress.postcode && otherAddress.city && otherAddress.country_iso
    ) {
      return address.contact.toLowerCase().trim() === otherAddress.contact.toLowerCase().trim() &&
        address.contact.toLowerCase().trim() === otherAddress.contact.toLowerCase().trim() &&
      address.telephone.toLowerCase().trim() === otherAddress.telephone.toLowerCase().trim() &&
      address.address_line.toLowerCase().trim() === otherAddress.address_line.toLowerCase().trim() &&
      address.postcode.toLowerCase().trim() === otherAddress.postcode.toLowerCase().trim() &&
        address.city.toLowerCase().trim() === otherAddress.city.toLowerCase().trim() &&
        address.country_iso.toLowerCase().trim() === otherAddress.country_iso.toLowerCase().trim();
    }
    return false;
  }

  submitAddress = () => {
    const data = {
      contact: this.state.contact,
      telephone: this.state.telephone,
      address_line: this.state.address_line,
      postcode: this.state.postcode,
      city: this.state.city,
      country_iso: this.state.countryPickerValue[0],
    };
    if (!this.state.contact || !this.state.telephone ||
      !this.state.address_line || !this.state.postcode || !this.state.city) {
      message.error(this.props.t('common.please_input_all_address_info'), 2);
      return;
    }
    this.props.updateReceiveAddress(data);
    if (!this.props.addressList || this.props.addressList.length === 0) {
      this.props.addAddress(Object.assign({ is_default: true }, data));
    } else {
      let hasFind = false;
      for (let i = 0; i < this.props.addressList.length; i++) {
        const address = this.props.addressList[i];
        if (this.isSameAddress(data, address)) {
          hasFind = true;
          this.props.updateAddress(Object.assign({ is_default: true }, address));
        }
      }
      if (!hasFind) {
        this.props.addAddress(Object.assign({ is_default: true }, data));
      }
    }
    if (!this.props.currentUser || !this.props.latestAction) {
      message.error(this.props.t('common.status_lost'), 2, () => {
        browserHistory.replace('/');
      });
    } else if (this.props.latestAction.type === SINGLE_BUYING ||
      this.props.latestAction.type === GROUP_LEADER_BUYING) {
      // start group
      const startData = {
        type: this.props.latestAction.type,
        product_id: this.props.latestAction.productId,
        product_quantity: this.props.latestAction.count,
        address: data,
        min_user_count: this.props.latestAction.minPeople,
        ending_date: this.props.latestAction.endingDate,
        product_sku_id: this.props.latestAction.skuId,
        delivery_service_id: this.props.latestAction.delivery_service_id,
      };
      if (this.props.latestAction.remark) {
        startData.remark = this.props.latestAction.remark;
      }
      this.props.startGroup(startData);
    } else if (this.props.latestAction.type === GROUP_BUYING) {
      // follow group
      const followData = {
        type: this.props.latestAction.type,
        product_id: this.props.latestAction.productId,
        product_quantity: this.props.latestAction.count,
        address: data,
        product_sku_id: this.props.latestAction.skuId,
        group_id: this.props.latestAction.groupId,
        remark: this.props.latestAction.remark,
      };
      if (this.props.latestAction.remark) {
        followData.remark = this.props.latestAction.remark;
      }
      this.props.followGroup(followData);
    }
  }

  render() {
    const t = this.props.t;
    const { type } = this.state;
    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.shipping_address')} />

        <div className={styles.subTitle}>
          { t('common.address_tips') }
        </div>
        <Picker
          visible={this.state.countryPickerVisible}
          data={supportedCountries}
          cols={1}
          value={this.state.countryPickerValue}
          onChange={this.onCountryPickerChange}
          onOk={() => this.setState({ countryPickerVisible: false })}
          onDismiss={() => this.setState({ countryPickerVisible: false })}
        >
          <div
            className={styles.countryWrap}
          >
            <div
              className={styles.actionItemLeft}
            >
              <CountryName flag iso={this.state.countryPickerValue[0]} />
            </div>
            {/* <div
              className={styles.actionItemRight}
              onClick={() => {
                this.setState({ countryPickerVisible: true });
              }}
            >
              <Icon type="right" />
            </div> */}
          </div>
        </Picker>
        <div className={styles.item}>
          <div className={styles.item}>

          </div>
          <InputItem
            type={type}
            clear
            placeholder={t('common.receiver')}
            onChange={(v) => {
              this.setState({ contact: v });
            }}
            onBlur={(v) => {
            }}
            value={this.state.contact}
          ></InputItem>
        </div>

        <div className={`${styles.item} ${styles.itemAddress}`}>
          <TextareaItem
            type={type}
            autoHeight
            clear
            rows={3}
            placeholder={t('common.receiver_address')}
            onChange={(v) => {
              this.setState({ address_line: v });
            }}
            onBlur={(v) => {
            }}
            value={this.state.address_line}
          ></TextareaItem>
        </div>
        <div className={styles.item}>
          <InputItem
            type={type}
            clear
            placeholder={t('common.city')}
            onChange={(v) => {
              this.setState({ city: v });
            }}
            onBlur={(v) => {
            }}
            value={this.state.city}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={type}
            clear
            placeholder={t('common.postcode')}
            onChange={(v) => {
              this.setState({ postcode: v });
            }}
            onBlur={(v) => {
            }}
            value={this.state.postcode}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={type}
            clear
            placeholder={t('common.contact_number')}
            onChange={(v) => {
              this.setState({ telephone: v });
            }}
            onBlur={(v) => {
            }}
            value={this.state.telephone}
          ></InputItem>
        </div>


        <div className={styles.actionWrap}>
          <Button
            className={styles.reinput}
            inline
            onClick={() => this.setState({
              selectAddressVisible: true,
            })}
          >{ t('common.address_list') }</Button>
          <CommonButton
            className={styles.finish}
            onClick={this.submitAddress}
          >{ t('common.save_address') }</CommonButton>
        </div>

        <Modal
          visible={this.state.selectAddressVisible}
          popup
          transparent
          maskClosable
          animationType="slide-up"
          title=""
          footer={[]}
          wrapClassName={styles.addressModal}
          className={styles.addressModal}
          onClose={() => {
            this.setState({
              selectAddressVisible: false,
            });
          }}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        >
          <div className={styles.addSelectWrap}>
            <AddressComponent
              isModal
              onSelectAddress={(address) => {
                this.setState({
                  selectAddressVisible: false,
                  contact: address.contact,
                  telephone: address.telephone,
                  address_line: address.address_line,
                  postcode: address.postcode,
                  city: address.city,
                });
                console.log(address);
              }}
            />
          </div>
        </Modal>
      </div>
    );
  }
}

ReceiveAddress.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  updateReceiveAddress: PropTypes.func,
  address: PropTypes.object,
  latestAction: PropTypes.object,
  startGroup: PropTypes.func,
  followGroup: PropTypes.func,
  location: PropTypes.object,
  changeCountry: PropTypes.func,
  addressList: PropTypes.array,
  addAddress: PropTypes.func,
  updateAddress: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectAddress(),
  selectAddressList(),
  selectLatestAction(),
  selectCurrentUser(),
  (address, addressList, latestAction, currentUser) => ({
    address: address && address.toJS(),
    addressList: (addressList && addressList.toJS()) || [],
    latestAction: latestAction && latestAction.toJS(),
    currentUser: currentUser && currentUser.toJS(),
  }),
);

const mapDispatchToProps = (dispatch) => ({
  updateReceiveAddress: (data) => dispatch(updateReceiveAddress(data)),
  startGroup: (data) => dispatch(startGroup(data)),
  followGroup: (data) => dispatch(followGroup(data)),
  changeCountry: (countryIso) => dispatch(changeCountry(countryIso)),
  addAddress: (data) => dispatch(addAddress(data)),
  updateAddress: (data) => dispatch(updateAddress(data, false)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(ReceiveAddress));
