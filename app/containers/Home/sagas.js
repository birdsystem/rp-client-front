/* eslint-disable no-param-reassign,quote-props,quotes,key-spacing,no-unused-vars */
import { call, put, race, take, select } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { DEFAULT_PAGE_SIZE } from 'utils/constants';
import {
  LOAD_PRODUCT_LIST, LOAD_CATEGORY, LOAD_CATEGORY_DETAIL,
  LOAD_ADVERTISEMENT, LOAD_DEAL_GROUP,
} from './constants';
import {
  loadProductListSuccess,
  loadNewsSuccess,
  loadCategorySuccess,
  loadCategoryDetailSuccess,
  loadAdvertisementSuccess,
  loadDealGroupSuccess,
} from './actions';

import {
  selectProductList,
} from './selectors';

export default [
  getLoadProductListWatcher,
  getLoadCategoryWatcher,
  getLoadCategoryDetailWatcher,
  getLoadAdvertisementWatcher,
  getDealGroupWatcher,
];

export function* getLoadProductListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadProductList: take(LOAD_PRODUCT_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const limit = 10;
    const page = watcher.loadProductList.page;
    const product = yield select(selectProductList());
    let showLoading = false;
    if (page === 1 && (!product || product.toJS().length === 0)) {
      showLoading = true;
    }
    let orderBy = 'deal_count';
    if (page > 1) {
      orderBy = 'rand';
    }
    const requestConfig = {
      urlParams: {
        limit,
        start: (page - 1) * limit,
        order_by: orderBy,
      },
    };
    if (showLoading) {
      requestConfig.feedback = { progress: { mask: false } };
    }
    const requestURL = '/product';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;

      yield put(loadProductListSuccess(list, result.data.total));
    }
  }
}

export function* getLoadCategoryWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadCategory: take(LOAD_CATEGORY),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const page = watcher.loadCategory.page;
    const parentId = watcher.loadCategory.parentId || 0;

    const requestConfig = {
      urlParams: {
        limit: -1,
        start: (page - 1) * DEFAULT_PAGE_SIZE,
        parent_id: parentId,
      },
    };
    const requestURL = '/product/category';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      yield put(loadCategorySuccess(list, parentId));
    }
  }
}

export function* getLoadCategoryDetailWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadCategoryDetail: take(LOAD_CATEGORY_DETAIL),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const id = watcher.loadCategoryDetail.id;
    const withChildren = watcher.loadCategoryDetail.withChildren;

    const requestConfig = {
      urlParams: {
        with_children: withChildren,
      },
    };
    const requestURL = `/product/category/${id}`;
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const data = result.data;
      yield put(loadCategoryDetailSuccess(data, id));
    }
  }
}

export function* getDealGroupWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadDealGroup: take(LOAD_DEAL_GROUP),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestConfig = {
      urlParams: {

      },
    };
    const requestURL = '/group/top-line';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const data = result.data;
      yield put(loadDealGroupSuccess(data));
    }
  }
}

export function* getLoadAdvertisementWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadAdvertisement: take(LOAD_ADVERTISEMENT),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = '/group/advertisement';
    const result = yield call(request, requestURL);
    // const result = { "success":true, "data":{ "banners":[{ "title":"\u4ee5\u8bfa \u5c0f\u7c73max3\u624b\u673a\u58f3max2\u4fdd\u62a4\u5957\u7845\u80f6\u7537\u9632\u6454\u5973\u5168\u5305\u6c14\u56ca\u8f6f\u58f3MAX\u4e2a\u6027\u521b\u610f\u7b80\u7ea6\u900f\u660e\u6f6e\u58f3", "link":"\/productdetail\/209", "image_url":"http:\/\/tuantuanxia.s3.amazonaws.com\/production\/ADVERTISING_PHOTO\/2\/9\/3c59dc048e8850243be8079a5c74d07921.jpg" }, { "title":"\u7c73\u767d\u8272\u76f4\u7b52\u52a0\u7ed2\u725b\u4ed4\u88e4\u5973\u79cb\u51ac\u5b632018\u65b0\u6b3e\u521d\u604b\u5b66\u751f\u9614\u817f\u5bbd\u677e\u674f\u8272\u88e4\u5b50", "link":"\/productdetail\/242", "image_url":"http:\/\/tuantuanxia.s3.amazonaws.com\/production\/ADVERTISING_PHOTO\/7\/9\/b6d767d2f8ed5d21a44b0e5886680cb922.jpg" }, { "title":"\u6d17\u8138\u5237\u6df1\u5c42\u6e05\u6d01\u6d01\u9762\u5de5\u5177\u53cc\u9762\u7ad9\u7acb\u7845\u80f6\u9ed1\u5934\u6d01\u9762\u5237", "link":"\/productdetail\/2858", "image_url":"http:\/\/tuantuanxia.s3.amazonaws.com\/production\/ADVERTISING_PHOTO\/2\/f\/37693cfc748049e45d87b8c7d8b9aacd23.jpg" }, { "title":"\u5e3d\u5b50\u5973\u51ac\u7f8a\u6bdb\u8d1d\u96f7\u5e3d\u5973\u82f1\u4f26\u79cb\u51ac\u97e9\u7248\u65e5\u7cfb\u9488\u7ec7\u753b\u5bb6\u5e3d\u5357\u74dc\u5e3d\u5973", "link":"\/productdetail\/3", "image_url":"http:\/\/tuantuanxia.s3.amazonaws.com\/production\/ADVERTISING_PHOTO\/b\/c\/1ff1de774005f8da13f42943881c655f24.jpeg" }], "news":[{ "title":"\u751f\u6d3b\u5143\u7d20\u9676\u74f7\u7535\u70ed\u996d\u76d2\u53cc\u5c42\u53ef\u63d2\u7535\u4fdd\u6e29\u996d\u76d2\u52a0\u70ed\u84b8\u716e\u996d\u76d2\u8ff7\u4f60\u70ed\u996d\u5668", "on_top":false, "link":"\/productdetail\/762" }, { "title":"\u9ed1\u8272\u8d1d\u96f7\u5e3d\u5973\u79cb\u51ac\u516b\u89d2\u5e3d\u590d\u53e4\u82f1\u4f26\u62a5\u7ae5\u5e3dins\u753b\u5bb6\u5e3d\u5b50\u97e9\u7248\u65e5\u7cfb\u767e\u642d", "on_top":false, "link":"\/productdetail\/1" }, { "title":"\u7f51\u7ea2\u767d\u8272\u6bdb\u6bdb\u978b\u5973\u51ac2018\u65b0\u6b3e\u5b55\u5987\u4e00\u811a\u8e6c\u7f8a\u7f94\u6bdb\u978b\u5916\u7a7f\u5e73\u5e95\u5927\u7801\u5973\u978b", "on_top":false, "link":"\/productdetail\/2278" }, { "title":"\u53ef\u7231\u8349\u8393\u8033\u73af\u5973\u6c14\u8d28\u97e9\u56fd\u4e2a\u6027\u767e\u642d\u8033\u5760\u7b80\u7ea6\u6e05\u65b0\u65e0\u8033\u6d1e\u8033\u5939\u7eaf\u94f6\u8033\u9489", "on_top":false, "link":"\/productdetail\/232" }, { "title":"\u82cf\u6cca\u5c14\u4fdd\u6e29\u996d\u76d2\u53cc\u5c42\u771f\u7a7a12\u5c0f\u65f62\/3\u5c42304\u4e0d\u9508\u94a2\u5b66\u751f\u4fbf\u643a\u8d85\u957f\u4fdd\u6e29\u6876", "on_top":false, "link":"\/productdetail\/214" }, { "title":"\u5c0f\u4e56\u86cb\u76ca\u667a\u7c7b\u6e38\u620f \u513f\u7ae5\u684c\u9762\u68cb\u724c\u667a\u529b\u73a9\u5177 \u903b\u8f91\u601d\u7ef4\u8ff7\u5bab \u4e09\u53ea\u5c0f\u732a", "on_top":false, "link":"\/productdetail\/2552" }] }, "message":"", "errCode":"" };
    if (result.success) {
      const data = result.data;
      yield put(loadAdvertisementSuccess(data));
    }
  }
}
