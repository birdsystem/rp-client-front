/* eslint-disable react/no-unused-prop-types,no-unused-vars,jsx-a11y/no-noninteractive-element-interactions,react/no-will-update-set-state */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory, withRouter } from 'react-router';
import _ from 'lodash';
import {
  getScrollTop,
  getScrollHeight,
  getWindowHeight,
} from 'utils/commonHelper';
import {
  selectCurrentUser,
  selectListLayoutType,
  selectHomeScrollTop,
  selectShowCouponModal,
  selectRegisterFrom,
} from 'containers/App/selectors';
import { LIST_LAYOUT_TYPE } from 'containers/App/constants';
import { Icon, Carousel, Modal, ListView, PullToRefresh } from 'antd-mobile';
import {
  clearLatestAction,
  clearOtherPageScrollTop,
  updateListLayoutType,
  setHomeScrollTop,
  setShowCouponModal,
} from 'containers/App/actions';
import HomeBottomAction from 'components/HomeBottomAction';
import ProductItem from 'components/ProductItem';
import WindowTitle from 'components/WindowTitle';
import wechatHelper from 'utils/wechatHelper';
import { getDisplayPrice } from 'utils/productHelper';
import { isBackendYes } from 'utils/backendHelper';
import { SHARE_TYPE, adFrom, DEFAULT_PAGE_SIZE } from 'utils/constants';
import {
  eventCategory,
  record,
  actions,
  eventParams,
} from 'utils/gaDIRecordHelper';
import categoryIcon from './category.png';
import customerIcon from './customer.png';
import searchIcon from './search.png';
import hotIcon from './hot-icon.png';
import hotLabel from './hot-label.png';
import hotText from './hot-text.png';
import touTiao from './toutiao.png';
import {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
  selectBanners,
  selectNews,
  selectCategory,
  selectCategoryLoading,
  selectCategoryDetailLoading,
  selectAdvertisement,
  selectDealGroups,
} from './selectors';
import {
  loadProductList,
  loadCategory,
  loadCategoryDetail,
  loadAdvertisement,
  loadDealGroup,
} from './actions';
import styles from './styles.css';

class Home extends Component {
  constructor(props) {
    super(props);
    const { location } = this.props;

    // for advertisement record
    if (location && location.query && location.query.from) {
      if (location.query.from.toLowerCase() === adFrom.dnvod_video) {
        record(actions.FROM_DNVOD_VIDEO, {}, eventCategory.ADVERTISEMENT);
      }
    }
    if (location && location.query && location.query.type) {
      if (location.query.type === SHARE_TYPE.PRODUCT_DETAIL) {
        window.location = `${window.location.protocol}//${window.location.host}/productdetail/${location.query.pid}`;
      } else if (location.query.type === SHARE_TYPE.GROUP_SHARE) {
        window.location = `${window.location.protocol}//${window.location.host}/productdetail/${location.query.pid}/?gid=${location.query.gid}`;
      } else if (location.query.type === SHARE_TYPE.FOLLOW_GROUP) {
        window.location = `${window.location.protocol}//${window.location.host}/followgroup/${location.query.gid}/${location.query.pid}`;
      }
    }
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    let currentPage = 1;
    if (this.props.productList && this.props.productList.length > 0) {
      currentPage = parseInt(this.props.productList.length / DEFAULT_PAGE_SIZE,
        10) + 1;
    }
    this.state = {
      currentPage,
      autoPlay: false,
      showCategoryModel: false,
      currentCategoryId: 0,
      isShowingChildCategory: false,
      currentParentCategoryId: -1,
      refreshing: false,
      dataSource,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    if (!this.props.productList || this.props.productList.length === 0) {
      this.props.loadProductList(this.state.currentPage);
    }
    // this.props.loadProductList(this.state.currentPage);
    this.props.clearLatestAction();
    this.props.clearOtherPageScrollTop();
    this.configWechatJSSdk();
    // this.props.loadCategory(this.state.currentCategoryId);
    this.props.loadAdvertisement();
    this.props.loadDealGroup();
    record(actions.ENTER_HOME_PAGE, {}, eventCategory.PAGE_ENTER);
    const lastScrollTop = this.props.homeScrollTop;
    let targetY = 0;
    if (lastScrollTop > 0) {
      targetY = lastScrollTop;
    }
    setTimeout(() => {
      window.scrollTo(0, targetY);
      setTimeout(() => {
        window.addEventListener('scroll', this.loadMore);
      }, 200);
    }, 200);
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.refreshing && this.props.productListLoading &&
      !nextProps.productListLoading) {
      this.setState({
        refreshing: false,
      });
    }
  }

  componentWillUnmount() {
    const scrollTop = getScrollTop();
    this.props.setHomeScrollTop(parseInt(scrollTop, 10));
    window.removeEventListener('scroll', this.loadMore);
  }

  onCloseCategoryModal() {
    this.setState({
      showCategoryModel: false,
    });
  }

  onTapCategoryWithChild(cid) {
    this.props.loadCategoryDetail(cid);
    this.setState({
      isShowingChildCategory: true,
      currentParentCategoryId: cid,
    });
  }

  loadMore() {
    if (this.props.productListLoading) {
      return;
    }
    const targetPaddingToBottom = 300;
    if (getScrollTop() + getWindowHeight() >=
      getScrollHeight() - targetPaddingToBottom) {
      const count = this.props.productList ? this.props.productList.length : 0;
      if (count < this.props.totalCount) {
        this.setState({
          currentPage: this.state.currentPage + 1,
        });
        console.log('load more');
        this.props.loadProductList(this.state.currentPage);
      }
    }
  }

  configWechatJSSdk() {
    wechatHelper.configShareCommon();
  }

  render() {
    const { productList, t, category, categoryLoading, listLayoutType, advertisement, dealGroup } = this.props;
    const banners = (advertisement && advertisement.banners) || [];
    const news = (advertisement && advertisement.news) || [];
    let bannerHeight = (window.screen.availWidth * 350) / 840;
    if (bannerHeight > 200) {
      bannerHeight = 200;
    }
    let currentParentCategory = {};
    if (this.state.isShowingChildCategory) {
      currentParentCategory = _.find(category,
        { id: this.state.currentParentCategoryId });
    }

    let hasMore = false;
    const count = this.props.productList ? this.props.productList.length : 0;
    if (count < this.props.totalCount) {
      hasMore = true;
    }

    const row = (rowData, sectionID, rowID) => (
      <ProductItem
        key={`p${rowID}`}
        product={rowData}
        onClickItem={() => {
          record(actions.CLICK_HOME_PRODUCT_ITEM,
            {},
            eventCategory.USER_ACTION,
          );
        }}
      />
    );

    let dataSource = this.state.dataSource.cloneWithRows({});
    if (productList) {
      dataSource = this.state.dataSource.cloneWithRows(productList);
    }

    return (
      <div className={styles.wrap}>
        <WindowTitle defaultTitle />

        <div
          className={styles.topWrapBg}
          style={{
            opacity: 0.8,
          }}
        ></div>
        <div className={styles.topWrap}>
          <img
            src={categoryIcon}
            className={styles.categoryIcon}
            alt={'category'}
            onClick={
              () => {
                record(actions.CLICK_HOME_CATEGORY,
                  {},
                  eventCategory.USER_ACTION,
                );
                browserHistory.push('/categories');
              }
            }
          />
          <div
            onClick={
              () => {
                record(actions.CLICK_HOME_SEARCH,
                  {},
                  eventCategory.USER_ACTION,
                );
                browserHistory.push('/search');
              }
            }
            className={styles.searchWrap}
          >
            <img
              src={searchIcon}
              className={styles.searchIcon}
              alt={'search'}
            />
            <span className={styles.holder}>{ this.props.t(
              'common.product_name') }</span>
          </div>
          <a
            href={'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515'}
            className={styles.customerIconWrap}
          >
            <img
              src={customerIcon}
              className={styles.customerIcon}
              alt={'customer'}
            />
          </a>
        </div>

        <div className={styles.bannerWrap}>
          { banners.length > 0 && <Carousel
            autoplay
            infinite
            dragging={false}
          >
            { banners.map((banner) => (
              <div
                key={`${banner.id}`}
                className={styles.bannerLink}
                style={
                {
                  height: `${bannerHeight}px`,
                }
                }
                onClick={() => {
                  const data = {};
                  // data.event_callback = () => {
                    if (banner && banner.link) {
                      browserHistory.push(banner.link);
                    }
                  // };
                  data[eventParams.ID] = banner.id;
                  data[eventParams.LINK] = banner.link;
                  record(actions.CLICK_BANNER,
                    data,
                    eventCategory.USER_ACTION,
                  );
                }}
              >
                <div
                  className={styles.bannerImg}
                  style={{
                    backgroundImage: `url("${banner.image_url}")`,
                    height: `${bannerHeight}px`,
                  }}
                />
              </div>
            )) }
          </Carousel>
          }
          {
            dealGroup.length > 0 && banners.length > 0 &&
            <div className={styles.dealGroupBg}></div>
          }
          {
            dealGroup.length > 0 && banners.length > 0 &&
            <div className={styles.dealGroupWrap}>
              <Carousel
                className={styles.dealGroupContent}
                vertical
                dots={false}
                dragging={false}
                autoplay
                autoplayInterval={7000}
                infinite
              >
                {
                  dealGroup.map((groupItem, inx) => {
                    const owner = groupItem.owner;
                    const name = owner && (owner.wechat_name || owner.username);
                    let firstLetter = '团';
                    if (name && name.length === 1) {
                      firstLetter = name;
                    } else if (name) {
                      firstLetter = owner.username.substr(owner.username.length -
                        1);
                    }
                    return (
                      <div
                        className={styles.dealGroupItemWrap}
                        key={`${inx}n`}
                        onClick={() => {
                          browserHistory.push(
                            `productdetail/${groupItem.product_detail.id}`);
                        }}
                      >
                        <div
                          className={styles.dealGroupName}
                        >
                          { t('common.success_deal', {
                            name: firstLetter,
                            product: groupItem.product_detail.name,
                          }) }
                        </div>
                      </div>
                    );
                  })
                }
              </Carousel>
            </div>
          }
        </div>
        { news.length > 0 && <div className={styles.newsWrap}>
          <div className={styles.newsTitle}>
            <img className={styles.toutiao} src={touTiao} alt={'团团头条'} />
          </div>
          <div className={styles.newsContentWrap}>
            <Carousel
              className={styles.newsContent}
              vertical
              dots={false}
              dragging={false}
              autoplay
              autoplayInterval={5000}
              infinite
            >
              {
                news.map((newsItem, inx) => (
                  <div className={styles.newsItemWrap} key={`${inx}n`}>
                    <a
                      className={styles.newsItem}
                      href={newsItem.link}
                    >
                      <span className={styles.hotText}>{ t('common.hot') }</span>
                      { newsItem.title }
                    </a>
                  </div>
                ))
              }
            </Carousel>
          </div>
        </div>
        }
        <div className={styles.hotSellRecommend}>
          <img src={hotIcon} className={styles.hotIcon} alt={'hot'} />
          <img
            src={hotText}
            className={styles.hotTextIcon}
            alt={t('common.hot_recommend')}
          />
          <img src={hotLabel} className={styles.hotLabel} alt={''} />
          <div className={styles.layoutSwitch}>
            <span
              className={styles.iconHamburgerWrap}
              onClick={() => {
                this.props.updateListLayoutType(LIST_LAYOUT_TYPE.SINGLE_COLUMN);
              }}
            >
              <span
                className={listLayoutType === LIST_LAYOUT_TYPE.SINGLE_COLUMN
                  ? styles.iconHamburgerFocus
                  : styles.iconHamburger}
              >
              </span>
            </span>
            <span
              className={styles.iconTilesWrap}
              onClick={() => {
                this.props.updateListLayoutType(LIST_LAYOUT_TYPE.TWO_COLUMN);
              }}
            >
              <span
                className={listLayoutType === LIST_LAYOUT_TYPE.TWO_COLUMN
                  ? styles.iconTilesFocus
                  : styles.iconTiles}
              >
              </span>
            </span>
          </div>
        </div>
        <Modal
          popup
          visible={this.state.showCategoryModel}
          onClose={() => { this.onCloseCategoryModal(); }}
          animationType="slide-down"
        >
          {
            this.state.isShowingChildCategory ?
              <div className={styles.categoryDetailWrap}>
                <div
                  className={styles.currentCategory}
                  onClick={() => {
                    this.setState({
                      isShowingChildCategory: false,
                    });
                  }}
                >
                  { `<< ${currentParentCategory.name}` }
                </div>
                <div className={styles.categoryDetailChildWrap}>
                  {
                    this.props.categoryDetailLoading ||
                    _.isEmpty(currentParentCategory) ||
                    !currentParentCategory.children ?
                      <div className={styles.categoryDetailLoadingWrap}>
                        <Icon type="loading" size={'md'} />
                      </div>
                      : <div className={styles.categoryDetailChildWrap}>
                        {
                          currentParentCategory.children.map((child, index) => {
                            const hasChild = isBackendYes(child.has_child);
                            return (
                              <div
                                key={`c${index}`}
                                className={styles.categoryChildItem}
                              >
                                <div
                                  onClick={() => {
                                    if (!hasChild) {
                                      browserHistory.push({
                                        pathname: `/categoryproductlist/${child.id}`,
                                        state: { title: child.name },
                                      });
                                    }
                                  }}
                                  className={styles.categoryChildTitle}
                                >
                                  { child.name }
                                </div>
                                {
                                  hasChild &&
                                  <div className={styles.categoryChildContent}>
                                    {
                                      child.children.map(
                                        (c) => (
                                          <div
                                            onClick={() => {
                                              browserHistory.push({
                                                pathname: `/categoryproductlist/${c.id}`,
                                                state: { title: c.name },
                                              });
                                            }}
                                            className={styles.categoryChildContentItem}
                                          >
                                            { c.name }
                                          </div>
                                        ),
                                      )
                                    }
                                  </div>
                                }
                              </div>
                            );
                          })
                        }
                      </div>
                  }
                </div>
              </div>
              : <div className={styles.categoryWrap}>
                {
                  categoryLoading ? <div className={styles.categoryLoadingWrap}>
                    <Icon type="loading" size={'md'} />
                  </div>
                    : <div className={styles.categoryInner}>
                      {
                        category.map((cate, index) => (
                          <div
                            key={`${index}_`}
                            onClick={
                              () => {
                                if (isBackendYes(cate.has_child)) {
                                  this.onTapCategoryWithChild(cate.id);
                                } else {
                                  browserHistory.push({
                                    pathname: `/categoryproductlist/${cate.id}`,
                                    state: { title: cate.name },
                                  });
                                }
                              }
                            }
                            className={styles.categoryItem}
                          >{ cate.name }</div>
                        ))
                      }
                    </div>
                }
              </div>
          }
        </Modal>
        <div className={styles.listWrap}>
          <ListView
            renderRow={row}
            useBodyScroll
            initialListSize={1000}
            scrollRenderAheadDistance={500}
            onEndReachedThreshold={10}
            rowIdentities={() => null}
            dataSource={dataSource}
            pullToRefresh={<PullToRefresh
              damping={1000}
              indicator={{
                activate: ' ',
                finish: ' ',
              }}
              direction={'down'}
              refreshing={this.state.refreshing}
              onRefresh={() => {
                if (this.props.productListLoading) {
                  return;
                }
                this.setState({
                  refreshing: true,
                  currentPage: 1,
                });
                console.log('onRefresh');
                this.props.loadProductList(1);
              }}
            />}
          />
        </div>
        {
          hasMore &&
          <div className={styles.hasMoreWrap}>
            <Icon type="loading" size={'md'} />
          </div>
        }
        <div className={styles.tabWrap}>
          <HomeBottomAction index={0} />
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  loadProductList: PropTypes.func,
  productList: PropTypes.array,
  productListLoading: PropTypes.bool,
  clearLatestAction: PropTypes.func,
  totalCount: PropTypes.number,
  banners: PropTypes.array,
  news: PropTypes.array,
  location: PropTypes.object,
  loadCategory: PropTypes.func,
  category: PropTypes.array,
  categoryLoading: PropTypes.bool,
  loadCategoryDetail: PropTypes.func,
  categoryDetailLoading: PropTypes.bool,
  listLayoutType: PropTypes.string,
  updateListLayoutType: PropTypes.func,
  loadAdvertisement: PropTypes.func,
  advertisement: PropTypes.object,
  dealGroup: PropTypes.array,
  loadDealGroup: PropTypes.func,
  setHomeScrollTop: PropTypes.func,
  homeScrollTop: PropTypes.number,
  clearOtherPageScrollTop: PropTypes.func,
  showCouponModal: PropTypes.bool,
  registerFrom: PropTypes.string,
  setShowCouponModal: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectProductList(),
  selectProductListLoading(),
  selectCurrentUser(),
  selectTotalCount(),
  selectBanners(),
  selectNews(),
  selectCategoryLoading(),
  selectCategory(),
  selectCategoryDetailLoading(),
  selectListLayoutType(),
  selectAdvertisement(),
  selectDealGroups(),
  selectHomeScrollTop(),
  selectShowCouponModal(),
  selectRegisterFrom(),
  (
    productList, productListLoading, currentUser, totalCount, banners, news,
    categoryLoading, category, categoryDetailLoading, listLayoutType,
    advertisement, dealGroup, homeScrollTop, showCouponModal, registerFrom) => ({
      productList: productList && productList.toJS(),
      productListLoading,
      currentUser: currentUser && currentUser.toJS(),
      totalCount,
      banners: banners && banners.toJS(),
      news: news && news.toJS(),
      categoryLoading,
      category: category && category.toJS(),
      categoryDetailLoading,
      listLayoutType,
      advertisement: advertisement && advertisement.toJS(),
      dealGroup: dealGroup && dealGroup.toJS(),
      homeScrollTop,
      showCouponModal,
      registerFrom,
    }),
);

const mapDispatchToProps = (dispatch) => ({
  loadProductList: (page) => dispatch(loadProductList(page)),
  clearLatestAction: () => dispatch(clearLatestAction()),
  loadCategory: (parentId) => dispatch(loadCategory(parentId)),
  loadCategoryDetail: (id) => dispatch(loadCategoryDetail(id)),
  updateListLayoutType: (layoutType) => dispatch(
    updateListLayoutType(layoutType)),
  loadAdvertisement: () => dispatch(loadAdvertisement()),
  loadDealGroup: () => dispatch(loadDealGroup()),
  setHomeScrollTop: (scrollTop) => dispatch(setHomeScrollTop(scrollTop)),
  clearOtherPageScrollTop: () => dispatch(clearOtherPageScrollTop()),
  setShowCouponModal: (show, from) => dispatch(setShowCouponModal(show, from)),
});

export default translate()(
  withRouter(connect(mapStateToProps, mapDispatchToProps)(Home)));
