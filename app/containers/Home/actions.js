/*
 *
 * Home actions
 *
 */

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
  LOAD_NEWS,
  LOAD_NEWS_SUCCESS,
  LOAD_NEWS_ERROR,
  LOAD_CATEGORY,
  LOAD_CATEGORY_ERROR,
  LOAD_CATEGORY_SUCCESS,
  LOAD_CATEGORY_DETAIL,
  LOAD_CATEGORY_DETAIL_SUCCESS,
  LOAD_CATEGORY_DETAIL_ERROR,
  LOAD_ADVERTISEMENT,
  LOAD_ADVERTISEMENT_ERROR,
  LOAD_ADVERTISEMENT_SUCCESS,
  LOAD_DEAL_GROUP,
  LOAD_DEAL_GROUP_SUCCESS,
  LOAD_DEAL_GROUP_ERROR,
} from './constants';

export function loadProductList(page) {
  return {
    type: LOAD_PRODUCT_LIST,
    page,
  };
}

export function loadProductListSuccess(list, count) {
  return {
    type: LOAD_PRODUCT_LIST_SUCCESS,
    list,
    count,
  };
}

export function loadProductListError() {
  return {
    type: LOAD_PRODUCT_LIST_ERROR,
  };
}

export function loadAdvertisement() {
  return {
    type: LOAD_ADVERTISEMENT,
  };
}

export function loadAdvertisementSuccess(data) {
  return {
    type: LOAD_ADVERTISEMENT_SUCCESS,
    data,
  };
}

export function loadAdvertisementError() {
  return {
    type: LOAD_ADVERTISEMENT_ERROR,
  };
}

export function loadNews() {
  return {
    type: LOAD_NEWS,
  };
}

export function loadNewsSuccess(data) {
  return {
    type: LOAD_NEWS_SUCCESS,
    data,
  };
}

export function loadNewsError() {
  return {
    type: LOAD_NEWS_ERROR,
  };
}

export function loadCategory(parentId) {
  return {
    type: LOAD_CATEGORY,
    parentId,
  };
}

export function loadCategorySuccess(data, parentId) {
  return {
    type: LOAD_CATEGORY_SUCCESS,
    data,
    parentId,
  };
}

export function loadCategoryError() {
  return {
    type: LOAD_CATEGORY_ERROR,
  };
}

export function loadCategoryDetail(id, withChildren = 1) {
  return {
    type: LOAD_CATEGORY_DETAIL,
    id,
    withChildren,
  };
}

export function loadCategoryDetailSuccess(data, id) {
  return {
    type: LOAD_CATEGORY_DETAIL_SUCCESS,
    data,
    id,
  };
}

export function loadCategoryDetailError() {
  return {
    type: LOAD_CATEGORY_DETAIL_ERROR,
  };
}

export function loadDealGroup() {
  return {
    type: LOAD_DEAL_GROUP,
  };
}

export function loadDealGroupSuccess(data) {
  return {
    type: LOAD_DEAL_GROUP_SUCCESS,
    data,
  };
}

export function loadDealGroupError() {
  return {
    type: LOAD_DEAL_GROUP_ERROR,
  };
}
