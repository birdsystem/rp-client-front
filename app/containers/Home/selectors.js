import { createSelector } from 'reselect';

const selectHomeDomain = () => (state) => state.get('home');

const selectProductList = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('productList')
);

const selectProductListLoading = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('productListLoading')
);

const selectTotalCount = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('totalCount')
);

const selectBanners = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('banners')
);

const selectBannersLoading = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('bannersLoading')
);

const selectNews = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('news')
);

const selectNewsLoading = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('newsLoading')
);

const selectCategoryLoading = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('categoryLoading')
);

const selectCategory = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('category')
);

const selectCategoryDetailLoading = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('categoryDetailLoading')
);

const selectAdvertisement = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('advertisement')
);

const selectDealGroups = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.get('dealGroups')
);

export {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
  selectBanners,
  selectBannersLoading,
  selectNews,
  selectNewsLoading,
  selectCategory,
  selectCategoryLoading,
  selectCategoryDetailLoading,
  selectAdvertisement,
  selectDealGroups,
};
