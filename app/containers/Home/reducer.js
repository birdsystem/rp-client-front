/*
 *
 * Template reducer
 *
 */

import { fromJS } from 'immutable';

import {
  localStorageKeys,
} from 'utils/constants';

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
  LOAD_BANNERS,
  LOAD_BANNERS_ERROR,
  LOAD_BANNERS_SUCCESS,
  LOAD_NEWS,
  LOAD_NEWS_SUCCESS,
  LOAD_NEWS_ERROR,
  LOAD_CATEGORY,
  LOAD_CATEGORY_SUCCESS,
  LOAD_CATEGORY_ERROR,
  LOAD_CATEGORY_DETAIL,
  LOAD_CATEGORY_DETAIL_SUCCESS,
  LOAD_CATEGORY_DETAIL_ERROR,
  LOAD_ADVERTISEMENT_SUCCESS,
  LOAD_DEAL_GROUP_SUCCESS,
} from './constants';

const initialState = fromJS({
  productList: null,

  productListLoading: false,

  currentPage: 1,

  totalCount: 0,

  newsLoading: false,

  bannersLoading: false,

  news: [],

  banners: [],

  categoryLoading: false,

  category: [],

  categoryDetailLoading: false,

  advertisement: (localStorage.getItem(localStorageKeys.ADVERTISEMENT) && JSON.parse(localStorage.getItem(localStorageKeys.ADVERTISEMENT))) || null,

  dealGroups: [],
});

function productListReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_PRODUCT_LIST:
      if (action.page === 1) {
        return state.set('currentPage', action.page)
        .set('totalCount', 0)
        .set('productListLoading', true);
      }
      return state.set('currentPage', action.page)
      .set('productListLoading', true);
    case LOAD_PRODUCT_LIST_SUCCESS: {
      if (state.get('currentPage') === 1) {
        return state.set('productList', fromJS(action.list))
        .set('totalCount', action.count)
        .set('productListLoading', false);
      }
      const list = state.get('productList').toJS().concat(action.list);
      return state.set('productList', fromJS(list))
      .set('productListLoading', false);
    }
    case LOAD_PRODUCT_LIST_ERROR:
      return state.set('productListLoading', false);
    case LOAD_BANNERS:
      return state.set('bannersLoading', true);
    case LOAD_BANNERS_SUCCESS:
      return state.set('bannersLoading', false)
        .set('banners', fromJS(action.data));
    case LOAD_BANNERS_ERROR:
      return state.set('bannersLoading', false);
    case LOAD_NEWS:
      return state.set('newsLoading', true);
    case LOAD_NEWS_SUCCESS:
      return state.set('newsLoading', false)
      .set('news', fromJS(action.data));
    case LOAD_NEWS_ERROR:
      return state.set('newsLoading', false);
    case LOAD_CATEGORY:
      return state.set('categoryLoading', true);
    case LOAD_CATEGORY_SUCCESS:
      {
        if (`${action.parentId}` === '0') {
          return state.set('categoryLoading', false)
          .set('category', fromJS(action.data));
        }
        return state;
      }
    case LOAD_CATEGORY_ERROR:
      return state.set('categoryLoading', false);
    case LOAD_CATEGORY_DETAIL:
      return state.set('categoryDetailLoading', true);
    case LOAD_CATEGORY_DETAIL_SUCCESS:
      {
        const category = (state.get('category') && state.get('category').toJS()) || [];
        for (let i = 0; i < category.length; i += 1) {
          if (`${category[i].id}` === `${action.id}`) {
            category[i].children = action.data.children;
          }
        }
        return state.set('categoryDetailLoading', false)
        .set('category', fromJS(category));
      }
    case LOAD_CATEGORY_DETAIL_ERROR:
      return state.set('categoryDetailLoading', false);
    case LOAD_ADVERTISEMENT_SUCCESS:
      localStorage.setItem(localStorageKeys.ADVERTISEMENT, JSON.stringify(action.data));
      return state.set('advertisement', fromJS(action.data));
    case LOAD_DEAL_GROUP_SUCCESS:
      return state.set('dealGroups', fromJS(action.data));
    default:
      return state;
  }
}

export default productListReducer;
