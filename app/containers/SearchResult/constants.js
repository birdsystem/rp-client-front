/*
 *
 * SearchResult constants
 *
 */

export const LOAD_PRODUCT_LIST = 'SearchResult/LOAD_PRODUCT_LIST';
export const LOAD_PRODUCT_LIST_SUCCESS = 'SearchResult/LOAD_PRODUCT_LIST_SUCCESS';
export const LOAD_PRODUCT_LIST_ERROR = 'SearchResult/LOAD_PRODUCT_LIST_ERROR';
