/*
 *
 * Global actions
 *
 */

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

export function loadProductList(keyword, page) {
  return {
    type: LOAD_PRODUCT_LIST,
    keyword,
    page,
  };
}

export function loadProductListSuccess(list, count, keyword) {
  return {
    type: LOAD_PRODUCT_LIST_SUCCESS,
    list,
    count,
    keyword,
  };
}

export function loadProductListError() {
  return {
    type: LOAD_PRODUCT_LIST_ERROR,
  };
}
