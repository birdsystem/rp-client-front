/* eslint-disable react/no-unused-prop-types,react/no-did-mount-set-state */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { Link, browserHistory } from 'react-router';
import {
  getScrollTop,
  getScrollHeight,
  getWindowHeight,
} from 'utils/commonHelper';
import { selectCurrentUser, selectOtherPageScrollTop, selectRecommendProductList } from 'containers/App/selectors';
import { setOtherPageScrollTop, loadRecommendProductList } from 'containers/App/actions';
import { record, eventCategory, actions, eventParams } from 'utils/gaDIRecordHelper';
import { DEFAULT_PAGE_SIZE } from 'utils/constants';
import { Icon, ListView } from 'antd-mobile';
import WindowTitle from 'components/WindowTitle';
import wechatHelper from 'utils/wechatHelper';
import ProductItem from 'components/ProductItem';
import homeIcon from './home.png';
import searchIcon from './search.png';
import {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
  selectCurrentDisplayWord,
} from './selectors';
import { loadProductList } from './actions';
import styles from './styles.css';

class SearchResult extends Component {
  constructor(props) {
    super(props);
    const { location = {} } = this.props;
    let keyword = location && location.query && decodeURI(location.query.q);
    if (!keyword || keyword === 'undefined') {
      keyword = '';
    }
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    let currentPage = 1;
    if (keyword === this.props.currentDisplayWord && this.props.productList && this.props.productList.length > 0) {
      currentPage = parseInt(this.props.productList.length / DEFAULT_PAGE_SIZE, 10) + 1;
    }
    this.state = {
      keyword,
      currentPage,
      dataSource,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    if (!(this.state.keyword === this.props.currentDisplayWord && this.props.productList && this.props.productList.length > 0)) {
      this.props.loadProductList(this.state.keyword, this.state.currentPage);
    }
    if (!this.props.recommendProductList || this.props.recommendProductList.length === 0) {
      this.props.loadRecommendProductList(1);
    }
    const data = {};
    data[eventParams.SEARCH] = this.state.keyword;
    record(actions.ENTER_SEARCH_RESULT_PAGE, data, eventCategory.PAGE_ENTER);
    const lastScrollTop = this.props.otherPageScrollTop;
    let targetY = 0;
    if (lastScrollTop > 0) {
      targetY = lastScrollTop;
    }
    // console.log(`scroll recommend to:${targetY}`);
    setTimeout(() => {
      window.scrollTo(0, targetY);
      setTimeout(() => {
        window.addEventListener('scroll', this.loadMore);
      }, 200);
    }, 200);
    wechatHelper.configShareCommon();
  }

  componentWillUnmount() {
    const scrollTop = getScrollTop();
    this.props.setOtherPageScrollTop(parseInt(scrollTop, 10));
    window.removeEventListener('scroll', this.loadMore);
  }

  loadMore() {
    if (this.props.productListLoading) {
      return;
    }
    const targetPaddingToBottom = 300;
    if (getScrollTop() + getWindowHeight() >=
      getScrollHeight() - targetPaddingToBottom) {
      const count = this.props.productList ? this.props.productList.length : 0;
      if (count < this.props.totalCount) {
        this.setState({
          currentPage: this.state.currentPage + 1,
        });
        this.props.loadProductList(this.state.keyword, this.state.currentPage);
      }
    }
  }

  render() {
    const { t } = this.props;
    const productList = this.props.productList;
    const recommendProductList = this.props.recommendProductList;
    const showRecommend = !this.props.productListLoading && (!productList || productList.length === 0) && recommendProductList && recommendProductList.length > 0;
    let hasMore = false;
    const count = this.props.productList ? this.props.productList.length : 0;
    if (count < this.props.totalCount) {
      hasMore = true;
    }
    const row = (rowData, sectionID, rowID) => (
      <ProductItem key={`p${rowID}`} product={rowData} />
    );

    let dataSource = this.state.dataSource.cloneWithRows({});
    if (productList) {
      dataSource = this.state.dataSource.cloneWithRows(productList);
    }

    let recommendSource = this.state.dataSource.cloneWithRows({});
    if (recommendProductList) {
      recommendSource = this.state.dataSource.cloneWithRows(recommendProductList);
    }

    return (
      <div className={styles.productWrap}>
        <WindowTitle title={t('pageTitles.product_search_result')} />

        <div className={styles.top}>
          <Link
            className={styles.homeIconWrap}
            to="/"
          >
            <img src={homeIcon} className={styles.homeIcon} alt={'home'} />
          </Link>
          <span
            onClick={() => {
              browserHistory.push(`/search?q=${encodeURI(this.state.keyword)}`);
            }}
            className={styles.searchInputWrap}
          >
            <img
              src={searchIcon}
              className={styles.searchIcon}
              alt={'search'}
            />
            <span className={styles.currentWord}>
              {this.state.keyword}
            </span>
          </span>
        </div>
        <div className={styles.listWrap}>
          <ListView
            renderRow={row}
            useBodyScroll
            initialListSize={500}
            scrollRenderAheadDistance={500}
            onEndReachedThreshold={10}
            rowIdentities={() => null}
            dataSource={dataSource}
          />
        </div>
        {
          hasMore &&
          <div className={styles.hasMoreWrap}>
            <Icon type="loading" size={'md'} />
          </div>
        }
        {
          showRecommend
          &&
            <div className={styles.emptyWrap}>
              <div className={styles.emptyText}>{this.props.t('empty.empty_search_product')}</div>
              <div className={styles.listWrap}>
                <div className={styles.moreProductTitle}>
                  {t('common.more_recommend_product')}
                </div>
                <ListView
                  renderRow={row}
                  useBodyScroll
                  initialListSize={500}
                  scrollRenderAheadDistance={500}
                  onEndReachedThreshold={10}
                  rowIdentities={() => null}
                  dataSource={recommendSource}
                />
              </div>
            </div>
        }

      </div>
    );
  }
}

SearchResult.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  loadProductList: PropTypes.func,
  location: PropTypes.object,
  productList: PropTypes.array,
  productListLoading: PropTypes.bool,
  totalCount: PropTypes.number,
  currentDisplayWord: PropTypes.string,
  setOtherPageScrollTop: PropTypes.func,
  otherPageScrollTop: PropTypes.number,
  loadRecommendProductList: PropTypes.func,
  recommendProductList: PropTypes.array,
};

const mapStateToProps = createSelector(
  selectProductList(),
  selectProductListLoading(),
  selectCurrentUser(),
  selectTotalCount(),
  selectCurrentDisplayWord(),
  selectOtherPageScrollTop(),
  selectRecommendProductList(),
  (productList, productListLoading, currentUser, totalCount, currentDisplayWord, otherPageScrollTop, recommendProductList) => ({
    productList: (productList && productList.toJS()) || [],
    productListLoading,
    currentUser: currentUser && currentUser.toJS(),
    totalCount,
    currentDisplayWord,
    otherPageScrollTop,
    recommendProductList: (recommendProductList && recommendProductList.toJS()) || [],
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadProductList: (keyword, page) => dispatch(loadProductList(keyword, page)),
  setOtherPageScrollTop: (scrollTop) => dispatch(setOtherPageScrollTop(scrollTop)),
  loadRecommendProductList: (page) => dispatch(loadRecommendProductList(page)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(SearchResult));
