import { createSelector } from 'reselect';

const selectSearchResultDomain = () => (state) => state.get('searchresult');

const selectProductList = () => createSelector(
  selectSearchResultDomain(),
  (substate) => substate.get('productList'),
);

const selectProductListLoading = () => createSelector(
  selectSearchResultDomain(),
  (substate) => substate.get('productListLoading'),
);

const selectTotalCount = () => createSelector(
  selectSearchResultDomain(),
  (substate) => substate.get('totalCount'),
);

const selectCurrentDisplayWord = () => createSelector(
  selectSearchResultDomain(),
  (substate) => substate.get('currentDisplayWord'),
);

export {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
  selectCurrentDisplayWord,
};
