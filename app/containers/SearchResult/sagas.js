import { call, put, race, take } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { DEFAULT_PAGE_SIZE } from 'utils/constants';
import { LOAD_PRODUCT_LIST } from './constants';
import {
  loadProductListSuccess,
} from './actions';

export default [
  getLoadProductListWatcher,
];

export function* getLoadProductListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadProductList: take(LOAD_PRODUCT_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const page = watcher.loadProductList.page;
    const keyword = watcher.loadProductList.keyword;
    const requestConfig = {
      method: 'POST',
      body: {
        keyword,
      },
      urlParams: {
        limit: DEFAULT_PAGE_SIZE,
        start: (page - 1) * DEFAULT_PAGE_SIZE,
      },
    };
    // console.log(page);
    // console.log('saga LOAD_PRODUCT_LIST');
    if (page === 1) {
      requestConfig.feedback = { progress: { mask: true } };
    }
    const requestURL = '/product/search';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      yield put(loadProductListSuccess(list, result.data.total, keyword));
    }
  }
}
