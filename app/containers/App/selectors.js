import { createSelector } from 'reselect';

/**
 * Direct selector to the global state domain
 */
const selectGlobalDomain = () => (state) => state.get('global');

/**
 * Other specific selectors
 */
const selectCurrentLanguage = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('lang'),
);

const selectLocalCurrencyCode = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('localCurrencyCode'),
);

const selectCountryIso = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('countryIso'),
);

const selectCurrentUser = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('currentUser'),
);

const selectUserInfoLoading = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('userInfoLoading'),
);

const selectUnpaidShippingList = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('unpaidShippingList'),
);

const selectPaymentRecords = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('paymentRecords'),
);

const selectUnpaidShippingListLoading = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('unpaidShippingListLoading'),
);

const selectLogining = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('logining'),
);

const selectRegistering = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('registering'),
);

const selectLoadingProduct = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('loadingProduct'),
);

const selectPendingProduct = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('pendingProduct'),
);

const selectLoadingGroup = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('loadingGroup'),
);

const selectPendingGroup = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('pendingGroup'),
);

const selectLatestAction = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('latestAction'),
);

const selectAddress = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('address'),
);

const selectCurrency = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('currency'),
);

const selectExchangeRateTable = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('exchangeRateTable'),
);

const selectStartGroupNotification = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('startGroupNotification'),
);

const selectFollowGroupNotification = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('followGroupNotification'),
);

const selectOpenId = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('openId'),
);

const selectWechatAuthRedirectUrl = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('wechatAuthRedirectUrl'),
);

const selectPurchaseModalVisible = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.getIn(['purchaseModal', 'visible']),
);

const selectCountryPickerModalVisible = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('countryPickerModalVisible'),
);

const selectPurchaseModalType = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.getIn(['purchaseModal', 'purchaseType']),
);

const selectSystemConfig = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('systemConfig'),
);

const selectDefaultGroupConfig = () => createSelector(
  selectSystemConfig(),
  (substate) => substate.get('group'),
);

const selectCouponList = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('coupons'),
);

const selectCouponListLoading = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('couponListLoading'),
);

const selectDeliveryServices = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('deliveryServices'),
);

const selectDeliveryServicesLoading = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('deliveryServicesLoading'),
);

const selectAddressList = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('addressList'),
);

const selectAddressListLoading = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('addressListLoading'),
);

const selectListLayoutType = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('listLayoutType'),
);

const selectLoginCallback = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('loginCallback'),
);

const selectPayPreShipmentRedirectUrl = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('payPreShipmentRedirectUrl'),
);

const selectHomeScrollTop = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('homeScrollTop'),
);

const selectRecommendScrollTop = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('recommendScrollTop'),
);

const selectOtherPageScrollTop = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('otherPageScrollTop'),
);

const selectCurrentCoupon = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('currentCoupon'),
);

const selectLatestCollectCouponTime = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('latestCollectCouponTime'),
);

const selectLatestCollectShareCouponTime = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('latestCollectShareCouponTime'),
);

const selectShowRedPacketAfterLogin = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('showRedPacketAfterLogin'),
);

const selectShowRedPacketModal = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('showRedPacketModal'),
);

const selectShowNewYearNotice = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('showNewYearNotice'),
);

const selectRecommendProductList = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('recommendProductList'),
);

const selectInfoDetailLoading = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('infoDetailLoading'),
);

const selectShowShareRedPacketModal = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('showShareRedPacketModal'),
);

const selectCurrentFixCoupon = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('currentFixCoupon'),
);

const selectCurrentCouponMapOrderId = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('currentCouponMapOrderId'),
);

const selectShowCouponModal = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('showCouponModal'),
);

const selectRegisterFrom = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('registerFrom'),
);

const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectGlobalDomain,
  selectCurrentUser,
  selectCurrentLanguage,
  selectLocalCurrencyCode,
  selectCountryIso,
  selectUserInfoLoading,
  selectLocationState,
  selectLogining,
  selectRegistering,
  selectLoadingProduct,
  selectPendingProduct,
  selectLoadingGroup,
  selectPendingGroup,
  selectLatestAction,
  selectAddress,
  selectCurrency,
  selectExchangeRateTable,
  selectStartGroupNotification,
  selectFollowGroupNotification,
  selectOpenId,
  selectWechatAuthRedirectUrl,
  selectPurchaseModalType,
  selectPurchaseModalVisible,
  selectSystemConfig,
  selectDefaultGroupConfig,
  selectAddressList,
  selectAddressListLoading,
  selectListLayoutType,
  selectLoginCallback,
  selectPayPreShipmentRedirectUrl,
  selectCouponList,
  selectHomeScrollTop,
  selectRecommendScrollTop,
  selectCouponListLoading,
  selectOtherPageScrollTop,
  selectCurrentCoupon,
  selectLatestCollectCouponTime,
  selectShowRedPacketAfterLogin,
  selectShowRedPacketModal,
  selectShowNewYearNotice,
  selectRecommendProductList,
  selectInfoDetailLoading,
  selectLatestCollectShareCouponTime,
  selectShowShareRedPacketModal,
  selectCurrentFixCoupon,
  selectCurrentCouponMapOrderId,
  selectShowCouponModal,
  selectRegisterFrom,
  selectUnpaidShippingList,
  selectUnpaidShippingListLoading,
  selectPaymentRecords,
  selectCountryPickerModalVisible,
  selectDeliveryServices,
  selectDeliveryServicesLoading,
};
