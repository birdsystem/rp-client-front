/* eslint-disable no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';

import 'sanitize.css/sanitize.css';
import i18n from 'i18next';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { withRouter } from 'react-router';
import CopyToClipboard from 'react-copy-to-clipboard';
import QRCode from 'qrcode.react';

import enUS from 'antd-mobile/lib/locale-provider/en_US';
import { LocaleProvider, Carousel, Radio, List } from 'antd-mobile';
import uaHelper from 'utils/uaHelper';
import WindowTitle from 'components/WindowTitle';
import RedPacketEvent from 'containers/RedPacketEvent';
import { localStorageKeys } from 'utils/constants';
import { isGoogleCrawl } from 'utils/commonHelper';
import CountryName from 'components/CountryName';
import CountryIcon from 'components/CountryIcon';
import CommonButton from 'components/CommonButton';
import message from 'components/message';
import {
  eventCategory,
  record,
  actions,
} from 'utils/gaDIRecordHelper';
import {
  changeLanguage,
  loadUserInfo,
  loadExchangeRate,
  changeCountry,
  showCountryPickerModal,
} from './actions';
import {
  selectCurrentLanguage,
  selectShowCouponModal,
  selectCurrentUser,
  selectUserInfoLoading,
  selectCountryPickerModalVisible,
} from './selectors';
import ErrorBoundary from './ErrorBoundary';
import styles from './styles.css';
import t01 from './01.png';
import t02 from './02.png';
import t03 from './03.png';
import t04 from './04.png';
import qrCode from './qrcode.jpg';

const isDevEnv = /localhost/i.test(window.location.hostname) || localStorage.getItem('ttx_text') === 'develop' ||
  (window.location.search && window.location.search.indexOf('develop') >= 0);
const RadioItem = Radio.RadioItem;
const supportedCountries = ['GB'];

class App extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    const { location } = this.props;
    let hideTutorial = false;
    if (location && location.query && location.query.hide_tutorial) {
      hideTutorial = location.query.hide_tutorial;
    }
    const isInitPage = window.location.pathname.indexOf('auth') >= 0 || window.location.href.indexOf('couponpackage') >= 0;
    this.state = {
      showTutorial: !isInitPage && !hideTutorial && !localStorage.getItem(localStorageKeys.HAS_SHOW_TUTORIAL),
      currentTutorialIndex: 0,
      countryIso: '',
    };
    if (!localStorage.getItem(localStorageKeys.HAS_SHOW_TUTORIAL)) {
      record(actions.VIEW_TUTORIAL, {}, eventCategory.USER_EVENT);
    }
    this.onChange = this.onChange.bind(this);
    this.onClickSkip = this.onClickSkip.bind(this);
  }

  componentWillMount() {
    // for production env: handling console errors and send message through telegram bot
    if (/localhost/i.test(window.location.hostname)) return;
    window.onerror = (error, url, line, column, errorObj) => {
      const logMessage = `------ CAUGHT BY App ------\n[LOCATION] ${window.location}\n[ERROR] ${error}\n[SOURCE] ${url}\n[LINE] ${line}\n[COLUMN] ${column}\n[STACK] ${errorObj && errorObj.stack ? errorObj.stack.toString() : ''}\n-------------------------------`;
      console.log(logMessage); // eslint-disable-line
      window.fetch(`https://api.telegram.org/bot569739033:AAHtHuoIAYgcLxHz8CZoc-s-0Mfmo2NO5yk/sendMessage?text=${encodeURI(logMessage)}&chat_id=-1001123482008`);
      if (/Loading chunk [\d]* failed/.test(error)) { // force wechat to reload page
        window.location.reload();
      }
    };
  }

  componentDidMount() {
    const isOpenInWechat = uaHelper.isWechatBrowser();
    // const callApi = isOpenInWechat || isDevEnv;
    const callApi = true;
    this.props.loadExchangeRate();
    if (callApi) {
      if (window.location.href.indexOf('authcallback') < 0) {
        this.props.loadUserInfo();
      }
    }
    if (window.gtag) {
      window.gtag(
        'event', 'pageEnter',
        {
          name: 'app',
          time: (new Date()).getTime(),
          event_callback(res) {
            // console.log(res);
          },
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentUser, userInfoLoading, countryPickerModalVisible } = nextProps;
    const isInitPage = window.location.pathname.indexOf('auth') >= 0 || window.location.href.indexOf('couponpackage') >= 0;

    console.log(currentUser);

    if (!countryPickerModalVisible && !userInfoLoading && !currentUser.countryIso
      && !localStorage.getItem(localStorageKeys.COUNTRY_ISO) && !isInitPage) {
      this.props.showCountryPickerModal();
    }
  }

  onChange(inx) {
    this.setState({
      currentTutorialIndex: inx,
    });
  }

  onClickSkip() {
    localStorage.setItem(localStorageKeys.HAS_SHOW_TUTORIAL, 'true');
    this.setState({
      showTutorial: false,
    });
  }

  changeCountry = (v) => {
    this.setState({
      countryIso: v,
    });
    this.props.changeCountry(v);
  }

  render() {
    let antdLang; // default language for antd-mobile is zh_CN
    if (this.props.currentLanguage === 'en_GB') {
      antdLang = enUS;
    }

    const { countryPickerModalVisible } = this.props;

    const isOpenInWechat = uaHelper.isWechatBrowser();
    const qrCodeUrl = window.location.href || 'https://www.tuantuanxia.com';
    const isHome = window.location.pathname === '' || window.location.pathname === '/';
    // const showTutorial = this.state.showTutorial && !this.props.showCouponModal;
    const showTutorial = false;
    return (
      <div className={styles.app}>
        <ErrorBoundary>
          <WindowTitle defaultTitle />
          <LocaleProvider locale={antdLang}>
            {/* { (isOpenInWechat || isDevEnv || isGoogleCrawl()) ? ( */}
              <div>
                { this.props.children }
                {/* <RedPacketEvent /> */}
                {
                  showTutorial &&
                  <div className={styles.tutorialWrap}>
                    <Carousel
                      effect={'fade'}
                      autoplay={false}
                      afterChange={this.onChange}
                    >
                      <div className={styles.tutorialItem}>
                        <img src={t01} alt={'01'} className={styles.tutorialItemImg} />
                      </div>
                      <div className={styles.tutorialItem}>
                        <img src={t02} alt={'02'} className={styles.tutorialItemImg} />
                      </div>
                      <div className={styles.tutorialItem}>
                        <img src={t03} alt={'03'} className={styles.tutorialItemImg} />
                      </div>
                      <div className={styles.tutorialItem}>
                        <img src={t04} alt={'04'} className={styles.tutorialItemImg} />
                      </div>
                    </Carousel>
                    { this.state.currentTutorialIndex === 3 &&
                    <CommonButton
                      className={styles.skip}
                      onClick={this.onClickSkip}
                    >开始团</CommonButton>
                    }
                  </div>
                }
                {
                  countryPickerModalVisible &&
                  <div className={styles.countryPickerWrap}>
                    <List renderHeader={() => (<div className={styles.countryPickerTitle}>您在哪个国家？</div>)}>
                      {supportedCountries.map((countryIso) => (
                        <RadioItem key={countryIso} checked={this.state.countryIso === countryIso} onChange={() => this.changeCountry(countryIso)}>
                          <div className={styles.countryRow}>
                            <CountryIcon size="large" iso={countryIso} />
                            <CountryName iso={countryIso} className={styles.countryName} />
                          </div>
                        </RadioItem>
                      ))}
                    </List>
                  </div>
                }
              </div>
            {/* ) : (
              <div className={styles.qrWrapper}>
                <p className={styles.qrCodeTips}>为保证您的购物体验<br />请用微信扫一扫打开团团侠<br />注册即送优惠券，最高可得99</p>
                <div className={styles.qrCode}>
                  {
                    isHome ?
                      <CopyToClipboard
                        text={qrCodeUrl}
                        onCopy={() => {
                          message.success(i18n.t('common.copy_success_view_on_wechat'));
                        }}
                      >
                        <img className={styles.qrImg} src={qrCode} alt={'qrcode'} />
                      </CopyToClipboard>
                      :
                      <CopyToClipboard
                        text={qrCodeUrl}
                        onCopy={() => {
                          message.success(i18n.t('common.copy_success_view_on_wechat'));
                        }}
                      >
                        <QRCode className={styles.qrImg} value={qrCodeUrl} />
                      </CopyToClipboard>
                  }
                </div>
              </div>
            ) } */}
          </LocaleProvider>
        </ErrorBoundary>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.node,
  currentLanguage: PropTypes.string,
  loadUserInfo: PropTypes.func,
  loadExchangeRate: PropTypes.func,
  showCouponModal: PropTypes.bool,
  location: PropTypes.object,
  changeCountry: PropTypes.func,
  currentUser: PropTypes.object,
  userInfoLoading: PropTypes.bool,
  countryPickerModalVisible: PropTypes.bool,
  showCountryPickerModal: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentLanguage(),
  selectShowCouponModal(),
  selectCurrentUser(),
  selectUserInfoLoading(),
  selectCountryPickerModalVisible(),
  (currentLanguage, showCouponModal, currentUser, userInfoLoading, countryPickerModalVisible) => ({
    currentLanguage,
    showCouponModal,
    currentUser: currentUser ? currentUser.toJS() : {},
    userInfoLoading,
    countryPickerModalVisible,
  }),
);

function mapDispatchToProps(dispatch) {
  return {
    changeLanguage: (lang) => dispatch(changeLanguage(lang)),
    loadUserInfo: () => dispatch(loadUserInfo()),
    loadExchangeRate: () => dispatch(loadExchangeRate()),
    changeCountry: (countryIso) => dispatch(changeCountry(countryIso)),
    showCountryPickerModal: () => dispatch(showCountryPickerModal()),
    dispatch,
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
