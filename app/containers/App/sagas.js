/* eslint-disable no-unused-vars,no-mixed-operators */
import { call, put, fork, race, take, select } from 'redux-saga/effects';
import request from 'utils/request';
import i18n from 'i18next';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { LOCATION_CHANGE } from 'react-router-redux';
import { DEFAULT_PAGE_SIZE, localStorageKeys } from 'utils/constants';
import message from 'components/message';
import { record, eventCategory, actions, params } from 'utils/gaDIRecordHelper';
import wechatHelper from 'utils/wechatHelper';
import {
  LOAD_USER_INFO,
  REGISTER,
  LOAD_PRODUCT_DETAIL,
  LOAD_GROUP_DETAIL,
  LOGIN,
  UPDATE_RECEIVE_ADDRESS,
  START_GROUP,
  FOLLOW_GROUP,
  LOAD_GROUP_NOTIFICATION,
  WECHAT_AUTH,
  LOGOUT,
  LOAD_SYSTEM_CONFIG,
  ADD_ADDRESS,
  UPDATE_ADDRESS,
  REMOVE_ADDRESS,
  LOAD_ADDRESS_LIST,
  RESET_PASSWORD,
  WECHAT_LOGIN,
  CHANGE_PASSWORD,
  SUBMIT_PRODUCT,
  LOAD_EXCHANGE_RATE,
  CANCEL_ORDER,
  CONFIRM_RECEIPT,
  LOAD_COUPON_LIST,
  LOAD_DELIVERY_SERVICES,
  LOAD_A_COUPON,
  COLLECT_A_COUPON,
  LOAD_RECOMMEND_PRODUCT_LIST,
  LOAD_USER_INFO_DETAIL,
  LOAD_FIX_COUPON,
  LOAD_UNPAID_SHIPPING,
  LOAD_PAYMENT_RECORDS,
  CHANGE_COUNTRY,
} from './constants';
import {
  loadUserInfoSuccess,
  registerSuccess,
  loadProductDetailSuccess,
  loadGroupDetailSuccess,
  loginSuccess,
  setToken,
  clearUserInfo,
  clearToken,
  updateReceiveAddressSuccess,
  clearLatestAction,
  loadUserInfoError,
  loadGroupNotificationSuccess,
  weChatAuthSuccess,
  loadSystemConfigSuccess,
  loadSystemConfigError,
  logoutSuccess,
  logoutError,
  addAddressSuccess,
  addAddressError,
  updateAddressSuccess,
  updateAddressError,
  loadAddressList,
  loadAddressListSuccess,
  loadAddressListError,
  resetPasswordSuccess,
  weChatLoginSuccess,
  changePasswordSuccess,
  clearLoginCallback,
  submitProductSuccess,
  submitProductError,
  loadExchangeRateSuccess,
  loadExchangeRateError,
  cancelOrderSuccess,
  cancelOrderError,
  confirmReceiptSuccess,
  confirmReceiptError,
  loadCouponListSuccess,
  loadCouponListError,
  loadACouponSuccess,
  loadACouponError,
  collectACouponSuccess,
  collectACouponError,
  setShowRedPacketModal,
  setLatestCollectCouponTime,
  loadExchangeRate,
  loadRecommendProductListSuccess,
  loadUserInfoDetail,
  loadUserInfoDetailSuccess,
  loadUserInfoDetailError,
  setShowShareRedPacketModal,
  setLatestCollectShareCouponTime,
  loadFixCouponSuccess,
  setShowCouponModal,
  loadCouponList,
  setLoginCallback,
  loadUnpaidShippingSuccess,
  loadUnpaidShippingError,
  loadPaymentRecordsSuccess,
  loadPaymentRecordsError,
  changeCountrySuccess,
  changeCountryError,
  hideCountryPickerModal,
  loadDeliveryServicesSuccess,
  loadDeliveryServicesError,
} from './actions';

import {
  selectWechatAuthRedirectUrl,
  selectPendingProduct,
  selectCurrentUser,
  selectCountryIso,
} from './selectors';

export default [
  getUserInfoWatcher,
  getRegisterWatcher,
  getLoginWatcher,
  getLoadingProductDetailWatcher,
  getLoadingGroupDetailWatcher,
  getUpdateReceiverAddress,
  getStartGroupWatch,
  getFollowGroupWatch,
  getGroupNotificationWatcher,
  getWechatAuth,
  getSystemConfigWatcher,
  logout,
  getAddAddressWatcher,
  getUpdateAddressWatcher,
  getRemoveAddressWatcher,
  getAddressWatcher,
  resetPassword,
  getWechatLogin,
  changePassword,
  submitProduct,
  getExchangeRate,
  cancelOrder,
  confirmReceipt,
  getCouponListWatcher,
  getACoupon,
  collectACoupon,
  getLoadRecommendProductListWatcher,
  getUserInfoDetailWatch,
  getFixCoupon,
  getUnpaidShippingList,
  getPaymentRecords,
  changeCountry,
  getDeliveryServicesWatcher,
];

export function* changeCountry() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      changeCountry: take(CHANGE_COUNTRY),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const countryIso = watcher.changeCountry.countryIso;
    const requestURL = '/auth/client/user/change-country';
    const requestConfig = {
      method: 'POST',
      body: {
        country_iso: countryIso,
      },
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      yield put(changeCountrySuccess(result.data.country_iso, result.data.local_currency_code));
      yield put(hideCountryPickerModal());
      message.success(i18n.t('common.change_country_success'));
    } else {
      yield put(changeCountryError());
    }
  }
}

export function* getStartGroupWatch() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      startGroup: take(START_GROUP),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.startGroup.data;
    const requestURL = '/group/start';
    const requestConfig = {
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const orderId = result.data.order_id;
      yield put(clearLatestAction());
      message.success(i18n.t('common.start_group_success'));
      browserHistory.push(`/payment/${orderId}`);
    }
  }
}

export function* getFollowGroupWatch() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      followGroup: take(FOLLOW_GROUP),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.followGroup.data;
    const requestURL = '/group/follow';
    const requestConfig = {
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const orderId = result.data.order_id;
      yield put(clearLatestAction());
      message.success(i18n.t('common.follow_group_success'));
      browserHistory.push(`/payment/${orderId}`);
    }
  }
}

export function* getUpdateReceiverAddress() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      updateReceiverAddress: take(UPDATE_RECEIVE_ADDRESS),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.updateReceiverAddress.data;
    // todo
    yield put(updateReceiveAddressSuccess(data));
  }
}

export function* getUserInfoWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadUserInfo: take(LOAD_USER_INFO),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;

    yield call(getUserInfo);
  }
}

export function* getRegisterWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      register: take(REGISTER),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.register.data;
    const requestURL = '/auth/client/public/register';
    const requestConfig = {
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      record(actions.REGISTER_SUCCESS, {}, eventCategory.USER_EVENT);
      message.success(i18n.t('common.register_success'));
      localStorage.setItem(localStorageKeys.TOKEN, result.data.access_token);
      if (result.data.wechat_open_id) {
        localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID,
          result.data.wechat_open_id);
      }
      yield put(setToken(result.data.access_token));
      yield put(registerSuccess(result.data));
      const latestAction = localStorage.getItem(localStorageKeys.LATEST_ACTION);
      const loginCallback = localStorage.getItem(
        localStorageKeys.LOGIN_CALLBACK);
      yield put(clearLoginCallback());
      if (latestAction) {
        browserHistory.replace('/receiveaddress');
      } else if (loginCallback) {
        browserHistory.replace(loginCallback);
      } else {
        browserHistory.replace('/');
      }
    }
  }
}

export function* resetPassword() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      resetPassword: take(RESET_PASSWORD),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.resetPassword.data;
    const requestURL = '/auth/client/public/reset-password';
    const requestConfig = {
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      message.success(i18n.t('common.reset_password_success'));
      yield put(resetPasswordSuccess());
      browserHistory.replace('/login');
    }
  }
}

export function* changePassword() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      changePassword: take(CHANGE_PASSWORD),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.changePassword.data;
    const requestURL = '/auth/client/user/change-password';
    const requestConfig = {
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      message.success(i18n.t('common.change_password_success'));
      yield put(changePasswordSuccess());
      browserHistory.replace('/myinfo');
    }
  }
}

export function* getLoginWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      login: take(LOGIN),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const data = watcher.login.data;
    const requestURL = '/auth/client/public/login';
    const requestConfig = {
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      record(actions.LOGIN_SUCCESS, {}, eventCategory.USER_EVENT);
      message.success(i18n.t('common.login_success'));
      localStorage.setItem(localStorageKeys.TOKEN, result.data.access_token);
      if (result.data.wechat_open_id) {
        localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID,
          result.data.wechat_open_id);
      }
      yield put(setToken(result.data.access_token));
      yield put(loginSuccess(result.data));
      yield put(loadAddressList());
      const latestAction = localStorage.getItem(localStorageKeys.LATEST_ACTION);
      const loginCallback = localStorage.getItem(
        localStorageKeys.LOGIN_CALLBACK);
      yield put(clearLoginCallback());
      if (latestAction) {
        browserHistory.replace('/receiveaddress');
      } else if (loginCallback) {
        browserHistory.replace(loginCallback);
      } else {
        browserHistory.replace('/');
      }
    }
  }
}

export function* getWechatLogin() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      wechatLogin: take(WECHAT_LOGIN),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;
    const code = watcher.wechatLogin.code;
    const from = watcher.wechatLogin.from;
    const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO);
    const data = {
      code,
    };
    if (countryIso) {
      data.country_iso = countryIso;
    }
    if (from) {
      data.from = from;
      record(`from_${from}`, {}, eventCategory.USER_EVENT);
    }
    const requestURL = '/auth/client/public/login-wechat';
    const requestConfig = {
      customHandler: true,
      method: 'POST',
      body: data,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      record(actions.LOGIN_SUCCESS, {}, eventCategory.USER_EVENT);
      yield put(weChatLoginSuccess(result.data));
      message.success(i18n.t('common.login_success'));
      localStorage.setItem(localStorageKeys.TOKEN, result.data.access_token);
      if (result.data.wechat_open_id) {
        localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID,
          result.data.wechat_open_id);
      }
      if (result.data.is_new_user && from && from !== 'wechat_push') {
        yield put(loadCouponList({ showLoading: false }));
        yield put(setShowCouponModal(true, from));
      }
      yield put(setToken(result.data.access_token));
      yield put(loginSuccess(result.data));
      yield put(loadAddressList());
      const latestAction = localStorage.getItem(localStorageKeys.LATEST_ACTION);
      const loginCallback = localStorage.getItem(
        localStorageKeys.LOGIN_CALLBACK);
      yield put(clearLoginCallback());
      if (latestAction) {
        browserHistory.replace('/receiveaddress');
      } else if (loginCallback) {
        browserHistory.replace(loginCallback);
      } else {
        let defaultUrl = '/';
        if (result.data.is_new_user && from && from !== 'wechat_push') {
          defaultUrl = '/couponpackage';
        }
        if (from === 'wechat_push') {
          defaultUrl = '/recommend?get_reward=true';
        }
        browserHistory.replace(defaultUrl);
      }
    } else if (`${result.errCode}` === '1105') {
      // storage and go login page
      browserHistory.push({
        pathname: '/login',
        state: {
          wechatInfo: {
            wechat_open_id: result.data.wechat_open_id,
            wechat_avatar_url: result.data.wechat_avatar_url,
            wechat_name: result.data.wechat_name,
          },
        },
      });
    } else {
      message.error(result.message);
      browserHistory.push('/');
    }
  }
}

export function* getLoadingProductDetailWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadProductDetail: take(LOAD_PRODUCT_DETAIL),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;

    const id = watcher.loadProductDetail.id;
    const countryIso = yield select(selectCountryIso());
    const requestURL = `/product/${id}?country_iso=${countryIso}`;
    const requestConfig = {
      feedback: { progress: { mask: true } },
    };
    const product = yield select(selectPendingProduct());
    if (watcher.loadProductDetail.hideLoading || product) {
      requestConfig.feedback = {};
    }

    if (watcher.loadProductDetail.recordToHistory) {
      requestConfig.urlParams = {
        record_to_history: true,
      };
    }

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      const data = result.data;
      yield put(loadProductDetailSuccess(data));
    }
  }
}

export function* getLoadingGroupDetailWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadGroupDetail: take(LOAD_GROUP_DETAIL),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const id = watcher.loadGroupDetail.id;
    const requestURL = `/group/view/${id}`;
    let requestConfig = {
      feedback: { progress: { mask: true } },
    };
    if (watcher.loadGroupDetail.hideLoading) {
      requestConfig = {};
    }
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadGroupDetailSuccess(result.data));
    }
  }
}

export function* getGroupNotificationWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadGroupNotification: take(LOAD_GROUP_NOTIFICATION),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;
    const type = watcher.loadGroupNotification.nType;
    const requestURL = `/group/notification?type=${type}`;
    const result = yield call(request, requestURL);
    if (result.success) {
      yield put(loadGroupNotificationSuccess(result.data, type));
    }
  }
}

export function* getUserInfo() {
  const requestURL = '/auth/client/user/info';
  const requestConfig = {
    customHandler: true,
  };
  const result = yield call(request, requestURL, requestConfig);
  // server return error
  if (result.success && result.data && result.data.username) {
    localStorage.setItem(localStorageKeys.TOKEN, result.data.access_token);
    if (result.data.wechat_open_id) {
      localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID,
        result.data.wechat_open_id);
    }
    const currentUser = result.data;
    yield put(loadUserInfoSuccess(currentUser));
    yield put(loadAddressList());
    if (wechatHelper.isOpenInWechat() && currentUser.wechat_open_id && !currentUser.wechat_union_id) {
      // const url = wechatHelper.genInWechatAuthUrl('login');
      // yield put(setLoginCallback(window.location.pathname));
      // window.location = url;
    }
  } else if (localStorage.getItem(localStorageKeys.TOKEN) && wechatHelper.isOpenInWechat()) {
    // auto auth
    yield put(clearUserInfo());
    yield put(clearToken());
    yield put(loadUserInfoError());
  } else {
    yield put(clearUserInfo());
    yield put(clearToken());
    yield put(loadUserInfoError());
  }
}

export function* getUserInfoDetailWatch() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadUserInfoDetail: take(LOAD_USER_INFO_DETAIL),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;
    const requestURL = '/auth/client/user/info-detail';
    let requestConfig = {
      customHandler: true,
      feedback: { progress: { mask: true } },
    };
    const currentUser = yield select(selectCurrentUser());
    if (!_.isEmpty(currentUser) && currentUser.toJS) {
      const user = currentUser.toJS();
      if (user && user.has_detail) {
        requestConfig = {
          customHandler: true, // hide loading progress
        };
      }
    }
    const result = yield call(request, requestURL, requestConfig);
    // const result = {"success":true,"errCode":"","message":"","data":{"id":1,"valid_coupon_count": 100, "telephone":"12345678901","dial_code":"86","username":"test","balance":"0.00","wechat_open_id":null,"wechat_name":null,"wechat_avatar_url":null,"status":"ACTIVE","create_time":"2019-01-30 03:00:24","update_time":"2019-01-30 03:00:24","access_token":"0276f11f303a53acbee9afa2b244d7fa","history_count":199,"coupon_count":200,"start_group_count":12,"follow_group_count":3,"progress_group_count":2,"finish_group_count":2}}

    if (result.success && result.data && result.data.username) {
      const user = result.data;
      yield put(loadUserInfoDetailSuccess(user));
    } else {
      yield put(loadUserInfoDetailError());
    }
  }
}

export function* getWechatAuth() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      wechatAuth: take(WECHAT_AUTH),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;
    const code = watcher.wechatAuth.code;
    const state = watcher.wechatAuth.state;
    const requestURL = '/auth/client/user/auth-wechat';
    const requestConfig = {
      method: 'POST',
      body: {
        code,
      },
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    let wechatAuthRedirectUrl = yield select(selectWechatAuthRedirectUrl());
    wechatAuthRedirectUrl = wechatAuthRedirectUrl || '/';
    if (result.success) {
      yield put(weChatAuthSuccess(result.data));
      if (result.data.wechat_open_id) {
        localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID,
          result.data.wechat_open_id);
      }
      if (state === 'register') {
        browserHistory.push({
          pathname: '/register',
          state: {
            wechatInfo: {
              wechat_open_id: result.data.wechat_open_id,
              wechat_avatar_url: result.data.wechat_avatar_url,
              wechat_name: result.data.wechat_name,
            },
          },
        });
      } else {
        browserHistory.replace(wechatAuthRedirectUrl);
      }
    } else {
      browserHistory.replace('/');
    }
  }
}

export function* getSystemConfigWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadConfig: take(LOAD_SYSTEM_CONFIG),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = '/group/config';

    const result = yield call(request, requestURL);
    if (result.success) {
      yield put(loadSystemConfigSuccess(result.data));
    } else {
      yield put(loadSystemConfigError());
    }
  }
}

export function* logout() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      logout: take(LOGOUT),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/auth/client/user/logout';
    const requestConfig = {
      method: 'POST',
      body: {},
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      yield put(logoutSuccess());
      yield put(loadExchangeRate());
      browserHistory.push('/');
    } else {
      yield put(logoutError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* getAddressWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadAddress: take(LOAD_ADDRESS_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;
    const requestURL = '/group/user-address';

    const result = yield call(request, requestURL);
    if (result.success) {
      yield put(loadAddressListSuccess(result.data));
    } else {
      yield put(loadAddressListError());
    }
  }
}

export function* getAddAddressWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      addAddress: take(ADD_ADDRESS),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;
    const requestConfig = {
      method: 'POST',
      body: watcher.addAddress.data,
      feedback: { progress: { mask: true } },
    };
    const requestURL = '/group/user-address';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      message.success(i18n.t('address.add_success'));
      yield put(addAddressSuccess(result.data));
      yield put(loadAddressList());
    } else {
      yield put(addAddressError());
    }
  }
}

export function* getUpdateAddressWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      updateAddress: take(UPDATE_ADDRESS),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;
    const requestConfig = {
      method: 'POST',
      body: watcher.updateAddress.data,
      feedback: { progress: { mask: true } },
    };
    const requestURL = `/group/user-address/${watcher.updateAddress.data.id}`;
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      if (actions.showSuccessMessage) {
        message.success(i18n.t('address.update_success'));
      }
      yield put(updateAddressSuccess(result.data));
      yield put(loadAddressList());
    } else {
      yield put(updateAddressError());
    }
  }
}

export function* getRemoveAddressWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      removeAddress: take(REMOVE_ADDRESS),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;
    const requestConfig = {
      method: 'DELETE',
      feedback: { progress: { mask: true } },
    };
    const requestURL = `/group/user-address/${watcher.removeAddress.id}`;
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      message.success(i18n.t('address.delete_success'));
      yield put(updateAddressSuccess(result.data));
      yield put(loadAddressList());
    } else {
      yield put(updateAddressError());
    }
  }
}

export function* submitProduct() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      submit: take(SUBMIT_PRODUCT),
      stop: take(LOCATION_CHANGE),
    });
    if (watcher.stop) break;

    const data = watcher.submit.data;
    const requestConfig = {
      method: 'POST',
      body: Object.assign({ is_from_client: true }, data),
      feedback: { progress: { mask: true, hint: '小团正在解析商品信息，请小主耐心等待一下哦' } },
    };
    const requestURL = '/product/parse';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      message.success(i18n.t('common.submit_product_success'));
      yield put(submitProductSuccess(result.data));
      browserHistory.push({
        pathname: '/follow-official-account',
        state: { product: result.data },
      });
    } else if (`${result.errCode}` === '1301' && result.data &&
      result.data.maximum_price) {
      message.error(
        i18n.t('error.max_price_limit', { price: result.data.maximum_price }));
      yield put(submitProductError());
    } else {
      yield put(submitProductError());
    }
  }
}

export function* getExchangeRate() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadRate: take(LOAD_EXCHANGE_RATE),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = '/product/exchange-rate';
    const result = yield call(request, requestURL);
    // const result = {
    //   success: true,
    //   data: {
    //     gbp_to_cny: 8.7,
    //     eur_to_cny: 7.9,
    //     usd_to_cny: 6.8,
    //   },
    // };

    if (result.success) {
      yield put(loadExchangeRateSuccess(result.data));
    } else {
      yield put(loadExchangeRateError());
    }
  }
}

export function* cancelOrder() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      cancelOrder: take(CANCEL_ORDER),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const id = watcher.cancelOrder.id;
    const hasPay = watcher.cancelOrder.hasPay;
    console.log(id);
    const requestConfig = {
      method: 'POST',
      body: {
        id,
      },
      feedback: { progress: { mask: true } },
    };
    const requestURL = '/group/order/cancel';
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      message.success(i18n.t('common.cancel_order_success'));
      if (hasPay) {
        record(actions.REFUND, {
          transaction_id: `${id}`,
        });
      }
      yield put(cancelOrderSuccess(result.data));
    } else {
      yield put(cancelOrderError());
    }
  }
}

export function* confirmReceipt() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      confirm: take(CONFIRM_RECEIPT),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const id = watcher.confirm.id;
    const requestConfig = {
      method: 'POST',
      body: {
        id,
      },
      feedback: { progress: { mask: true } },
    };
    const requestURL = '/group/order/confirm-receipt';
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      if (parseFloat(result.data.cash_back_amount) > 0) {
        message.success(i18n.t('common.confirm_receipt_success_with_cashback', { cashback: parseFloat(result.data.cash_back_amount) }));
      } else {
        message.success(i18n.t('common.confirm_receipt_success'));
      }
      yield put(confirmReceiptSuccess(result.data));
    } else {
      yield put(confirmReceiptError());
    }
  }
}

export function* getCouponListWatcher() {
  while (true) { // eslint-disable-line no-constant-condition
    const watcher = yield race({
      loadCoupons: take(LOAD_COUPON_LIST),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    yield fork(getCouponList, watcher.loadCoupons);
  }
}

export function* getCouponList(props) {
  const { isValid, isExpired, isUsed, orderId, showLoading = false } = props;

  const requestURL = '/payment/coupon?limit=-1';

  const requestConfig = {
    urlParams: Object.assign({}, { type: 'COUPON' }),
  };

  if (showLoading) {
    requestConfig.feedback = {
      progress: { mask: true },
    };
  }

  if (isValid) {
    requestConfig.urlParams.is_valid = isValid;
  }

  if (isExpired) {
    requestConfig.urlParams.is_valid = '0';
    requestConfig.urlParams.is_expired = isExpired;
  }

  if (isUsed) {
    requestConfig.urlParams.is_valid = '0';
    requestConfig.urlParams.is_used = isUsed;
  }

  if (orderId) {
    requestConfig.urlParams.order_id = orderId;
  }

  const result = yield call(request, requestURL, requestConfig);

  // const result = {
  //   success: true,
  //   data: {
  //     list: [{
  //       id: 1,
  //       name: '满100减10',
  //       apply_scope: 'ORDER_TOTAL',
  //       discount_type: 'AMOUNT', // 优惠作用类型：1. AMOUNT 直接减去一个金额 2. RATE 乘以一个折扣
  //       discount: '10', // 在类型为 AMOUNT 的时候，表示优惠金额，这里为优惠20，货币为人民币
  //       discount_threshold: '100', // 优惠门槛，大于等于这个金额才能使用，货币为人民币
  //       start_time: '2017-11-28 08:00:00', // 优惠开始时间，还没到这个时间就不能使用
  //       end_time: '2018-12-31 08:00:00', // 优惠结束时间，超过这个时间为无效
  //       is_valid: 1, // 此优惠券是否还有效
  //       is_used: 0, // 是否已经使用过
  //       is_expired: 0,
  //     }, {
  //       id: 2,
  //       apply_scope: 'PRODUCT_SUBTOTAL',
  //       discount_type: 'AMOUNT', // 优惠作用类型：1. AMOUNT 直接减去一个金额 2. RATE 乘以一个折扣
  //       name: '无门槛20元',
  //       discount: '20', // 在类型为 AMOUNT 的时候，表示优惠金额，这里为优惠20，货币为人民币
  //       discount_threshold: 0, // 优惠门槛，大于等于这个金额才能使用，货币为人民币
  //       start_time: '2017-11-28 08:00:00', // 优惠开始时间，还没到这个时间就不能使用
  //       end_time: '2018-12-31 08:00:00', // 优惠结束时间，超过这个时间为无效
  //       is_valid: 0, // 此优惠券是否还有效
  //       is_used: 1, // 是否已经使用过
  //       is_expired: 0,
  //     }, {
  //       id: 4,
  //       name: '8折',
  //       discount_type: 'RATE', // 优惠作用类型：1. AMOUNT 直接减去一个金额 2. RATE 乘以一个折扣
  //       apply_scope: 'CROSS_BORDER_SHIPPING_SUBTOTAL',
  //       discount: '0.8', // 在类型为 AMOUNT 的时候，表示优惠金额，这里为优惠20，货币为人民币
  //       discount_threshold: 0, // 优惠门槛，大于等于这个金额才能使用，货币为人民币
  //       start_time: '2017-11-28 08:00:00', // 优惠开始时间，还没到这个时间就不能使用
  //       end_time: '2018-12-31 08:00:00', // 优惠结束时间，超过这个时间为无效
  //       is_valid: 0, // 此优惠券是否还有效
  //       is_used: 0, // 是否已经使用过
  //       is_expired: 1,
  //     }, {
  //       id: 3,
  //       name: '9折',
  //       discount_type: 'RATE', // 优惠作用类型：1. AMOUNT 直接减去一个金额 2. RATE 乘以一个折扣
  //       apply_scope: 'CROSS_BORDER_SHIPPING_SUBTOTAL',
  //       discount: '0.9', // 在类型为 AMOUNT 的时候，表示优惠金额，这里为优惠20，货币为人民币
  //       discount_threshold: 0, // 优惠门槛，大于等于这个金额才能使用，货币为人民币
  //       start_time: '2017-11-28 08:00:00', // 优惠开始时间，还没到这个时间就不能使用
  //       end_time: '2018-12-31 08:00:00', // 优惠结束时间，超过这个时间为无效
  //       is_valid: 0, // 此优惠券是否还有效
  //       is_used: 0, // 是否已经使用过
  //       is_expired: 0,
  //     }],
  //   },
  // };

  if (result.success) {
    yield put(loadCouponListSuccess(result.data.list));
  } else {
    yield put(loadCouponListError());
    console.log(result.message); // eslint-disable-line no-console
  }
}

export function* getDeliveryServicesWatcher() {
  while (true) { // eslint-disable-line no-constant-condition
    const watcher = yield race({
      load: take(LOAD_DELIVERY_SERVICES),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    yield fork(getDeliveryServices, watcher.load);
  }
}

export function* getDeliveryServices(props) {
  const requestURL = '/delivery-service?limit=-1';

  // const result = yield call(request, requestURL);

  const result = {
    success: true,
    data: {
      list: [{
        id: 1,
        name: '满100减10',
      }, {
        id: 2,
        name: '无门槛20元',
      }],
    },
  };

  if (result.success) {
    yield put(loadDeliveryServicesSuccess(result.data.list));
  } else {
    yield put(loadDeliveryServicesError());
    console.log(result.message); // eslint-disable-line no-console
  }
}

export function* getACoupon() {
  while (true) { // eslint-disable-line no-constant-condition
    const watcher = yield race({
      loadACoupon: take(LOAD_A_COUPON),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestConfig = {
      method: 'GET',
    };

    if (watcher.loadACoupon.showLoading) {
      requestConfig.feedback = { progress: { mask: true } };
    }

    const requestURL = '/payment/a-coupon';
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadACouponSuccess(result.data));
    } else {
      yield put(loadACouponError());
    }
  }
}

export function* collectACoupon() {
  while (true) { // eslint-disable-line no-constant-condition
    const watcher = yield race({
      collect: take(COLLECT_A_COUPON),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const fromSinglePage = !!watcher.collect.data.fromSinglePage;
    const requestConfig = {
      method: 'POST',
      body: watcher.collect.data,
      feedback: { progress: { mask: true } },
    };
    const requestURL = '/payment/collect-coupon';
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      if (fromSinglePage) {
        message.success(i18n.t('common.collect_coupon_success'));
        browserHistory.push('/');
      } else {
        yield put(setShowRedPacketModal(true));
      }
      yield put(collectACouponSuccess());
    } else {
      if (`${result.errCode}` === '1208' && result.data &&
        result.data.limit_time) {
        const lastCollectTime = (new Date()).getTime() +
          (result.data.limit_time * 1000) - 86400000;
        yield put(setLatestCollectCouponTime(lastCollectTime));
      }
      yield put(collectACouponError());
    }
  }
}

export function* getLoadRecommendProductListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadRecommendProductList: take(LOAD_RECOMMEND_PRODUCT_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const page = watcher.loadRecommendProductList.page;
    const requestConfig = {
      urlParams: {
        limit: DEFAULT_PAGE_SIZE,
        start: (page - 1) * DEFAULT_PAGE_SIZE,
      },
    };
    const requestURL = '/product';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      yield put(loadRecommendProductListSuccess(list, result.data.total));
    }
  }
}

// auto collect
export function* getFixCoupon() {
  while (true) { // eslint-disable-line no-constant-condition
    const watcher = yield race({
      collect: take(LOAD_FIX_COUPON),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestConfig = {
      method: 'GET',
      feedback: { progress: { mask: true } },
    };
    const requestURL = '/payment/get-coupon';
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadFixCouponSuccess(result.data));
      yield put(loadUserInfoDetail());
      yield put(setShowShareRedPacketModal(true));
      yield put(setLatestCollectShareCouponTime((new Date()).getTime()));
    }
  }
}

export function* getUnpaidShippingList() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      load: take(LOAD_UNPAID_SHIPPING),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = '/payment/unpaid-cross-border-freight?limit=-1';
    const result = yield call(request, requestURL);

    if (result.success) {
      yield put(loadUnpaidShippingSuccess(result.data.list));
    } else {
      yield put(loadUnpaidShippingError());
    }
  }
}

export function* getPaymentRecords() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      load: take(LOAD_PAYMENT_RECORDS),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = '/payment/statement?limit=-1';
    const requestConfig = {
      method: 'GET',
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadPaymentRecordsSuccess(result.data.list));
    } else {
      yield put(loadPaymentRecordsError());
    }
  }
}
