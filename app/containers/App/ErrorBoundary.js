import React from 'react';
import PropTypes from 'prop-types';
import loadingImg from './loading.gif';
import styles from './styles.css';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info = { componentStack: '' }) {
    const isWechatCacheProblem = /Loading chunk [\d]* failed/.test(error);
    if (!isWechatCacheProblem) {
      this.setState({ hasError: true });
    }
    // for production env: handling console errors and send message through telegram bot
    if (/localhost/i.test(window.location.hostname)) return;
    const stackInfo = info.componentStack.split('\n')[1].trim();
    const message = `-- CAUGHT BY ErrorBoundary --\n[LOCATION] ${window.location}\n[ERROR] ${error}\n[STACK INFO] ${stackInfo}\n---------------------------------`;
    console.log(message); // eslint-disable-line
    window.fetch(`https://api.telegram.org/bot569739033:AAHtHuoIAYgcLxHz8CZoc-s-0Mfmo2NO5yk/sendMessage?text=${encodeURI(message)}&chat_id=-1001123482008`);
    if (isWechatCacheProblem) { // force wechat to reload page
      window.location.reload();
    }
  }

  render() {
    if (this.state.hasError) {
      // Render any custom fallback UI
      return (
        <div className={styles.errorBoundaryWrapper}>
          <div className={styles.errorBoundaryImgWrapper}>
            <img src={loadingImg} alt="Loading..." className={styles.errorBoundaryImg} />
          </div>
          <p>啊哦，小团遇到问题了</p>
          <p>程序员小哥哥小姐姐正在赶来的路上</p>
          <p>再一下下就好了！</p>
        </div>
      );
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node,
};

export default ErrorBoundary;
