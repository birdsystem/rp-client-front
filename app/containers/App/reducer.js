/* eslint-disable no-unused-vars,no-plusplus */
/*
 *
 * Global reducer
 *
 */

import { fromJS } from 'immutable';
import i18next from 'i18next';
import _ from 'lodash';
import {
  GROUP_LEADER_BUYING,
  localStorageKeys,
  DEFAULT_COUNTRY_ISO,
  DEFAULT_LOCAL_CURRENCY_CODE,
} from 'utils/constants';
import {
  genLrcQueue,
} from 'utils/commonHelper';
import { isBackendYes, isBackendNo } from 'utils/backendHelper';
import {
  CHANGE_LANGUAGE,
  CHANGE_COUNTRY_SUCCESS,
  LOAD_USER_INFO,
  LOAD_USER_INFO_SUCCESS,
  LOAD_USER_INFO_ERROR,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
  LOAD_PRODUCT_DETAIL,
  LOAD_PRODUCT_DETAIL_SUCCESS,
  LOAD_PRODUCT_DETAIL_ERROR,
  LOAD_GROUP_DETAIL,
  LOAD_GROUP_DETAIL_SUCCESS,
  LOAD_GROUP_DETAIL_ERROR,
  SET_LATEST_ACTION,
  SET_TOKEN,
  CLEAR_TOKEN,
  CLEAR_USER_INFO,
  UPDATE_RECEIVE_ADDRESS_SUCCESS,
  CLEAR_LATEST_ACTION,
  LOAD_GROUP_NOTIFICATION_SUCCESS,
  WECHAT_AUTH_SUCCESS,
  WECHAT_CLEAR_OPEN_ID,
  SET_WECHAT_AUTH_REDIRECT_URL,
  SHOW_PURCHASE_MODAL,
  HIDE_PURCHASE_MODAL,
  // LOAD_SYSTEM_CONFIG,
  // LOAD_SYSTEM_CONFIG_ERROR,
  LOAD_SYSTEM_CONFIG_SUCCESS,
  ADD_ADDRESS_SUCCESS,
  ADD_ADDRESS_ERROR,
  UPDATE_ADDRESS_SUCCESS,
  UPDATE_ADDRESS_ERROR,
  UPDATE_ADDRESS_LIST_SUCCESS,
  UPDATE_ADDRESS_LIST_ERROR,
  LOAD_ADDRESS_LIST,
  LOAD_ADDRESS_LIST_SUCCESS,
  LOAD_ADDRESS_LIST_ERROR,
  REMOVE_ADDRESS_SUCCESS,
  LIST_LAYOUT_TYPE,
  UPDATE_LIST_LAYOUT_TYPE,
  SET_LOGIN_CALLBACK,
  CLEAR_LOGIN_CALLBACK,
  LOAD_EXCHANGE_RATE,
  LOAD_EXCHANGE_RATE_SUCCESS,
  LOAD_EXCHANGE_RATE_ERROR,
  SET_PAY_PRE_SHIPMENT_REDIRECT_URL,
  CLEAR_PAY_PRE_SHIPMENT_REDIRECT_URL,
  LOAD_COUPON_LIST_SUCCESS,
  SET_HOME_SCROLL_TOP,
  SET_RECOMMEND_SCROLL_TOP,
  LOAD_COUPON_LIST,
  LOAD_COUPON_LIST_ERROR,
  SET_OTHER_PAGE_SCROLL_TOP,
  CLEAR_OTHER_PAGE_SCROLL_TOP,
  LOAD_A_COUPON,
  LOAD_A_COUPON_SUCCESS,
  LOAD_A_COUPON_ERROR,
  COLLECT_A_COUPON_SUCCESS,
  SET_LATEST_COLLECT_COUPON_TIME,
  SET_LATEST_COLLECT_SHARE_COUPON_TIME,
  SET_SHOW_RED_PACKET_AFTER_LOGIN,
  SET_SHOW_RED_PACKET_MODAL,
  LOAD_RECOMMEND_PRODUCT_LIST,
  LOAD_RECOMMEND_PRODUCT_LIST_SUCCESS,
  LOAD_USER_INFO_DETAIL,
  LOAD_USER_INFO_DETAIL_SUCCESS,
  LOAD_USER_INFO_DETAIL_ERROR,
  SET_SHOW_SHARE_RED_PACKET_MODAL,
  LOAD_FIX_COUPON_SUCCESS, LOAD_FIX_COUPON,
  SET_SHOW_COUPON_MODAL,
  LOAD_UNPAID_SHIPPING,
  LOAD_UNPAID_SHIPPING_ERROR,
  LOAD_UNPAID_SHIPPING_SUCCESS,
  LOAD_PAYMENT_RECORDS,
  LOAD_PAYMENT_RECORDS_SUCCESS,
  LOAD_PAYMENT_RECORDS_ERROR,
  SHOW_COUNTRY_PICKER_MODAL,
  HIDE_COUNTRY_PICKER_MODAL,
  LOAD_DELIVERY_SERVICES,
  LOAD_DELIVERY_SERVICES_ERROR,
  LOAD_DELIVERY_SERVICES_SUCCESS,
} from './constants';

const productLrcQueue = genLrcQueue();

const initialState = fromJS({
  // lang: localStorage.getItem('ttxLang') || getBrowserDefaultLanguage() || 'zh_CN',
  lang: 'zh_CN',

  callbackRoute: localStorage.getItem('callback') || false,

  userInfoLoading: false,

  currentUser: null,

  logining: false,

  registering: false,

  loadingProduct: false,

  pendingGroup: null,

  pendingProduct: null,

  loadingGroup: false,

  coupons: fromJS([]),
  couponListLoading: false,

  deliveryServices: fromJS([]),
  deliveryServicesLoading: false,

  // {type: '"GROUP_LEADER_BUYING" | "SINGLE_BUYING" | "GROUP_BUYING"', productId:'1', groupId:'1', count:1, minPeople: 10, sku: 'xxx'}
  latestAction: localStorage.getItem(localStorageKeys.LATEST_ACTION) &&
  JSON.parse(localStorage.getItem(localStorageKeys.LATEST_ACTION)),

  token: localStorage.getItem(localStorageKeys.TOKEN),

  address: localStorage.getItem(localStorageKeys.ADDRESS) &&
  JSON.parse(localStorage.getItem(localStorageKeys.ADDRESS)),

  currency: 'CNY',

  exchangeRateTable: localStorage.getItem(localStorageKeys.EXCHANGE_RATE_TABLE) && JSON.parse(localStorage.getItem(localStorageKeys.EXCHANGE_RATE_TABLE)),

  countryIso: localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO,

  localCurrencyCode: localStorage.getItem(localStorageKeys.LOCAL_CURRENCY_CODE) || DEFAULT_LOCAL_CURRENCY_CODE,

  countryPickerModalVisible: false,

  startGroupNotification: {},

  followGroupNotification: {},

  openId: localStorage.getItem(localStorageKeys.WECHAT_OPEN_ID) || '',

  wechatAuthRedirectUrl: localStorage.getItem(localStorageKeys.WECHAT_REDIRECT_URL) || '/',

  purchaseModal: {
    visible: false,
    purchaseType: GROUP_LEADER_BUYING,
  },

  systemConfig: {},

  addressListLoading: false,

  addressList: [],

  listLayoutType: localStorage.getItem(localStorageKeys.LIST_LAYOUT_TYPE) || LIST_LAYOUT_TYPE.TWO_COLUMN,

  loginCallback: (localStorage.getItem(localStorageKeys.LOGIN_CALLBACK)) || '',

  payPreShipmentRedirectUrl: '/',

  homeScrollTop: 0,

  recommendScrollTop: 0,

  otherPageScrollTop: 0, // use for category product list and search list

  currentCoupon: null,

  latestCollectCouponTime: (localStorage.getItem(localStorageKeys.LATEST_COLLECT_COUPON_TIME)
    && parseInt(localStorage.getItem(localStorageKeys.LATEST_COLLECT_COUPON_TIME), 10)) || 0,

  latestCollectShareCouponTime: (localStorage.getItem(localStorageKeys.LATEST_COLLECT_SHARE_COUPON_TIME)
    && parseInt(localStorage.getItem(localStorageKeys.LATEST_COLLECT_SHARE_COUPON_TIME), 10)) || 0,

  showRedPacketAfterLogin: !!localStorage.getItem(localStorageKeys.SHOW_RED_PACKET_AFTER_LOGIN),

  showRedPacketModal: false,

  showNewYearNotice: false,

  recommendProductList: [],

  infoDetailLoading: false,

  currentFixCoupon: null,

  showShareRedPacketModal: false,

  currentCouponMapOrderId: -1,

  showCouponModal: false,

  registerFrom: '',

  unpaidShippingList: fromJS([]),
  unpaidShippingListLoading: false,

  paymentRecords: fromJS([]),
});

function setCountryAndCurrency(countryIso, currencyCode) {
  localStorage.setItem(localStorageKeys.COUNTRY_ISO, countryIso);
  localStorage.setItem(localStorageKeys.LOCAL_CURRENCY_CODE, currencyCode);
}

function globalReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      i18next.changeLanguage(action.lang);
      localStorage.setItem('ttxLang', action.lang);
      return state.set('lang', action.lang);
    case LOAD_USER_INFO:
      return state.set('userInfoLoading', true);
    case LOAD_USER_INFO_SUCCESS:
      {
        let user = action.currentUser;
        let currentUser = state.get('currentUser') ? state.get('currentUser').toJS() : {};
        if (!currentUser) {
          currentUser = {};
        }
        user = Object.assign(currentUser, user);
        window.user = user;

        // localStorage.setItem(localStorageKeys.COUNTRY_ISO, user.country_iso);
        // localStorage.setItem(localStorageKeys.LOCAL_CURRENCY_CODE, user.local_currency_code);

        setCountryAndCurrency(user.country_iso || DEFAULT_COUNTRY_ISO, user.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE);

        return state.set('currentUser', fromJS(user))
          .set('localCurrencyCode', user.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE)
          .set('countryIso', user.country_iso || DEFAULT_COUNTRY_ISO)
          .set('userInfoLoading', false);
      }
    case LOAD_USER_INFO_ERROR:
      return state.set('userInfoLoading', false);
    case SHOW_COUNTRY_PICKER_MODAL:
      return state.set('countryPickerModalVisible', true);
    case HIDE_COUNTRY_PICKER_MODAL:
      return state.set('countryPickerModalVisible', false);
    case CHANGE_COUNTRY_SUCCESS:
      {
        // localStorage.setItem(localStorageKeys.COUNTRY_ISO, action.countryIso);
        // localStorage.setItem(localStorageKeys.LOCAL_CURRENCY_CODE, action.localCurrencyCode);

        setCountryAndCurrency(action.countryIso, action.localCurrencyCode);

        const currentUser = state.get('currentUser') ? state.get('currentUser').toJS() : {};

        if (!_.isEmpty(currentUser)) {
          state.setIn(['currentUser', 'local_currency_code'], action.localCurrencyCode)
          .setIn(['currentUser', 'country_iso'], action.countryIso);
        }

        return state
          .set('localCurrencyCode', action.localCurrencyCode)
          .set('countryIso', action.countryIso);
      }
    case LOGIN:
      return state.set('logining', true);
    case LOGIN_SUCCESS:
      window.user = action.currentUser;

      // localStorage.setItem(localStorageKeys.COUNTRY_ISO, action.currentUser.country_iso);
      // localStorage.setItem(localStorageKeys.LOCAL_CURRENCY_CODE, action.currentUser.local_currency_code);

      setCountryAndCurrency(action.currentUser.country_iso || DEFAULT_COUNTRY_ISO, action.currentUser.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE);

      return state.set('logining', false)
        .set('currentUser', fromJS(action.currentUser))
        .set('localCurrencyCode', action.currentUser.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE)
        .set('countryIso', action.currentUser.country_iso || DEFAULT_COUNTRY_ISO);
    case LOGIN_ERROR:
      return state.set('logining', false);
    case REGISTER:
      return state.set('registering', true);
    case REGISTER_SUCCESS:
      window.user = action.currentUser;

      // localStorage.setItem(localStorageKeys.COUNTRY_ISO, action.currentUser.country_iso);
      // localStorage.setItem(localStorageKeys.LOCAL_CURRENCY_CODE, action.currentUser.local_currency_code);

      setCountryAndCurrency(action.currentUser.country_iso || DEFAULT_COUNTRY_ISO, action.currentUser.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE);

      return state.set('registering', false)
        .set('currentUser', fromJS(action.currentUser))
        .set('localCurrencyCode', action.currentUser.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE)
        .set('countryIso', action.currentUser.country_iso || DEFAULT_COUNTRY_ISO);
    case REGISTER_ERROR:
      return state.set('registering', false);
    case LOAD_PRODUCT_DETAIL: {
      const product = productLrcQueue.get(action.id);
      const loading = !product;
      return state.set('loadingProduct', loading)
      .set('pendingProduct', fromJS(product));
    }
    case LOAD_PRODUCT_DETAIL_SUCCESS:
      productLrcQueue.set(`${action.data.id}`, action.data);
      return state.set('loadingProduct', false)
      .set('pendingProduct', fromJS(action.data));
    case LOAD_PRODUCT_DETAIL_ERROR:
      return state.set('loadingProduct', false);
    case LOAD_GROUP_DETAIL:
      return state.set('loadingGroup', true).set('pendingGroup', null);
    case LOAD_GROUP_DETAIL_SUCCESS:
      return state.set('loadingGroup', false)
      .set('pendingGroup', fromJS(action.data));
    case LOAD_GROUP_DETAIL_ERROR:
      return state.set('loadingGroup', false);
    case SET_LATEST_ACTION:
      localStorage.setItem(localStorageKeys.LATEST_ACTION, JSON.stringify(action.data));
      return state.set('latestAction', fromJS(action.data));
    case CLEAR_LATEST_ACTION:
      localStorage.removeItem(localStorageKeys.LATEST_ACTION);
      return state.set('latestAction', null);
    case SET_TOKEN:
      localStorage.setItem(localStorageKeys.TOKEN, action.token);
      return state.set('token', action.token);
    case CLEAR_TOKEN:
      localStorage.removeItem(localStorageKeys.TOKEN);
      return state.set('token', null);
    case CLEAR_USER_INFO:
      window.user = null;
      return state.set('currentUser', null);
    case UPDATE_RECEIVE_ADDRESS_SUCCESS: {
      localStorage.setItem(localStorageKeys.ADDRESS, JSON.stringify(action.data));
      return state.set('address', fromJS(action.data));
    }
    case LOAD_GROUP_NOTIFICATION_SUCCESS: {
      if (action.nType === 1) {
        return state.set('startGroupNotification', fromJS(action.data));
      }
      return state.set('followGroupNotification', fromJS(action.data));
    }
    case WECHAT_AUTH_SUCCESS: {
      localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID, action.data.wechat_open_id);
      return state.set('openId', action.data.wechat_open_id);
    }
    case WECHAT_CLEAR_OPEN_ID: {
      localStorage.setItem(localStorageKeys.WECHAT_OPEN_ID, action.data.wechat_open_id);
      return state.set('openId', action.data.wechat_open_id);
    }
    case SET_WECHAT_AUTH_REDIRECT_URL: {
      localStorage.setItem(localStorageKeys.WECHAT_REDIRECT_URL, action.url);
      return state.set('wechatAuthRedirectUrl', action.url);
    }
    case SHOW_PURCHASE_MODAL:
      return state.setIn(['purchaseModal', 'visible'], true)
      .setIn(['purchaseModal', 'purchaseType'], action.purchaseType);
    case HIDE_PURCHASE_MODAL:
      return state.setIn(['purchaseModal', 'visible'], false);
    case LOAD_SYSTEM_CONFIG_SUCCESS:
      return state.set('systemConfig', fromJS(action.data));

    case LOAD_ADDRESS_LIST:
      return state.set('addressListLoading', true);
    case LOAD_ADDRESS_LIST_SUCCESS: {
      const addressList = action.data;
      if (!localStorage.getItem(localStorageKeys.ADDRESS)) {
        if (addressList.length > 0) {
          let address = addressList[0];
          for (let i = 0; i < addressList.length; i++) {
            if (isBackendYes(addressList[i].is_default)) {
              address = addressList[i];
            }
          }
          localStorage.setItem(localStorageKeys.ADDRESS, JSON.stringify(address));
          return state.set('addressListLoading', false)
          .set('address', fromJS(address))
          .set('addressList', fromJS(action.data));
        }
      }
      return state.set('addressListLoading', false).set('addressList', fromJS(action.data));
    }
    case LOAD_ADDRESS_LIST_ERROR:
      return state.set('addressListLoading', false);
    case UPDATE_LIST_LAYOUT_TYPE:
      localStorage.setItem(localStorageKeys.LIST_LAYOUT_TYPE, action.layoutType);
      return state.set('listLayoutType', action.layoutType);
    case SET_LOGIN_CALLBACK:
      localStorage.setItem(localStorageKeys.LOGIN_CALLBACK, action.url);
      return state.set('loginCallback', action.url);
    case CLEAR_LOGIN_CALLBACK:
      localStorage.removeItem(localStorageKeys.LOGIN_CALLBACK);
      return state.set('loginCallback', '');
    case LOAD_EXCHANGE_RATE_SUCCESS:
      localStorage.setItem(localStorageKeys.EXCHANGE_RATE_TABLE, JSON.stringify(action.rateTable));
      return state.set('exchangeRateTable', fromJS(action.rateTable));
    case SET_PAY_PRE_SHIPMENT_REDIRECT_URL:
      return state.set('payPreShipmentRedirectUrl', action.url);
    case CLEAR_PAY_PRE_SHIPMENT_REDIRECT_URL:
      return state.set('payPreShipmentRedirectUrl', '/');
    case LOAD_COUPON_LIST:
      return state.set('couponListLoading', true).set('coupons', fromJS([])).set('currentCouponMapOrderId', action.orderId);
    case LOAD_COUPON_LIST_SUCCESS:
      return state
        .set('coupons', fromJS(action.list))
        .set('couponListLoading', false);
    case LOAD_COUPON_LIST_ERROR:
      return state.set('couponListLoading', false);
    case LOAD_DELIVERY_SERVICES:
      return state.set('deliveryServicesLoading', true).set('deliveryServices', fromJS([]));
    case LOAD_DELIVERY_SERVICES_SUCCESS:
      return state
        .set('deliveryServices', fromJS(action.list))
        .set('deliveryServicesLoading', false);
    case LOAD_DELIVERY_SERVICES_ERROR:
      return state.set('deliveryServicesLoading', false);
    case SET_HOME_SCROLL_TOP:
      return state.set('homeScrollTop', action.scrollTop);
    case SET_RECOMMEND_SCROLL_TOP:
      return state.set('recommendScrollTop', action.scrollTop);
    case SET_OTHER_PAGE_SCROLL_TOP:
      return state.set('otherPageScrollTop', action.scrollTop);
    case CLEAR_OTHER_PAGE_SCROLL_TOP:
      return state.set('otherPageScrollTop', 0);
    case LOAD_A_COUPON_SUCCESS:
      return state.set('currentCoupon', fromJS(action.data));
    case COLLECT_A_COUPON_SUCCESS:
      localStorage.setItem(localStorageKeys.LATEST_COLLECT_COUPON_TIME, (new Date()).getTime());
      return state.set('latestCollectCouponTime', (new Date()).getTime());
    case SET_SHOW_RED_PACKET_AFTER_LOGIN:
      if (action.show === true) {
        localStorage.setItem(localStorageKeys.SHOW_RED_PACKET_AFTER_LOGIN, 'true');
        return state.set('showRedPacketAfterLogin', true);
      }
      localStorage.removeItem(localStorageKeys.SHOW_RED_PACKET_AFTER_LOGIN);
      return state.set('showRedPacketAfterLogin', false);
    case SET_SHOW_RED_PACKET_MODAL:
      return state.set('showRedPacketModal', action.show);
    case SET_LATEST_COLLECT_COUPON_TIME:
      localStorage.setItem(localStorageKeys.LATEST_COLLECT_COUPON_TIME, action.time);
      return state.set('latestCollectCouponTime', action.time);
    case SET_LATEST_COLLECT_SHARE_COUPON_TIME:
      localStorage.setItem(localStorageKeys.LATEST_COLLECT_SHARE_COUPON_TIME, action.time);
      return state.set('latestCollectShareCouponTime', action.time);
    case LOAD_RECOMMEND_PRODUCT_LIST_SUCCESS:
      return state
      .set('recommendProductList', fromJS(action.list));
    case LOAD_USER_INFO_DETAIL:
      return state
      .set('infoDetailLoading', true);
    case LOAD_USER_INFO_DETAIL_ERROR:
      return state
      .set('infoDetailLoading', false);
    case SET_SHOW_SHARE_RED_PACKET_MODAL:
      return state.set('showShareRedPacketModal', action.show);
    case LOAD_FIX_COUPON:
      return state.set('currentFixCoupon', fromJS(null));
    case LOAD_FIX_COUPON_SUCCESS:
      return state.set('currentFixCoupon', fromJS(action.data));
    case LOAD_USER_INFO_DETAIL_SUCCESS:
      {
        let user = action.data;
        let currentUser = state.get('currentUser') ? state.get('currentUser').toJS() : {};
        if (!currentUser) {
          currentUser = {};
        }
        user = Object.assign(currentUser, user);
        user.has_detail = true;
        window.user = user;

        // localStorage.setItem(localStorageKeys.COUNTRY_ISO, user.country_iso);
        // localStorage.setItem(localStorageKeys.LOCAL_CURRENCY_CODE, user.local_currency_code);

        setCountryAndCurrency(user.country_iso || DEFAULT_COUNTRY_ISO, user.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE);

        return state.set('currentUser', fromJS(user))
          .set('infoDetailLoading', false)
          .set('localCurrencyCode', user.local_currency_code || DEFAULT_LOCAL_CURRENCY_CODE)
          .set('countryIso', user.country_iso || DEFAULT_COUNTRY_ISO);
      }
    case SET_SHOW_COUPON_MODAL:
      return state.set('showCouponModal', action.show)
                  .set('registerFrom', action.from);
    case LOAD_UNPAID_SHIPPING:
      return state
        .set('unpaidShippingListLoading', true)
        .set('unpaidShippingList', fromJS([]));
    case LOAD_UNPAID_SHIPPING_SUCCESS:
      return state
        .set('unpaidShippingListLoading', false)
        .set('unpaidShippingList', fromJS(action.data));
    case LOAD_UNPAID_SHIPPING_ERROR:
      return state
        .set('unpaidShippingListLoading', false)
        .set('unpaidShippingList', fromJS([]));
    case LOAD_PAYMENT_RECORDS:
      return state.set('paymentRecords', fromJS([]));
    case LOAD_PAYMENT_RECORDS_SUCCESS:
      return state.set('paymentRecords', fromJS(action.data));
    case LOAD_PAYMENT_RECORDS_ERROR:
      return state.set('paymentRecords', fromJS([]));
    default:
      return state;
  }
}

export default globalReducer;
