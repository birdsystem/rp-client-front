/*
 *
 * Global actions
 *
 */

import {
  CHANGE_LANGUAGE,
  CHANGE_LANGUAGE_SUCCESS,
  CHANGE_LANGUAGE_ERROR,
  CHANGE_COUNTRY,
  CHANGE_COUNTRY_ERROR,
  CHANGE_COUNTRY_SUCCESS,
  LOAD_USER_INFO,
  LOAD_USER_INFO_SUCCESS,
  LOAD_USER_INFO_ERROR,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
  LOAD_PRODUCT_DETAIL,
  LOAD_PRODUCT_DETAIL_SUCCESS,
  LOAD_PRODUCT_DETAIL_ERROR,
  LOAD_GROUP_DETAIL,
  LOAD_GROUP_DETAIL_SUCCESS,
  LOAD_GROUP_DETAIL_ERROR,
  SET_LATEST_ACTION,
  SET_TOKEN,
  CLEAR_TOKEN,
  CLEAR_USER_INFO,
  UPDATE_RECEIVE_ADDRESS,
  UPDATE_RECEIVE_ADDRESS_SUCCESS,
  START_GROUP,
  START_GROUP_ERROR,
  START_GROUP_SUCCESS,
  FOLLOW_GROUP,
  FOLLOW_GROUP_ERROR,
  FOLLOW_GROUP_SUCCESS,
  CLEAR_LATEST_ACTION,
  LOAD_GROUP_NOTIFICATION,
  LOAD_GROUP_NOTIFICATION_SUCCESS,
  LOAD_GROUP_NOTIFICATION_FAIL,
  WECHAT_AUTH,
  WECHAT_AUTH_SUCCESS,
  WECHAT_AUTH_ERROR,
  WECHAT_CLEAR_OPEN_ID,
  SET_WECHAT_AUTH_REDIRECT_URL,
  SHOW_PURCHASE_MODAL,
  HIDE_PURCHASE_MODAL,
  LOAD_SYSTEM_CONFIG,
  LOAD_SYSTEM_CONFIG_ERROR,
  LOAD_SYSTEM_CONFIG_SUCCESS,
  LOGOUT,
  LOGOUT_ERROR,
  LOGOUT_SUCCESS,
  ADD_ADDRESS,
  ADD_ADDRESS_SUCCESS,
  ADD_ADDRESS_ERROR,
  UPDATE_ADDRESS,
  UPDATE_ADDRESS_SUCCESS,
  UPDATE_ADDRESS_ERROR,
  REMOVE_ADDRESS,
  REMOVE_ADDRESS_SUCCESS,
  REMOVE_ADDRESS_ERROR,
  LOAD_ADDRESS_LIST,
  LOAD_ADDRESS_LIST_SUCCESS,
  LOAD_ADDRESS_LIST_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_ERROR,
  CHANGE_PASSWORD_SUCCESS,
  WECHAT_LOGIN,
  WECHAT_LOGIN_SUCCESS,
  WECHAT_LOGIN_ERROR,
  UPDATE_LIST_LAYOUT_TYPE,
  SET_LOGIN_CALLBACK,
  CLEAR_LOGIN_CALLBACK,
  SUBMIT_PRODUCT,
  SUBMIT_PRODUCT_ERROR,
  SUBMIT_PRODUCT_SUCCESS,
  LOAD_EXCHANGE_RATE,
  LOAD_EXCHANGE_RATE_ERROR,
  LOAD_EXCHANGE_RATE_SUCCESS,
  CANCEL_ORDER,
  CANCEL_ORDER_SUCCESS,
  CANCEL_ORDER_ERROR,
  CONFIRM_RECEIPT,
  CONFIRM_RECEIPT_ERROR,
  CONFIRM_RECEIPT_SUCCESS,
  SET_PAY_PRE_SHIPMENT_REDIRECT_URL,
  CLEAR_PAY_PRE_SHIPMENT_REDIRECT_URL,
  LOAD_COUPON_LIST,
  LOAD_COUPON_LIST_SUCCESS,
  LOAD_COUPON_LIST_ERROR,
  SET_HOME_SCROLL_TOP,
  SET_RECOMMEND_SCROLL_TOP,
  SET_OTHER_PAGE_SCROLL_TOP,
  CLEAR_OTHER_PAGE_SCROLL_TOP,
  LOAD_A_COUPON,
  LOAD_A_COUPON_SUCCESS,
  LOAD_A_COUPON_ERROR,
  COLLECT_A_COUPON,
  COLLECT_A_COUPON_SUCCESS,
  COLLECT_A_COUPON_ERROR,
  SET_LATEST_COLLECT_COUPON_TIME,
  SET_SHOW_RED_PACKET_AFTER_LOGIN,
  SET_SHOW_RED_PACKET_MODAL,
  LOAD_RECOMMEND_PRODUCT_LIST,
  LOAD_RECOMMEND_PRODUCT_LIST_ERROR,
  LOAD_RECOMMEND_PRODUCT_LIST_SUCCESS,
  LOAD_USER_INFO_DETAIL,
  LOAD_USER_INFO_DETAIL_SUCCESS,
  LOAD_USER_INFO_DETAIL_ERROR,
  SET_LATEST_COLLECT_SHARE_COUPON_TIME,
  LOAD_FIX_COUPON,
  LOAD_FIX_COUPON_ERROR,
  LOAD_FIX_COUPON_SUCCESS,
  SET_SHOW_SHARE_RED_PACKET_MODAL,
  SET_SHOW_COUPON_MODAL,
  NEW_REGISTER_FROM,
  LOAD_UNPAID_SHIPPING,
  LOAD_UNPAID_SHIPPING_ERROR,
  LOAD_UNPAID_SHIPPING_SUCCESS,
  LOAD_PAYMENT_RECORDS,
  LOAD_PAYMENT_RECORDS_ERROR,
  LOAD_PAYMENT_RECORDS_SUCCESS,
  HIDE_COUNTRY_PICKER_MODAL,
  SHOW_COUNTRY_PICKER_MODAL,
  LOAD_DELIVERY_SERVICES,
  LOAD_DELIVERY_SERVICES_ERROR,
  LOAD_DELIVERY_SERVICES_SUCCESS,
} from './constants';

export function changeLanguage(lang, currentServerLang) {
  return {
    type: CHANGE_LANGUAGE,
    lang,
    currentServerLang,
  };
}

export function changeLanguageSuccess(lang) {
  return {
    type: CHANGE_LANGUAGE_SUCCESS,
    lang,
  };
}

export function changeLanguageError() {
  return {
    type: CHANGE_LANGUAGE_ERROR,
  };
}

export function changeCountry(countryIso) {
  return {
    type: CHANGE_COUNTRY,
    countryIso,
  };
}

export function changeCountrySuccess(countryIso, localCurrencyCode) {
  return {
    type: CHANGE_COUNTRY_SUCCESS,
    countryIso,
    localCurrencyCode,
  };
}

export function changeCountryError() {
  return {
    type: CHANGE_COUNTRY_ERROR,
  };
}

export function loadUserInfo() {
  return {
    type: LOAD_USER_INFO,
  };
}

export function loadUserInfoSuccess(currentUser) {
  return {
    type: LOAD_USER_INFO_SUCCESS,
    currentUser,
  };
}

export function loadUserInfoError() {
  return {
    type: LOAD_USER_INFO_ERROR,
  };
}

export function login(data) {
  return {
    type: LOGIN,
    data,
  };
}

export function loginSuccess(currentUser) {
  return {
    type: LOGIN_SUCCESS,
    currentUser,
  };
}

export function loginError() {
  return {
    type: LOGIN_ERROR,
  };
}

export function resetPassword(data) {
  return {
    type: RESET_PASSWORD,
    data,
  };
}

export function resetPasswordSuccess() {
  return {
    type: RESET_PASSWORD_SUCCESS,
  };
}

export function resetPasswordError() {
  return {
    type: RESET_PASSWORD_ERROR,
  };
}

export function changePassword(data) {
  return {
    type: CHANGE_PASSWORD,
    data,
  };
}

export function changePasswordSuccess() {
  return {
    type: CHANGE_PASSWORD_SUCCESS,
  };
}

export function changePasswordError() {
  return {
    type: CHANGE_PASSWORD_ERROR,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS,
  };
}

export function logoutError() {
  return {
    type: LOGOUT_ERROR,
  };
}

export function register(data) {
  return {
    type: REGISTER,
    data,
  };
}

export function registerSuccess(currentUser) {
  return {
    type: REGISTER_SUCCESS,
    currentUser,
  };
}

export function registerError() {
  return {
    type: REGISTER_ERROR,
  };
}

export function loadProductDetail({ id, hideLoading = false, recordToHistory = false }) {
  return {
    type: LOAD_PRODUCT_DETAIL,
    id,
    hideLoading,
    recordToHistory,
  };
}

export function loadProductDetailSuccess(data) {
  return {
    type: LOAD_PRODUCT_DETAIL_SUCCESS,
    data,
  };
}

export function loadProductDetailError() {
  return {
    type: LOAD_PRODUCT_DETAIL_ERROR,
  };
}

export function loadGroupDetail(id, hideLoading = false) {
  return {
    type: LOAD_GROUP_DETAIL,
    id,
    hideLoading,
  };
}

export function loadGroupDetailSuccess(data) {
  return {
    type: LOAD_GROUP_DETAIL_SUCCESS,
    data,
  };
}

export function loadGroupDetailError() {
  return {
    type: LOAD_GROUP_DETAIL_ERROR,
  };
}

export function setLatestAction(data) {
  return {
    type: SET_LATEST_ACTION,
    data,
  };
}

export function setToken(token) {
  return {
    type: SET_TOKEN,
    token,
  };
}

export function clearToken() {
  return {
    type: CLEAR_TOKEN,
  };
}

export function clearUserInfo() {
  return {
    type: CLEAR_USER_INFO,
  };
}

export function updateReceiveAddress(data) {
  return {
    type: UPDATE_RECEIVE_ADDRESS,
    data,
  };
}

export function updateReceiveAddressSuccess(data) {
  return {
    type: UPDATE_RECEIVE_ADDRESS_SUCCESS,
    data,
  };
}

export function startGroup(data) {
  return {
    type: START_GROUP,
    data,
  };
}

export function startGroupSuccess(data) {
  return {
    type: START_GROUP_SUCCESS,
    data,
  };
}

export function startGroupError() {
  return {
    type: START_GROUP_ERROR,
  };
}

export function followGroup(data) {
  return {
    type: FOLLOW_GROUP,
    data,
  };
}

export function followGroupSuccess(data) {
  return {
    type: FOLLOW_GROUP_SUCCESS,
    data,
  };
}

export function followGroupError() {
  return {
    type: FOLLOW_GROUP_ERROR,
  };
}

export function clearLatestAction() {
  return {
    type: CLEAR_LATEST_ACTION,
  };
}

export function loadGroupNotification(nType) {
  return {
    type: LOAD_GROUP_NOTIFICATION,
    nType,
  };
}

export function loadGroupNotificationSuccess(data, nType) {
  return {
    type: LOAD_GROUP_NOTIFICATION_SUCCESS,
    data,
    nType,
  };
}

export function loadGroupNotificationFail() {
  return {
    type: LOAD_GROUP_NOTIFICATION_FAIL,
  };
}

export function weChatAuth(code, state) {
  return {
    type: WECHAT_AUTH,
    code,
    state,
  };
}

export function weChatAuthSuccess(data) {
  return {
    type: WECHAT_AUTH_SUCCESS,
    data,
  };
}

export function weChatAuthFail() {
  return {
    type: WECHAT_AUTH_ERROR,
  };
}

export function weChatLogin(code, from = '') {
  return {
    type: WECHAT_LOGIN,
    from,
    code,
  };
}

export function weChatLoginSuccess(data) {
  return {
    type: WECHAT_LOGIN_SUCCESS,
    data,
  };
}

export function weChatLoginError() {
  return {
    type: WECHAT_LOGIN_ERROR,
  };
}

export function weChatClearOpenId() {
  return {
    type: WECHAT_CLEAR_OPEN_ID,
  };
}

export function setWeChatAuthDirectUrl(url) {
  return {
    type: SET_WECHAT_AUTH_REDIRECT_URL,
    url,
  };
}

export function showPurchaseModal(purchaseType) {
  return {
    type: SHOW_PURCHASE_MODAL,
    purchaseType,
  };
}

export function hidePurchaseModal() {
  return {
    type: HIDE_PURCHASE_MODAL,
  };
}

export function hideCountryPickerModal() {
  return {
    type: HIDE_COUNTRY_PICKER_MODAL,
  };
}

export function showCountryPickerModal() {
  return {
    type: SHOW_COUNTRY_PICKER_MODAL,
  };
}

export function loadSystemConfig() {
  return {
    type: LOAD_SYSTEM_CONFIG,
  };
}

export function loadSystemConfigSuccess(data) {
  return {
    type: LOAD_SYSTEM_CONFIG_SUCCESS,
    data,
  };
}

export function loadSystemConfigError() {
  return {
    type: LOAD_SYSTEM_CONFIG_ERROR,
  };
}

export function loadAddressList() {
  return {
    type: LOAD_ADDRESS_LIST,
  };
}

export function loadAddressListSuccess(data) {
  return {
    type: LOAD_ADDRESS_LIST_SUCCESS,
    data,
  };
}

export function loadAddressListError() {
  return {
    type: LOAD_ADDRESS_LIST_ERROR,
  };
}

export function addAddress(data) {
  return {
    type: ADD_ADDRESS,
    data,
  };
}

export function addAddressSuccess(data) {
  return {
    type: ADD_ADDRESS_SUCCESS,
    data,
  };
}

export function addAddressError() {
  return {
    type: ADD_ADDRESS_ERROR,
  };
}

export function updateAddress(data, showSuccessMessage) {
  return {
    type: UPDATE_ADDRESS,
    data,
    showSuccessMessage,
  };
}

export function updateAddressSuccess(data) {
  return {
    type: UPDATE_ADDRESS_SUCCESS,
    data,
  };
}

export function updateAddressError() {
  return {
    type: UPDATE_ADDRESS_ERROR,
  };
}

export function removeAddress(id) {
  return {
    type: REMOVE_ADDRESS,
    id,
  };
}

export function removeAddressSuccess(id) {
  return {
    type: REMOVE_ADDRESS_SUCCESS,
    id,
  };
}

export function removeAddressError() {
  return {
    type: REMOVE_ADDRESS_ERROR,
  };
}

export function updateListLayoutType(layoutType) {
  return {
    type: UPDATE_LIST_LAYOUT_TYPE,
    layoutType,
  };
}

export function setLoginCallback(url) {
  // url should has decode
  return {
    type: SET_LOGIN_CALLBACK,
    url,
  };
}

export function clearLoginCallback() {
  return {
    type: CLEAR_LOGIN_CALLBACK,
  };
}

export function submitProduct(data) {
  return {
    type: SUBMIT_PRODUCT,
    data,
  };
}

export function submitProductSuccess() {
  return {
    type: SUBMIT_PRODUCT_SUCCESS,
  };
}

export function submitProductError() {
  return {
    type: SUBMIT_PRODUCT_ERROR,
  };
}

export function loadExchangeRate() {
  return {
    type: LOAD_EXCHANGE_RATE,
  };
}

export function loadExchangeRateSuccess(rateTable) {
  return {
    type: LOAD_EXCHANGE_RATE_SUCCESS,
    rateTable,
  };
}

export function loadExchangeRateError() {
  return {
    type: LOAD_EXCHANGE_RATE_ERROR,
  };
}

export const loadCouponList = ({ isValid, isExpired, isUsed, orderId, showLoading } = {}) => ({
  // isValid, isExpired, isUsed are all strings with value '1' or '0'
  type: LOAD_COUPON_LIST,
  isValid,
  orderId,
  isExpired,
  isUsed,
  showLoading,
});

export const loadCouponListSuccess = (list = []) => ({
  type: LOAD_COUPON_LIST_SUCCESS,
  list,
});

export function loadCouponListError() {
  return {
    type: LOAD_COUPON_LIST_ERROR,
  };
}

export const loadDeliveryServices = () => ({
  type: LOAD_DELIVERY_SERVICES,
});

export const loadDeliveryServicesSuccess = (list = []) => ({
  type: LOAD_DELIVERY_SERVICES_SUCCESS,
  list,
});

export function loadDeliveryServicesError() {
  return {
    type: LOAD_DELIVERY_SERVICES_ERROR,
  };
}

export function cancelOrder(id, hasPay = true) {
  return {
    type: CANCEL_ORDER,
    id,
    hasPay,
  };
}

export function cancelOrderSuccess() {
  return {
    type: CANCEL_ORDER_SUCCESS,
  };
}

export function cancelOrderError() {
  return {
    type: CANCEL_ORDER_ERROR,
  };
}

export function confirmReceipt(id) {
  return {
    type: CONFIRM_RECEIPT,
    id,
  };
}

export function confirmReceiptSuccess() {
  return {
    type: CONFIRM_RECEIPT_SUCCESS,
  };
}

export function confirmReceiptError() {
  return {
    type: CONFIRM_RECEIPT_ERROR,
  };
}

export function setPayPreShipmentRedirectUrl(url) {
  // url should has decode
  return {
    type: SET_PAY_PRE_SHIPMENT_REDIRECT_URL,
    url,
  };
}

export function clearPayPreShipmentRedirectUrl() {
  return {
    type: CLEAR_PAY_PRE_SHIPMENT_REDIRECT_URL,
  };
}

export function setHomeScrollTop(scrollTop) {
  return {
    type: SET_HOME_SCROLL_TOP,
    scrollTop,
  };
}

export function setRecommendScrollTop(scrollTop) {
  return {
    type: SET_RECOMMEND_SCROLL_TOP,
    scrollTop,
  };
}

export function setOtherPageScrollTop(scrollTop) {
  return {
    type: SET_OTHER_PAGE_SCROLL_TOP,
    scrollTop,
  };
}

export function clearOtherPageScrollTop() {
  return {
    type: CLEAR_OTHER_PAGE_SCROLL_TOP,
  };
}

export function loadACoupon(showLoading = false) {
  return {
    type: LOAD_A_COUPON,
    showLoading,
  };
}

export function loadACouponSuccess(data) {
  return {
    type: LOAD_A_COUPON_SUCCESS,
    data,
  };
}

export function loadACouponError() {
  return {
    type: LOAD_A_COUPON_ERROR,
  };
}

/**
 coupon_id
 telephone // 可选参数，未登录或未注册情况下需要输入手机号领取
 dial_code // 可选参数
 * */

export function collectACoupon(data) {
  return {
    type: COLLECT_A_COUPON,
    data,
  };
}

export function collectACouponSuccess() {
  return {
    type: COLLECT_A_COUPON_SUCCESS,
  };
}

export function collectACouponError() {
  return {
    type: COLLECT_A_COUPON_ERROR,
  };
}

export function setLatestCollectCouponTime(time) {
  return {
    type: SET_LATEST_COLLECT_COUPON_TIME,
    time,
  };
}

export function setShowRedPacketAfterLogin(show) {
  return {
    type: SET_SHOW_RED_PACKET_AFTER_LOGIN,
    show,
  };
}

export function setShowRedPacketModal(show) {
  return {
    type: SET_SHOW_RED_PACKET_MODAL,
    show,
  };
}

export function loadRecommendProductList(page) {
  return {
    type: LOAD_RECOMMEND_PRODUCT_LIST,
    page,
  };
}

export function loadRecommendProductListSuccess(list, count) {
  return {
    type: LOAD_RECOMMEND_PRODUCT_LIST_SUCCESS,
    list,
    count,
  };
}

export function loadRecommendProductListError() {
  return {
    type: LOAD_RECOMMEND_PRODUCT_LIST_ERROR,
  };
}

export function loadUserInfoDetail() {
  return {
    type: LOAD_USER_INFO_DETAIL,
  };
}

export function loadUserInfoDetailSuccess(data) {
  return {
    type: LOAD_USER_INFO_DETAIL_SUCCESS,
    data,
  };
}

export function loadUserInfoDetailError() {
  return {
    type: LOAD_USER_INFO_DETAIL_ERROR,
  };
}

export function loadFixCoupon() {
  return {
    type: LOAD_FIX_COUPON,
  };
}

export function loadFixCouponSuccess(data) {
  return {
    type: LOAD_FIX_COUPON_SUCCESS,
    data,
  };
}

export function loadFixCouponError() {
  return {
    type: LOAD_FIX_COUPON_ERROR,
  };
}

export function setLatestCollectShareCouponTime(time) {
  return {
    type: SET_LATEST_COLLECT_SHARE_COUPON_TIME,
    time,
  };
}

export function setShowShareRedPacketModal(show) {
  return {
    type: SET_SHOW_SHARE_RED_PACKET_MODAL,
    show,
  };
}

export function setShowCouponModal(show, from = NEW_REGISTER_FROM.NEW_USER) {
  return {
    type: SET_SHOW_COUPON_MODAL,
    show,
    from,
  };
}

export function loadUnpaidShipping() {
  return {
    type: LOAD_UNPAID_SHIPPING,
  };
}

export function loadUnpaidShippingSuccess(data) {
  return {
    type: LOAD_UNPAID_SHIPPING_SUCCESS,
    data,
  };
}

export function loadUnpaidShippingError() {
  return {
    type: LOAD_UNPAID_SHIPPING_ERROR,
  };
}

export function loadPaymentRecords() {
  return {
    type: LOAD_PAYMENT_RECORDS,
  };
}

export function loadPaymentRecordsSuccess(data) {
  return {
    type: LOAD_PAYMENT_RECORDS_SUCCESS,
    data,
  };
}

export function loadPaymentRecordsError() {
  return {
    type: LOAD_PAYMENT_RECORDS_ERROR,
  };
}
