/*
 *
 * GroupDetail constants
 *
 */

export const LOAD_GROUP_DETAIL = 'GroupDetail/LOAD_GROUP_DETAIL';
export const LOAD_GROUP_DETAIL_SUCCESS = 'GroupDetail/LOAD_GROUP_DETAIL_SUCCESS';
export const LOAD_GROUP_DETAIL_ERROR = 'GroupDetail/LOAD_GROUP_DETAIL_ERROR';

