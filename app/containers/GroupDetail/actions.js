/*
 *
 * Group detail actions
 *
 */

import {
  LOAD_GROUP_DETAIL,
  LOAD_GROUP_DETAIL_SUCCESS,
  LOAD_GROUP_DETAIL_ERROR,
} from './constants';

export function loadGroupDetail(id) {
  return {
    type: LOAD_GROUP_DETAIL,
    id,
  };
}

export function loadGroupDetailSuccess(data) {
  return {
    type: LOAD_GROUP_DETAIL_SUCCESS,
    data,
  };
}

export function loadGroupDetailError() {
  return {
    type: LOAD_GROUP_DETAIL_ERROR,
  };
}

