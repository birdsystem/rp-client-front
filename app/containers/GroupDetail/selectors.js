import { createSelector } from 'reselect';

const selectGroupDetailDomain = () => (state) => state.get('groupdetail');

const selectGroupDetail = () => createSelector(
  selectGroupDetailDomain(),
  (substate) => substate.get('groupDetail')
);

const selectGroupDetailLoading = () => createSelector(
  selectGroupDetailDomain(),
  (substate) => substate.get('groupDetailLoading')
);

export {
  selectGroupDetail,
  selectGroupDetailLoading,
};
