/* eslint-disable no-unused-vars,react/no-will-update-set-state,radix,react/sort-comp,no-empty,default-case,react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import Money from 'components/Money';
import WeChatShare from 'components/WeChatShare';
import WindowTitle from 'components/WindowTitle';
import { browserHistory, Link } from 'react-router';
import message from 'components/message';
import { Icon, Button, Modal, Carousel, ImagePicker } from 'antd-mobile';
import { getTimeLeftDisplay } from 'utils/commonHelper';
import GraySpace from 'components/GraySpace';
import wechatHelper from 'utils/wechatHelper';
import { getDisplayPrice, getStartGroupPrice, getGroupPriceFromProps, getGroupPrice, getSingleBuyPrice, getDisplayPriceFromProps, getStartGroupPriceFromProps, getSingleBuyPriceFromProps } from 'utils/productHelper';
import CopyToClipboard from 'react-copy-to-clipboard';
import _ from 'lodash';
import {
  SINGLE_BUYING,
  GROUP_BUYING,
  GROUP_LEADER_BUYING,
  GROUP_STATUS_ACTIVE,
  GROUP_STATUS_PENDING,
  GROUP_STATUS_SUCCESS,
  GROUP_STATUS_FAILED,
  MY_ORDER_TYPE_MY_START,
  MY_ORDER_TYPE_MY_FOLLOW,
  MY_ORDER_TYPE_PROCESS,
  MY_ORDER_TYPE_FINISH,
} from 'utils/constants';
import { selectCurrentUser, selectLocationState } from 'containers/App/selectors';
import { loadGroupDetail } from './actions';
import { selectGroupDetail, selectGroupDetailLoading } from './selectors';
import styles from './styles.css';

class GroupDetail extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props;
    const id = params.groupId;
    this.state = {
      id,
      hasConfigWeChatShare: false,
      showWeChatShareTips: false,
    };
    this.props.loadGroupDetail(id);
  }

  componentDidMount() {

  }

  componentWillUpdate(nextProps, nextState) {
    if (!_.isEmpty(nextProps.groupDetail) && nextState.hasConfigWeChatShare === false) {
      wechatHelper.configFollowGroupShare(nextProps.groupDetail.product_detail.name, getGroupPrice(nextProps.groupDetail.product_detail, nextProps.groupDetail.min_user_count), nextProps.groupDetail.product_detail.id, nextProps.groupDetail.id, nextProps.groupDetail.product_detail.image_url);
      this.setState({
        hasConfigWeChatShare: true,
      });
    }
  }


  goBack = () => {
    if (this.props.locationState.previousPathname !== '/') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  }

  getStatusDisplay(item) {
    let ret = '';
    if (item.status === GROUP_STATUS_ACTIVE) {
      ret = this.props.t('group.status_ACTIVE');
    } else if (item.status === GROUP_STATUS_SUCCESS) {
      ret = this.props.t('group.status_SUCCESS');
    } else if (item.status === GROUP_STATUS_FAILED) {
      ret = this.props.t('group.status_FAILED');
    } else if (item.status === GROUP_STATUS_PENDING) {
      ret = this.props.t('group.status_PENDING_OWNER');
    }
    return ret;
  }

  getDisplayImageFromGroupDetail(groupDetail) {
    let ret = groupDetail.product_detail.image_url;
    if (groupDetail.buy_info && groupDetail.buy_info.sku_info && groupDetail.buy_info.sku_info.product_info && groupDetail.buy_info.sku_info.product_info.image_url) {
      ret = groupDetail.buy_info.sku_info.product_info.image_url;
    }
    return ret;
  }

  getBuyPrice(group) {
    let ret = 0;
    if (group.buy_info && group.buy_info.sku_info) {
      if (group.type === SINGLE_BUYING || group.type === GROUP_BUYING) {
        ret = getGroupPriceFromProps(group.product_detail, group.buy_info.sku_info.prop_path, group.min_user_count);
      } else if (group.type === GROUP_LEADER_BUYING) {
        ret = getStartGroupPriceFromProps(group.product_detail, group.buy_info.sku_info.prop_path);
      }
    } else if (group.type === SINGLE_BUYING) {
      ret = getSingleBuyPrice(group.product_detail);
    } else if (group.type === GROUP_BUYING) {
      ret = getGroupPrice(group.product_detail, group.min_user_count);
    } else if (group.type === GROUP_LEADER_BUYING) {
      ret = getStartGroupPrice(group.product_detail);
    }
    return ret;
  }

  render() {
    const { groupDetail, groupDetailLoading, t } = this.props;
    let shareUrl = `${window.location.protocol}//${window.location.host}`;
    if (!_.isEmpty(groupDetail)) {
      shareUrl = `${window.location.protocol}//${window.location.host}/productdetail/${groupDetail.product_detail.id}?gid=${groupDetail.id}`;
    }
    const copySuccess = wechatHelper.isOpenInWechat() ? t('common.copy_success_wechat') : t('common.copy_success');
    let imageUrl = '';
    if (!_.isEmpty(groupDetail)) {
      imageUrl = this.getDisplayImageFromGroupDetail(groupDetail);
    }

    const priceText = t('common.group_discount_price');
    // if (groupDetail && groupDetail.type === GROUP_LEADER_BUYING) {
    //  priceText = t('common.group_leader_discount_price');
    // }
    let totalCount = 0;
    let totalPrice = 0;
    if (groupDetail) {
      const buyPrice = this.getBuyPrice(groupDetail);
      if (groupDetail.buy_info) {
        totalCount = groupDetail.buy_info.count;
      }
      if (groupDetail.buy_info && groupDetail.buy_info.sku_info) {
        totalPrice = buyPrice * totalCount;
      }
    }
    let label = '';
    if (groupDetail) {
      switch (groupDetail.type) {
        case SINGLE_BUYING: {
          label = t('common.single_buy');
          break;
        }
        case GROUP_LEADER_BUYING: {
          label = t('common.group_owner');
          break;
        }
        case GROUP_BUYING: {
          label = t('common.follow_group');
          break;
        }
      }
    }
    let singleBuyPrice = 0;
    if (groupDetail && groupDetail.product_detail) {
      singleBuyPrice = getSingleBuyPrice(groupDetail.product_detail);
      if (groupDetail.buy_info && groupDetail.buy_info.sku_info) {
        singleBuyPrice = getSingleBuyPriceFromProps(groupDetail.product_detail, groupDetail.buy_info.sku_info.prop_path);
      }
    }

    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.group_detail')} />

        {
          (groupDetailLoading || !groupDetail) ?
            null
            :
            <div className={styles.wrapInner}>
              <div className={styles.itemWrap}>
                <div className={styles.stickyHeader}>
                  <div onClick={this.goBack}>
                    <Icon type="left" size="lg" color="#333" />
                  </div>
                  <p>{groupDetail.product_detail.name}</p>
                </div>
              </div>
              <div className={styles.groupDetailWrap}>
                <div className={styles.detailTitle}>
                  <div className={styles.detailTitleText}> {t('common.user_start_group', { name: groupDetail.owner.username })}</div>
                  <div className={styles.detailTitleStatus}>{this.getStatusDisplay(groupDetail)}</div>
                </div>
                <div className={styles.itemInfo}>
                  <div className={styles.peopleInfoWrap}>
                    <div className={styles.itemCurrentPeople}>{ t(
                      'common.current_join_count') }: { groupDetail.current_user_count }
                    </div>
                    <div className={styles.itemNeedPeople}>
                      {
                        groupDetail.min_user_count - groupDetail.current_user_count > 0 ? t(
                          'common.people_left', {
                            count: groupDetail.min_user_count -
                            groupDetail.current_user_count,
                          })
                          : t('common.finish_group')
                      }
                    </div>
                  </div>

                  <div className={styles.timeInfoWrap}>
                    <div className={styles.itemStartTime}>
                      { t('common.ending_time') }：{groupDetail.ending_time}
                    </div>
                    {
                      (groupDetail.status === GROUP_STATUS_ACTIVE && groupDetail.left_time > 0) && <div className={styles.itemLeftTime}>
                        { t('common.left_time') }：{ getTimeLeftDisplay(
                        groupDetail.left_time) }
                      </div>
                    }
                  </div>
                </div>
              </div>

              <div className={styles.productDetailWrap}>
                <div className={styles.productTitle}>
                  <div className={styles.detailTitleText}> {t('common.product_detail')}</div>
                </div>
                <div className={styles.imageWrap}>
                  <img alt={'product'} className={styles.productImage} src={imageUrl} />
                  <div className={styles.dealText}>{t('common.already_deal_count', { count: groupDetail.product_detail.deal_count })}</div>
                </div>
                <div className={styles.productInfoWrap}>
                  <div className={styles.productPriceInfo}>
                    <div className={styles.originalPrice}>
                      <span>{t('common.original_price')}: </span>
                      <Money
                        className={styles.price}
                        amount={singleBuyPrice}
                      />
                    </div>
                    <div className={styles.groupPrice}>
                      <span>{priceText}: </span>
                      <Money
                        className={styles.price}
                        amount={this.getBuyPrice(groupDetail)}
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className={styles.priceWrap}>
                <span className={styles.groupLabel}>
                  {label}
                </span>
                <div className={styles.totalPriceWrap}>
                  <span className={styles.totalPriceText}>{ t(
                    'common.total_product_acount', { count: totalCount }) }: </span>
                  <span className={styles.totalPrice}>
                    <Money
                      amount={totalPrice}
                    />
                  </span>
                </div>
              </div>

              <div className={styles.actionWrap}>
                <Link target="_blank" to={'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515'} className={styles.askWrap}>
                  <div className={styles.ask_customer}>
                    <p>{t('common.ask')}</p>
                    <p>{t('common.custome_service')}</p>
                  </div>
                </Link>
                <div className={styles.shareButton}>
                  <CopyToClipboard
                    text={shareUrl}
                    onCopy={() => {
                      if (wechatHelper.isOpenInWechat()) {
                        this.setState({
                          showWeChatShareTips: true,
                        });
                      } else {
                        message.success(copySuccess);
                      }
                    }}
                  >
                    <span>{t('common.share')}</span>
                  </CopyToClipboard>
                </div>
              </div>
            </div>
        }
        { this.state.showWeChatShareTips &&
        <div
          onClick={
            () => {
              this.setState({
                showWeChatShareTips: false,
              });
            }
          }
        ><WeChatShare />
        </div>
        }
      </div>
    );
  }
}

GroupDetail.propTypes = {
  t: PropTypes.func,
  params: PropTypes.object,
  loadGroupDetail: PropTypes.func,
  groupDetailLoading: PropTypes.bool,
  groupDetail: PropTypes.object,
  currentUser: PropTypes.object,
  locationState: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectGroupDetailLoading(),
  selectGroupDetail(),
  selectCurrentUser(),
  selectLocationState(),
  (groupDetailLoading, groupDetail, currentUser, locationState) => ({
    groupDetailLoading,
    groupDetail: (groupDetail && groupDetail.toJS()) || null,
    currentUser: currentUser && currentUser.toJS(),
    locationState,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadGroupDetail: (id) => dispatch(loadGroupDetail(id)),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(GroupDetail));
