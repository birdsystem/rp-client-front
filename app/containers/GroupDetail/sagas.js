import { call, put, race, take } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_GROUP_DETAIL } from './constants';
import {
  loadGroupDetailSuccess,
  loadGroupDetailError,
} from './actions';

export default [
  getLoadGroupDetailWatcher,
];

export function* getLoadGroupDetailWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadGroupDetail: take(LOAD_GROUP_DETAIL),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestURL = `/group/view/${watcher.loadGroupDetail.id}`;

    const requestConfig = {
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      yield put(loadGroupDetailSuccess(result.data));
    } else {
      yield put(loadGroupDetailError(watcher.loadOrder.id));
    }
  }
}

