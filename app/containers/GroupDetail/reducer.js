/*
 *
 * Group Detail reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_GROUP_DETAIL,
  LOAD_GROUP_DETAIL_SUCCESS,
  LOAD_GROUP_DETAIL_ERROR,
} from './constants';

const initialState = fromJS({
  groupDetail: null,
  groupDetailLoading: false,
});

function groupDetailReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_GROUP_DETAIL:
      return state
        .set('groupDetailLoading', true)
      .set('groupDetail', null);
    case LOAD_GROUP_DETAIL_SUCCESS:
      return state
        .set('groupDetailLoading', false)
      .set('groupDetail', fromJS(action.data));
    case LOAD_GROUP_DETAIL_ERROR:
      return state
      .set('groupDetailLoading', false);
    default:
      return state;
  }
}

export default groupDetailReducer;
