/*
 *
 * MyOrderList constants
 *
 */

export const LOAD_ORDER_LIST = 'MyOrderList/LOAD_ORDER_LIST';
export const LOAD_ORDER_LIST_SUCCESS = 'MyOrderList/LOAD_ORDER_LIST_SUCCESS';
export const LOAD_ORDER_LIST_ERROR = 'MyOrderList/LOAD_ORDER_LIST_ERROR';
