/*
 *
 * Template reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_ORDER_LIST,
  LOAD_ORDER_LIST_ERROR,
  LOAD_ORDER_LIST_SUCCESS,
} from './constants';

const initialState = fromJS({
  orderList: [],

  orderListLoading: false,

  currentPage: 1,
});

function orderListReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_ORDER_LIST:
      return state.set('currentPage', action.page)
      .set('orderListLoading', true);
    case LOAD_ORDER_LIST_SUCCESS: {
      if (state.get('currentPage') === 1) {
        return state.set('orderList', fromJS(action.list))
        .set('orderListLoading', false);
      }
      const list = state.get('orderList').toJS().concat(action.list);
      return state.set('orderList', fromJS(list))
      .set('orderListLoading', false);
    }
    case LOAD_ORDER_LIST_ERROR:
      return state.set('orderListLoading', false);
    default:
      return state;
  }
}

export default orderListReducer;
