/* eslint-disable react/no-will-update-set-state,camelcase,no-unused-vars,react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory } from 'react-router';
import { Modal, Tabs, Badge } from 'antd-mobile';
import WindowTitle from 'components/WindowTitle';
import wechatHelper from 'utils/wechatHelper';
import CommonButton from 'components/CommonButton';
import _ from 'lodash';
import {
  record,
  eventCategory,
  actions,
} from 'utils/gaDIRecordHelper';
import {
  selectCurrentUser,
  selectCurrency,
  selectUserInfoLoading,
} from 'containers/App/selectors';
import {
  getScrollTop,
  getScrollHeight,
  getWindowHeight,
  getTimeLeftDisplay,
} from 'utils/commonHelper';
import OrderList from 'components/OrderList';
import Empty from 'components/Empty';
import HomeBottomAction from 'components/HomeBottomAction';
import { cancelOrder, confirmReceipt } from 'containers/App/actions';
import {
  SINGLE_BUYING,
  ORDER_STATUS_PAYMENT_PENDING,
  ORDER_STATUS_GROUP_ACTIVE,
  ORDER_STATUS_GROUP_SUCCESS,
  ORDER_STATUS_GROUP_FAILED,
  ORDER_STATUS_CANCELLED,
  ORDER_STATUS_REFUNDED,
  ORDER_STATUS_WAIT_WAREHOUSE_PURCHASE,
  ORDER_STATUS_WAREHOUSE_PURCHASED,
  ORDER_STATUS_SUPPLIER_SHIPPED,
  ORDER_STATUS_WAREHOUSE_RECEIVED,
  ORDER_STATUS_ORDER_SHIPPED,
  ORDER_STATUS_USER_RECEIVED,
  ORDER_STATUS_PROBLEM,
  ORDER_STATUS_SUPPLEMENTARY_PAYMENT_PENDING,
} from 'utils/constants';
import styles from './styles.css';
import { selectOrderList, selectOrderListLoading } from './selectors';
import { loadOrderList } from './actions';
import questionIcon from './question.png';

const alert = Modal.alert;

class MyOrderList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      showParentOrderTips: false,
      isOrderCanPay: false,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    if (this.props.currentUser) {
      this.props.loadOrderList(1);
    }
    // window.addEventListener('scroll', this.loadMore);
    wechatHelper.configShareCommon();
    record(actions.ENTER_MY_ORDER_LIST_PAGE, {}, eventCategory.PAGE_ENTER);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.currentUser && nextProps.currentUser) {
      this.props.loadOrderList(1);
    }
    if (this.state.refreshing && this.props.orderListLoading &&
      !nextProps.orderListLoading) {
      this.setState({
        refreshing: false,
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.loadMore);
  }

  onCloseModal() {
    this.setState({
      showParentOrderTips: false,
    });
  }

  getStatusDisplay(item) {
    const ret = this.props.t(`order.status_${item.status}`);
    return ret;
  }

  loadMore() {
    if (this.props.orderListLoading) {
      return;
    }
    const targetPaddingToBottom = 300;
    if (getScrollTop() + getWindowHeight() >=
      getScrollHeight() - targetPaddingToBottom) {
      // todo load more

    }
  }

  render() {
    const { orderList, t } = this.props;
    const path = encodeURIComponent('/myorderlist');
    const tabs = [
      { title: <Badge>{ t('order.all') }</Badge> },
      { title: <Badge>{ t('order.payment_pending') }</Badge> },
      // { title: <Badge>{ t('order.group_active') }</Badge> },
      { title: <Badge>{ t('order.wait_send') }</Badge> },
      { title: <Badge>{ t('order.wait_receive') }</Badge> },
    ];
    const pendingOrder = orderList ? _.filter(orderList,
      (order) => order.status === ORDER_STATUS_PAYMENT_PENDING) : [];
    const groupActiveOrder = orderList ? _.filter(orderList,
      (order) => order.status === ORDER_STATUS_GROUP_ACTIVE) : [];
    const waitShipping = orderList ? _.filter(orderList, (order) =>
      order.status === ORDER_STATUS_GROUP_SUCCESS ||
      order.status === ORDER_STATUS_WAIT_WAREHOUSE_PURCHASE ||
      order.status === ORDER_STATUS_WAREHOUSE_PURCHASED ||
      order.status === ORDER_STATUS_SUPPLIER_SHIPPED ||
      order.status === ORDER_STATUS_WAREHOUSE_RECEIVED
    ) : [];
    const waitReceive = orderList ? _.filter(orderList, (order) =>
      order.status === ORDER_STATUS_ORDER_SHIPPED
    ) : [];
    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.my_order_list')} />
        <div className={styles.orderOutWrap}>
          <Tabs
            swipeable={false}
            tabs={tabs}
            initialPage={0}
            onChange={(tab, index) => {
              console.log('onChange', index, tab);
            }}
            onTabClick={(tab, index) => {
              console.log('onTabClick', index, tab);
            }}
          >
            <div className={styles.listWrap}>
              <OrderList
                isLoading={this.props.orderListLoading}
                orderList={orderList}
                t={t}
                onClickShowParentOrderTips={
                  (isOrderCanPay) => {
                    this.setState({
                      showParentOrderTips: true,
                      isOrderCanPay,
                    });
                  }
                }
              />
            </div>
            <div className={styles.listWrap}>
              <OrderList
                isLoading={this.props.orderListLoading}
                orderList={pendingOrder}
                t={t}
                onClickShowParentOrderTips={
                  (isOrderCanPay) => {
                    this.setState({
                      showParentOrderTips: true,
                      isOrderCanPay,
                    });
                  }
                }
              />
            </div>
            <div className={styles.listWrap}>
              <OrderList
                isLoading={this.props.orderListLoading}
                orderList={groupActiveOrder}
                t={t}
                onClickShowParentOrderTips={
                  (isOrderCanPay) => {
                    this.setState({
                      showParentOrderTips: true,
                      isOrderCanPay,
                    });
                  }
                }
              />
            </div>
            <div className={styles.listWrap}>
              <OrderList
                isLoading={this.props.orderListLoading}
                orderList={waitShipping}
                t={t}
                onClickShowParentOrderTips={
                  (isOrderCanPay) => {
                    this.setState({
                      showParentOrderTips: true,
                      isOrderCanPay,
                    });
                  }
                }
              />
            </div>
            <div className={styles.listWrap}>
              <OrderList
                isLoading={this.props.orderListLoading}
                orderList={waitReceive}
                t={t}
                onClickShowParentOrderTips={
                  (isOrderCanPay) => {
                    this.setState({
                      showParentOrderTips: true,
                      isOrderCanPay,
                    });
                  }
                }
              />
            </div>

          </Tabs>
        </div>
        {
          _.isEmpty(this.props.currentUser) && !this.props.userInfoLoading &&
          <div className={styles.loginWrap}>
            <div className={styles.loginTips}>
              { t('common.you_need_to_login_first') }
            </div>
            <div>
              <CommonButton
                className={styles.loginButton}
                onClick={
                  () => {
                    browserHistory.push(`/login?service=${path}`);
                  }
                }
              >{ t('common.confirm') }</CommonButton>
            </div>
          </div>
        }
        <div className={styles.tabWrap}>
          <HomeBottomAction index={2} />
        </div>
        <Modal
          visible={this.state.showParentOrderTips}
          transparent
          maskClosable={false}
          onClose={() => { this.onCloseModal(); }}
          title={t('common.singly_order_display')}
          footer={[
            {
              text: t('common.i_know'),
              onPress: () => { this.onCloseModal(); },
            }]}
        >
          <div className={styles.singleOrderTips}>
            { this.state.isOrderCanPay ? t(
              'common.singly_order_can_pay_display_reason') : t(
              'common.singly_order_display_reason') }
          </div>
        </Modal>
      </div>
    );
  }
}

MyOrderList.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  orderList: PropTypes.array,
  orderListLoading: PropTypes.bool,
  loadOrderList: PropTypes.func,
  currency: PropTypes.string,
  userInfoLoading: PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectOrderList(),
  selectOrderListLoading(),
  selectCurrency(),
  selectUserInfoLoading(),
  (currentUser, orderList, orderListLoading, currency, userInfoLoading) => ({
    currentUser: currentUser && currentUser.toJS(),
    orderList: (orderList && orderList.toJS()) || [],
    orderListLoading,
    currency,
    userInfoLoading,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadOrderList: (page) => dispatch(loadOrderList(page)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(MyOrderList));
