import { call, put, race, take, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import request from 'utils/request';
import { CANCEL_ORDER_SUCCESS, CONFIRM_RECEIPT_SUCCESS } from 'containers/App/constants';
import { LOAD_ORDER_LIST } from './constants';
import {
  loadOrderListSuccess,
} from './actions';

import {
  selectOrderList,
} from './selectors';
export default [
  getLoadOrderListWatcher,
];


export function* getLoadOrderListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadOrderList: take(LOAD_ORDER_LIST),
      reloadAfterCancelOrder: take(CANCEL_ORDER_SUCCESS),
      reloadAfterConfirmReceipt: take(CONFIRM_RECEIPT_SUCCESS),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const page = watcher.loadOrderList ? watcher.loadOrderList.page : 1;
    const order = yield select(selectOrderList());
    let showLoading = false;
    if (page === 1 && (!order || order.toJS().length === 0)) {
      showLoading = true;
    }

    // const page = watcher.loadGroupList.page;
    const requestURL = '/group/order';
    const requestConfig = {
      urlParams: {
        limit: -1,
        start: 0,
      },
    };
    if (showLoading) {
      requestConfig.feedback = { progress: { mask: false } };
    }
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = [];
      let pendingData = {
        isParentOrder: true,
      };
      let needAddPendingData = false;
      for (let i = 0; i < result.data.length; i += 1) {
        const orderData = result.data[i];
        if (!orderData.order_number) {
          // old
          if (needAddPendingData) {
            list.push(Object.assign({}, pendingData));
            needAddPendingData = false;
            pendingData = {
              isParentOrder: true,
            };
          }
          list.push(orderData);
        } else if (i === result.data.length - 1) {
            // last one
          if (needAddPendingData) {
            if (pendingData.order_number === orderData.order_number) {
              pendingData.orders.push(orderData);
              list.push(Object.assign({}, pendingData));
              needAddPendingData = false;
            } else {
              list.push(Object.assign({}, pendingData));
              needAddPendingData = false;
              list.push(orderData);
            }
          } else {
            list.push(orderData);
          }
        } else if (needAddPendingData) {
          if (pendingData.order_number === orderData.order_number) {
            pendingData.orders.push(orderData);
          } else {
            list.push(Object.assign({}, pendingData));
            needAddPendingData = false;
            pendingData = {
              isParentOrder: true,
            };
            const nextData = result.data[i + 1];
            if (nextData.order_number === orderData.order_number) {
              needAddPendingData = true;
              pendingData.order_number = orderData.order_number;
              pendingData.status = orderData.status;
              if (!pendingData.orders) {
                pendingData.orders = [];
              }
              pendingData.orders.push(orderData);
            } else {
              list.push(orderData);
            }
          }
        } else {
          const nextData = result.data[i + 1];
          if (nextData.order_number === orderData.order_number) {
            needAddPendingData = true;
            pendingData.order_number = orderData.order_number;
            pendingData.status = orderData.status;
            if (!pendingData.orders) {
              pendingData.orders = [];
            }
            pendingData.orders.push(orderData);
          } else {
            list.push(orderData);
          }
        }
      }
      if (needAddPendingData) {
        list.push(Object.assign({}, pendingData));
      }
      // console.log(list);
      yield put(loadOrderListSuccess(list));
    }
  }
}
