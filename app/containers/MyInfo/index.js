/* eslint-disable no-param-reassign */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, Icon, List, Picker } from 'antd-mobile';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory, withRouter, Link } from 'react-router';
import Money from 'components/Money';
import RedPacket from 'components/RedPacket';
import CopyToClipboard from 'react-copy-to-clipboard';
import HomeBottomAction from 'components/HomeBottomAction';
import WindowTitle from 'components/WindowTitle';
import GraySpace from 'components/GraySpace';
import message from 'components/message';
import CountryName from 'components/CountryName';
import WeChatShare from 'components/WeChatShare';
import CommonButton from 'components/CommonButton';
import {
  RED_PACKET_MODAL_TYPE,
} from 'containers/App/constants';
import _ from 'lodash';
import {
  selectCurrentUser,
  selectUserInfoLoading,
  selectOpenId,
  selectLatestCollectShareCouponTime,
  selectShowShareRedPacketModal,
  selectCurrentFixCoupon,
  selectUnpaidShippingList,
  selectUnpaidShippingListLoading,
} from 'containers/App/selectors';
import wechatHelper from 'utils/wechatHelper';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import {
  localStorageKeys,
  MY_ORDER_TYPE_MY_START,
  MY_ORDER_TYPE_MY_FOLLOW,
  GROUP_STATUS_ACTIVE,
  GROUP_STATUS_FINISH,
  DEFAULT_COUNTRY_ISO,
} from 'utils/constants';

import {
  setWeChatAuthDirectUrl,
  logout,
  loadUserInfoDetail,
  loadFixCoupon,
  setShowShareRedPacketModal,
  loadUnpaidShipping,
  changeCountry,
} from 'containers/App/actions';
import close from './close.png';
import styles from './styles.css';
import start from './my_start.png';
import follow from './my_follow.png';
import progress from './my_progress.png';
import finish from './my_finish.png';
import userIcon from './user.png';
import questionIcon from './question.png';
import submitIcon from './submit.png';
import giftIcon from './gift.png';
import feedbackIcon from './feedback.png';
import logo from './logo.png';
import billingIcon from './billing.png';
import countryIcon from './country.png';
import addressIcon from './address.png';
import updatePasswordIcon from './update_password.png';

const Item = List.Item;
const alert = Modal.alert;

const supportedCountries = [{
  label: (<div>
    <CountryName flag iso="GB" />
  </div>),
  value: 'GB',
}, {
  label: (<div>
    <CountryName flag iso="DE" />
  </div>),
  value: 'DE',
}, {
  label: (<div>
    <CountryName flag iso="FR" />
  </div>),
  value: 'FR',
}];

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector ||
    el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement;
  }
  return null;
}

class MyInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showUnpaidShippingModal: false,
      showWeChatShareTips: false,
      visibleStatus: 'visible',
      countryPickerVisible: false,
      countryPickerValue: [localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO],
      hasInitSetting: false,
    };
  }

  componentDidMount() {
    this.props.loadUserInfoDetail();
    wechatHelper.configShareCommon();
    record(actions.ENTER_MY_INFO_PAGE, {}, eventCategory.PAGE_ENTER);
    document.addEventListener('visibilitychange', this.visibleStatusChange.bind(this));
  }

  componentWillReceiveProps(props) {
    const { currentUser } = props;
    if (!_.isEmpty(currentUser) && !this.state.hasInitSetting && currentUser.country_iso) {
      this.setState({
        countryPickerValue: [currentUser.country_iso],
        hasInitSetting: true,
      });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('visibilitychange', this.visibleStatusChange);
  }

  onCloseUnpaidShippingModal = () => {
    this.setState({
      showUnpaidShippingModal: false,
    });
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  }

  onCountryPickerChange = (v) => {
    this.setState({ countryPickerValue: v });
    if (!_.isEmpty(this.props.currentUser) && this.props.currentUser.country_iso !== v[0]) {
      this.props.changeCountry(v[0]);
    }
  }

  followOfficialAccount = () => {
    window.location = wechatHelper.genWechatServiceUrl();
  }

  visibleStatusChange() {
    const current = this.state.visibleStatus;
    const showShareSuggest = ((new Date()).getTime() - this.props.latestCollectShareCouponTime) > 60000;
    const currentUser = this.props.currentUser;
    if (current === 'hidden' && document.visibilityState === 'visible' && showShareSuggest && !_.isEmpty(currentUser)) {
      this.props.loadFixCoupon();
    }
    this.setState({
      visibleStatus: document.visibilityState,
    });
  }

  render() {
    const { t, currentUser = {}, userInfoLoading, latestCollectShareCouponTime = 0, unpaidShippingList = [], unpaidShippingListLoading } = this.props;
    const needLogin = userInfoLoading ? false : _.isEmpty(currentUser);
    const hasLogin = !_.isEmpty(currentUser);
    let couponCount = currentUser.valid_coupon_count || 0;
    const showCouponRedDot = parseInt(couponCount, 10) > 0;
    let historyCount = currentUser.history_count;
    if (parseInt(couponCount, 10) > 99) {
      couponCount = '99+';
    }
    if (parseInt(historyCount, 10) > 99) {
      historyCount = '99+';
    }
    let avatar = userIcon;
    if (!_.isEmpty(currentUser)) {
      avatar = currentUser.wechat_avatar_url || userIcon;
    }
    let progressGroupCount = currentUser.progress_group_count || 0;
    const showProgressGroupCount = parseInt(progressGroupCount, 10) > 0;
    const path = encodeURIComponent('/myinfo');
    if (parseInt(progressGroupCount, 10) > 99) {
      progressGroupCount = 99;
    }
    const showShareSuggest = ((new Date()).getTime() - latestCollectShareCouponTime) > 60000;
    const copySuccess = wechatHelper.isOpenInWechat() ? t('common.copy_success_wechat') : t('common.copy_success');
    const url = `${window.location.protocol}//${window.location.host}`;
    return (
      <div className={styles.myInfoWrap}>
        <WindowTitle title={t('pageTitles.my_account')} />
        <div className={styles.topWrap}>
          <div className={styles.userInfo}>
            <img
              className={styles.userIcon}
              alt={'user'}
              src={avatar}
            ></img>
            <div className={styles.userName}>
              {currentUser.wechat_name || currentUser.username}
            </div>
            <div className={styles.userPhone}>
              {`${currentUser.dial_code || ''}${currentUser.telephone || ''}`}
            </div>
          </div>
          <div className={styles.userTab}>
            <div className={styles.userTabItem}>
              <div className={styles.userTabBalance}>
                <Money symbolClassName={styles.userTabSymbol} amount={currentUser ? currentUser.balance : 0} />
              </div>
              <div className={styles.userTabLabel}>
                {t('my_info.balance')}
              </div>
            </div>
            <div className={styles.userTabItem}>
              <div className={styles.userTabBalance}>
                <Money symbolClassName={styles.userTabSymbol} amount={currentUser && currentUser.unpaid_shipping_amount ? currentUser.unpaid_shipping_amount : 0} />
                {!_.isEmpty(currentUser) && parseFloat(currentUser.unpaid_shipping_amount) > 0 && (
                  <span
                    onClick={() => {
                      this.props.loadUnpaidShipping();
                      this.setState({
                        showUnpaidShippingModal: true,
                      });
                    }}
                  >
                    <img src={questionIcon} className={styles.questionIcon} alt={'?'} />
                  </span>
                )}
              </div>
              <div className={styles.userTabLabel}>
                {t('my_info.unpaid_shipping')}
              </div>
            </div>
            {/* <div
              className={styles.userTabItem}
              onClick={() => {
                browserHistory.push('/coupons');
              }}
            >
              <div className={styles.userTabCoupon}>
                {
                  showCouponRedDot && <span className={styles.redDot}></span>
                }
                {couponCount}
              </div>
              <div className={styles.userTabLabel}>
                {t('my_info.coupon')}
              </div>
            </div> */}
            <div
              className={styles.userTabItem}
              onClick={() => {
                browserHistory.push('/product-browse-history');
              }}
            >
              <div className={styles.userTabHistory}>
                {historyCount}
              </div>
              <div className={styles.userTabLabel}>
                {t('my_info.history')}
              </div>
            </div>
          </div>
        </div>

        <div className={styles.myOrderWrap}>
          <Link to={'/myorderlist'} className={styles.myOrderTop}>
            <span className={styles.myOrderTopTitle}>{t('my_info.my_order')}</span>
            <span className={styles.myOrderTopView}>{t('my_info.view_all_order')}</span>
          </Link>
          {/* <div className={styles.myGroupList}>
            <Link to={`/orderlist/${MY_ORDER_TYPE_MY_START}`} className={styles.myGroupItem}>
              <img src={start} className={styles.myGroupIcon} alt={'start'} />
              <span>{t('my_info.start')}</span>
            </Link>
            <Link to={`/orderlist/${MY_ORDER_TYPE_MY_FOLLOW}`} className={styles.myGroupItem}>
              <img src={follow} className={styles.myGroupIcon} alt={'follow'} />
              <span>{t('my_info.follow')}</span>
            </Link>
            <Link to={`/orderlist/${GROUP_STATUS_ACTIVE}`} className={`${styles.myGroupItem} ${styles.myGroupItemProgressGroup}`}>
              {
                showProgressGroupCount && <span className={styles.groupCount}>{progressGroupCount}</span>
              }
              <img src={progress} className={styles.myGroupIcon} alt={'progress'} />
              <span>{t('my_info.progress')}</span>
            </Link>
            <Link to={`/orderlist/${GROUP_STATUS_FINISH}`} className={styles.myGroupItem}>
              <img src={finish} className={styles.myGroupIcon} alt={'finish'} />
              <span>{t('my_info.finish')}</span>
            </Link>
          </div> */}
        </div>

        <div className={styles.actionWrap}>
          {/* <Picker
            visible={this.state.countryPickerVisible}
            data={supportedCountries}
            cols={1}
            value={this.state.countryPickerValue}
            onChange={this.onCountryPickerChange}
            onOk={() => this.setState({ countryPickerVisible: false })}
            onDismiss={() => this.setState({ countryPickerVisible: false })}
          >
            <div
              className={styles.actionItem}
            >
              <div
                className={styles.actionItemLeft}
              >
                <img src={countryIcon} alt={''} className={styles.actionItemLeftIcon} />
                <span className={styles.actionItemLeftText}>
                  {t('common.current_country')}
                </span>
              </div>
              <div
                className={styles.actionItemRight}
                onClick={() => {
                  this.setState({ countryPickerVisible: true });
                }}
              >
                <span className={styles.actionItemNav}>
                  <CountryName iso={this.state.countryPickerValue[0]} />
                  <Icon type="right" />
                </span>
              </div>
            </div>
          </Picker> */}

          <div
            className={styles.actionItem}
            onClick={() => {
              browserHistory.push('/mysubmitproductlist');
            }}
          >
            <div
              className={styles.actionItemLeft}
            >
              <img src={submitIcon} alt={''} className={styles.actionItemLeftIcon} />
              <span className={styles.actionItemLeftText}>
                {t('my_info.my_submit_product')}
              </span>
            </div>
            <div className={styles.actionItemRight}>
              <span className={styles.actionItemNav}><Icon type="right" /></span>
            </div>
          </div>

          <div
            className={styles.actionItem}
            onClick={() => {
              browserHistory.push('/payment-records');
            }}
          >
            <div
              className={styles.actionItemLeft}
            >
              <img src={billingIcon} alt={''} className={styles.actionItemLeftIcon} />
              <span className={styles.actionItemLeftText}>
                {t('pageTitles.payment_records')}
              </span>
            </div>
            <div className={styles.actionItemRight}>
              <span className={styles.actionItemNav}><Icon type="right" /></span>
            </div>
          </div>

          <div
            className={styles.actionItem}
            onClick={() => {
              browserHistory.push('/address');
            }}
          >
            <div
              className={styles.actionItemLeft}
            >
              <img src={addressIcon} alt={''} className={styles.actionItemLeftIcon} />
              <span className={styles.actionItemLeftText}>
                {t('pageTitles.address_manage')}
              </span>
            </div>
            <div className={styles.actionItemRight}>
              <span className={styles.actionItemNav}><Icon type="right" /></span>
            </div>
          </div>

          <div
            className={styles.actionItem}
            onClick={() => {
              browserHistory.push('/change-password');
            }}
          >
            <div className={styles.actionItemLeft}>
              <img src={updatePasswordIcon} alt={''} className={styles.actionItemLeftIcon} />
              <span className={styles.actionItemLeftText}>
                {t('my_info.update_password')}
              </span>
            </div>
            <div className={styles.actionItemRight}>
              <span className={styles.actionItemNav}><Icon type="right" /></span>
            </div>
          </div>
          {/* <CopyToClipboard
            text={url}
            onCopy={() => {
              if (wechatHelper.isOpenInWechat()) {
                this.setState({
                  showWeChatShareTips: true,
                });
              } else {
                message.success(copySuccess);
              }
            }}
          >
            <div className={styles.actionItem}>
              <div className={styles.actionItemLeft}>
                <img src={giftIcon} alt={''} className={styles.actionItemLeftIcon} />
                <span className={styles.actionItemLeftText}>
                  {t('my_info.share')}
                </span>
              </div>
              <div className={styles.actionItemRight}>
                {
                showShareSuggest &&
                  <span className={styles.shareSuggest}>{t('my_info.getShareCoupon')}</span>
                }
                <span className={styles.actionItemNav}><Icon type="right" /></span>
              </div>
            </div>
          </CopyToClipboard> */}
          {/* <div
            className={styles.actionItem}
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              window.location = 'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515';
            }}
          >
            <div className={styles.actionItemLeft}>
              <img src={feedbackIcon} alt={''} className={styles.actionItemLeftIcon} />
              <span className={styles.actionItemLeftText}>
                {t('my_info.feedback')}
              </span>
            </div>
            <div className={styles.actionItemRight}>
              <span className={styles.actionItemNav}><Icon type="right" /></span>
            </div>
          </div> */}

          {/* <div
            className={styles.actionItem}
            onClick={this.followOfficialAccount}
          >
            <div className={styles.actionItemLeft}>
              <img src={logo} alt={''} className={styles.followOfficialAccountIcon} />
              <span className={styles.actionItemLeftText}>
                {t('pageTitles.follow_official_account')}
              </span>
            </div>
            <div className={styles.actionItemRight}>
              <span className={styles.actionItemNav}><Icon type="right" /></span>
            </div>
          </div> */}
        </div>

        { hasLogin && <div className={styles.logoutWrap}>
          <CommonButton
            className={styles.logout}
            onClick={() =>
              alert('', t('common.confirm_logout'), [
                { text: t('common.cancel') },
                {
                  text: t('common.logout'),
                  onPress: () => this.props.logout(),
                },
              ])
            }
          >
            { t('common.logout') }
          </CommonButton>
        </div>
        }

        <GraySpace size="xxl" />

        {
          needLogin
          &&
          <div className={styles.loginWrap}>
            <div className={styles.loginTips}>
              {t('common.you_need_to_login_first')}
            </div>
            <div>
              <CommonButton
                onClick={
                  () => {
                    browserHistory.push(`/login?service=${path}`);
                  }
                }
                className={styles.loginButton}
              >{t('common.confirm')}</CommonButton>
            </div>
          </div>
        }

        <Modal
          visible={this.state.showUnpaidShippingModal}
          transparent
          maskClosable
          onClose={this.onCloseUnpaidShippingModal}
          wrapClassName={styles.unpaidWrapper}
        >
          <p className={styles.unpaidTips}>{t('common.unpaid_shipping_tips')}</p>
          {unpaidShippingListLoading ? (
            <div className={styles.loadingIcon}><Icon type="loading" /></div>
          ) : (
            <List className={styles.unpaidList}>
              {unpaidShippingList.map((item) => (
                <Item
                  key={item.id}
                  extra={(
                    <Money amount={Math.abs(item.amount)} />
                  )}
                  align="top"
                  multipleLine
                  onClick={() => {
                    if (item.order_id) {
                      browserHistory.push(`/orderdetail/${item.order_id}`);
                    }
                  }}
                >
                  {item.product_name}
                </Item>
              ))}
            </List>
          )}
        </Modal>
        { this.state.showWeChatShareTips &&
          <div
            onClick={
              (e) => {
                e.preventDefault();
                this.setState({
                  showWeChatShareTips: false,
                });
              }
            }
          >
            <WeChatShare />
          </div>
        }
        {/* <Modal
          className={'redPacketModal'}
          visible={this.props.showShareRedPacketModal}
          transparent
          maskClosable
          onClose={() => {
            this.props.setShowShareRedPacketModal(false);
          }}
          afterClose={() => {
            // this.props.loadACoupon();
          }}
          title=""
          footer={[]}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        >
          <div className={styles.collectContainer}>
            <RedPacket
              coupon={this.props.currentFixCoupon}
              type={RED_PACKET_MODAL_TYPE.PRESENT}
              onClickOpenCouponPage={() => {
                this.props.setShowShareRedPacketModal(false);
              }}
            />
            <div
              onClick={() => {
                this.props.setShowShareRedPacketModal(false);
              }}
            >
              <img
                src={close}
                className={styles.close}
                alt={'X'}
              />
            </div>
          </div>
        </Modal> */}
        <div className={styles.tabWrap}>
          <HomeBottomAction index={3} />
        </div>
      </div>
    );
  }
}

MyInfo.propTypes = {
  currentUser: PropTypes.object,
  userInfoLoading: PropTypes.bool,
  t: PropTypes.func,
  logout: PropTypes.func,
  loadUserInfoDetail: PropTypes.func,
  latestCollectShareCouponTime: PropTypes.number,
  loadFixCoupon: PropTypes.func,
  showShareRedPacketModal: PropTypes.bool,
  setShowShareRedPacketModal: PropTypes.func,
  currentFixCoupon: PropTypes.object,
  loadUnpaidShipping: PropTypes.func,
  unpaidShippingList: PropTypes.array,
  unpaidShippingListLoading: PropTypes.bool,
  changeCountry: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectUserInfoLoading(),
  selectOpenId(),
  selectUnpaidShippingList(),
  selectUnpaidShippingListLoading(),
  selectLatestCollectShareCouponTime(),
  selectShowShareRedPacketModal(),
  selectCurrentFixCoupon(),
  (currentUser, userInfoLoading, openId, unpaidShippingList, unpaidShippingListLoading, latestCollectShareCouponTime, showShareRedPacketModal, currentFixCoupon) => ({
    currentUser: currentUser ? currentUser.toJS() : {},
    userInfoLoading,
    openId: openId || localStorage.getItem(localStorageKeys.WECHAT_OPEN_ID),
    unpaidShippingList: unpaidShippingList ? unpaidShippingList.toJS() : [],
    unpaidShippingListLoading,
    latestCollectShareCouponTime,
    showShareRedPacketModal,
    currentFixCoupon: currentFixCoupon && currentFixCoupon.toJS(),
  }),
);

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
  setWeChatAuthDirectUrl: (url) => dispatch(setWeChatAuthDirectUrl(url)),
  loadUserInfoDetail: () => dispatch(loadUserInfoDetail()),
  loadFixCoupon: () => dispatch(loadFixCoupon()),
  setShowShareRedPacketModal: (show) => dispatch(setShowShareRedPacketModal(show)),
  loadUnpaidShipping: () => dispatch(loadUnpaidShipping()),
  changeCountry: (countryIso) => dispatch(changeCountry(countryIso)),
  dispatch,
});

export default translate()(
  withRouter(connect(mapStateToProps, mapDispatchToProps)(MyInfo)));
