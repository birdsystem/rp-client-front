/* eslint-disable react/sort-comp,react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { Icon } from 'antd-mobile';
import { selectCouponList, selectCouponListLoading, selectRegisterFrom } from 'containers/App/selectors';
import { loadCouponList } from 'containers/App/actions';
import Coupon from 'components/Coupon';
import CommonButton from 'components/CommonButton';
import wechatHelper from 'utils/wechatHelper';
import styles from './styles.css';

class CouponPackage extends Component {

  componentDidMount() {
    wechatHelper.configShareCommon();
    this.props.loadCouponList({ showLoading: false });
  }

  componentWillUnmount() {

  }

  render() {
    return (
      <div className={styles.wrap}>
        <div className={styles.inner}>
          <div className={styles.list}>
            <div className={styles.title}>
              <div>{this.props.t('common.register_success_and_get_coupon_package')}</div>
              <div className={styles.packageName}>
                {this.props.t(`coupon_package_type.${this.props.registerFrom}`)}
              </div>
            </div>
            { (this.props.couponList && this.props.couponList.length === 0) ? <div className={styles.loadingWrap}>
              { !this.props.loadingCouponList && this.props.t('coupon.empty_coupon') }
              { this.props.loadingCouponList && <Icon type="loading" size={'medium'} /> }
            </div>
              : <ul className={styles.couponListWrap}>
                { this.props.couponList.map((coupon) => (
                  <li className={styles.couponWrap} key={coupon.id}>
                    <Coupon
                      showToUser={false}
                      coupon={coupon}
                    />
                  </li>
                )) }
              </ul>
            }
          </div>
          <div className={styles.footer}>
            <div className={styles.tips}>{this.props.t('common.follow_official_account_text')}</div>
            <div className={styles.footerAction}>
              <div className={styles.footerItem}>
                <CommonButton
                  onClick={() => {
                    browserHistory.replace('/');
                  }}
                  className={styles.followButton}
                >{ this.props.t('common.shop_at_once') }</CommonButton>
              </div>
              {/* <div className={styles.footerItem}>*/}
              {/* <CommonButton*/}
              {/* onClick={() => {*/}
              {/* window.location = wechatHelper.genWechatServiceUrl();*/}
              {/* }}*/}
              {/* type={'dark'}*/}
              {/* className={styles.followButton}*/}
              {/* >{ this.props.t('pageTitles.follow_official_account') }</CommonButton>*/}
              {/* </div>*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CouponPackage.propTypes = {
  t: PropTypes.func,
  show: PropTypes.bool,
  onTapStart: PropTypes.func,
  loadingCouponList: PropTypes.bool,
  couponList: PropTypes.array,
  registerFrom: PropTypes.string,
  loadCouponList: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCouponList(),
  selectCouponListLoading(),
  selectRegisterFrom(),
  (couponList, loadingCouponList, registerFrom) => ({
    couponList: couponList.toJS(),
    loadingCouponList,
    registerFrom,
  }),
);


const mapDispatchToProps = (dispatch) => ({
  loadCouponList: (data) => dispatch(loadCouponList(data)),
  dispatch,
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(CouponPackage));
