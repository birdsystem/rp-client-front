/* eslint-disable react/sort-comp,react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import WindowTitle from 'components/WindowTitle';
import wechatHelper from 'utils/wechatHelper';
import {
  selectCurrentUser,
} from 'containers/App/selectors';
import AddressComponent from 'components/Address';
import styles from './styles.css';

class Address extends Component {

  componentDidMount() {
    wechatHelper.configShareCommon();
  }

  componentWillUnmount() {

  }

  render() {
    return (
      <div className={styles.main}>
        <WindowTitle title={this.props.t('pageTitles.address_manage')} />
        <AddressComponent />
      </div>
    );
  }
}

Address.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  (currentUser) => ({
    currentUser: currentUser && currentUser.toJS(),
  }),
);

const mapDispatchToProps = null;
export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(Address));
