import { createSelector } from 'reselect';

const selectRecommendDomain = () => (state) => state.get('recommend');

const selectRecommendData = () => createSelector(
  selectRecommendDomain(),
  (substate) => substate.get('recommendData')
);

const selectRecommendDataLoading = () => createSelector(
  selectRecommendDomain(),
  (substate) => substate.get('recommendDataLoading')
);

const selectAdvertisement = () => createSelector(
  selectRecommendDomain(),
  (substate) => substate.get('advertisement')
);

export {
  selectRecommendData,
  selectRecommendDataLoading,
  selectAdvertisement,
};
