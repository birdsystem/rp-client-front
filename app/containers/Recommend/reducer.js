/*
 *
 * Template reducer
 *
 */

import { fromJS } from 'immutable';
import {
  localStorageKeys,
} from 'utils/constants';
import {
  LOAD_RECOMMEND_DATA,
  LOAD_RECOMMEND_DATA_ERROR,
  LOAD_RECOMMEND_DATA_SUCCESS,
  LOAD_ADVERTISEMENT_SUCCESS,
} from './constants';

const initialState = fromJS({
  recommendData: [],

  recommendDataLoading: false,

  advertisement: (localStorage.getItem(localStorageKeys.ADVERTISEMENT) && JSON.parse(localStorage.getItem(localStorageKeys.ADVERTISEMENT))) || null,
});

function recommendReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_RECOMMEND_DATA:
      return state.set('recommendDataLoading', true);
    case LOAD_RECOMMEND_DATA_SUCCESS: {
      return state.set('recommendData', fromJS(action.data))
      .set('recommendDataLoading', false);
    }
    case LOAD_RECOMMEND_DATA_ERROR:
      return state.set('recommendDataLoading', false);
    case LOAD_ADVERTISEMENT_SUCCESS:
      localStorage.setItem(localStorageKeys.ADVERTISEMENT, JSON.stringify(action.data));
      return state.set('advertisement', fromJS(action.data));
    default:
      return state;
  }
}

export default recommendReducer;
