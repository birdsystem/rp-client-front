/* eslint-disable react/sort-comp,react/no-unused-prop-types */
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory } from 'react-router';
import WindowTitle from 'components/WindowTitle';
import CommonButton from 'components/CommonButton';
import { Carousel, Badge, Tabs, ListView, Modal } from 'antd-mobile';
import wechatHelper from 'utils/wechatHelper';
import RedPacket from 'components/RedPacket';
import { RED_PACKET_MODAL_TYPE } from 'containers/App/constants';
import { categoryMap } from 'utils/constants';
import { selectCurrency, selectCurrentUser, selectUserInfoLoading, selectShowShareRedPacketModal, selectCurrentFixCoupon, selectLatestCollectShareCouponTime } from 'containers/App/selectors';
import { setShowShareRedPacketModal, loadFixCoupon } from 'containers/App/actions';
import {
  eventCategory,
  record,
  actions,
  eventParams,
} from 'utils/gaDIRecordHelper';
import ProductItem from 'components/ProductItem';
import close from './close.png';
import styles from './styles.css';
import { selectAdvertisement, selectRecommendData, selectRecommendDataLoading } from './selectors';
import { loadAdvertisement, loadRecommendData } from './actions';

class Recommend extends Component {

  constructor(props) {
    super(props);
    const { location } = this.props;
    let getReward = false;
    if (location && location.query && location.query.get_reward) {
      getReward = true;
    }
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    this.state = {
      dataSource,
      getReward,
    };
  }

  componentDidMount() {
    this.props.loadRecommendData();
    this.props.loadAdvertisement();
    const canGetReward = ((new Date()).getTime() - this.props.latestCollectShareCouponTime) > 3600 * 24 * 1000;
    // const canGetReward = true;
    if (this.state.getReward && canGetReward) {
      this.props.loadFixCoupon();
    }
    wechatHelper.configShareCommon();
    record(actions.ENTER_RECOMMEND_PAGE, {}, eventCategory.PAGE_ENTER);
  }

  render() {
    const { advertisement, t, recommendData } = this.props;
    const banners = (advertisement && advertisement.banners) || [];
    let bannerHeight = (window.screen.availWidth * 350) / 840;
    if (bannerHeight > 200) {
      bannerHeight = 200;
    }
    const row = (rowData) => (
      <ProductItem
        key={`p${rowData.id}`}
        product={rowData}
        onClickItem={() => {
          record(actions.CLICK_RECOMMEND_PRODUCT_ITEM, {}, eventCategory.USER_ACTION);
        }}
        withoutOutMargin
      />
    );
    let tabs = [];
    if (!_.isEmpty(recommendData)) {
      tabs = recommendData.map((item, index) => {
        let name = item.name;
        if (categoryMap[name]) {
          name = categoryMap[name];
        }
        return (
          { title: <Badge keys={`b${index}`}>{ name }</Badge> }
        );
      });
    }
    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.recommend')} />

        <div className={styles.bannerWrap}>
          { banners.length > 0 && <Carousel
            autoplay
            infinite
            dragging={false}
          >
            { banners.map((banner, index) => (
              <div
                key={`banner${index}`}
                className={styles.bannerLink}
                style={
                {
                  height: `${bannerHeight}px`,
                }
                }
                onClick={() => {
                  const data = {};
                  // data.event_callback = () => {
                    if (banner && banner.link) {
                      browserHistory.push(banner.link);
                    }
                  // };
                  data[eventParams.ID] = banner.id;
                  data[eventParams.LINK] = banner.link;
                  record(actions.CLICK_RECOMMEND_BANNER,
                    data,
                    eventCategory.USER_ACTION,
                  );
                }}
              >
                <div
                  className={styles.bannerImg}
                  style={{
                    backgroundImage: `url("${banner.image_url}")`,
                    height: `${bannerHeight}px`,
                  }}
                />
              </div>
            )) }
          </Carousel>
          }
        </div>
        <div className={styles.tabWrap}>
          <Tabs
            swipeable={false}
            tabs={tabs}
            initialPage={0}
            onTabClick={(tab, index) => {
              console.log(index);
            }}
          >
            {
              recommendData.map((item, index) => (
                <div className={styles.listWrap} key={`p${index}`}>
                  <ListView
                    renderRow={row}
                    useBodyScroll
                    initialListSize={500}
                    scrollRenderAheadDistance={500}
                    onEndReachedThreshold={10}
                    rowIdentities={() => null}
                    dataSource={this.state.dataSource.cloneWithRows(item.list)}
                  />
                </div>
                ))
            }
          </Tabs>
        </div>

        <div className={styles.moreWrap}>
          <CommonButton
            type={'dark'}
            className={styles.moreButton}
            onClick={() => {
              record(actions.CLICK_RECOMMEND_BACK_HOME, {}, eventCategory.USER_ACTION);
              browserHistory.push('/');
            }}
          >{ t('common.more_product') }</CommonButton>
        </div>

        <Modal
          className={'redPacketModal'}
          visible={this.props.showShareRedPacketModal}
          transparent
          maskClosable
          onClose={() => {
            this.props.setShowShareRedPacketModal(false);
          }}
          afterClose={() => {
            // this.props.loadACoupon();
          }}
          title=""
          footer={[]}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        >
          <div className={styles.collectContainer}>
            <RedPacket
              coupon={this.props.currentFixCoupon}
              type={RED_PACKET_MODAL_TYPE.PRESENT}
              onClickOpenCouponPage={() => {
                this.props.setShowShareRedPacketModal(false);
              }}
              showUse={false}
            />
            <div
              onClick={() => {
                this.props.setShowShareRedPacketModal(false);
              }}
            >
              <img
                src={close}
                className={styles.close}
                alt={'X'}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

Recommend.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  recommendData: PropTypes.array,
  recommendDataLoading: PropTypes.bool,
  loadRecommendData: PropTypes.func,
  currency: PropTypes.string,
  userInfoLoading: PropTypes.bool,
  params: PropTypes.object,
  loadAdvertisement: PropTypes.func,
  advertisement: PropTypes.object,
  location: PropTypes.object,
  loadFixCoupon: PropTypes.func,
  showShareRedPacketModal: PropTypes.bool,
  setShowShareRedPacketModal: PropTypes.func,
  currentFixCoupon: PropTypes.object,
  latestCollectShareCouponTime: PropTypes.number,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectRecommendData(),
  selectRecommendDataLoading(),
  selectCurrency(),
  selectUserInfoLoading(),
  selectAdvertisement(),
  selectShowShareRedPacketModal(),
  selectCurrentFixCoupon(),
  selectLatestCollectShareCouponTime(),
  (currentUser, recommendData, recommendDataLoading, currency, userInfoLoading, advertisement, showShareRedPacketModal, currentFixCoupon, latestCollectShareCouponTime) => ({
    currentUser: currentUser && currentUser.toJS(),
    recommendData: recommendData && recommendData.toJS(),
    recommendDataLoading,
    currency,
    userInfoLoading,
    advertisement: advertisement && advertisement.toJS(),
    showShareRedPacketModal,
    currentFixCoupon: currentFixCoupon && currentFixCoupon.toJS(),
    latestCollectShareCouponTime,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadRecommendData: () => dispatch(loadRecommendData()),
  loadAdvertisement: () => dispatch(loadAdvertisement()),
  loadFixCoupon: () => dispatch(loadFixCoupon()),
  setShowShareRedPacketModal: (show) => dispatch(setShowShareRedPacketModal(show)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(Recommend));
