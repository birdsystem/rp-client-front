/*
 *
 * Global actions
 *
 */

import {
  LOAD_RECOMMEND_DATA,
  LOAD_RECOMMEND_DATA_SUCCESS,
  LOAD_RECOMMEND_DATA_ERROR,
  LOAD_ADVERTISEMENT,
  LOAD_ADVERTISEMENT_SUCCESS,
  LOAD_ADVERTISEMENT_ERROR,
} from './constants';

export function loadRecommendData() {
  return {
    type: LOAD_RECOMMEND_DATA,
  };
}

export function loadRecommendDataSuccess(data) {
  return {
    type: LOAD_RECOMMEND_DATA_SUCCESS,
    data,
  };
}

export function loadRecommendDataError() {
  return {
    type: LOAD_RECOMMEND_DATA_ERROR,
  };
}

export function loadAdvertisement() {
  return {
    type: LOAD_ADVERTISEMENT,
  };
}

export function loadAdvertisementSuccess(data) {
  return {
    type: LOAD_ADVERTISEMENT_SUCCESS,
    data,
  };
}

export function loadAdvertisementError() {
  return {
    type: LOAD_ADVERTISEMENT_ERROR,
  };
}
