/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { WingBlank, TextareaItem, Modal, Icon } from 'antd-mobile';
import WindowTitle from 'components/WindowTitle';
import CommonButton from 'components/CommonButton';
import Tips from 'components/Tips';
import HomeBottomAction from 'components/HomeBottomAction';
import { submitProduct } from 'containers/App/actions';
import { selectCurrentUser, selectLocationState } from 'containers/App/selectors';
import { localStorageKeys } from 'utils/constants';
import wechatHelper from 'utils/wechatHelper';
import {
  record,
  eventCategory,
  actions,
  eventParams,
} from 'utils/gaDIRecordHelper';
import styles from './styles.css';
import image1 from './1.png';
import image2 from './2.png';
import image3 from './3.png';
import image4 from './4.png';
import image5 from './5.png';
import image6 from './6.png';
import image7 from './7.png';
import image8 from './8.png';
import image9 from './9.png';
import image10 from './10.png';
import image11 from './11.png';
import image12 from './12.png';
import image13 from './13.png';
import image14 from './14.png';
import image15 from './15.png';
import image16 from './16.png';
import image17 from './17.png';
import image18 from './18.png';
import image19 from './19.png';
import image20 from './20.png';
import image21 from './21.png';
import image22 from './22.png';
import image23 from './23.png';
import image24 from './24.png';
import image25 from './25.png';
import image26 from './26.png';
import image27 from './27.png';
import image28 from './28.png';
import image29 from './29.png';
import image30 from './30.png';
import image31 from './31.png';
import image32 from './32.png';
import image33 from './33.png';
import image34 from './34.png';
import image35 from './35.png';
import image36 from './36.png';
import image37 from './37.png';
import image38 from './38.png';
import image39 from './39.png';
import image40 from './40.png';
import image41 from './41.png';
import image42 from './42.png';
import image43 from './43.png';
import image44 from './44.png';
import image45 from './45.png';
import image46 from './46.png';
import image47 from './47.png';
import image48 from './48.png';
import image49 from './49.png';
import image50 from './50.png';
import image51 from './51.png';
import image52 from './52.png';
import image53 from './53.png';
import image54 from './54.png';
import image55 from './55.png';
import image56 from './56.png';
import image57 from './57.png';
import image58 from './58.png';
import image59 from './59.png';
import image60 from './60.png';
import image61 from './61.png';
import image62 from './62.png';

class SubmitProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      show: false,
      showMore: false,
      hasShowTips: !!localStorage.getItem(localStorageKeys.HAS_SHOW_SUBMIT_TIPS),
    };
  }

  componentDidMount() {
    this.autoFocusInst.focus();
    record(actions.ENTER_SUBMIT_PRODUCT_PAGE, {}, eventCategory.PAGE_ENTER);
    wechatHelper.configSubmitProductShare();
  }

  onClose() {
    this.setState({
      show: false,
      showMore: false,
    });
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/' && this.props.locationState.previousPathname !== '/collect-coupon') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  }

  submit = () => {
    if (!this.state.hasShowTips) {
      localStorage.setItem(localStorageKeys.HAS_SHOW_SUBMIT_TIPS, 'true');
      this.setState({
        hasShowTips: true,
        show: true,
      });
      return;
    }
    const user = this.props.currentUser;
    let userId = -1;
    if (user) {
      userId = user.id;
    }
    const data = {
      url: this.state.url,
      user_id: userId,
    };
    const gaData = {};
    // gaData.event_callback = () => {
      this.props.submitProduct(data);
    // };
    gaData[eventParams.LINK] = this.state.url;
    record(actions.CLICK_SUBMIT_PRODUCT_LINK,
      gaData,
      eventCategory.USER_ACTION,
    );
  };

  render() {
    const { t } = this.props;
    const maxHeight = window.screen.availHeight - 300;
    return (
      <div>
        <WindowTitle title={t('pageTitles.submit_product')} />

        <WingBlank size="lg">
          <div className={styles.tips}>
            <Tips>{t('common.tips_submit_product')}</Tips>
            {/* <Tips>{t('common.tips_submit_food')}</Tips> */}
          </div>

          <div className={styles.label}>
            { t('common.please_input_product_link') }
          </div>
          <div>
            <TextareaItem
              onChange={(url) => { this.setState({ url }); }}
              autoHeight
              className={styles.linkInput}
              clear
              ref={(el) => { this.autoFocusInst = el; }}
            />
          </div>

          <CommonButton
            disabled={!this.state.url}
            onClick={this.submit}
            className={styles.button}
          >{ t(
            'common.submit_product_link') }</CommonButton>
          <div
            className={styles.needKnow}
            onClick={() => {
              localStorage.setItem(localStorageKeys.HAS_SHOW_SUBMIT_TIPS, 'true');
              this.setState({
                hasShowTips: true,
                show: true,
              });
            }}
          >
            {t('common.need_know')}
          </div>
        </WingBlank>

        <Modal
          visible={this.state.show}
          transparent
          maskClosable={false}
          onClose={() => { this.onClose(''); }}
          title={t('common.goods_request')}
          footer={[
            {
              text: t('common.i_know'),
              onPress: () => {
                console.log('ok');
                this.onClose();
              },
            }]}
          wrapProps={{ onTouchStart: this.onWrapTouchStart }}
        >
          <div
            style={{
              height: 'auto',
              overflow: 'scroll',
              maxHeight: `${maxHeight}px`,
              textAlign: 'left',
            }}
          >
            <div className={styles.request}>
              1、禁止侵权仿品产品：以目的国品牌网站查询，已成功注册的品牌logo图片或商标，注册品类分类下的产品，为注册品牌产品。这类产品必须提供有效的品牌注册证明文件，或者品牌持有者的授权文件（英文），否则，这类产品为侵权仿品产品；
              UK品牌网站：<a
                href={'https://www.ipo.gov.uk/types/tm/t-os/t-find/tmtext.htm'}
              >https://www.ipo.gov.uk/types/tm/t-os/t-find/tmtext.htm</a>
            </div>
            <div className={styles.request}>
              2、禁止发货产品：违禁品、液体、粉末状、膏状物品、军事用品、管制类物品（枪支/刀具）、易燃易爆物品、贵金属、有价货币/纪念币、出版社出版的书籍/光盘、涉及商检的货物（如化工品、食品、稀土等）、药品、强磁产品、纯电池（移动电源）；动物标本/皮毛、植物标本及种子；
            </div>
            <div className={styles.request}>
              3、食品线特殊说明<br />
              3.1 违禁品<br />
              由于用户购买了以下违禁商品而产生的退运等费用，需用户自行承担。违禁品包括：<br />
              纯粉末类、含液体类、易燃易爆等危险品类；<br />
              无正规包装、用黄色胶带缠绕、散装的食品；<br />
              散发任何气味的食品；<br />
              3.2 保质期<br />
              用户在购买食品时需自行留意淘宝商家给出的保质期限（建议保质期在一个月以上），在我们物流正常的情况下，如果出现用户购买商品过期的问题，团团侠不予赔付。<br />
            </div>

            { !this.state.showMore && <div
              className={styles.viewMore}
              onClick={() => {
                this.setState({ showMore: true });
              }}
            >
              { t('common.view_more_info') }
            </div>
            }
            {
              this.state.showMore &&
              <div className={styles.more}>
                <div className={styles.title}>
                  违禁品
                </div>
                <div className={styles.item}>
                  <div className={styles.subTitle}>
                    一、禁止寄递物品规定
                  </div>
                  <div className={styles.itemContent}>
                    1、有爆炸性、易燃性、腐蚀性、毒性、强酸碱性和放射性的各种危险物品。如雷管、火药、爆竹、汽油、酒精、煤油、桐油、生漆、火柴、农药等；
                    <br />
                    2、麻醉药物和精神药品。如鸦片、吗啡、可卡因（高根）等；
                    <br />
                    3、国家法令禁止流通或寄递的物品。如军火武器、本国或外国货币等；
                    <br />
                    4、容易腐烂的物品。如鲜鱼、鲜肉等；
                    <br />
                    5、妨碍公共卫生的物品。如尸骨（包括已焚化的骨灰）、未经硝制的兽皮、未经药制的兽骨等；
                    <br />
                    6、反动报刊书籍、宣传品和淫秽或有伤风化的物品；各种活的动物（但蜜蜂、水蛭、蚕以及医药卫生科学研究机构封装严密并出具证明交寄的寄生虫以及供作药物或用以杀灭害虫的虫类，不在此限）。
                  </div>
                </div>

                <div className={styles.item}>
                  <div className={styles.subTitle}>
                    二、海关明确的禁止出境物品
                  </div>
                  <div className={styles.itemContent}>
                    1、各种武器、仿真武器、弹药及爆炸物品；
                    <br />
                    2、伪造的货币及伪造的有价证券；
                    <br />
                    3、对中国政治、经济、文化、道德有害的印刷品、胶卷、照片、唱片、影片、录音带、录像带、激光视盘、计算机存储介质及其它物品；
                    <br />
                    4、各种烈性毒药；
                    <br />
                    5、鸦片、吗啡、海洛英、大麻以及其它能使人成瘾的麻醉品、精神药物；带有危险性病菌、害虫及其它有害生物的动物、植物及其产品；
                    <br />
                    7、有碍人畜健康的、来自疫区的以及其它能传播疾病的食品、药品或其它物品。
                    <br />
                    8、内容涉及国家秘密的手稿、印刷品、胶卷、照片、唱片、影片、录音带、录像带、激光视盘、计算机存储介质及其它物品；
                    <br />
                    9、珍贵文物及其它禁止出境的文体；
                    <br />
                    10、濒危的和珍贵的动物、植物（均含标本）及其种子和繁殖材料；
                    <br />
                    11、国家货币；
                    <br />
                    12、外币及其有价证券。
                  </div>
                </div>

                <div className={styles.item}>
                  <div className={styles.subTitle}>
                    三、海关明确限制邮寄出境的物品
                  </div>
                  <div className={styles.itemContent}>
                    1、金银等贵重金属及其制品；
                    <br />
                    2、无线电收发信机、通信保密机；
                    <br />
                    3、贵重中药材；
                    <br />
                    4、一般文物；
                    <br />
                    5、海关限制出境的其它物品。
                    <br />
                    6、具体详尽禁限寄规定，可查阅中国海关官方网站<a
                      href={'http://www.customs.gov.cn'}
                    >http://www.customs.gov.cn</a>
                  </div>
                </div>

                <div className={styles.item}>
                  <div className={styles.subTitle}>
                    四、航空安检不合格物品明细说明
                  </div>
                  <div className={styles.itemContent}>
                    1、 仿真枪、消防枪、子弹及弹壳。如：任何仿真枪、可射击子弹的玩具枪均属禁寄。
                    <br />
                    2、各种管制刀具、弩箭。
                    <br />
                    3、易燃、易爆危险品、化工试剂、液体。
                    <br />
                    4、带电池的各类电子产品。如：任何可重复使用的充电电池（锂电池、内置电池、笔记本长电池、蓄电池、高容量电池等），无法通过安检；MP3、MP4、手机、电脑等电子产品及户外照明用品、登山用品，若无内置电池，单机可通过安检；若有内置电池，即使电池已独立包装，仍无法通过安检；含有干电池的玩具、荧光棒等，都无法通过安检。
                    <br />
                    5、带磁性的物品（扩音器）：磁性物品，如含喇叭的大小音响、有变压器的电路板等。因为会影响到飞机信号，不能通过安检。
                    <br />
                    6、各种压力容器、各类压缩气体制品（自动充气救生设备、灭火器）。
                    <br />
                    7、内装化妆品、药品、粉末状物品的邮件。如：液体、粉状、膏状物品，无论如何包装，均无法通过安检，胶囊类药品也受此限制。
                    <br />
                    8、批量电子元件。如：整包单独寄递的批量纽扣电池、干电池，无法通过安检。
                    <br />
                    9、任何在安检视图呈现“不明图像”的物品不能寄递。如：包装使用较厚的固体泡沫，导致内包装物品形状、特性难以显示，出于航空安全考虑，不予通过安检；有锡纸的茶叶盒等。
                  </div>
                </div>
                <div className={styles.item}>
                  <div className={styles.subTitle}>
                    其它：
                  </div>
                  <div className={styles.itemContent}>
                    此外，烟草、植物种子、活体、非法药品甚至毒品、伪造驾照护照、粉末及液体状物品、不明成分物品及食品、药品、假冒伪劣品等禁运物品。
                  </div>
                </div>

                <div className={styles.item}>
                  <div className={styles.subTitle}>
                    其他禁发产品确认清单：
                  </div>
                  <div className={styles.itemContent}>
                    <div className={styles.goodTable}>
                      <div className={styles.goodRowHead}>
                        <div className={styles.goodName}>
                          名称
                        </div>
                        <div className={styles.goodImage}>
                          图片
                        </div>
                        <div className={styles.goodName}>
                          名称
                        </div>
                        <div className={styles.goodImage}>
                          图片
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          镁棒打火石flintstone
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image1} />
                        </div>
                        <div className={styles.goodName}>
                          补胎胶水
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image2} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          药皂
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image3} />
                        </div>
                        <div className={styles.goodName}>
                          固体香皂
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image4} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          固体车载清新剂
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image5} />
                        </div>
                        <div className={styles.goodName}>
                          电蚊香片
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image6} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          电弧打火机
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image7} />
                        </div>
                        <div className={styles.goodName}>
                          瓦斯点火器 Gas lighter
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image8} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          伸缩棍
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image9} />
                        </div>
                        <div className={styles.goodName}>
                          弹弓
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image10} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          户外刀（凶器）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image11} />
                        </div>
                        <div className={styles.goodName}>
                          户外刀（凶器）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image12} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          狩猎箭头
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image13} />
                        </div>
                        <div className={styles.goodName}>
                          蝴蝶训练刀(未开刃)
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image14} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          户外瞄准镜
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image15} />
                        </div>
                        <div className={styles.goodName}>
                          光学瞄准镜
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image16} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          瞄准器
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image17} />
                        </div>
                        <div className={styles.goodName}>
                          孔明灯
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image18} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          指南针手带（带液体）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image19} />
                        </div>
                        <div className={styles.goodName}>
                          彩虹高盖TS-120SB恒温器（带液体）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image20} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          墨盒ink box
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image21} />
                        </div>
                        <div className={styles.goodName}>
                          电子烟electronic cigarette
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image22} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          车载强磁支架
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image23} />
                        </div>
                        <div className={styles.goodName}>
                          USB充电暖手器
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image24} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          背夹手机壳（内置电池）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image25} />
                        </div>
                        <div className={styles.goodName}>
                          相机充电器+电池
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image26} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          海洋生物球
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image27} />
                        </div>
                        <div className={styles.goodName}>
                          动物羽毛Animal feathers
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image28} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          指甲油（非易燃品）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image29} />
                        </div>
                        <div className={styles.goodName}>
                          沙漏
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image30} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          指甲胶
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image31} />
                        </div>
                        <div className={styles.goodName}>
                          美甲粉
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image32} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          蜡烛candle
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image33} />
                        </div>
                        <div className={styles.goodName}>
                          沐浴盐球，材质是海盐
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image34} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          键盘清洁泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image35} />
                        </div>
                        <div className={styles.goodName}>
                          橡皮泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image36} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          宝宝手印泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image39} />
                        </div>
                        <div className={styles.goodName}>
                          修复泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image40} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          3M洗车用品具橡皮泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image41} />
                        </div>
                        <div className={styles.goodName}>
                          手工雕塑泥彩泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image42} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          泳池消毒剂丸
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image43} />
                        </div>
                        <div className={styles.goodName}>
                          脱毛豆
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image44} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          荧光棒（液体）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image45} />
                        </div>
                        <div className={styles.goodName}>
                          雪花泥
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image46} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          面膜
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image47} />
                        </div>
                        <div className={styles.goodName}>
                          眼贴
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image48} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          眼影
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image49} />
                        </div>
                        <div className={styles.goodName}>
                          纹身修复膏
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image50} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          足膜
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image51} />
                        </div>
                        <div className={styles.goodName}>
                          唇膏
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image52} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          烟斗(水晶烟斗)
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image53} />
                        </div>
                        <div className={styles.goodName}>
                          户外折叠锯
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image54} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          玩具枪
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image55} />
                        </div>
                        <div className={styles.goodName}>
                          威士忌石头冰块
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image56} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          仿真手铐（情趣套装专线可以发）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image57} />
                        </div>
                        <div className={styles.goodName}>
                          野营气炉（带打火功能）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image58} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          吉他防锈除锈笔（内有润滑油）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image59} />
                        </div>
                        <div className={styles.goodName}>
                          清洗汽车的水枪（枪形）
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image60} />
                        </div>
                      </div>
                      <div className={styles.goodRow}>
                        <div className={styles.goodName}>
                          松香rosin
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image61} />
                        </div>
                        <div className={styles.goodName}>
                          粉笔chalk
                        </div>
                        <div className={styles.goodImage}>
                          <img alt={'good'} src={image62} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
        </Modal>

        <div className={styles.tabWrap}>
          <HomeBottomAction index={1} />
        </div>

      </div>
    );
  }
}

SubmitProduct.propTypes = {
  t: PropTypes.func,
  submitProduct: PropTypes.func,
  currentUser: PropTypes.object,
  locationState: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectLocationState(),
  (currentUser, locationState) => ({
    currentUser: currentUser && currentUser.toJS(),
    locationState,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  submitProduct: (data) => dispatch(submitProduct(data)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(SubmitProduct));
