import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { browserHistory } from 'react-router';
import WindowTitle from 'components/WindowTitle';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import Coupon from 'components/Coupon';
import { WingBlank } from 'antd-mobile';
import {
  selectCouponList,
  selectCouponListLoading,
  selectLocationState,
} from 'containers/App/selectors';
import { loadCouponList } from 'containers/App/actions';
import styles from './styles.css';

class Coupons extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadCouponList({ showLoading: true });
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/' && this.props.locationState.previousPathname !== '/collect-coupon') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  render() {
    const { couponList, loadingCouponList, t } = this.props;

    return (
      <div className={styles.couponWrapper}>
        <WindowTitle title={t('pageTitles.my_coupons')} />
        <WingBlank>
          { (couponList && couponList.length === 0) ?
            <div className={styles.emptyWrap}>
              { !loadingCouponList && t('coupon.empty_coupon')}
            </div>
            : <ul className={styles.couponListWrap}>
              {couponList.map((coupon) => (
                <li className={styles.couponWrap} key={coupon.id}>
                  <Coupon
                    coupon={coupon}
                  />
                </li>
              ))}
            </ul>
          }
        </WingBlank>
      </div>
    );
  }
}

Coupons.propTypes = {
  couponList: PropTypes.array,
  loadingCouponList: PropTypes.bool,
  loadCouponList: PropTypes.func,
  t: PropTypes.func,
  locationState: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectCouponList(),
  selectCouponListLoading(),
  selectLocationState(),
  (couponList, loadingCouponList, locationState) => ({
    couponList: couponList.toJS(),
    loadingCouponList,
    locationState,
  }),
);

function mapDispatchToProps(dispatch) {
  return {
    loadCouponList: (data) => dispatch(loadCouponList(data)),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Coupons));
