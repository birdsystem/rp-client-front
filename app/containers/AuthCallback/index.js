import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { translate } from 'react-i18next';
import { weChatAuth, weChatLogin } from 'containers/App/actions';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import WindowTitle from 'components/WindowTitle';
import styles from './styles.css';

class AuthCallback extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const query = this.props.location && this.props.location.query;
    if (query && query.code) {
      if (query.state === 'login') {
        record(actions.LOGIN, {}, eventCategory.USER_EVENT);
        this.props.weChatLogin(query.code);
        return;
      } else if (query.state.indexOf('login_from') >= 0) {
        const from = query.state.substr(11);
        record(actions.LOGIN, {}, eventCategory.USER_EVENT);
        this.props.weChatLogin(query.code, from);
        return;
      }
      this.props.weChatAuth(query.code, query.state);
    } else {
      browserHistory.replace('/');
    }
  }


  render() {
    const { t } = this.props;
    return (
      <div className={styles.main}>
        <WindowTitle title={t('pageTitles.authenticating')} />

        <div className={styles.tips}>{this.props.t('common.auth_tips')}</div>
      </div>
    );
  }
}

AuthCallback.propTypes = {
  location: PropTypes.object,
  weChatAuth: PropTypes.func,
  weChatLogin: PropTypes.func,
  t: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({
  weChatAuth: (code, state) => dispatch(weChatAuth(code, state)),
  weChatLogin: (code, from) => dispatch(weChatLogin(code, from)),
});

export default translate()(connect(null, mapDispatchToProps)(AuthCallback));
