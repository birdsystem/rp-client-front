import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import WindowTitle from 'components/WindowTitle';
import wechatHelper from 'utils/wechatHelper';
import {
  selectCurrentUser,
} from 'containers/App/selectors';
import { record, actions, constantsValue } from 'utils/gaDIRecordHelper';
import styles from './styles.css';

const pid = 999999;
const pName = 'test item';
const pCategory = 'test category';
const pSku = 'test sky';
const pPrice = 1;
const pOrderId = 109999;
const pCoupon = constantsValue.COUPON_START_GROUP;

class LogGA extends Component {

  componentDidMount() {
    wechatHelper.configShareCommon();
  }

  componentWillUnmount() {

  }

  render() {
    return (
      <div className={styles.main}>
        <WindowTitle title={'GA test page'} />
        <button
          onClick={() => {
            record(actions.VIEW_ITEM, {
              id: pid,
              name: pName,
              list_name: '',
              brand: '',
              category: pCategory,
              variant: '',
              list_position: -1,
              quantity: 1,
              price: pPrice,
            });
          }}
        >Log view</button>

        <button
          onClick={() => {
            record(actions.BEGIN_CHECKOUT, {
              items: [
                {
                  id: pid,
                  name: pName,
                  list_name: '',
                  brand: '',
                  category: pCategory,
                  variant: pSku,
                  list_position: -1,
                  quantity: 1,
                  price: pPrice,
                },
              ],
              coupon: pCoupon,
            });
          }}
        >Begin checkout</button>

        <button
          onClick={() => {
            record(actions.SET_CHECKOUT_OPTION, {
              checkout_step: 1,
              checkout_option: 'wechat',
              value: '',
            });
          }}
        >set checkout option</button>


        <button
          onClick={() => {
            record(actions.CHECKOUT_PROGRESS, {
              items: [
                {
                  id: pid,
                  name: pName,
                  list_name: '',
                  brand: '',
                  category: pCategory,
                  variant: '',
                  list_position: -1,
                  quantity: 1,
                  price: pPrice,
                },
              ],
              coupon: pCoupon,
            });
          }}
        >checkout progress</button>

        <button
          onClick={() => {
            record(actions.PURCHASE, {
              transaction_id: pOrderId,
              affiliation: 'tuantuanxia',
              value: 2,
              currency: 'CNY',
              tax: 0,
              shipping: 1,
              items: [
                {
                  id: pid,
                  name: pName,
                  list_name: '',
                  brand: '',
                  category: '',
                  variant: '',
                  list_position: -1,
                  quantity: 1,
                  price: pPrice,
                },
              ],
            });
          }}
        >Purchase</button>
      </div>
    );
  }
}

LogGA.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  (currentUser) => ({
    currentUser: currentUser && currentUser.toJS(),
  }),
);

const mapDispatchToProps = null;
export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(LogGA));
