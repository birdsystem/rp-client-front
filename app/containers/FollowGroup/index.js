/* eslint-disable no-mixed-operators, jsx-a11y/no-noninteractive-element-interactions, no-unused-vars,react/no-will-update-set-state,radix */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import Money from 'components/Money';
import WeChatShare from 'components/WeChatShare';
import WindowTitle from 'components/WindowTitle';
import ProductItem from 'components/ProductItem';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import CommonButton from 'components/CommonButton';
import Tips from 'components/Tips';
import {
  selectLoadingProduct,
  selectLoadingGroup,
  selectPendingProduct,
  selectPendingGroup,
  selectPurchaseModalType,
  selectPurchaseModalVisible,
  selectCurrentUser,
  selectLocationState,
} from 'containers/App/selectors';
import {
  loadProductDetail,
  loadGroupDetail,
  showPurchaseModal,
  hidePurchaseModal,
  loadGroupDetailSuccess,
} from 'containers/App/actions';
import { browserHistory, Link } from 'react-router';
import { Icon, Button, ListView } from 'antd-mobile';
import { getTimeLeftDisplay, getTimeLeftDisplaySecond } from 'utils/commonHelper';
import { record, eventCategory, actions, eventParams } from 'utils/gaDIRecordHelper';
import { isBackendYes } from 'utils/backendHelper';
import GraySpace from 'components/GraySpace';
import wechatHelper from 'utils/wechatHelper';
import {
  getDisplayPrice,
  getStartGroupPrice,
  getGroupPrice,
  getSingleBuyPrice,
  getDisplayPriceFromProps,
  getStartGroupPriceFromProps,
  getSingleBuyPriceFromProps,
} from 'utils/productHelper';
import _ from 'lodash';
import {
  SINGLE_BUYING,
  GROUP_LEADER_BUYING,
  GROUP_BUYING,
  GROUP_STATUS_PENDING,
} from 'utils/constants';
import { loadProductList } from './actions';
import { selectProductList } from './selectors';
import styles from './styles.css';
import PurchaseModal from '../PurchaseModal';
import defaultAvatar from './default.png';

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector ||
    el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement; // eslint-disable-line
  }
  return null;
}

class FollowGroup extends Component {
  constructor(props) {
    super(props);
    const { params, location } = this.props;
    const groupId = params.groupId;
    const productId = params.productId;
    if (location && location.query && location.query.back) {
      window.location = `${window.location.protocol}//${window.location.host}`;
    }
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    this.state = {
      groupId,
      productId,
      skuIndex: 0,
      showMoreGroup: false,
      showWeChatShareTips: false,
      hasConfigWeChatShare: false,
      hasAutoPopFollow: false,
      images: [],
      carouselSet: false,
      imageIndex: 0,
      leftTime: 0,
      hasStartCountDown: false,
      dataSource,
      currentGroup: null,
      hasSetViewItemLog: false,
    };
    this.props.loadProductDetail(productId);
    this.props.loadGroupDetail(groupId);
    this.props.loadProductList();
  }

  componentDidMount() {
    record(actions.ENTER_FOLLOW_GROUP_PAGE, {}, eventCategory.PAGE_ENTER);
  }

  componentWillUpdate(nextProps, nextState) {
    if (!_.isEmpty(nextProps.pendingGroup) &&
      nextState.hasConfigWeChatShare === false) {
      wechatHelper.configFollowGroupShare(nextProps.pendingGroup.product_detail.name,
        nextProps.pendingGroup.product_detail.price_including_platform_delivery_fee,
        nextState.productId, nextState.groupId,
        nextProps.pendingGroup.product_detail.image_url);
      this.setState({
        hasConfigWeChatShare: true,
        currentGroup: nextProps.pendingGroup,
      });
    }
    if (!_.isEmpty(nextProps.pendingGroup) && !this.state.hasStartCountDown) {
      this.startCountDown(nextProps.pendingGroup.left_time);
    }
    if (!_.isEmpty(nextProps.pendingProduct) && !this.state.hasSetViewItemLog) {
      this.setState({
        hasSetViewItemLog: true,
      });
      const product = nextProps.pendingProduct;
      let category = '';
      if (product.category && product.category.length > 0) {
        const categoryItem = product.category[0];
        category = `${categoryItem.id}|${categoryItem.name}`;
      }
      record(actions.VIEW_ITEM, {
        id: product.id,
        name: product.name,
        list_name: '',
        brand: '',
        category,
        variant: '',
        list_position: -1,
        quantity: 1,
        price: getDisplayPrice(product),
      });
    }
  }

  componentWillUnmount() {
    if (this.countDownInterval) {
      clearInterval(this.countDownInterval);
      this.countDownInterval = null;
    }
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  };

  startCountDown(time) {
    if (this.countDownInterval) {
      clearInterval(this.countDownInterval);
      this.countDownInterval = null;
    }
    this.setState({
      leftTime: time,
      hasStartCountDown: true,
    });
    this.state.countDownInterval = setInterval(
      () => {
        let leftTime = this.state.leftTime - 1;
        if (leftTime <= 0) {
          leftTime = 0;
          clearInterval(this.countDownInterval);
          this.countDownInterval = null;
        }
        this.setState({
          leftTime,
        });
      }, 1000,
    );
  }

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  showMoreGroup() {
    this.setState({
      showMoreGroup: true,
    });
  }

  render() {
    const { purchaseModalType, purchaseModalVisible, loadingProduct, loadingGroup, pendingProduct, pendingGroup, productList, t } = this.props;
    const currentGroup = this.state.currentGroup;
    const maxHeight = window.screen.availHeight * 0.8;
    const copySuccess = wechatHelper.isOpenInWechat() ? t(
      'common.copy_success_wechat') : t('common.copy_success');
    const currentUserId = this.props.currentUser
      ? this.props.currentUser.id
      : -1;
    let grouperElement = null;
    const productElement = null;
    let grouper = [];
    let hasGrouped = false;
    let hasEnd = false;
    let joinText = '';
    let isMyGroup = false;
    let displayGroupLength = 0;
    const row = (rowData, sectionID, rowID) => (
      <ProductItem key={`p${rowID}`} product={rowData} />
    );
    let dataSource = this.state.dataSource.cloneWithRows({});
    if (productList) {
      dataSource = this.state.dataSource.cloneWithRows(productList);
    }
    if (!_.isEmpty(currentGroup)) {
      isMyGroup = parseInt(currentUserId) === parseInt(currentGroup.owner.id);
      if (currentGroup.current_user_count >= currentGroup.min_user_count) {
        hasGrouped = true;
        joinText = t('common.finish_group_and_start');
      } else if (currentGroup.status === GROUP_STATUS_PENDING) {
        hasEnd = true;
        joinText = t('common.group_pending');
      } else if (parseInt(currentGroup.left_time) <= 0) {
        hasEnd = true;
        joinText = t('common.finish_group_and_start');
      } else {
        joinText = t('group.people_left_time_left',
          {
            count: currentGroup.min_user_count - currentGroup.current_user_count,
            time: getTimeLeftDisplaySecond(this.state.leftTime),
          });
      }
      if (!_.isEmpty(currentGroup.owner)) {
        grouper.push(currentGroup.owner);
        if (currentGroup.users && currentGroup.users.length > 0) {
          currentGroup.users.map(
            (user) => {
              if (`${user.id}` !== `${currentGroup.owner.id}`) {
                grouper.push(user);
              }
              return null;
            },
          );
        }
        if (hasGrouped) {
          if (grouper.length > 3) {
            // noinspection JSAnnotator
            grouper = grouper.slice(0, 3);
          }
        } else {
          if (grouper.length > 2) {
            // noinspection JSAnnotator
            grouper = grouper.slice(0, 2);
          }
          grouper.push({});
        }
      }
    }
    if (grouper.length > 0) {
      grouperElement = grouper.map((user, inx) => {
        if (_.isEmpty(user)) {
          return (
            <div
              className={`${styles.grouperItem} ${styles.grouperWaitingItem}`}
              key={`${inx}`}
            >
              <div className={styles.groupAvatar}>
                ?
              </div>
            </div>
          );
        }
        const hasAvatar = !!user.wechat_avatar_url;
        let firstLetter = '';
        if (!hasAvatar) {
          firstLetter = (user.wechat_name || user.username).substr(0, 1);
        }
        let isOwner = false;
        if (inx === 0) {
          isOwner = true;
        }
        let wrapStyle = styles.grouperItem;
        if (isOwner) {
          wrapStyle = `${styles.grouperItem} ${styles.grouperOwnerItem}`;
        }
        return (
          <div
            className={wrapStyle}
            key={`${inx}`}
          >
            {
              hasAvatar ? <img
                alt={'avater'}
                className={styles.groupAvatar}
                src={user.wechat_avatar_url}
              />
                : <div className={styles.groupAvatar}>
                  { firstLetter }
                </div>
            }
            {
              inx === 0 &&
              <span className={styles.isGroupOwner}>
                  { t('common.group_owner') }
                </span>
            }
          </div>
        );
      });
    }
    let groupElement = null;
    if (pendingProduct && pendingProduct.group) {
      let groups = [];
      for (let i = 0; i < pendingProduct.group.length; i += 1) {
        if (`${pendingProduct.group[i].id}` !== `${this.state.groupId}` &&
          `${pendingProduct.group[i].owner.id}` !== `${currentUserId}`) {
          groups.push(pendingProduct.group[i]);
        }
      }
      displayGroupLength = groups.length;

      if (groups.length > 2) {
        groups = groups.slice(0, 2);
      }

      groupElement = groups.map((group, inx) => {
        let followEnable = true;
        if (group.min_user_count - group.current_user_count <= 0) {
          followEnable = false;
        }
        if (parseInt(group.left_time) < 0) {
          followEnable = false;
        }
        return (
          <div
            className={styles.groupItem}
            key={inx}
            onClick={(e) => {
              if (!followEnable) {
                return;
              }
              e.preventDefault(); // 修复 Android 上点击穿透
              if (parseInt(currentUserId) !== parseInt(group.owner.id)) {
                const data = {};
                data.event_callback = () => {
                  const productDetail = Object.assign(pendingProduct);
                  delete productDetail.group;
                  const groupData = Object.assign(group,
                    { product_detail: productDetail });
                  this.props.loadGroupDetailSuccess(groupData);
                  this.props.showPurchaseModal(GROUP_BUYING);
                };
                data[eventParams.ID] = pendingProduct.id;
                data[eventParams.GROUP_ID] = group.id;
                record(actions.CLICK_FOLLOW_GROUP_FOLLOW,
                  data,
                  eventCategory.USER_ACTION,
                );
              }
            }}
          >
            <div className={styles.groupName}>
              <img
                alt={'avatar'}
                className={styles.groupItemAvatar}
                src={group.owner.wechat_avatar_url || defaultAvatar}
              />
              <span>{ group.owner.wechat_name || group.owner.username }</span>
            </div>
            <div className={styles.groupInfo}>
              <div className={styles.groupLeft}>
                {
                  group.min_user_count - group.current_user_count > 0
                    ? t('common.people_left',
                    { count: group.min_user_count - group.current_user_count })
                    : t('common.finish_group')
                }
              </div>
              <div className={styles.leftTime}>
                {
                  parseInt(group.left_time) > 0 ? `${t(
                    'common.left_time')}: ${getTimeLeftDisplay(
                    group.left_time)}`
                    : t('common.group_finish')
                }
              </div>
            </div>
            { (parseInt(currentUserId) !== parseInt(group.owner.id))
              ? <CommonButton
                disabled={!followEnable}
                className={styles.shareButton}
              >{ t('common.join_group') }</CommonButton>
              : <span className={styles.myGroupText}>{ t(
                'common.my_group') }</span>
            }
          </div>
        );
      });
    }

    let skuElement = null;
    if (pendingProduct && pendingProduct.meta &&
      pendingProduct.meta.sku_info) {
      skuElement = pendingProduct.meta.sku_info.map((sku, inx) => (
        <div
          className={inx === this.state.skuIndex
            ? styles.skuItemFocus
            : styles.skuItem}
          key={inx}
          onClick={() => {
            this.setState({
              skuIndex: inx,
            });
          }}
        >
          <div className={styles.skuName}>{ sku.name }</div>
          <div className={styles.skuPrice}>{ sku.price }</div>
        </div>
      ));
    }

    // get from pop modal
    const currentSku = pendingProduct && pendingProduct.meta &&
      pendingProduct.meta.sku_info &&
      pendingProduct.meta.sku_info[this.state.skuIndex] && null;
    const displayPrice = currentSku ? getDisplayPriceFromProps(pendingProduct,
      currentSku.prop_path) : getDisplayPrice(pendingProduct);
    const startGroupPrice = currentSku ? getStartGroupPriceFromProps(
      pendingProduct, currentSku.prop_path) : getStartGroupPrice(
      pendingProduct);
    const singleBuyPrice = currentSku ? getSingleBuyPriceFromProps(
      pendingProduct, currentSku.prop_path) : getSingleBuyPriceFromProps(
      pendingProduct);
    let groupProductPrice = 0;
    if (!_.isEmpty(currentGroup) && !_.isEmpty(pendingProduct)) {
      groupProductPrice = getGroupPrice(pendingProduct,
        currentGroup.min_user_count);
    }

    let isUncheckProduct = false;
    if (!_.isEmpty(pendingProduct) && !isBackendYes(pendingProduct.has_check)) {
      isUncheckProduct = true;
    }

    return (
      <div>
        <WindowTitle title={t('pageTitles.default_page_title')} />

        { (loadingProduct || !pendingProduct || loadingGroup || !currentGroup)
          ? <div className={styles.loadingWrap}>
            <Icon type={'loading'} size={'lg'} />
          </div>
          : (
            <div className={styles.wrapInner}>
              <div
                onClick={() => {
                  browserHistory.push(`/productdetail/${pendingProduct.id}`);
                }}
                className={styles.itemWrap}
              >
                <div
                  alt={'product'}
                  className={styles.itemImage}
                  style={{ backgroundImage: `url("${pendingProduct.image_url}")` }}
                >
                </div>
                <div className={styles.itemInfo}>
                  <div className={styles.itemTitle}>
                    { pendingProduct.name }
                  </div>
                  <div className={styles.priceWrap}>
                    <div className={styles.price}>
                      <MoneyInLocalCurrency cnyAmount={groupProductPrice} />
                      <span className={styles.cnyPrice}><Money amount={groupProductPrice} /></span>
                    </div>
                    <div className={styles.dealCount}>{ t(
                      'common.already_deal_count',
                      { count: pendingProduct.deal_count }) }</div>
                  </div>
                </div>
              </div>

              <GraySpace />

              { isUncheckProduct && (
                <Tips>
                  { t('common.tips_follow_group_pay_shipping_later') }
                </Tips>
              ) }

              <div className={styles.currentGroupWrap}>
                <div className={styles.currentGrouperWrap}>
                  { grouperElement }
                </div>
                { !isMyGroup && !hasEnd && !hasGrouped && <CommonButton
                  onClick={(e) => {
                    e.preventDefault();
                    if (isMyGroup || hasEnd || hasGrouped) {
                      return;
                    }
                    if (parseInt(currentUserId) !==
                      parseInt(currentGroup.owner.id)) {
                      const data = {};
                      data.event_callback = () => {
                        const productDetail = Object.assign(pendingProduct);
                        delete productDetail.group;
                        const groupData = Object.assign(currentGroup,
                          { product_detail: productDetail });
                        this.props.loadGroupDetailSuccess(groupData);
                        this.props.showPurchaseModal(GROUP_BUYING);
                      };
                      data[eventParams.ID] = pendingProduct.id;
                      data[eventParams.GROUP_ID] = currentGroup.id;
                      record(actions.CLICK_FOLLOW_GROUP,
                        data,
                        eventCategory.USER_ACTION,
                      );
                    }
                  }}
                  className={styles.joinGroupButton}
                >
                  { isMyGroup ? t('common.my_group') : t('group.join_s_group', {
                    name: (currentGroup.owner.wechat_name ||
                      currentGroup.owner.username),
                  }) }
                </CommonButton>
                }
                <div className={styles.joinGroupText}>
                  {
                    joinText
                  }
                </div>
              </div>
              { displayGroupLength === 0 && (hasGrouped || hasEnd) && !isMyGroup &&
              <div
                onClick={() => {
                  browserHistory.push(`/productdetail/${pendingProduct.id}`);
                }}
                className={styles.letMeGroup}
              >
                { t('common.let_me_group') }
              </div>
              }
              { displayGroupLength > 0 &&
              <div className={styles.groupWrap}>
                <div className={styles.groupTitle}>
                  {
                    <span>{ t('common.suggest_group') }</span>
                  }
                </div>
                { groupElement }
              </div>
              }
              { (isMyGroup || hasEnd || hasGrouped) && <div className={styles.backHomeWrap}>
                <CommonButton
                  onClick={() => {
                    browserHistory.replace('/');
                  }}
                >
                  { t('common.back_to_home_page') }
                </CommonButton>
              </div>
              }
              <div className={styles.moreProductWrap}>
                <div className={styles.moreProductTitle}>
                  { t('common.more_recommend_product') }
                </div>
                <div className={styles.productListWrap}>
                  <ListView
                    renderRow={row}
                    useBodyScroll
                    scrollRenderAheadDistance={500}
                    onEndReachedThreshold={10}
                    rowIdentities={() => null}
                    dataSource={dataSource}
                  />

                </div>
              </div>
              <PurchaseModal
                visible={purchaseModalVisible}
                type={purchaseModalType}
                hide={this.props.hidePurchaseModal}
              />
            </div>
          ) }

        { this.state.showWeChatShareTips && (
          <div
            onClick={
              () => {
                this.setState({
                  showWeChatShareTips: false,
                });
              }
            }
          >
            <WeChatShare />
          </div>
        ) }
      </div>
    );
  }
}

FollowGroup.propTypes = {
  t: PropTypes.func,
  params: PropTypes.object,
  loadProductDetail: PropTypes.func,
  loadingProduct: PropTypes.bool,
  loadGroupDetail: PropTypes.func,
  loadingGroup: PropTypes.bool,
  pendingGroup: PropTypes.object,
  pendingProduct: PropTypes.object,
  showPurchaseModal: PropTypes.func,
  hidePurchaseModal: PropTypes.func,
  purchaseModalVisible: PropTypes.bool,
  purchaseModalType: PropTypes.string,
  loadGroupDetailSuccess: PropTypes.func,
  location: PropTypes.object,
  currentUser: PropTypes.object,
  locationState: PropTypes.object,
  loadProductList: PropTypes.func,
  productList: PropTypes.array,
};

const mapStateToProps = createSelector(
  selectLoadingProduct(),
  selectLoadingGroup(),
  selectPendingProduct(),
  selectPendingGroup(),
  selectPurchaseModalType(),
  selectPurchaseModalVisible(),
  selectCurrentUser(),
  selectLocationState(),
  selectProductList(),
  (
    loadingProduct, loadingGroup, pendingProduct, pendingGroup,
    purchaseModalType, purchaseModalVisible, currentUser, locationState, productList) => ({
      loadingProduct,
      loadingGroup,
      pendingProduct: pendingProduct && pendingProduct.toJS(),
      pendingGroup: pendingGroup && pendingGroup.toJS(),
      purchaseModalType,
      purchaseModalVisible,
      currentUser: currentUser && currentUser.toJS(),
      locationState,
      productList: (productList && productList.toJS && productList.toJS()) || [],
    }),
);

const mapDispatchToProps = (dispatch) => ({
  showPurchaseModal: (type) => dispatch(showPurchaseModal(type)),
  hidePurchaseModal: () => dispatch(hidePurchaseModal()),
  test: (data) => dispatch(test(data)),
  loadProductDetail: (id) => dispatch(loadProductDetail({
    id,
    hideLoading: true,
  })),
  loadGroupDetail: (id) => dispatch(loadGroupDetail(id, true)),
  loadGroupDetailSuccess: (data) => dispatch(loadGroupDetailSuccess(data)),
  loadProductList: () => dispatch(loadProductList()),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(FollowGroup));
