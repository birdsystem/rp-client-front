/*
 *
 * product reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

const initialState = fromJS({
  productList: [],
  productListLoading: false,
});

function productReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_PRODUCT_LIST:
      return state.set('currentPage', action.page)
      .set('productListLoading', true);
    case LOAD_PRODUCT_LIST_SUCCESS: {
      return state.set('productList', fromJS(action.list))
      .set('totalCount', action.count)
      .set('productListLoading', false);
    }
    case LOAD_PRODUCT_LIST_ERROR:
      return state.set('productListLoading', false);
    default:
      return state;
  }
}

export default productReducer;
