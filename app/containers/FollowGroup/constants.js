/*
 *
 * FollowGroup constants
 *
 */

export const LOAD_PRODUCT_LIST = 'FollowGroup/LOAD_PRODUCT_LIST';
export const LOAD_PRODUCT_LIST_SUCCESS = 'FollowGroup/LOAD_PRODUCT_LIST_SUCCESS';
export const LOAD_PRODUCT_LIST_ERROR = 'FollowGroup/LOAD_PRODUCT_LIST_ERROR';
