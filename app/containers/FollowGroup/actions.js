/*
 *
 * Global actions
 *
 */

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

export function loadProductList(page) {
  return {
    type: LOAD_PRODUCT_LIST,
    page,
  };
}

export function loadProductListSuccess(list, count) {
  return {
    type: LOAD_PRODUCT_LIST_SUCCESS,
    list,
    count,
  };
}

export function loadProductListError() {
  return {
    type: LOAD_PRODUCT_LIST_ERROR,
  };
}
