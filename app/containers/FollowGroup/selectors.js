import { createSelector } from 'reselect';

const selectFollowGroupDomain = () => (state) => state.get('followgroup');

const selectProductList = () => createSelector(
  selectFollowGroupDomain(),
  (substate) => substate.get('productList')
);

export {
  selectProductList,
};
