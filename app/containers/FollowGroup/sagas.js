import { call, put, race, take } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_PRODUCT_LIST } from './constants';
import {
  loadProductListSuccess,
} from './actions';

export default [
  getLoadProductListWatcher,
];


export function* getLoadProductListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadProductList: take(LOAD_PRODUCT_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const requestConfig = {
      urlParams: {
        limit: 30,
        start: 0,
        order_by: 'rand',
      },
    };
    const requestURL = '/product';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      yield put(loadProductListSuccess(list, result.data.total));
    }
  }
}
