/* eslint-disable no-unused-vars,react/sort-comp */
import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { InputItem, Icon, Button, WingBlank } from 'antd-mobile';
import { Link, browserHistory } from 'react-router';
import message from 'components/message';
import WindowTitle from 'components/WindowTitle';
import { selectCurrentUser } from 'containers/App/selectors';
import { record, eventCategory, actions } from 'utils/gaDIRecordHelper';
import { register, setWeChatAuthDirectUrl } from 'containers/App/actions';
import wechatHelper from 'utils/wechatHelper';
import phoneNumberHelper from 'utils/phoneNumberHelper';
import CommonButton from 'components/CommonButton';
import request from 'utils/request';
import styles from './styles.css';
import WechatLogo from './wechat-logo.png';

class Register extends Component {
  constructor(props) {
    super(props);
    const { location = {} } = this.props;
    const { wechatInfo = {} } = location.state || {};
    let hasWechatInfo = false;
    if (!_.isEmpty(wechatInfo)) {
      hasWechatInfo = true;
    }
    this.state = {
      countryCode: '44',
      name: '',
      password: '',
      confirmPassword: '',
      code: '',
      mobile: '',
      autoLogin: true,
      countDownSecond: 0,
      countingDown: false,
      hasWechatInfo,
      wechatInfo,
    };
    if (wechatHelper.isOpenInWechat() && !hasWechatInfo) {
      this.props.setWeChatAuthDirectUrl('/register');
      const url = wechatHelper.genInWechatAuthUrl('register');
      window.location = url;
    }
  }

  componentDidMount() {
    record(actions.ENTER_REGISTER_PAGE, {}, eventCategory.PAGE_ENTER);
    wechatHelper.configShareCommon();
  }

  getCode = () => {
    if (this.state.countingDown) {
      return;
    }
    const mobileValid = this.state.countryCode === '86' ? phoneNumberHelper.isCNMobileNumber(this.state.mobile) : phoneNumberHelper.isUKMobileNumber(this.state.mobile);
    if (!mobileValid) {
      message.error(this.props.t('error.invalid_mobile_number'));
      return;
    }
    this.startCodeCountDown(60);
    const requestConfig = {
      method: 'POST',
      body: {
        telephone: this.state.mobile,
        dial_code: this.state.countryCode,
      },
      feedback: { progress: { mask: true } },
    };
    request('/auth/client/public/send-verification-code', requestConfig).then((response) => {
      if (response.success) {
        message.success(this.props.t('common.verification_code_sent'));
      } else {
        this.stopCodeCountDown();
      }
    }, () => {
      this.stopCodeCountDown();
    });
  }

  stopCodeCountDown = () => {
    if (this.state.codeCountDown) {
      window.clearInterval(this.state.codeCountDown);
    }
    this.setState({
      countingDown: false,
      codeCountDown: undefined,
    });
  };

  countryCodeChange = (event) => {
    this.setState({ countryCode: event.target.value });
  };

  startCodeCountDown = (second) => {
    if (this.state.codeCountDown) {
      window.clearInterval(this.state.codeCountDown);
    }
    this.state.countDownSecond = second;
    const countDown = window.setInterval(() => {
      if (this.state.countDownSecond <= 0) {
        this.stopCodeCountDown();
        return;
      }
      this.setState({
        countDownSecond: this.state.countDownSecond - 1,
      });
    }, 1000);
    this.setState({
      countingDown: true,
      codeCountDown: countDown,
    });
  }

  register = () => {
    if (this.state.password !== this.state.confirmPassword) {
      message.error(this.props.t('common.password_must_be_the_same'));
      return;
    }
    if (this.state.name) {
      const firstLetter = this.state.name.charAt(0).toLowerCase();
      if (firstLetter > 'z' || firstLetter < 'a') {
        message.error(this.props.t('common.use_name_must_start_with_letter'));
        return;
      }
    }
    let data = {
      username: this.state.name,
      dial_code: this.state.countryCode,
      password: this.state.password,
      telephone: this.state.mobile,
      verification_code: this.state.code,
    };
    if (this.state.hasWechatInfo) {
      data = Object.assign(data, this.state.wechatInfo);
    }
    this.props.register(data);
  }

  onClickWechatRegister = () => {
    const url = wechatHelper.genInWechatAuthUrl('login');
    window.location = url;
  }

  render() {
    const { t } = this.props;
    const { type } = this.state;
    let getCodeText = t('common.get_code');
    if (this.state.countingDown) {
      getCodeText = `${getCodeText} ( ${this.state.countDownSecond} )`;
    }
    const isOpenInWechat = wechatHelper.isOpenInWechat();
    return (
      <WingBlank size="lg" className={styles.main}>
        <WindowTitle title={t('pageTitles.register')} />
        {/* { isOpenInWechat && (
          <div>
            <p className={styles.welcome}>{t('common.welcome')}</p>
            <div
              className={styles.wechatButton}
              type="primary"
              size="large"
              onClick={() => {
                this.onClickWechatRegister();
              }}
            >
              <img
                src={WechatLogo}
                alt="wechat logo"
                className={styles.wechatLogo}
              />
              <span>{ t('common.wechat_register') }</span>
            </div>
            <p className={styles.or}><span>{t('common.or_register_with_phone')}</span></p>
          </div>
        )} */}

        <div className={styles.label}>
          { t('common.please_input_mobile') }
        </div>
        <div className={styles.mobileWrap}>
          <select
            className={styles.countryCodeSelect}
            value={this.state.countryCode}
            onChange={this.countryCodeChange}
          >
            <option value="86">+86</option>
            <option value="44">+44</option>
          </select>
          <Icon type="down" size={'xs'} />
          <div className={styles.mobileInputWrap}>
            <InputItem
              type={type}
              className={styles.registerInput}
              clear
              onChange={(v) => { this.setState({ mobile: v }); }}
              onBlur={(v) => { }}
            ></InputItem>
          </div>
        </div>

        <div className={styles.label}>
          { t('common.please_input_code') }
        </div>
        <div className={styles.codeWrap}>
          <div className={styles.codeInputWrap}>
            <InputItem
              type={type}
              className={styles.registerInput}
              onChange={(v) => { this.setState({ code: v }); }}
              onBlur={(v) => { }}
            ></InputItem>
          </div>
          <CommonButton className={styles.getCodeButton} disabled={this.state.countingDown} onClick={() => this.getCode()} size="small" inline>{getCodeText}</CommonButton>
        </div>
        <div className={styles.item}>
          <InputItem
            type={type}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_input_account')}
            onChange={(v) => { this.setState({ name: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_input_password')}
            onChange={(v) => { this.setState({ password: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <div className={styles.item}>
          <InputItem
            type={'password'}
            clear
            className={styles.registerInput}
            placeholder={t('common.please_confirm_password')}
            onChange={(v) => { this.setState({ confirmPassword: v }); }}
            onBlur={(v) => { }}
          ></InputItem>
        </div>

        <CommonButton
          disabled={!this.state.name || !this.state.password || !this.state.confirmPassword}
          onClick={() => this.register()}
          className={styles.registerButton}
        >{t('common.confirm_register')}</CommonButton>

        <div className={styles.registerWrap}>
          <Link to="/login"><p className={styles.loginTips}>{t('common.login_tips')}</p></Link>
        </div>
      </WingBlank>
    );
  }
}

Register.propTypes = {
  t: PropTypes.func,
  register: PropTypes.func,
  setWeChatAuthDirectUrl: PropTypes.func,
  location: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  (currentUser) => ({
    currentUser,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  register: (data) => dispatch(register(data)),
  setWeChatAuthDirectUrl: (url) => dispatch(setWeChatAuthDirectUrl(url)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(Register));
