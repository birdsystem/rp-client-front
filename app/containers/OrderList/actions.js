/*
 *
 * Global actions
 *
 */

import {
  LOAD_ORDER_LIST,
  LOAD_ORDER_LIST_SUCCESS,
  LOAD_ORDER_LIST_ERROR,
} from './constants';

export function loadOrderList(orderType, page) {
  return {
    type: LOAD_ORDER_LIST,
    page,
    orderType,
  };
}

export function loadOrderListSuccess(list) {
  return {
    type: LOAD_ORDER_LIST_SUCCESS,
    list,
  };
}

export function loadOrderListError() {
  return {
    type: LOAD_ORDER_LIST_ERROR,
  };
}
