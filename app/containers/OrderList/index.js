/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory, Link } from 'react-router';
import WindowTitle from 'components/WindowTitle';
import wechatHelper from 'utils/wechatHelper';
import { ListView } from 'antd-mobile';
import {
  selectCurrentUser,
  selectCurrency,
  selectUserInfoLoading,
} from 'containers/App/selectors';
import {
  getScrollTop,
  getScrollHeight,
  getWindowHeight,
  getTimeLeftDisplay,
} from 'utils/commonHelper';
import {
  SINGLE_BUYING,
  GROUP_BUYING,
  GROUP_LEADER_BUYING,
  GROUP_STATUS_ACTIVE,
  GROUP_STATUS_SUCCESS,
  GROUP_STATUS_FAILED,
  GROUP_STATUS_FINISH,
  MY_ORDER_TYPE_MY_START,
  MY_ORDER_TYPE_MY_FOLLOW,
} from 'utils/constants';
import Money from 'components/Money';
import Empty from 'components/Empty';
import { getStartGroupPrice, getSingleBuyPrice, getGroupPrice, getGroupPriceFromProps, getStartGroupPriceFromProps, getSingleBuyPriceFromProps } from 'utils/productHelper';
import styles from './styles.css';
import { selectOrderList, selectOrderListLoading } from './selectors';
import { loadOrderList } from './actions';

class OrderList extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props;
    const type = params.type;
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    this.state = {
      orderType: type,
      dataSource,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.props.loadOrderList(this.state.orderType, 1);
    wechatHelper.configShareCommon();
    // window.addEventListener('scroll', this.loadMore);
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.userInfoLoading && !nextProps.currentUser) {
      browserHistory.replace('/myinfo');
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.loadMore);
  }

  loadMore() {
    if (this.props.orderListLoading) {
      return;
    }
    const targetPaddingToBottom = 300;
    if (getScrollTop() + getWindowHeight() >=
      getScrollHeight() - targetPaddingToBottom) {
      // todo load more

    }
  }

  getStatusDisplay(item) {
    let ret = '';
    if (item.group_status === GROUP_STATUS_ACTIVE) {
      ret = this.props.t('group.process');
    } else if (item.group_status === GROUP_STATUS_SUCCESS) {
      ret = this.props.t('group.success');
    } else if (item.group_status === GROUP_STATUS_FAILED) {
      ret = this.props.t('group.fail');
    }
    return ret;
  }

  getBuyPrice(group) {
    let ret = 0;
    if (group.product_detail && group.buy_info && group.buy_info.sku_info) {
      if (group.type === SINGLE_BUYING) {
        ret = getSingleBuyPriceFromProps(group.product_detail, group.buy_info.sku_info.prop_path);
      } else if (group.type === GROUP_BUYING) {
        ret = getGroupPriceFromProps(group.product_detail, group.buy_info.sku_info.prop_path, group.min_user_count);
      } else if (group.type === GROUP_LEADER_BUYING) {
        ret = getStartGroupPriceFromProps(group.product_detail, group.buy_info.sku_info.prop_path);
      }
    } else if (group.type === SINGLE_BUYING) {
      ret = getSingleBuyPrice(group.product_detail);
    } else if (group.type === GROUP_BUYING) {
      ret = getGroupPrice(group.product_detail, group.min_user_count);
    } else if (group.type === GROUP_LEADER_BUYING) {
      ret = getStartGroupPrice(group.product_detail);
    }
    return ret;
  }

  render() {
    const { orderList, t } = this.props;
    let title = t('common.my_group');
    if (this.state.orderType === MY_ORDER_TYPE_MY_START) {
      title = t('common.my_start_group');
    }
    if (this.state.orderType === MY_ORDER_TYPE_MY_FOLLOW) {
      title = t('common.my_follow_group');
    }
    if (this.state.orderType === GROUP_STATUS_ACTIVE) {
      title = t('common.my_current_group');
    }
    if (this.state.orderType === GROUP_STATUS_FINISH) {
      title = t('common.my_finish_group');
    }
    const row = (group, sectionID, inx) => {
      let totalCount = 0;
      let totalPrice = 0;
      const buyPrice = this.getBuyPrice(group);
      if (group.buy_info) {
        totalCount = group.buy_info.count;
      }
      if (group.buy_info && group.buy_info.sku_info) {
        totalPrice = buyPrice * totalCount;
      }
      return (
        <Link
          to={`/groupdetail/${group.id}`}
          className={styles.itemWrap}
          key={inx}
        >
          <div className={styles.itemStatusWrap}>
            <p
              className={styles.itemTitle}
            >{ group.product_detail.name }</p>
            <span className={styles.itemStatus}>{ this.getStatusDisplay(
              group) }</span>
          </div>
          <div className={styles.itemInnerWrap}>
            <div
              onClick={() => {
                browserHistory.push(`/productdetail/${group.product_detail.id}`);
              }}
              className={styles.itemImageDiv}
              style={{ backgroundImage: `url("${group.product_detail.image_url}")` }}
            >
            </div>
            <div className={styles.itemInfo}>
              <div className={styles.peopleInfoWrap}>
                <div className={styles.itemCurrentPeople}>{ t(
                  'common.current_join_count') }: { group.current_user_count }
                </div>
                <div className={styles.itemNeedPeople}>
                  {
                    group.min_user_count - group.current_user_count > 0 ? t(
                      'common.people_left', {
                        count: group.min_user_count -
                        group.current_user_count,
                      })
                      : t('common.finish_group')
                  }
                </div>
              </div>

              {
                (group.group_status === GROUP_STATUS_ACTIVE && group.left_time > 0) && <div className={styles.itemTime}>
                  { t('common.left_time') }：{ getTimeLeftDisplay(
                  group.left_time) }
                </div>
              }

              {
                group.owner &&
                <div className={styles.ownerWrap}>
                  <span className={styles.ownerLabel}>
                    {t('common.group_owner')}:
                     </span>
                  <span className={styles.ownerName}>
                    {group.owner.username}
                  </span>
                </div>
              }
            </div>

            <div className={styles.itemPrice}>
              <span className={styles.groupPrice}>{ t(
                  'common.buy_price') }</span>
              <Money
                amount={this.getBuyPrice(group)}
                currencyCode={this.props.currency}
              />
            </div>
          </div>
          <div className={styles.totalPriceWrap}>
            <span className={styles.totalPriceText}>{ t(
                'common.total_product_acount', { count: totalCount }) }: </span>
            <span className={styles.totalPrice}>
              <Money
                amount={totalPrice}
                currencyCode={this.props.currency}
              />
            </span>
          </div>
        </Link>
      );
    };

    let dataSource = this.state.dataSource.cloneWithRows({});
    if (orderList) {
      dataSource = this.state.dataSource.cloneWithRows(orderList);
    }
    return (
      <div className={styles.main}>
        <WindowTitle title={title} />

        <ListView
          renderRow={row}
          useBodyScroll
          scrollRenderAheadDistance={500}
          onEndReachedThreshold={10}
          rowIdentities={() => null}
          dataSource={dataSource}
        />
        {
          !this.props.orderListLoading &&
          this.props.orderList.length === 0
          &&
            <Empty
              title={this.props.t('empty.empty_group')}
            />
        }
      </div>
    );
  }
}

OrderList.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  orderList: PropTypes.array,
  orderListLoading: PropTypes.bool,
  loadOrderList: PropTypes.func,
  currency: PropTypes.string,
  userInfoLoading: PropTypes.bool,
  params: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectOrderList(),
  selectOrderListLoading(),
  selectCurrency(),
  selectUserInfoLoading(),
  (currentUser, orderList, orderListLoading, currency, userInfoLoading) => ({
    currentUser: currentUser && currentUser.toJS(),
    orderList: orderList && orderList.toJS(),
    orderListLoading,
    currency,
    userInfoLoading,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadOrderList: (page, status) => dispatch(loadOrderList(page, status)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(OrderList));
