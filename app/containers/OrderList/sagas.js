/* eslint-disable no-unused-vars */
import { call, put, race, take } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import request from 'utils/request';
import {
  MY_ORDER_TYPE_MY_START,
  MY_ORDER_TYPE_MY_FOLLOW,
  GROUP_STATUS_ACTIVE,
  GROUP_STATUS_FINISH,
  GROUP_STATUS_SUCCESS,
  GROUP_STATUS_FAILED,
  GROUP_LEADER_BUYING,
  GROUP_BUYING,
} from 'utils/constants';
import { LOAD_ORDER_LIST } from './constants';
import {
  loadOrderListSuccess,
} from './actions';

export default [
  getLoadOrderListWatcher,
];


export function* getLoadOrderListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadOrderList: take(LOAD_ORDER_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    // const page = watcher.loadGroupList.page;
    let requestURL = '/group/my-group';
    const orderType = watcher.loadOrderList.orderType;
    let urlParams = {};
    switch (orderType) {
      case MY_ORDER_TYPE_MY_START: {
        urlParams = { type: GROUP_LEADER_BUYING };
        break;
      }
      case MY_ORDER_TYPE_MY_FOLLOW: {
        urlParams = { type: GROUP_BUYING };
        break;
      }
      case GROUP_STATUS_ACTIVE: {
        urlParams = { 'group-status': 'ACTIVE' };
        break;
      }
      case GROUP_STATUS_FINISH: {
        requestURL = `${requestURL}?group-status[]=SUCCESS&group-status[]=FAILED`;
        // urlParams = { 'group-status': 'FINISH' };
        // urlParams = { status: [GROUP_STATUS_SUCCESS, GROUP_STATUS_FAILED] };
        break;
      }
      default : {
        urlParams = {};
      }
    }
    const requestConfig = {
      urlParams,
      feedback: { progress: { mask: true } },
    };
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data;
      yield put(loadOrderListSuccess(list));
    }
  }
}
