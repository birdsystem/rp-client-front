/*
 *
 * OrderList constants
 *
 */

export const TEST = 'OrderList/TEST';
export const TEST_SAGA = 'OrderList/TEST_SAGA';
export const TEST_SAGA_SUCCESS = 'OrderList/TEST_SAGA_SUCCESS';

export const LOAD_ORDER_LIST = 'OrderList/LOAD_ORDER_LIST';
export const LOAD_ORDER_LIST_SUCCESS = 'OrderList/LOAD_ORDER_LIST_SUCCESS';
export const LOAD_ORDER_LIST_ERROR = 'OrderList/LOAD_ORDER_LIST_ERROR';
