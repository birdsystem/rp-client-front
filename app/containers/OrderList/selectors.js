import { createSelector } from 'reselect';

const selectOrderListDomain = () => (state) => state.get('orderlist');

const selectOrderList = () => createSelector(
  selectOrderListDomain(),
  (substate) => substate.get('orderList')
);

const selectOrderListLoading = () => createSelector(
  selectOrderListDomain(),
  (substate) => substate.get('orderListLoading')
);

export {
  selectOrderList,
  selectOrderListLoading,
};
