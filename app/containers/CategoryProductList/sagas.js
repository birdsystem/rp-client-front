import { call, put, race, take, select } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { DEFAULT_PAGE_SIZE } from 'utils/constants';
import { LOAD_PRODUCT_LIST } from './constants';
import {
  loadProductListSuccess,
} from './actions';

import {
  selectProductList,
} from './selectors';

export default [
  getLoadProductListWatcher,
];

export function* getLoadProductListWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadProductList: take(LOAD_PRODUCT_LIST),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;

    const page = watcher.loadProductList.page;
    const product = yield select(selectProductList());
    let showLoading = false;
    if (page === 1 && (!product || product.toJS().length === 0)) {
      showLoading = true;
    }
    const requestConfig = {
      urlParams: {
        limit: DEFAULT_PAGE_SIZE,
        start: (page - 1) * DEFAULT_PAGE_SIZE,
        product_category_id: watcher.loadProductList.id,
      },
    };
    if (showLoading) {
      requestConfig.feedback = { progress: { mask: false } };
    }
    const requestURL = '/product';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      yield put(loadProductListSuccess(list, result.data.total));
    }
  }
}
