import { fromJS } from 'immutable';

import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

const initialState = fromJS({
  productList: null,

  productListLoading: false,

  currentPage: 1,

  totalCount: 0,

  currentCategoryId: -1,
});

function productListReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_PRODUCT_LIST:
      if (action.page === 1) {
        if (state.get('currentCategoryId') === action.id) {
          return state
          .set('currentPage', action.page)
          .set('totalCount', 0)
          .set('currentCategoryId', action.id)
          .set('productListLoading', true);
        }
        return state
        .set('productList', fromJS([]))
        .set('currentPage', action.page)
        .set('totalCount', 0)
        .set('currentCategoryId', action.id)
        .set('productListLoading', true);
      }
      return state
      .set('currentPage', action.page)
      .set('productListLoading', true);
    case LOAD_PRODUCT_LIST_SUCCESS: {
      if (state.get('currentPage') === 1) {
        return state
        .set('productList', fromJS(action.list))
        .set('totalCount', action.count)
        .set('productListLoading', false);
      }
      const list = state.get('productList').toJS().concat(action.list);
      return state
      .set('productList', fromJS(list)).set('productListLoading', false);
    }
    case LOAD_PRODUCT_LIST_ERROR:
      return state
      .set('productListLoading', false);
    default:
      return state;
  }
}

export default productListReducer;
