/*
 *
 * CategoryProductList constants
 *
 */

export const LOAD_PRODUCT_LIST = 'CategoryProductList/LOAD_PRODUCT_LIST';
export const LOAD_PRODUCT_LIST_SUCCESS = 'CategoryProductList/LOAD_PRODUCT_LIST_SUCCESS';
export const LOAD_PRODUCT_LIST_ERROR = 'CategoryProductList/LOAD_PRODUCT_LIST_ERROR';
