/* eslint-disable react/no-unused-prop-types,react/no-will-update-set-state,no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import { browserHistory, withRouter } from 'react-router';
import {
  getScrollTop,
  getScrollHeight,
  getWindowHeight,
} from 'utils/commonHelper';
import { record, eventCategory, actions, eventParams } from 'utils/gaDIRecordHelper';
import WindowTitle from 'components/WindowTitle';
import Empty from 'components/Empty';
import { selectCurrentUser } from 'containers/App/selectors';
import { LIST_LAYOUT_TYPE } from 'containers/App/constants';
import { Icon, PullToRefresh, ListView } from 'antd-mobile';
import ProductItem from 'components/ProductItem';
import wechatHelper from 'utils/wechatHelper';
import { getDisplayPrice } from 'utils/productHelper';
import {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
} from './selectors';
import { loadProductList } from './actions';
import styles from './styles.css';

class CategoryProductList extends Component {
  constructor(props) {
    super(props);
    const { params, location = {} } = this.props;
    const title = (location.state && location.state.title) || props.t('common.hot_product');
    let cid = -1;
    if (params && params.cid) {
      cid = params.cid;
    }
    if (cid < 0) {
      browserHistory.replace('/');
    }
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    this.state = {
      cid,
      currentPage: 1,
      title,
      refreshing: false,
      dataSource,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.props.loadProductList(this.state.cid, this.state.currentPage);
    window.addEventListener('scroll', this.loadMore);
    this.configWechatJSSdk();
    const data = {};
    data[eventParams.ID] = this.state.cid;
    record(actions.ENTER_CATEGORY_PRODUCT_PAGE, data, eventCategory.PAGE_ENTER);
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.refreshing && this.props.productListLoading && !nextProps.productListLoading) {
      this.setState({
        refreshing: false,
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.loadMore);
  }

  configWechatJSSdk() {
    wechatHelper.configShareCommon();
  }

  goBack() {
    browserHistory.goBack();
  }

  loadMore() {
    if (this.props.productListLoading) {
      return;
    }
    const targetPaddingToBottom = 300;
    if (getScrollTop() + getWindowHeight() >=
      getScrollHeight() - targetPaddingToBottom) {
      const count = this.props.productList ? this.props.productList.length : 0;
      if (count < this.props.totalCount) {
        this.setState({
          currentPage: this.state.currentPage + 1,
        });
        this.props.loadProductList(this.state.cid, this.state.currentPage);
      }
    }
  }

  render() {
    const { t } = this.props;
    const productList = this.props.productList;
    let hasMore = false;
    const count = this.props.productList ? this.props.productList.length : 0;
    if (count < this.props.totalCount) {
      hasMore = true;
    }
    const row = (rowData, sectionID, rowID) => (
      <ProductItem key={`p${rowID}`} product={rowData} />
    );

    let dataSource = this.state.dataSource.cloneWithRows({});
    if (productList) {
      dataSource = this.state.dataSource.cloneWithRows(productList);
    }

    return (
      <div className={styles.productWrap}>
        <WindowTitle title={this.state.title} />

        <div className={styles.title}>
          <div onClick={this.goBack}>
            <Icon type="left" size="lg" />
          </div>
          {this.state.title}
        </div>
        <div className={styles.content}>
          <ListView
            renderRow={row}
            useBodyScroll
            scrollRenderAheadDistance={500}
            onEndReachedThreshold={10}
            rowIdentities={() => null}
            dataSource={dataSource}
            pullToRefresh={<PullToRefresh
              damping={1000}
              indicator={{ activate: ' ', finish: ' ' }}
              direction={'down'}
              refreshing={this.state.refreshing}
              onRefresh={() => {
                if (this.props.productListLoading) {
                  return;
                }
                this.setState({
                  refreshing: true,
                  currentPage: 1,
                });
                this.props.loadProductList(this.state.cid, 1);
              }}
            />}
          />

          {
          !this.props.productListLoading && this.props.productList.length === 0
          &&
            <Empty title={t('empty.empty_search_product')} />
        }
          {
          hasMore &&
          <div className={styles.hasMoreWrap}>
            <Icon type="loading" size={'md'} />
          </div>
        }
        </div>
      </div>
    );
  }
}

CategoryProductList.propTypes = {
  currentUser: PropTypes.object,
  t: PropTypes.func,
  loadProductList: PropTypes.func,
  productList: PropTypes.array,
  productListLoading: PropTypes.bool,
  totalCount: PropTypes.number,
  params: PropTypes.object,
  location: PropTypes.object,
};

const mapStateToProps = createSelector(
  selectProductList(),
  selectProductListLoading(),
  selectCurrentUser(),
  selectTotalCount(),
  (productList, productListLoading, currentUser, totalCount) => ({
    productList: (productList && productList.toJS()) || [],
    productListLoading,
    currentUser: currentUser && currentUser.toJS(),
    totalCount,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadProductList: (cid, page) => dispatch(loadProductList(cid, page)),
});

export default translate()(
  withRouter(connect(mapStateToProps, mapDispatchToProps)(CategoryProductList)));
