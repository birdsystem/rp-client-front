import { createSelector } from 'reselect';

const selectProductListDomain = () => (state) => state.get('categoryproductlist');

const selectProductList = () => createSelector(
  selectProductListDomain(),
  (substate) => substate.get('productList')
);

const selectCurrentCategoryId = () => createSelector(
  selectProductListDomain(),
  (substate) => substate.get('currentCategoryId')
);

const selectProductListLoading = () => createSelector(
  selectProductListDomain(),
  (substate) => substate.get('productListLoading')
);

const selectTotalCount = () => createSelector(
  selectProductListDomain(),
  (substate) => substate.get('totalCount')
);

export {
  selectProductList,
  selectProductListLoading,
  selectTotalCount,
  selectCurrentCategoryId,
};
