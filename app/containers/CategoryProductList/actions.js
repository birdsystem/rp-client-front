import {
  LOAD_PRODUCT_LIST,
  LOAD_PRODUCT_LIST_SUCCESS,
  LOAD_PRODUCT_LIST_ERROR,
} from './constants';

export function loadProductList(id, page) {
  return {
    type: LOAD_PRODUCT_LIST,
    id,
    page,
  };
}

export function loadProductListSuccess(list, count) {
  return {
    type: LOAD_PRODUCT_LIST_SUCCESS,
    list,
    count,
  };
}

export function loadProductListError() {
  return {
    type: LOAD_PRODUCT_LIST_ERROR,
  };
}
