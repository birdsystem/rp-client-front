/*
 *
 * Template reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_COMMENT,
  LOAD_COMMENT_SUCCESS,
  LOAD_COMMENT_FAIL,
} from './constants';

const initialState = fromJS({
  comments: [],
  totalComments: 0,
  currentPage: 1,
  loadingComment: false,
});

function productDetailReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_COMMENT:
      if (action.page === 1) {
        return state
        .set('loadingComment', true)
        .set('currentPage', action.page)
        .set('comments', fromJS([]));
      }
      return state
        .set('loadingComment', true)
        .set('currentPage', action.page);
    case LOAD_COMMENT_SUCCESS:
      {
        if (state.get('currentPage') === 1) {
          return state
          .set('loadingComment', false)
          .set('comments', fromJS(action.data))
          .set('totalComments', action.total);
        }
        const list = state.get('comments').toJS().concat(action.data);
        return state
          .set('loadingComment', false)
          .set('comments', fromJS(list));
      }
    case LOAD_COMMENT_FAIL:
      return state
      .set('loadingComment', false);
    default:
      return state;
  }
}

export default productDetailReducer;
