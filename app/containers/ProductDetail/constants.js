/*
 *
 * ProductDetail constants
 *
 */

export const LOAD_COMMENT = 'ProductDetail/LOAD_COMMENT';
export const LOAD_COMMENT_SUCCESS = 'ProductDetail/LOAD_COMMENT_SUCCESS';
export const LOAD_COMMENT_FAIL = 'ProductDetail/LOAD_COMMENT_FAIL';
