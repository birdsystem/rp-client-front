/* eslint-disable react/sort-comp,react/no-will-update-set-state,radix,no-mixed-operators, jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Lightbox from 'react-image-lightbox';
import { createSelector } from 'reselect';
import { translate } from 'react-i18next';
import Money from 'components/Money';
import WeChatShare from 'components/WeChatShare';
import Tips from 'components/Tips';
import WindowTitle from 'components/WindowTitle';
import DeliveryFeeCalculator from 'components/DeliveryFeeCalculator';
import {
  selectLoadingProduct,
  selectPendingProduct,
  selectPurchaseModalType,
  selectPurchaseModalVisible,
  selectCurrentUser,
  selectLocationState,
} from 'containers/App/selectors';
import {
  loadProductDetail,
  showPurchaseModal,
  hidePurchaseModal,
  loadGroupDetailSuccess,
  clearPayPreShipmentRedirectUrl,
} from 'containers/App/actions';
import { browserHistory, Link } from 'react-router';
import { isBackendYes } from 'utils/backendHelper';
import message from 'components/message';
import { Icon, Modal, Carousel, ListView, Badge } from 'antd-mobile';
import { getTimeLeftDisplay } from 'utils/commonHelper';
import GraySpace from 'components/GraySpace';
import wechatHelper from 'utils/wechatHelper';
import {
  getDisplayPrice,
  getStartGroupPrice,
  getDisplayPriceFromProps,
  getStartGroupPriceFromProps,
  getSingleBuyPriceFromProps,
  isProductActive,
  getCategoryDisplay,
} from 'utils/productHelper';
import CopyToClipboard from 'react-copy-to-clipboard';
import _ from 'lodash';
import MoneyInLocalCurrency from 'components/MoneyInLocalCurrency';
import CommonButton from 'components/CommonButton';
import ShareTips from 'components/ShareTips';
import Comment from 'components/Comment';
import {
  SINGLE_BUYING,
  GROUP_LEADER_BUYING,
  GROUP_BUYING,
  SHARE_TYPE,
} from 'utils/constants';
import {
  record,
  eventCategory,
  actions,
  eventParams,
} from 'utils/gaDIRecordHelper';
import { loadComment } from './actions';
import {
  selectComments,
  selectTotalComments,
  selectLoadingComment,
} from './selectors';
import styles from './styles.css';
import homeIcon from './home.png';
import customerIcon from './customer.png';
import PurchaseModal from '../PurchaseModal';
import calcIcon from './calculator.png';

function closest(el, selector) {
  const matchesSelector = el.matches || el.webkitMatchesSelector ||
    el.mozMatchesSelector || el.msMatchesSelector;
  while (el) {
    if (matchesSelector.call(el, selector)) {
      return el;
    }
    el = el.parentElement; // eslint-disable-line
  }
  return null;
}

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    const { params, location } = this.props;
    let gid = -1;
    if (location && location.query && location.query.gid) {
      gid = location.query.gid;
    }
    if (location && location.query && location.query.back) {
      window.location = `${window.location.protocol}//${window.location.host}`;
    }

    if (location && location.query && location.query.type) {
      if (location.query.type === SHARE_TYPE.FOLLOW_GROUP) {
        window.location = `${window.location.protocol}//${window.location.host}/followgroup/${location.query.gid}/${location.query.pid}`;
      }
    }

    // if (location && location.query && location.query.from_user) {
    //  console.log(location.query.from_user);
    // }

    const id = params.productId;
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    this.state = {
      gid,
      id,
      skuIndex: 0,
      showMoreGroup: false,
      showWeChatShareTips: false,
      hasConfigWeChatShare: false,
      hasAutoPopFollow: false,
      images: [],
      carouselSet: false,
      lightboxOpen: false,
      imageIndex: 0,
      showMoreComment: false,
      currentPage: 1,
      dataSource,
      calculatorModalVisible: false,
      commentLightboxOpen: false,
      commentImageIndex: 0,
      commentImages: [],
    };
    // this.loadMoreComment = this.loadMoreComment.bind(this);
  }

  componentDidMount() {
    this.props.clearPayPreShipmentRedirectUrl();
    const data = {};
    data[eventParams.ID] = this.state.id;
    record(actions.ENTER_PRODUCT_PAGE, data, eventCategory.PAGE_ENTER);
    record(`${this.state.id}_${actions.ENTER_PRODUCT_PAGE}`, data, eventCategory.PAGE_ENTER);
    this.props.loadProductDetail(this.state.id);
    this.props.loadComment(this.state.id, 1);
  }
  componentWillUpdate(nextProps, nextState) {
    if (!_.isEmpty(nextProps.pendingProduct) &&
      nextState.hasConfigWeChatShare === false) {
      const currentSku = nextProps.pendingProduct && nextProps.pendingProduct.meta &&
        nextProps.pendingProduct.meta.sku_info &&
        nextProps.pendingProduct.meta.sku_info[this.state.skuIndex];
      const displayPrice = currentSku ? getDisplayPriceFromProps(nextProps.pendingProduct,
        currentSku.prop_path) : getDisplayPrice(nextProps.pendingProduct);

      if (this.state.gid && parseInt(this.state.gid) > 0) {
        wechatHelper.configFollowGroupShare(nextProps.pendingProduct.name,
          displayPrice,
          nextProps.pendingProduct.id, this.state.gid,
          nextProps.pendingProduct.image_url);
      } else {
        wechatHelper.configProductShare(nextProps.pendingProduct.name,
          displayPrice,
          nextProps.pendingProduct.id, nextProps.pendingProduct.image_url);
      }
      this.setState({
        hasConfigWeChatShare: true,
      });
      const product = nextProps.pendingProduct;
      let category = '';
      if (product.category && product.category.length > 0) {
        const categoryItem = product.category[0];
        category = `${categoryItem.id}|${categoryItem.name}`;
      }
      record(actions.VIEW_ITEM, {
        id: product.id,
        name: product.name,
        list_name: '',
        brand: '',
        category,
        variant: '',
        list_position: -1,
        quantity: 1,
        price: getDisplayPrice(product),
      });
    }

    if (!_.isEmpty(nextProps.pendingProduct) &&
      nextState.hasAutoPopFollow === false && nextState.gid &&
      parseInt(nextState.gid, 10) > 0) {
      const groups = nextProps.pendingProduct.group;
      const currentUserId = this.props.currentUser
        ? this.props.currentUser.id
        : -1;
      for (let i = 0; i < groups.length; i += 1) {
        if (parseInt(groups[i].id, 10) === parseInt(nextState.gid, 10)) {
          if ((parseInt(currentUserId, 10) !==
              parseInt(groups[i].owner.id, 10))) {
            const productDetail = Object.assign(nextProps.pendingProduct);
            delete productDetail.group;
            const groupData = Object.assign(groups[i],
              { product_detail: productDetail });
            this.props.loadGroupDetailSuccess(groupData);
            this.props.showPurchaseModal(GROUP_BUYING);

            this.setState({
              hasAutoPopFollow: true,
            });
            break;
          } else {
            // auto follow group is my group
            this.setState({
              hasAutoPopFollow: true,
            });
          }
        }
      }
    }

    if (!this.state.carouselSet && !_.isEmpty(nextProps.pendingProduct) &&
      nextProps.pendingProduct.meta && nextProps.pendingProduct.meta.pic_url &&
      nextProps.pendingProduct.meta.pic_url.length > 0) {
      this.setState({
        images: nextProps.pendingProduct.meta.pic_url,
        carouselSet: true,
      });
    }
  }

  componentWillUnmount() {
    this.stopBindCommentScroll();
  }

  onCloseMoreGroup() {
    this.setState({
      showMoreGroup: false,
    });
  }

  onCloseMoreComment() {
    this.setState({
      showMoreComment: false,
    });
    this.stopBindCommentScroll();
  }

  onWrapTouchStart = (e) => {
    // fix touch to scroll background page on iOS
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  };

  goBack = () => {
    if (this.props.locationState.previousPathname !== '/') {
      browserHistory.goBack();
    } else {
      browserHistory.push('/');
    }
  };

  showMoreGroup() {
    this.setState({
      showMoreGroup: true,
    });
  }

  showMoreComment() {
    this.startBindCommentScroll();
    this.setState({
      showMoreComment: true,
    });
  }

  onEndReached() {
    console.log('onEndReached');
  }

  startBindCommentScroll() {
    if (this.startBindCommentScrollTimeout) {
      clearTimeout(this.startBindCommentScroll);
    }
    const el = document.getElementById('allCommentWrap');
    if (el) {
      el.addEventListener('scroll', () => {
        const clientHeight = el.clientHeight;
        const scrollTop = el.scrollTop;
        const scrollHeight = el.scrollHeight;
        if (clientHeight && scrollTop && scrollHeight) {
          const reacthEnd = scrollHeight - (scrollTop + clientHeight) < 100;
          if (reacthEnd && !this.props.loadingComment) {
            this.props.loadComment(this.state.id, this.state.currentPage + 1);
            this.setState({
              currentPage: this.state.currentPage + 1,
            });
          }
        }
      });
    } else {
      this.startBindCommentScrollTimeout = setTimeout(() => {
        this.startBindCommentScroll();
      }, 500);
    }
  }

  stopBindCommentScroll() {
    if (this.startBindCommentScrollTimeout) {
      clearTimeout(this.startBindCommentScroll);
    }
  }

  render() {
    const { purchaseModalType, purchaseModalVisible, comments, totalComments } = this.props;
    const hasMore = comments && comments.length < totalComments;
    const hasComments = comments && comments.length > 0;
    const t = this.props.t;
    const loadingProduct = this.props.loadingProduct;
    const pendingProduct = this.props.pendingProduct;
    const isActive = pendingProduct && isProductActive(pendingProduct);
    const categoryDisplay = getCategoryDisplay(pendingProduct);
    const showUnActiveTips = !_.isEmpty(pendingProduct) && !isActive;
    const maxHeight = window.screen.availHeight * 0.65;
    const copySuccess = wechatHelper.isOpenInWechat() ? t(
      'common.copy_success_wechat') : t('common.copy_success');
    let copyText = window.location.href;
    if (!_.isEmpty(pendingProduct)) {
      copyText = t('common.product_detail_share_text', {
        name: pendingProduct.name,
        url: window.location.href,
        price: getStartGroupPrice(pendingProduct),
      });
    }
    const currentUserId = this.props.currentUser
      ? this.props.currentUser.id
      : -1;
    let groupElement = null;
    let fullGroupElement = null;
    let hasMoreGroup = false;
    if (pendingProduct && pendingProduct.group) {
      let groups = pendingProduct.group;
      if (pendingProduct.group.length > 2) {
        groups = pendingProduct.group.slice(0, 2);
        hasMoreGroup = true;
      }
      const popGroup = pendingProduct.group;
      fullGroupElement = popGroup.map((group, inx) => (
        <div
          className={styles.groupItem}
          key={`fg${inx}`}
          onClick={(e) => {
            e.preventDefault(); // 修复 Android 上点击穿透
            if (parseInt(currentUserId) !== parseInt(group.owner.id)) {
              const productDetail = Object.assign(pendingProduct);
              delete productDetail.group;
              const groupData = Object.assign(group,
                { product_detail: productDetail });
              this.props.loadGroupDetailSuccess(groupData);
              this.onCloseMoreGroup();
              this.props.showPurchaseModal(GROUP_BUYING);
            }
          }}
        >
          <div className={styles.groupName}>#{ group.id } { t(
            'common.group_owner') }: { group.owner.username }</div>
          <div className={styles.groupInfo}>
            <div className={styles.groupLeft}>
              {
                parseInt(group.min_user_count) -
                parseInt(group.current_user_count) > 0
                  ? t('common.people_left',
                  { count: group.min_user_count - group.current_user_count })
                  : t('common.finish_group')
              }
            </div>
            {
              parseInt(group.min_user_count) -
              parseInt(group.current_user_count) > 0
              &&
              <div className={styles.leftTime}>
                { t('common.left_time') } :{ getTimeLeftDisplay(group.left_time) }
              </div>
            }
          </div>
          { (parseInt(currentUserId) !== parseInt(group.owner.id))
            ?
              <CommonButton type={'dark'} className={styles.joinButton}>{ t('common.join_group') }</CommonButton>
            :
              <Badge text={t('common.my_group')} hot style={{ marginLeft: 12 }} />
          }
        </div>
      ));

      groupElement = groups.map((group, inx) => (
        <div
          className={styles.groupItem}
          key={`g${inx}`}
          onClick={(e) => {
            e.preventDefault(); // 修复 Android 上点击穿透
            if (parseInt(currentUserId) !== parseInt(group.owner.id)) {
              const data = {};
              data.event_callback = () => {
                const productDetail = Object.assign(pendingProduct);
                delete productDetail.group;
                const groupData = Object.assign(group,
                  { product_detail: productDetail });
                this.props.loadGroupDetailSuccess(groupData);
                this.props.showPurchaseModal(GROUP_BUYING);
              };
              data[eventParams.ID] = this.state.id;
              data[eventParams.GROUP_ID] = group.id;
              record(actions.CLICK_FOLLOW_GROUP,
                data,
                eventCategory.USER_ACTION,
              );
            }
          }}
        >
          <div className={styles.groupName}>#{ group.id } { t(
            'common.group_owner') }: { group.owner.username }</div>
          <div className={styles.groupInfo}>
            <div className={styles.groupLeft}>
              {
                group.min_user_count - group.current_user_count > 0
                  ? t('common.people_left',
                  { count: group.min_user_count - group.current_user_count })
                  : t('common.finish_group')
              }
            </div>
            <div className={styles.leftTime}>
              { t('common.left_time') } :{ getTimeLeftDisplay(group.left_time) }
            </div>
          </div>
          { (parseInt(currentUserId) !== parseInt(group.owner.id))
          ?
            <CommonButton type={'dark'} className={styles.joinButton}>{ t('common.join_group') }</CommonButton>
            :
            <Badge text={t('common.my_group')} hot style={{ marginLeft: 12 }} />
          }
        </div>
      ));
    }
    // get from pop modal
    const currentSku = pendingProduct && pendingProduct.meta &&
      pendingProduct.meta.sku_info &&
      pendingProduct.meta.sku_info[this.state.skuIndex];
    const displayPrice = currentSku ? getDisplayPriceFromProps(pendingProduct,
      currentSku.prop_path) : getDisplayPrice(pendingProduct);

    let lowestDisplayPrice = displayPrice;
    let highestDisplayPrice = displayPrice;
    if (pendingProduct && pendingProduct.meta && pendingProduct.meta.sku_info) {
      pendingProduct.meta.sku_info.map((sku) => {
        if (parseFloat(sku.price_including_platform_delivery_fee) < parseFloat(lowestDisplayPrice)) {
          lowestDisplayPrice = sku.price_including_platform_delivery_fee;
        }
        if (parseFloat(sku.price_including_platform_delivery_fee) > parseFloat(highestDisplayPrice)) {
          highestDisplayPrice = sku.price_including_platform_delivery_fee;
        }
        return null;
      });
    }

    const hasPriceSection = lowestDisplayPrice !== highestDisplayPrice;

    const startGroupPrice = currentSku ? getStartGroupPriceFromProps(
      pendingProduct, currentSku.prop_path) : getStartGroupPrice(
      pendingProduct);
    const singleBuyPrice = currentSku ? getSingleBuyPriceFromProps(
      pendingProduct, currentSku.prop_path) : getSingleBuyPriceFromProps(
      pendingProduct);

    const { images, lightboxOpen, imageIndex, commentLightboxOpen, commentImages, commentImageIndex } = this.state;

    const lightboxImages = images.length > 0
      ? images
      : [
        pendingProduct
          ? pendingProduct.image_url
          : ''];

    const commentLightboxImages = commentImages.length > 0 ? commentImages : [];

    let hasCheck = true;
    let checkInfo = '';
    let canBuy = true;
    if (pendingProduct) {
      // pendingProduct.has_check = 0;
      // pendingProduct.limit_for_purchasing = 0;
      hasCheck = isBackendYes(pendingProduct.has_check);
      if (!hasCheck) {
        // const limitForPurchasing = parseInt(pendingProduct.limit_for_purchasing,
        //   10);
        // if (limitForPurchasing <= 0) {
        //   canBuy = false;
        //   checkInfo = t('common.you_have_bought_uncheck_product_before');
        // } else {
        //   checkInfo = `${t('common.you_can_pay_shipping_fee_later', { count: limitForPurchasing })}`;
        // }
        checkInfo = `${t('common.you_can_pay_shipping_fee_later_no_limit')}`;
      }
    }

    const row = (rowData, sectionID, rowID) => (
      <Comment
        key={rowID}
        comment={rowData}
        index={`modal_comment_${rowID}`}
        onImagesClick={(currentCommentImages, inx) => {
          const ua = window.navigator.userAgent || '';
          const isSafari = ua.toLowerCase().indexOf('safari') !== -1 && ua.toLowerCase().indexOf('vervion') !== -1;
          // issue for translate 3d
          if (isSafari) {
            return;
          }
          this.setState({
            commentLightboxOpen: true,
            commentImageIndex: inx,
            commentImages: currentCommentImages,
          });
        }}
      />
    );

    let dataSource = this.state.dataSource.cloneWithRows({});
    if (comments) {
      dataSource = this.state.dataSource.cloneWithRows(comments);
    }

    return (
      <div>
        <WindowTitle title={t('pageTitles.product_detail')} />

        { (loadingProduct || _.isEmpty(pendingProduct)) ? null : (
          <div className={styles.wrapInner}>
            <div className={styles.itemWrap}>
              <div className={styles.stickyHeader}>
                <div className={styles.backIcon} onClick={this.goBack}>
                  <Icon type="left" size="md" />
                </div>
              </div>
              { images.length > 0 ? (
                <Carousel
                  autoplay={false}
                  infinite
                  afterChange={(index) => this.setState(
                    { imageIndex: index })}
                  selectedIndex={imageIndex}
                >
                  { images.map((url) => (
                    <img
                      key={url}
                      alt={'product'}
                      className={styles.itemImage}
                      src={url}
                      onLoad={() => {
                        // fire window resize event to change height
                        window.dispatchEvent(new Event('resize'));
                      }}
                      onClick={() => this.setState({ lightboxOpen: true })}
                    />
                  )) }
                </Carousel>
              ) : (
                <img
                  alt={'product'}
                  className={styles.itemImage}
                  src={pendingProduct.image_url}
                  onClick={() => this.setState({ lightboxOpen: true })}
                />
              ) }
              <div className={styles.priceWrap}>
                {
                  hasPriceSection ?
                    <div className={styles.price}>
                      <MoneyInLocalCurrency cnyAmount={lowestDisplayPrice} />
                      -
                      <MoneyInLocalCurrency cnyAmount={highestDisplayPrice} displaySymbol={false} />
                      <span className={styles.cnyPrice}>
                        <Money
                          amount={lowestDisplayPrice}
                        />
                        -
                        <Money
                          amount={highestDisplayPrice}
                          displaySymbol={false}
                        />
                      </span>
                    </div>
                    :
                    <div className={styles.price}>
                      <MoneyInLocalCurrency cnyAmount={displayPrice} />
                      <span className={styles.cnyPrice}>
                        <Money
                          amount={displayPrice}
                        />
                      </span>
                    </div>
                }
                <div className={styles.dealCount}>{ t(
                  'common.already_deal_count',
                  { count: pendingProduct.deal_count }) }</div>
              </div>
              {/* {isBackendYes(pendingProduct.is_recommended) && (
                <div className={styles.recommenedProduct}>
                  <Tips fontSize="13px">
                    {t('common.recommended_product_free_shipping_on_group_buying')}
                  </Tips>
                </div>
              )} */}
              {/* {hasCheck ? (
                <div className={styles.deliverFee}>
                  <span>{ t('common.org_delivery_fee') }:</span>&nbsp;
                  <MoneyInLocalCurrency
                    cnyAmount={pendingProduct.single_buying_cross_border_shipping}
                  />
                </div>
              ) : (
                <div className={styles.deliverFeeCalculator} onClick={() => this.setState({ calculatorModalVisible: true })}>
                  <img src={calcIcon} className={styles.calcIcon} />
                  <div className={styles.calculatorLabel}>{ t('common.estimated_delivery_fee') }</div>
                </div>
              )} */}

              <DeliveryFeeCalculator
                visible={this.state.calculatorModalVisible}
                feeTable={pendingProduct.cross_border_shipping_fee_table}
                hide={() => this.setState({ calculatorModalVisible: false })}
              />

              {!hasCheck && (
                <div className={styles.prepareDeliverFee}>
                  <span>{ checkInfo }</span>
                </div>
              )}
              <div className={styles.itemTitleWrap}>
                <div
                  className={styles.itemTitle}
                >{ pendingProduct.name }</div>
                <CopyToClipboard
                  text={copyText}
                  onCopy={() => {
                    if (wechatHelper.isOpenInWechat()) {
                      this.setState({
                        showWeChatShareTips: true,
                      });
                    } else {
                      message.success(copySuccess);
                    }
                  }}
                >
                  <CommonButton className={styles.shareButton}>{t('common.share')}</CommonButton>
                </CopyToClipboard>
              </div>
            </div>

            { categoryDisplay &&
            <div
              className={styles.category}
              onClick={
                () => {
                  browserHistory.push({
                    pathname: `/categoryproductlist/${pendingProduct.category[pendingProduct.category.length -
                    1].id}`,
                    state: {
                      title: pendingProduct.category[pendingProduct.category.length -
                      1].name,
                    },
                  });
                }
              }
            >
              { `${t('common.category')}: ${categoryDisplay}` }
            </div>
            }

            {
              pendingProduct.origin_url &&
              <div className={styles.source}>
                <span className={styles.sourceLabel}>{ t(
                  'common.source') }: </span>
                <a href={pendingProduct.origin_url}>{ t(
                  'common.product_source') }</a>
              </div>
            }

            <GraySpace />

            {/* <div className={styles.groupWrap}>
              <div className={styles.groupTitle}>

                {
                  pendingProduct.group && pendingProduct.group.length > 0
                    ? <span>{ t('common.current_group', {
                      count: (pendingProduct.group
                        ? pendingProduct.group.length
                        : 0),
                    }) }</span>
                    : <span>{ t('common.empty_group') }</span>
                }
                { hasMoreGroup &&
                <span
                  onClick={() => { this.showMoreGroup(); }}
                  className={styles.viewMoreGroup}
                >{ t(
                  'common.view_more_group') }</span>
                }
              </div>
              { groupElement }
            </div> */}

            <GraySpace />

            {
              hasComments &&
              <div>
                <div className={styles.commentContainer}>
                  <div className={styles.commentTitleWrap}>
                    <div className={styles.totalComment}>
                      { t('common.total_comments', { count: totalComments }) }
                    </div>
                    <div
                      className={styles.viewMore}
                      onClick={() => { this.showMoreComment(); }}
                    >
                      { t('common.view_more_group') }
                    </div>
                  </div>
                  <div className={styles.commentsWrap}>
                    {
                      comments.slice(0, 5).map((comment, index) => (
                        <Comment
                          comment={comment}
                          index={index}
                          key={index}
                          onImagesClick={(currentCommentImages, inx) => {
                            this.setState({
                              commentLightboxOpen: true,
                              commentImageIndex: inx,
                              commentImages: currentCommentImages,
                            });
                          }}
                        />
                      ))
                    }
                  </div>
                </div>
                <GraySpace />
              </div>
            }

            <div className={styles.itemDetail}>
              <div
                dangerouslySetInnerHTML={{ __html: pendingProduct.description }}
              />
            </div>
            <div className={styles.actionWrap}>
              <Link
                to="/"
                className={styles.iconWrap}
                onClick={() => {
                  record(actions.CLICK_PRODUCT_BACK_HOME,
                  {},
                  eventCategory.USER_ACTION,
                );
                }}
              >
                <img alt="home" src={homeIcon} className={styles.icon} />
                { t('common.home_page') }
              </Link>
              {/* <Link
                target="_blank"
                to={'http://18824590439.udesk.cn/im_client/?web_plugin_id=56515'}
                className={styles.iconWrap}
              >
                <img
                  alt="customer service"
                  src={customerIcon}
                  className={styles.icon}
                />
                { t('common.custome_service') }
              </Link> */}
              <div
                className={(isActive && canBuy)
                  ? styles.singleBuy
                  : styles.singleBuyDisable}
                onClick={(e) => {
                  if (!isActive || !canBuy) {
                    message.info(t('common.product_cannot_buy'));
                    return;
                  }
                  e.preventDefault(); // 修复 Android 上点击穿透
                  const data = {};
                  // data.event_callback = () => {
                    this.props.showPurchaseModal(SINGLE_BUYING);
                  // };
                  data[eventParams.ID] = this.state.id;
                  record(actions.CLICK_SINGLE_BUY,
                    data,
                    eventCategory.USER_ACTION,
                  );
                }}
              >
                {/* <div
                  className={styles.actionPrice}
                >
                  <MoneyInLocalCurrency cnyAmount={singleBuyPrice} />
                </div> */}
                <div>{ !canBuy
                  ? t('common.product_wait_check')
                  : showUnActiveTips
                    ? t('common.product_has_sold_out')
                    : t('common.buy_single') }</div>
              </div>
              {/* <div
                className={(isActive && canBuy)
                  ? styles.startGroup
                  : styles.startGroupDisable}
                onClick={(e) => {
                  if (!isActive || !canBuy) {
                    return;
                  }
                  e.preventDefault(); // 修复 Android 上点击穿透
                  const data = {};
                  data.event_callback = () => {
                    this.props.showPurchaseModal(GROUP_LEADER_BUYING);
                  };
                  data[eventParams.ID] = this.state.id;
                  record(actions.CLICK_START_GROUP,
                    data,
                    eventCategory.USER_ACTION,
                  );
                }}
              >
                <div className={styles.actionPrice}>
                  <MoneyInLocalCurrency cnyAmount={startGroupPrice} />
                </div>
                <div>{ t('common.start_group') }</div>
              </div> */}
            </div>
            <Modal
              popup
              visible={this.state.showMoreGroup}
              onClose={() => { this.onCloseMoreGroup(); }}
              animationType="slide-up"
              wrapProps={{ onTouchStart: this.onWrapTouchStart }}
            >
              <div
                className={styles.allGroupWrap}
                style={{ maxHeight: `${maxHeight}px` }}
              >
                { fullGroupElement }
              </div>
            </Modal>

            <Modal
              popup
              visible={this.state.showMoreComment}
              onClose={() => { this.onCloseMoreComment(); }}
              animationType="slide-up"
              wrapProps={{ onTouchStart: this.onWrapTouchStart }}
            >
              <div>
                <div className={styles.divider}></div>
                <div
                  className={styles.allCommentWrap}
                  id={'allCommentWrap'}
                  style={{ maxHeight: `${maxHeight}px` }}
                >
                  <ListView
                    renderRow={row}
                    useBodyScroll
                    initialListSize={1000}
                    scrollRenderAheadDistance={500}
                    onEndReachedThreshold={10}
                    rowIdentities={() => null}
                    dataSource={dataSource}
                    onEndReached={this.onEndReached}
                  />
                  {
                    hasMore &&
                    <div className={styles.hasMoreWrap}>
                      <Icon type="loading" size={'md'} />
                    </div>
                  }
                </div>
              </div>
            </Modal>

            <PurchaseModal
              visible={purchaseModalVisible}
              type={purchaseModalType}
              hide={this.props.hidePurchaseModal}
            />
            { lightboxOpen && (
              <Lightbox
                mainSrc={lightboxImages[imageIndex]}
                nextSrc={lightboxImages[(imageIndex + 1) %
                lightboxImages.length]}
                prevSrc={lightboxImages[(imageIndex + lightboxImages.length -
                  1) % lightboxImages.length]}
                onCloseRequest={() => this.setState({ lightboxOpen: false })}
                onMovePrevRequest={() =>
                  this.setState({
                    imageIndex: (imageIndex + lightboxImages.length - 1) %
                    lightboxImages.length,
                  })
                }
                onMoveNextRequest={() =>
                  this.setState({
                    imageIndex: (imageIndex + 1) % lightboxImages.length,
                  })
                }
              />
            ) }

            { commentLightboxOpen && (
              <Lightbox
                mainSrc={commentLightboxImages[commentImageIndex]}
                nextSrc={commentImageIndex + 1 < commentLightboxImages.length ? commentLightboxImages[commentImageIndex + 1] : null}
                prevSrc={commentImageIndex - 1 >= 0 ? commentLightboxImages[commentImageIndex - 1] : null}
                onCloseRequest={() => this.setState({ commentLightboxOpen: false })}
                onMovePrevRequest={() =>
                  this.setState({
                    commentImageIndex: commentImageIndex - 1,
                  })
                }
                onMoveNextRequest={() =>
                  this.setState({
                    commentImageIndex: commentImageIndex + 1,
                  })
                }
              />
            ) }

          </div>
        ) }

        { this.state.showWeChatShareTips && (
          <div
            onClick={
              () => {
                this.setState({
                  showWeChatShareTips: false,
                });
              }
            }
          >
            <WeChatShare />
          </div>
        ) }
        <ShareTips />
      </div>
    );
  }
}

ProductDetail.propTypes = {
  t: PropTypes.func,
  params: PropTypes.object,
  loadProductDetail: PropTypes.func,
  loadingProduct: PropTypes.bool,
  pendingProduct: PropTypes.object,
  showPurchaseModal: PropTypes.func,
  hidePurchaseModal: PropTypes.func,
  purchaseModalVisible: PropTypes.bool,
  purchaseModalType: PropTypes.string,
  loadGroupDetailSuccess: PropTypes.func,
  location: PropTypes.object,
  currentUser: PropTypes.object,
  locationState: PropTypes.object,
  clearPayPreShipmentRedirectUrl: PropTypes.func,
  loadComment: PropTypes.func,
  comments: PropTypes.array,
  totalComments: PropTypes.number,
  loadingComment: PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectLoadingProduct(),
  selectPendingProduct(),
  selectPurchaseModalType(),
  selectPurchaseModalVisible(),
  selectCurrentUser(),
  selectLocationState(),
  selectComments(),
  selectTotalComments(),
  selectLoadingComment(),
  (
    loadingProduct, pendingProduct, purchaseModalType, purchaseModalVisible,
    currentUser, locationState, comments, totalComments, loadingComment) => ({
      loadingProduct,
      pendingProduct: pendingProduct && pendingProduct.toJS &&
    pendingProduct.toJS(),
      purchaseModalType,
      purchaseModalVisible,
      currentUser: currentUser && currentUser.toJS(),
      locationState,
      comments: comments && comments.toJS(),
      totalComments,
      loadingComment,
    }),
);

const mapDispatchToProps = (dispatch) => ({
  showPurchaseModal: (type) => dispatch(showPurchaseModal(type)),
  hidePurchaseModal: () => dispatch(hidePurchaseModal()),
  test: (data) => dispatch(test(data)),
  loadProductDetail: (id) => dispatch(loadProductDetail({ id, recordToHistory: true })),
  loadGroupDetailSuccess: (data) => dispatch(loadGroupDetailSuccess(data)),
  clearPayPreShipmentRedirectUrl: () => dispatch(
    clearPayPreShipmentRedirectUrl()),
  loadComment: (id, page) => dispatch(loadComment(id, page)),
});

export default translate()(
  connect(mapStateToProps, mapDispatchToProps)(ProductDetail));
