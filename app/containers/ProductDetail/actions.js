/*
 *
 * Global actions
 *
 */

import {
  LOAD_COMMENT,
  LOAD_COMMENT_SUCCESS,
  LOAD_COMMENT_FAIL,
} from './constants';

export function loadComment(id, page = 1) {
  return {
    type: LOAD_COMMENT,
    id,
    page,
  };
}

export function loadCommentSuccess(data, total) {
  return {
    type: LOAD_COMMENT_SUCCESS,
    data,
    total,
  };
}


export function loadCommentFail() {
  return {
    type: LOAD_COMMENT_FAIL,
  };
}
