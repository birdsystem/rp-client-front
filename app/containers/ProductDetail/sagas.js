import { call, put, race, take } from 'redux-saga/effects';
import request from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_COMMENT } from './constants';
import {
  loadCommentSuccess,
} from './actions';

export default [
  getCommentWatcher,
];

export function* getCommentWatcher() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadCommentSaga: take(LOAD_COMMENT),
      stop: take(LOCATION_CHANGE),
    });

    if (watcher.stop) break;
    const page = watcher.loadCommentSaga.page;
    const id = watcher.loadCommentSaga.id;
    const requestConfig = {
      urlParams: {
        id,
        page,
      },
    };
    const requestURL = '/product/comment';
    const result = yield call(request, requestURL, requestConfig);
    if (result.success) {
      const list = result.data.list;
      yield put(loadCommentSuccess(list, result.data.total));
    }
  }
}
