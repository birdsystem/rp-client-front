import { createSelector } from 'reselect';

const selectProductDetailDomain = () => (state) => state.get('productdetail');

const selectComments = () => createSelector(
  selectProductDetailDomain(),
  (substate) => substate.get('comments')
);

const selectTotalComments = () => createSelector(
  selectProductDetailDomain(),
  (substate) => substate.get('totalComments')
);

const selectLoadingComment = () => createSelector(
  selectProductDetailDomain(),
  (substate) => substate.get('loadingComment')
);

export {
  selectComments,
  selectTotalComments,
  selectLoadingComment,
};

