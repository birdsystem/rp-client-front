# ttx-client-locale

该仓库用于共享和同步团团侠 [网页版前端项目](https://bitbucket.org/birdsystem/ttx-client-front/) 和 [微信小程序前端项目](https://bitbucket.org/birdsystem/ttx-program/) 的本地化语言文件。

它同时作为前者两个仓库的 subtree 而存在。

## 提交更新

一般情况下，不需要直接更改此仓库的代码，也不要直接在此仓库提交，而是从父仓库（上面提到的两个仓库）使用如下命令提交到此仓库：（注意执行时需要在项目的根目录）

```
git subtree push --prefix=locale目录在父仓库中的位置 git@bitbucket.org:birdsystem/ttx-client-locale.git master
```

对于网页版前端项目（此后简称 W 仓库），凡是涉及到了 `app/locale/` 目录下文件的修改，先正常提交到 W 仓库，然后再通过如下命令提交到此仓库：（注意执行时需要在项目的根目录）

```
git subtree push --prefix=app/locale git@bitbucket.org:birdsystem/ttx-client-locale.git master
```

对于微信小程序前端项目（此后简称 P 仓库），凡是涉及到了 `app/locale/` 目录下文件的修改，先正常提交到 P 仓库，然后再通过如下命令提交到此仓库：（注意执行时需要在项目的根目录）

```
git subtree push --prefix=app/locale git@bitbucket.org:birdsystem/ttx-client-locale.git master
```

## 同步更新

每次更新 W 仓库的代码时，都同时执行如下命令：（注意执行时需要在项目的根目录）

```
git subtree pull --prefix=app/locale git@bitbucket.org:birdsystem/ttx-client-locale.git master
```

每次更新 P 仓库的代码时，都同时执行如下命令：（注意执行时需要在项目的根目录）

```
git subtree pull --prefix=app/locale git@bitbucket.org:birdsystem/ttx-client-locale.git master
```
