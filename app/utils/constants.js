export const WECHAT_PAYMENT_RESULT_FAIL = '0';
export const WECHAT_PAYMENT_RESULT_SUCCESS = '1';
export const WECHAT_PAYMENT_RESULT_PENDING = '2';

export const SINGLE_BUYING = 'SINGLE_BUYING';
export const GROUP_LEADER_BUYING = 'GROUP_LEADER_BUYING';
export const GROUP_BUYING = 'GROUP_BUYING';

export const WAIT_GROUP_SUCCESS = 'WAIT_GROUP_SUCCESS';
export const SUCCESS = 'SUCCESS';
export const FAILED = 'FAILED';
export const WAIT_PAYMENT = 'WAIT_PAYMENT';

export const DEFAULT_PAGE_SIZE = 20;
export const DEFAULT_GROUP_SIZE = 2;
export const DEFAULT_GROUP_LASTING_DAYS = 7;

export const MY_ORDER_TYPE_MY_START = '1';
export const MY_ORDER_TYPE_MY_FOLLOW = '2';
export const MY_ORDER_TYPE_PROCESS = '3';
export const MY_ORDER_TYPE_FINISH = '4';

// order status
export const ORDER_STATUS_PAYMENT_PENDING = 'PAYMENT_PENDING';
export const ORDER_STATUS_GROUP_ACTIVE = 'GROUP_ACTIVE';
export const ORDER_STATUS_GROUP_SUCCESS = 'GROUP_SUCCESS';
export const ORDER_STATUS_GROUP_FAILED = 'GROUP_FAILED';
export const ORDER_STATUS_CANCELLED = 'CANCELLED';
export const ORDER_STATUS_REFUNDED = 'REFUNDED';
export const ORDER_STATUS_WAIT_WAREHOUSE_PURCHASE = 'WAIT_WAREHOUSE_PURCHASE';
export const ORDER_STATUS_WAREHOUSE_PURCHASED = 'WAREHOUSE_PURCHASED';
export const ORDER_STATUS_SUPPLIER_SHIPPED = 'SUPPLIER_SHIPPED';
export const ORDER_STATUS_WAREHOUSE_RECEIVED = 'WAREHOUSE_RECEIVED';
export const ORDER_STATUS_ORDER_SHIPPED = 'ORDER_SHIPPED';
export const ORDER_STATUS_USER_RECEIVED = 'USER_RECEIVED';
export const ORDER_STATUS_PROBLEM = 'PROBLEM';
export const ORDER_STATUS_SUPPLEMENTARY_PAYMENT_PENDING = 'SUPPLEMENTARY_PAYMENT_PENDING';

// group status
export const GROUP_STATUS_ACTIVE = 'ACTIVE';
export const GROUP_STATUS_SUCCESS = 'SUCCESS';
export const GROUP_STATUS_FAILED = 'FAILED';
export const GROUP_STATUS_PENDING = 'PENDING';
export const GROUP_STATUS_FINISH = 'FINISH';

// order actions
export const ORDER_ACTION_COMPLETE_PAYMENT = 'COMPLETE_PAYMENT';
export const ORDER_ACTION_CONFIRM_RECEIPT = 'CONFIRM_RECEIPT';
export const ORDER_ACTION_CANCEL_ORDER = 'CANCEL_ORDER';
export const ORDER_ACTION_TRACK_ORDER = 'TRACK_ORDER';
export const ORDER_ACTION_COMPLETE_SUPPLEMENTARY_PAYMENT = 'COMPLETE_SUPPLEMENTARY_PAYMENT';

// address
export const DEFAULT_COUNTRY_ISO = 'GB';

export const DEFAULT_LOCAL_CURRENCY_CODE = 'GBP';

// product status
export const PRODUCT_STATUS_ACTIVE = 'ACTIVE';
export const PRODUCT_STATUS_WAIT_CHECK = 'WAIT_CHECK';

// share
export const SHARE_TYPE = {
  FOLLOW_GROUP: 'followgroup',
  PRODUCT_DETAIL: 'productdetail',
  GROUP_SHARE: 'groupshare',
};

export const localStorageKeys = {
  ADVERTISEMENT: 'ttx_advertisement',
  RECENT_SEARCH: 'ttx_recent_search',
  LATEST_ACTION: 'ttx_latest_action',
  TOKEN: 'ttx_token',
  ADDRESS: 'ttx_address',
  WECHAT_OPEN_ID: 'ttx_wechat_open_id',
  WECHAT_REDIRECT_URL: 'ttx_wechat_redirect_url',
  LIST_LAYOUT_TYPE: 'ttx_list_layout_type',
  LOGIN_CALLBACK: 'ttx_login_callback',
  WECHAT_TICKET: 'ttx_ticket',
  WECHAT_TICKET_EXPIRE_TIME_KEY: 'ttx_ticket_expire_time',
  HAS_SHOW_SUBMIT_TIPS: 'ttx_has_show_submit_tips',
  LATEST_OUT_ORDER: 'ttx_latest_out_order',
  LATEST_COLLECT_COUPON_TIME: 'ttx_latest_collect_coupon_time',
  LATEST_COLLECT_SHARE_COUPON_TIME: 'ttx_latest_collect_share_coupon_time',
  SHOW_RED_PACKET_AFTER_LOGIN: 'ttx_show_red_packet_after_login',
  HAS_SHOW_TUTORIAL: 'ttx_has_show_tutorial',
  EXCHANGE_RATE_TABLE: 'ttx_exchange_rate_table',
  LOCAL_CURRENCY_CODE: 'ttx_local_currency_code',
  COUNTRY_ISO: 'ttx_country_iso',
};

export const adFrom = {
  dnvod_video: 'dnvod_video',
};

export const whiteList = ['om83_0j8pdUN-BcT50cmCvj9-1PV8', 'om83_0hGvJ9V76YLYeHgaRTT6gHU', 'om83_0nDNL6SO4Rq9uCShLMKMlYE', 'om83_0v5-MDYsIAMfNcSaB8NQuFI',
  'om83_0ttUbqXSv_JaIRSxPHRPwgY', 'om83_0lVhweezGh2O_k_GzaWy-IM', 'om83_0ndR8xZTDwjyepFDrR04ItI', 'om83_0ndwe4ilgq3-ZkzREDT6T54'];

export const categoryMap = {
  腾讯QQ专区: '游戏相关',
  '网游装备/游戏币/帐号/代练': '游戏相关',
  网络游戏点卡: '游戏相关',
  女鞋: '流行男鞋',
  男装: '流行男鞋',
  流行男鞋: '流行男鞋',
  '女装/女士精品': '女士精品',
  '箱包皮具/热销女包/男包': '箱包皮具',
  '女士内衣/男士内衣/家居服': '男女服装',
  '服饰配件/皮带/帽子/围巾': '服饰配件',
  电子元器件市场: '电子元件',
  手机: '数码产品',
  '数码相机/单反相机/摄像机': '数码产品',
  'MP3/MP4/iPod/录音笔': '数码产品',
  笔记本电脑: '数码产品',
  '平板电脑/MID': '数码产品',
  DIY电脑: '数码产品',
  '电脑硬件/显示器/电脑周边': '数码产品',
  '网络设备/网络相关': '数码产品',
  '3C数码配件': '手机数码',
  '闪存卡/U盘/存储/移动硬盘': '数码产品',
  '办公设备/耗材/相关服务': '数码产品',
  '电子词典/电纸书/文化用品': '数码产品',
  '电玩/配件/游戏/攻略': '游戏相关',
  '品牌台机/品牌一体机/服务器': '数码产品',
  二手数码: '数码产品',
  大家电: '家居家电',
  影音电器: '家居家电',
  生活电器: '家居家电',
  厨房电器: '家居家电',
  '个人护理/保健/按摩器材': '个人护理',
  家庭保健: '个人护理',
  '彩妆/香水/美妆工具': '美妆美容',
  '美容护肤/美体/精油': '美妆美容',
  '美发护发/假发': '美妆美容',
  '珠宝/钻石/翡翠/黄金': '美妆美容',
  'ZIPPO/瑞士军刀/眼镜': '日常用品',
  '饰品/流行首饰/时尚饰品新': '美妆美容',
  手表: '时钟手表',
  '奶粉/辅食/营养品/零食': '精选食品',
  '尿片/洗护/喂哺/推车床': '日常用品',
  '孕妇装/孕产妇用品/营养': '日常用品',
  '童装/婴儿装/亲子装': '日常用品',
  '玩具/童车/益智/积木/模型': '儿童玩具',
  '童鞋/婴儿鞋/亲子鞋': '儿童衣饰',
  家居饰品: '家居饰品',
  特色手工艺: '家居饰品',
  住宅家具: '家居饰品',
  '商业/办公家具': '办公家具',
  家装主材: '家装主材',
  基础建材: '基础建材',
  '五金/工具': '五金工具',
  '电子/电工': '电子电工',
  床上用品: '床上用品',
  '装修设计/施工/监理': '装修设计',
  居家布艺: '居家布艺',
  全屋定制: '全屋定制',
  茶: '日常饮品',
  居家日用: '居家日用',
  '厨房/烹饪用具': '烹饪用具',
  '家庭/个人清洁工具': '日常用品',
  传统滋补营养品: '滋补营养',
  '零食/坚果/特产': '零食特产',
  '粮油米面/南北干货/调味品': '南北干货',
  '成人用品/情趣用品': '成人用品',
  'OTC药品/医疗器械/计生用品': '医疗器械',
  '洗护清洁剂/卫生巾/纸/香薰': '日常用品',
  '咖啡/麦片/冲饮': '日常饮品',
  '保健食品/膳食营养补充食品': '保健食品',
  '水产肉类/新鲜蔬果/熟食': '精选食品',
  '节庆用品/礼品': '节庆用品',
  餐饮具: '日常用品',
  收纳整理: '日常用品',
  '运动/瑜伽/健身/球迷用品': '健身用品',
  '户外/登山/野营/旅行用品': '旅行用品',
  '运动服/休闲服装': '服装服饰',
  运动鞋new: '服装服饰',
  '运动包/户外包/配件': '服装服饰',
  '自行车/骑行装备/零配件': '骑行装备',
  '电动车/配件/交通工具': '骑行装备',
  '度假线路/签证送关/旅游服务': '旅游服务',
  '特价酒店/特色客栈/公寓旅馆': '公寓旅馆',
  '古董/邮币/字画/收藏': '日常用品',
  '书籍/杂志/报纸': '日常用品',
  '音乐/影视/明星/音像': '日常用品',
  '乐器/吉他/钢琴/配件': '日常用品',
  '宠物/宠物食品及用品': '宠物相关',
  '景点门票/演艺演出/周边游': '景点门票',
  '模玩/动漫/周边/cos/桌游': '日常用品',
  购物提货券: '日常用品',
  餐饮美食卡券: '日常用品',
  消费卡: '日常用品',
  教育培训: '日常用品',
  '鲜花速递/花卉仿真/绿植园艺': '花卉园艺',
  '网店/网络服务/软件': '忘了相关',
  '房产/租房/新房/二手房/委托服务': '房屋相关',
  '个性定制/设计服务/DIY': '个性定制',
  '电影/演出/体育赛事': '电影赛事',
  '网络店铺代金/优惠券': '网络店铺',
  本地化生活服务: '生活服务',
  '汽车/用品/配件/改装': '汽配相关',
  '新车/二手车': '汽配相关',
  '摩托车/装备/配件': '汽配相关',
  其他: '其他产品',
  众筹: '众筹产品',
  包装: '包装加工',
  淘女郎: '日常用品',
  阿里通信专属类目: '通信相关',
  智能设备: '智能设备',
  农用物资: '农用物资',
  '农机/农具/农膜': '农用物资',
  '畜牧/养殖物资': '农用物资',
  室内设计师: '室内设计',
  装修服务: '装修服务',
  拍卖会专用: '其他产品',
  到家业务: '其他产品',
  家装灯饰光源: '家居饰品',
  美容美体仪器: '美容美体',
  '标准件/零部件/工业耗材': '工业耗材',
  '润滑/胶粘/试剂/实验室耗材': '工业耗材',
  机械设备: '机械设备',
  '搬运/仓储/物流设备': '物流设备',
  '纺织面料/辅料/配套': '纺织面料',
  金属材料及制品: '金属材料',
  橡塑材料及制品: '橡塑材料',
  '清洗/食品/商业设备': '商业设备',
  闲鱼优品: '闲鱼优品',
  '隐形眼镜/护理液': '日常用品',
};
