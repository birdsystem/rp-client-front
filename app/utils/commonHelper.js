/* eslint-disable no-bitwise,no-mixed-operators,no-param-reassign,prefer-rest-params,no-undef */

export function getScrollTop() {
  let scrollTop = 0;
  let bodyScrollTop = 0;
  let documentScrollTop = 0;
  if (document.body) {
    bodyScrollTop = document.body.scrollTop;
  }
  if (document.documentElement) {
    documentScrollTop = document.documentElement.scrollTop;
  }
  scrollTop = (bodyScrollTop - documentScrollTop > 0)
    ? bodyScrollTop
    : documentScrollTop;
  return scrollTop;
}

export function getScrollHeight() {
  let scrollHeight = 0;
  let bodyScrollHeight = 0;
  let documentScrollHeight = 0;
  if (document.body) {
    bodyScrollHeight = document.body.scrollHeight;
  }
  if (document.documentElement) {
    documentScrollHeight = document.documentElement.scrollHeight;
  }
  scrollHeight = (bodyScrollHeight - documentScrollHeight > 0)
    ? bodyScrollHeight
    : documentScrollHeight;
  return scrollHeight;
}

export function getWindowHeight() {
  let windowHeight = 0;
  if (document.compatMode === 'CSS1Compat') {
    windowHeight = document.documentElement.clientHeight;
  } else {
    windowHeight = document.body.clientHeight;
  }
  return windowHeight;
}

export function isOpenInWechat() {
  const ua = window.navigator.userAgent.toLowerCase();
  if (ua.match(/micromessenger/i) || window.WeixinJSBridge) {
    return true;
  }
  return false;
}

export function getTimeLeftDisplay(second) {
  let leftSecond = second;
  let ret = '';

  if (leftSecond > 86400) {
    const day = Math.floor(leftSecond / 86400);
    leftSecond %= 86400;
    ret = `${ret + day}天`;
  }

  if (leftSecond > 3600) {
    const hour = Math.floor(leftSecond / 3600);
    leftSecond %= 3600;
    ret = `${ret + hour}小时`;
  }

  if (leftSecond > 60) {
    const min = Math.floor(leftSecond / 60);
    leftSecond %= 60;
    ret = `${ret + min}分`;
  }

  // ret = `${ret + leftSecond}秒`;

  return ret;
}

export function getTimeLeftDisplaySecond(second) {
  let leftSecond = second;
  let ret = '';

  if (leftSecond > 86400) {
    const day = Math.floor(leftSecond / 86400);
    leftSecond %= 86400;
    ret = `${ret + day}天`;
  }

  if (leftSecond > 3600) {
    const hour = Math.floor(leftSecond / 3600);
    leftSecond %= 3600;
    ret = `${ret + hour}小时`;
  }

  if (leftSecond > 60) {
    const min = Math.floor(leftSecond / 60);
    leftSecond %= 60;
    ret = `${ret + min}分`;
  }
  ret = `${ret + leftSecond}秒`;
  return ret;
}

export function getTimeDispla() {
  const myDate = new Date();
  const year = myDate.getFullYear();
  let month = myDate.getMonth() + 1;
  let date = myDate.getDate();
  let hours = myDate.getHours();
  let minutes = myDate.getMinutes();
  let seconds = myDate.getSeconds();

  if (month < 10) {
    month = `0${month}`;
  }
  if (date < 10) {
    date = `0${date}`;
  }

  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  if (seconds < 10) {
    seconds = `0${seconds}`;
  }

  // 时间拼接
  const dateTime = `${year}-${month}-${date} ${hours}:${minutes}:${seconds}`;
  return dateTime;
}

export function genLrcQueue(limit = 30) {
  const queue = {};
  queue.max = limit;
  queue.cache = new Map();
  queue.get = (key) => {
    let mKey = key;
    if (!isNaN(key)) {
      mKey += '';
    }
    const item = queue.cache.get(mKey);
    if (item) {
      queue.cache.delete(mKey);
      queue.cache.set(mKey, item);
    }
    return item;
  };
  queue.set = (key, val) => {
    let mKey = key;
    if (!isNaN(key)) {
      mKey += '';
    }
    if (queue.cache.has(mKey)) {
      queue.cache.delete(mKey);
    } else if (queue.cache.size === queue.max) {
      queue.cache.delete(queue.first());
    }
    queue.cache.set(mKey, val);
  };
  queue.first = () => queue.cache.keys().next().value;
  return queue;
}

export function isGoogleCrawl() {
  const ua = (navigator && navigator.userAgent) || '';
  return /Googlebot|Mediapartners-Google|Mediapartners|AdsBot-Google|AdsBot-Google-Mobile-Apps/i.test(ua);
}
