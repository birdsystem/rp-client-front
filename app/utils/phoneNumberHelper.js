const isCNMobileNumber = (number) => /^1[3|4|5|6|7|8|9][0-9]{9}$/.test(number);

const isUKMobileNumber = (number) => /^(7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/.test(number);

export default {
  isCNMobileNumber,
  isUKMobileNumber,
};
