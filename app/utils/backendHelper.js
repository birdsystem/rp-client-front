export const isBackendYes = (backendValue) => (backendValue === true || backendValue === 'true' || backendValue === 1 || backendValue === '1');
export const isBackendNo = (backendValue) => (backendValue === false || backendValue === 'false' || backendValue === 0 || backendValue === '0');
