import { isBackendNo } from 'utils/backendHelper';

export function getCouponApplyScopeDisplay(t, coupon) {
  let ret = '';
  if (coupon && coupon.apply_scope) {
    const applyScopeArray = coupon.apply_scope.split(',');
    if (applyScopeArray && applyScopeArray.length > 0) {
      for (let i = 0; i < applyScopeArray.length; i += 1) {
        let service = '';
        if (applyScopeArray[i] === 'ORDER_TOTAL') {
          service = t('coupon.order_total');
        } else if (applyScopeArray[i] === 'PRODUCT_SUBTOTAL') {
          service = t('coupon.product_subtotal');
        } else if (applyScopeArray[i] === 'CROSS_BORDER_SHIPPING_SUBTOTAL') {
          service = t('coupon.cross_border_shipping_subtotal');
        }
        if (i === applyScopeArray.length - 1) {
          ret += service;
        } else {
          ret = `${ret + service} | `;
        }
      }
    }
  }
  return ret;
}

export function isUpcoming(coupon) {
  if (isBackendNo(coupon.is_valid) && isBackendNo(coupon.is_expired) && isBackendNo(coupon.is_used)) {
    return true;
  }
  return false;
}

export default { getCouponApplyScopeDisplay, isUpcoming };
