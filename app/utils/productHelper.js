import _ from 'lodash';
import { isBackendYes } from 'utils/backendHelper';
import { PRODUCT_STATUS_ACTIVE, PRODUCT_STATUS_WAIT_CHECK } from './constants';

export function getDisplayPrice(product) {
  if (!product) return 0;
  return product.price_including_platform_delivery_fee || product.price;
}

export function getStartGroupPrice(product) {
  if (!product) return 0;
  if (isBackendYes(product.is_recommended)) {
    return parseFloat(product.price_including_platform_delivery_fee);
  }
  return parseFloat(product.price_including_platform_delivery_fee) +
    parseFloat(product.lowest_cross_border_shipping);
}

export function getFollowGroupPrice(product) {
  if (!product) return 0;
  if (isBackendYes(product.is_recommended)) {
    return parseFloat(product.price_including_platform_delivery_fee);
  }
  return parseFloat(product.price_including_platform_delivery_fee) +
    parseFloat(product.group_buying_cross_border_shipping);
}

export function getSingleBuyPrice(product) {
  if (!product) return 0;
  return parseFloat(product.price_including_platform_delivery_fee) +
    parseFloat(product.single_buying_cross_border_shipping) +
    parseFloat(product.service_charge);
}

export function getGroupPrice(product, peopleCount = 1) {
  if (!product) return 0;
  const table = product.cross_border_shipping_table;
  let price = parseFloat(product.price_including_platform_delivery_fee);
  if (isBackendYes(product.is_recommended)) {
    return price;
  }
  let founded = false;
  if (table && table.length > 0) {
    for (let i = 0; i < table.length; i += 1) {
      const section = table[i];
      if (peopleCount < section.min) {
        price += parseFloat(section.cross_border_shipping);
        founded = true;
        break;
      }
    }
  }
  if (!founded) {
    // reach lowest price
    price += parseFloat(product.lowest_cross_border_shipping);
  }
  return price;
}

export function getSkuInfoFromProps(product, propPath) {
  if (!product) return null;
  if (!product.meta || !product.meta.sku_info || !product.meta.props ||
    !propPath) {
    return null;
  }
  const skuInfo = product.meta.sku_info;
  const targetSku = _.find(
    skuInfo,
    {
      prop_path: propPath,
    });
  return targetSku;
}

export function getDisplayPriceFromProps(product, propPath) {
  if (!product) return 0;
  if (!product.meta || !product.meta.sku_info || !product.meta.props ||
    !propPath) {
    return getDisplayPrice(product);
  }
  const skuInfo = getSkuInfoFromProps(product, propPath);

  if (!skuInfo) {
    return getDisplayPrice(product);
  }
  return skuInfo.price_including_platform_delivery_fee;
}

export function getStartGroupPriceFromProps(product, propPath) {
  if (!product) return 0;
  if (!product.meta || !product.meta.sku_info || !product.meta.props ||
    !propPath) {
    return getStartGroupPrice(product);
  }

  const skuInfo = getSkuInfoFromProps(product, propPath);
  if (!skuInfo) {
    return getStartGroupPrice(product);
  }
  if (isBackendYes(product.is_recommended)) {
    return parseFloat(skuInfo.price_including_platform_delivery_fee);
  }
  return parseFloat(skuInfo.price_including_platform_delivery_fee) +
    parseFloat(product.lowest_cross_border_shipping);
}

export function getFollowGroupPriceFromProps(product, propPath) {
  if (!product) return 0;
  if (!product.meta || !product.meta.sku_info || !product.meta.props ||
    !propPath) {
    return getFollowGroupPrice(product);
  }
  const skuInfo = getSkuInfoFromProps(product, propPath);
  if (!skuInfo) {
    return getFollowGroupPrice(product);
  }
  return parseFloat(skuInfo.price_including_platform_delivery_fee) +
    parseFloat(product.group_buying_cross_border_shipping);
}

export function getSingleBuyPriceFromProps(product, propPath) {
  if (!product) return 0;
  if (!product.meta || !product.meta.sku_info || !product.meta.props ||
    !propPath) {
    return getSingleBuyPrice(product);
  }
  const skuInfo = getSkuInfoFromProps(product, propPath);
  if (!skuInfo) {
    return getSingleBuyPrice(product);
  }
  return parseFloat(skuInfo.price_including_platform_delivery_fee) +
    parseFloat(product.single_buying_cross_border_shipping) +
    parseFloat(product.service_charge);
}

export function getGroupPriceFromProps(product, propPath, peopleCount = 1) {
  if (!product) return 0;
  if (!product.meta || !product.meta.sku_info || !product.meta.props ||
    !propPath) {
    return getGroupPrice(product, peopleCount);
  }
  const skuInfo = getSkuInfoFromProps(product, propPath);
  if (!skuInfo) {
    return getGroupPrice(product, peopleCount);
  }

  const table = product.cross_border_shipping_table;
  let price = parseFloat(skuInfo.price_including_platform_delivery_fee);
  let founded = false;
  if (table && table.length > 0) {
    for (let i = 0; i < table.length; i += 1) {
      const section = table[i];
      if (peopleCount < section.min) {
        price += parseFloat(section.cross_border_shipping);
        founded = true;
        break;
      }
    }
  }
  if (!founded) {
    // reach lowest price
    price += parseFloat(product.lowest_cross_border_shipping);
  }
  return price;
}

export function isProductActive(product) {
  return product && (product.status === PRODUCT_STATUS_ACTIVE || product.status === PRODUCT_STATUS_WAIT_CHECK);
}

export function getCategoryDisplay(product) {
  if (product && product.category && product.category.length > 0) {
    let ret = '';
    for (let i = 0; i < product.category.length; i += 1) {
      ret += product.category[i].name;
      if (i !== product.category.length - 1) {
        ret += ' => ';
      }
    }
    return ret;
  }
  return '';
}

export default {
  getDisplayPrice,
  getStartGroupPrice,
  getFollowGroupPrice,
  getGroupPrice,
  getSingleBuyPrice,
  getSingleBuyPriceFromProps,
  getDisplayPriceFromProps,
  getStartGroupPriceFromProps,
  getFollowGroupPriceFromProps,
  getGroupPriceFromProps,
  getSkuInfoFromProps,
  isProductActive,
  getCategoryDisplay,
};
