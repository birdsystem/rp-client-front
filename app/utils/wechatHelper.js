/* eslint-disable no-var,camelcase,no-undef,no-bitwise,block-scoped-var,no-plusplus,no-mixed-operators,one-var,vars-on-top,no-array-constructor,no-confusing-arrow,no-nested-ternary,space-in-parens,no-unused-vars,radix,indent */

/* https://birdsystem.atlassian.net/wiki/spaces/BE/pages/661881132/JS+-+SDK */
import i18n from 'i18next';
import { SHARE_TYPE, localStorageKeys, DEFAULT_COUNTRY_ISO, DEFAULT_LOCAL_CURRENCY_CODE } from './constants';
import request from './request';
import {
  record,
  eventCategory,
  actions,
  eventParams,
} from './gaDIRecordHelper';

const LOCAL_TICKET_KEY = localStorageKeys.WECHAT_TICKET;
const LOCAL_TICKET_EXPIRE_TIME_KEY = localStorageKeys.WECHAT_TICKET_EXPIRE_TIME_KEY;
const EXCHANGE_RATE_KEY = localStorageKeys.EXCHANGE_RATE;
const APP_ID = 'wx863aa621ffeaddc3';
const FROM_TAG = 'tfrom';
const FROM_TAG_WECHCAT = 'wechat_friend'; // 微信好友
const FROM_TAG_WECHCAT_TIMELINE = 'wechat_timeline'; // 微信朋友圈
const DIRECT_PARAM = {
  FORCE_DIRECT: 'force_direct',
  DIRECT_TO: 'direct_to',
};
const FROM_USER = 'from_user';
const hexcase = 0;
/* hex output format. 0 - lowercase; 1 - uppercase */
const hex_sha1 = (s) => binb2hex(core_sha1(AlignSHA1(s)));

const core_sha1 = (blockArray) => {
  var x = blockArray; // append padding
  var w = Array(80);
  var a = 1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d = 271733878;
  var e = -1009589776;
  for (let i = 0; i < x.length; i += 16) {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;
    var olde = e;
    for (var j = 0; j < 80; j++) {
      if (j < 16) { w[j] = x[i + j]; } else {
        w[j] = rol(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1);
      }
      var t = safe_add(safe_add(rol(a, 5), sha1_ft(j, b, c, d)),
        safe_add(safe_add(e, w[j]), sha1_kt(j)));
      e = d;
      d = c;
      c = rol(b, 30);
      b = a;
      a = t;
    }
    a = safe_add(a, olda);
    b = safe_add(b, oldb);
    c = safe_add(c, oldc);
    d = safe_add(d, oldd);
    e = safe_add(e, olde);
  }

  return new Array(a, b, c, d, e);
};

/*
 *
 * Perform the appropriate triplet combination function for the current
 * iteration
 *
 * 返回对应F函数的值
 *
 */
const sha1_ft = (t, b, c, d) => {
  if (t < 20) { return (b & c) | ((~b) & d); }
  if (t < 40) { return b ^ c ^ d; }
  if (t < 60) { return (b & c) | (b & d) | (c & d); }
  return b ^ c ^ d; // t<80
};

/*
 *
 * Determine the appropriate additive constant for the current iteration
 *
 * 返回对应的Kt值
 *
 */
const sha1_kt = (t) => (t < 20)
  ? 1518500249
  : (t < 40)
    ? 1859775393
    : (t < 60)
      ? -1894007588
      : -899497514;

/*
 *
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 *
 * to work around bugs in some JS interpreters.
 *
 * 将32位数拆成高16位和低16位分别进行相加，从而实现 MOD 2^32 的加法
 *
 */
const safe_add = (x, y) => {
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
};

/*
 *
 * Bitwise rotate a 32-bit number to the left.
 *
 * 32位二进制数循环左移
 *
 */
const rol = (num, cnt) => (num << cnt) | (num >>> (32 - cnt));

/*
 *
 * The standard SHA1 needs the input string to fit into a block
 *
 * This function align the input string to meet the requirement
 *
 */
const AlignSHA1 = (str) => {
  var nblk = ((str.length + 8) >> 6) + 1,
      blks = new Array(nblk * 16);
  for (var i = 0; i < nblk * 16; i++) { blks[i] = 0; }
  for (i = 0; i < str.length; i++) {
    blks[i >> 2] |= str.charCodeAt(i) << (24 - (i & 3) * 8);
  }
  blks[i >> 2] |= 0x80 << (24 - (i & 3) * 8);
  blks[nblk * 16 - 1] = str.length * 8;
  return blks;
};

/*
 *
 * Convert an array of big-endian words to a hex string.
 *
 */
const binb2hex = (binarray) => {
  var hex_tab = hexcase ? '0123456789ABCDEF' : '0123456789abcdef';
  var str = '';
  for (var i = 0; i < binarray.length * 4; i++) {
    str += hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 + 4)) & 0xF) +
      hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8)) & 0xF);
  }
  return str;
};

const isAndroid = () => {
  const u = navigator.userAgent;
  return u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
};

const configJsSDK = (successCallback, errorCallback) => {
  return;
  if (!window.wx) {
    return;
  }
  window.wx.ready(() => {
    console.log('wx ready');
    if (successCallback) successCallback();
  });

  window.wx.error((err) => {
    if (errorCallback) errorCallback(err);
  });

  const requestConfig = {
    customHandler: true,
    method: 'GET',
  };

  const ticket = localStorage.getItem(LOCAL_TICKET_KEY);
  const ticketExpireTime = (localStorage.getItem(
    LOCAL_TICKET_EXPIRE_TIME_KEY) &&
    parseInt(localStorage.getItem(LOCAL_TICKET_EXPIRE_TIME_KEY))) || 0;
  const currentTime = parseInt((new Date()).getTime() / 1000);
  if (ticket && ticketExpireTime > currentTime) {
    configJsSDKWithTicket(ticket);
    return;
  }

  request('/payment/we-chat-api/get-ticket', requestConfig).then((response) => {
    if (response && response.success) {
      const jsapiTicket = response.data.ticket; // get from server
      const expireIn = response.data.expire_in;
      localStorage.setItem(LOCAL_TICKET_KEY, jsapiTicket);
      if (expireIn) {
        const expireTime = currentTime + parseInt(expireIn) - 1000;
        localStorage.setItem(LOCAL_TICKET_EXPIRE_TIME_KEY, expireTime);
      }
      const timestamp = (new Date()).getTime();
      const noncestr = `abc${timestamp}cd`;
      const url = location.href;
      const qString = `jsapi_ticket=${jsapiTicket}&noncestr=${noncestr}&timestamp=${timestamp}&url=${url}`;
      const signature = hex_sha1(qString);
      let jsApiList = [
        'updateAppMessageShareData',
        'updateTimelineShareData',
        'chooseWXPay'];
      if (isAndroid()) {
        jsApiList = [
          'onMenuShareAppMessage',
          'onMenuShareTimeline',
          'chooseWXPay'];
      }
      configJsSDKWithTicket(jsapiTicket);
    } else {
      console.log(response);
    }
  }, (err) => {
    console.log(err);
  });
};

const configJsSDKWithTicket = (jsapiTicket) => {
  const timestamp = (new Date()).getTime();
  const noncestr = `abc${timestamp}cd`;
  const url = location.href;
  const qString = `jsapi_ticket=${jsapiTicket}&noncestr=${noncestr}&timestamp=${timestamp}&url=${url}`;
  const signature = hex_sha1(qString);
  let jsApiList = [
    'updateAppMessageShareData',
    'updateTimelineShareData',
    'chooseWXPay'];
  if (isAndroid()) {
    jsApiList = [
      'onMenuShareAppMessage',
      'onMenuShareTimeline',
      'chooseWXPay'];
  }
  window.wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: APP_ID,
    timestamp, // 必填，生成签名的时间戳
    nonceStr: noncestr, // 必填，生成签名的随机串
    signature, // 必填，签名
    jsApiList, // 必填，需要使用的JS接口列表
  });
};

const addFromTagToUrl = (url, tag) => {
  let ret = url;
  if (url.indexOf('?') > 0) {
    ret = `${url}&${FROM_TAG}=${tag}`;
  } else {
    ret = `${url}?${FROM_TAG}=${tag}`;
  }
  return ret;
};

const updateAppMessageShareData = (title, desc, link, imgUrl, callback) => {
  let linkWithUserId = link;
  if (window.user && window.user.id) {
    if (link.indexOf('?') > 0) {
      linkWithUserId = `${link}${FROM_USER}=${window.user.id}`;
    } else {
      linkWithUserId = `${link}?${FROM_USER}=${window.user.id}`;
    }
  }
  if (!window.wx) {
    if (callback) {
      callback('un support');
    }
  } else {
    if (isAndroid()) {
      wx.onMenuShareAppMessage({
        title,
        desc,
        link: addFromTagToUrl(linkWithUserId, FROM_TAG_WECHCAT),
        imgUrl,
        success() {
        },
      });
      return;
    }
    wx.updateAppMessageShareData({
      title,
      desc,
      link: addFromTagToUrl(linkWithUserId, FROM_TAG_WECHCAT),
      imgUrl,
    }, (res) => {
      console.log(res);
      if (callback) {
        callback(res);
      }
    });
  }
};

const updateTimelineShareData = (title, desc, link, imgUrl, callback) => {
  let linkWithUserId = link;
  if (window.user && window.user.id) {
    if (link.indexOf('?') > 0) {
      linkWithUserId = `${link}from_user=${window.user.id}`;
    } else {
      linkWithUserId = `${link}?from_user=${window.user.id}`;
    }
  }
  if (!window.wx) {
    if (callback) {
      callback('un support');
    }
  } else {
    if (isAndroid()) {
      wx.onMenuShareTimeline({
        title,
        link: addFromTagToUrl(linkWithUserId, FROM_TAG_WECHCAT_TIMELINE),
        imgUrl,
        success() {
        },
      });
      return;
    }
    wx.updateTimelineShareData({
      title,
      link: addFromTagToUrl(linkWithUserId, FROM_TAG_WECHCAT_TIMELINE),
      imgUrl,
    }, (res) => {
      if (callback) {
        callback(res);
      }
    });
  }
};

const configShareCommon = () => {
  return;
  let url = `${location.protocol}//${location.host}`;
  const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO;
  if (!isAndroid() && window.location.pathname !== '/' && window.location.pathname !== '/home') {
    // enter page isn't home page
    url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${DIRECT_PARAM.FORCE_DIRECT}=true&${DIRECT_PARAM.DIRECT_TO}=${encodeURIComponent('/')}`;
  }
  configJsSDK(
    () => {
      updateAppMessageShareData(i18n.t('share.common.title'),
        i18n.t(`share.common.des_${countryIso}`), url,
        'https://www.tuantuanxia.com/logo_2019-02-01.png',
        (res) => {
          console.log(res);
        },
      );

      updateTimelineShareData(i18n.t('share.common.title'),
        i18n.t(`share.common.des_${countryIso}`), url,
        'https://www.tuantuanxia.com/logo_2019-02-01.png',
        (res) => {
          console.log(res);
        },
      );
    },
  );
};

const getDisplayPrice = (price) => {
  let displayPrice = `${price}元`;
  const localCurrencyCode = localStorage.getItem(localStorageKeys.LOCAL_CURRENCY_CODE) || DEFAULT_LOCAL_CURRENCY_CODE;
  const exchangeRateTable = localStorage.getItem(localStorageKeys.EXCHANGE_RATE_TABLE) && JSON.parse(localStorage.getItem(localStorageKeys.EXCHANGE_RATE_TABLE)) || {};
  const exchangeRate = exchangeRateTable[`${localCurrencyCode.toLowerCase()}_to_cny`];
  if (exchangeRate && !Number.isNaN(parseFloat(price)) && exchangeRate > 0) {
    const amountInLocalCurrency = (parseFloat(price) / parseFloat(exchangeRate)).toFixed(2);
    const currencyList = [{
      code: 'CNY',
      symbol: '¥',
      zhName: '元',
    }, {
      code: 'EUR',
      symbol: '€',
      zhName: '欧',
    }, {
      code: 'GBP',
      symbol: '£',
      zhName: '镑',
    }, {
      code: 'USD',
      symbol: '$',
      zhName: '美元',
    }, {
      code: 'AUD',
      symbol: 'A$',
      zhName: '澳元',
    }];
    const currencyItem = currencyList.find((c) => c.code === localCurrencyCode);
    if (currencyItem) {
      displayPrice = `${currencyItem.symbol}${amountInLocalCurrency}`;
    }
  }
  return displayPrice;
};

const configProductShare = (name, price, productId, image) => {
  const displayPrice = getDisplayPrice(price);
  const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO;

  let url = `${location.protocol}//${location.host}/productdetail/${productId}`;
  if (!isAndroid() && window.location.href.indexOf(`${window.location.protocol}//${window.location.host}/productdetail`) < 0) {
    // enter page isn't product detail page
    const path = encodeURIComponent(`/productdetail/${productId}`);
    url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${DIRECT_PARAM.FORCE_DIRECT}=true&${DIRECT_PARAM.DIRECT_TO}=${path}`;
  }
  const title = `${i18n.t('share.wechat_share_prefix')} ${i18n.t('share.product.price', { price: displayPrice })} ${name}`;
  const des = i18n.t(`share.product.title_${countryIso}`);
  configJsSDK(
    () => {
      updateAppMessageShareData(title, des, url,
        image,
        (res) => {
          console.log(res);
        },
      );

      updateTimelineShareData(title, des, url,
        image,
        (res) => {
          console.log(res);
        },
      );
    });
};

const configGroupShare = (
  name, price, productId, groupId, image, isStart = false) => {
  const displayPrice = getDisplayPrice(price);
  const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO;

  let url = `${location.protocol}//${location.host}/productdetail/${productId}?gid=${groupId}`;
  if (!isAndroid() || window.location.href.indexOf(`${window.location.protocol}//${window.location.host}/productdetail/`) < 0) {
    const path = encodeURIComponent(`/productdetail/${productId}`);
    url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${DIRECT_PARAM.FORCE_DIRECT}=true&${DIRECT_PARAM.DIRECT_TO}=${path}&gid=${groupId}`;
  }
  const title = `${i18n.t('share.wechat_share_prefix')} ${i18n.t('share.group.price', { price: displayPrice })} ${name}`;
  let des = i18n.t(`share.group.follow_title_${countryIso}`);
  if (isStart) {
    des = i18n.t(`share.group.start_title_${countryIso}`);
  }
  configJsSDK(
    () => {
      updateAppMessageShareData(title, des, url,
        image,
        (res) => {
          console.log(res);
        },
      );

      updateTimelineShareData(title, des, url,
        image,
        (res) => {
          console.log(res);
        },
      );
    });
};

const configFollowGroupShare = (name, price, productId, groupId, image) => {
  const displayPrice = getDisplayPrice(price);
  const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO;

  let url = `${location.protocol}//${location.host}/followgroup/${groupId}/${productId}`;
  if (!isAndroid() || window.location.href.indexOf(`${window.location.protocol}//${window.location.host}/followgroup/`) < 0) {
    const path = encodeURIComponent(`/followgroup/${groupId}/${productId}`);
    url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${DIRECT_PARAM.FORCE_DIRECT}=true&${DIRECT_PARAM.DIRECT_TO}=${path}`;
  }
  const title = `${i18n.t('share.wechat_share_prefix')} ${i18n.t('share.group.price', { price: displayPrice })} ${name}`;

  const des = i18n.t(`share.group.follow_title_${countryIso}`);

  configJsSDK(
    () => {
      updateAppMessageShareData(title, des, url,
        image,
        (res) => {
          console.log(res);
        },
      );

      updateTimelineShareData(title, des, url,
        image,
        (res) => {
          console.log(res);
        },
      );
    });
};

const configCouponCollectShare = () => {
  const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO;
  let url = `${location.protocol}//${location.host}/collect-coupon`;
  if (!isAndroid() || window.location.href.indexOf(`${window.location.protocol}//${window.location.host}/collect-coupon`) < 0) {
    const path = encodeURIComponent('/collect-coupon');
    url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${DIRECT_PARAM.FORCE_DIRECT}=true&${DIRECT_PARAM.DIRECT_TO}=${path}`;
  }
  const title = `${i18n.t('share.wechat_share_prefix')} ${i18n.t(`share.coupon.title_${countryIso}`)}`;

  const des = i18n.t(`share.coupon.des_${countryIso}`);

  configJsSDK(
    () => {
      updateAppMessageShareData(title, des, url,
        'https://www.tuantuanxia.com/red.png',
        (res) => {
          console.log(res);
        },
      );

      updateTimelineShareData(title, des, url,
        'https://www.tuantuanxia.com/red.png',
        (res) => {
          console.log(res);
        },
      );
    });
};

const configSubmitProductShare = () => {
  let url = `${location.protocol}//${location.host}/submit-product`;
  const countryIso = localStorage.getItem(localStorageKeys.COUNTRY_ISO) || DEFAULT_COUNTRY_ISO;
  if (!isAndroid() || window.location.href.indexOf(`${window.location.protocol}//${window.location.host}/submit-product`) < 0) {
    const path = encodeURIComponent('/submit-product');
    url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${DIRECT_PARAM.FORCE_DIRECT}=true&${DIRECT_PARAM.DIRECT_TO}=${path}`;
  }
  const title = `${i18n.t('share.wechat_share_prefix')} ${i18n.t(`share.submit.title_${countryIso}`)}`;

  const des = i18n.t('share.submit.des');

  configJsSDK(
    () => {
      updateAppMessageShareData(title, des, url,
        'https://www.tuantuanxia.com/logo_2019-02-01.png',
        (res) => {
          console.log(res);
        },
      );

      updateTimelineShareData(title, des, url,
        'https://www.tuantuanxia.com/logo_2019-02-01.png',
        (res) => {
          console.log(res);
        },
      );
    });
};

const isOpenInWechat = () => {
  const ua = window.navigator.userAgent.toLowerCase();
  if (ua.match(/micromessenger/i) || window.WeixinJSBridge) {
    return true;
  }
  return false;
};

const genInWechatAuthUrl = (state = 'nstate') => {
  const redirectUri = encodeURIComponent(`${window.location.protocol || 'https:'}//${window.location.host || 'www.tuantuanxia.com'}/authcallback`);
  if (window.location.host !== 'www.tuantuanxia.com') {
    return `https://www.tuantuanxia.com/get-weixin-code.html?appid=${APP_ID}&redirect_uri=${redirectUri}&response_type=code&scope=snsapi_userinfo&state=${state}#wechat_redirect`;
  }
  return `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${APP_ID}&redirect_uri=${redirectUri}&response_type=code&scope=snsapi_userinfo&state=${state}#wechat_redirect`;
};

const genWechatServiceUrl = () => 'https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzUzOTk3OTY3NQ==&scene=126#wechat_redirect';

const detectAndRedirect = () => {
  let query = '';
  if (window.location && window.location.search) {
    query = window.location.search.substring(1);
  }
  const vars = query.split('&');
  const queryData = {};
  let search = '';
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    if (pair && pair.length === 2 && pair[0] && pair[1]) {
      queryData[pair[0]] = pair[1];
      if (pair[0] !== DIRECT_PARAM.FORCE_DIRECT && pair[0] !== DIRECT_PARAM.DIRECT_TO) {
        search = `${pair[0]}=${pair[1]}&`;
      }
    }
  }
  if (search) {
    search = search.substr(0, search.length - 1);
  }

  if (queryData[FROM_USER]) {
    record(`enter_from_share_${queryData[FROM_USER]}`, {}, eventCategory.PAGE_ENTER);
  }

  if (queryData[DIRECT_PARAM.FORCE_DIRECT] && queryData[DIRECT_PARAM.DIRECT_TO]) {
    let pathname = decodeURIComponent(queryData[DIRECT_PARAM.DIRECT_TO]);
    if (pathname.lastIndexOf('/') === pathname.length - 1) {
      pathname = pathname.substr(0, pathname.length - 1);
    }
    window.location = `${location.protocol}//${location.host}${pathname}?${search}`;
  }
};

export default {
  hex_sha1,
  configJsSDK,
  updateAppMessageShareData,
  updateTimelineShareData,
  genInWechatAuthUrl,
  isOpenInWechat,
  configShareCommon,
  configProductShare,
  configGroupShare,
  configFollowGroupShare,
  configCouponCollectShare,
  configSubmitProductShare,
  genWechatServiceUrl,
  addFromTagToUrl,
  detectAndRedirect,
  DIRECT_PARAM,
};
