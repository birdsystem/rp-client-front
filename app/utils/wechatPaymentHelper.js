import {
  WECHAT_PAYMENT_RESULT_FAIL,
  WECHAT_PAYMENT_RESULT_PENDING,
  WECHAT_PAYMENT_RESULT_SUCCESS,
} from './constants';

const isPaymentSuccess = (result) => `${result}` === WECHAT_PAYMENT_RESULT_SUCCESS;
const isPaymentFail = (result) => `${result}` === WECHAT_PAYMENT_RESULT_FAIL;
const isPaymentPending = (result) => `${result}` === WECHAT_PAYMENT_RESULT_PENDING;


export default {
  isPaymentSuccess,
  isPaymentFail,
  isPaymentPending,
};
