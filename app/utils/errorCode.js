/**
 * Error Code Wiki:
 * https://birdsystem.atlassian.net/wiki/spaces/TT/pages/703397963/API
 *
 */
import i18n from 'i18next';
import { browserHistory } from 'react-router';

export const ERROR_LEVEL_IGNORE = 'ignore';
export const ERROR_LEVEL_WARNING = 'warning';
export const ERROR_LEVEL_ERROR = 'error';
export const ERROR_LEVEL_FATAL = 'fatal';

const handlerMap = {
  1001: (data) => errorAssessment(false, data.message || '无错误信息', ERROR_LEVEL_FATAL),
  1002: (data) => errorAssessment(false, data.message || i18n.t('error.invalid_param'), ERROR_LEVEL_ERROR),
  1003: (data) => errorAssessment(false, data.message || i18n.t('error.invalid_param'), ERROR_LEVEL_ERROR),
  1004: (data) => errorAssessment(false, data.message || i18n.t('error.system_error'), ERROR_LEVEL_ERROR),
  1005: (data) => errorAssessment(false, data.message || i18n.t('error.cannot_find_record'), ERROR_LEVEL_ERROR),
  1006: (data) => errorAssessment(false, data.message || i18n.t('error.do_not_have_privilege'), ERROR_LEVEL_WARNING),
  1007: (data) => errorAssessment(false, data.message || i18n.t('error.unable_to_log'), ERROR_LEVEL_ERROR),
  1008: (data) => errorAssessment(false, data.message || i18n.t('error.method_not_allowed'), ERROR_LEVEL_ERROR),
  1009: (data) => handleAuthFail(data),
  1403: (data) => handleAuthFail(data),
  1101: (data) => errorAssessment(false, data.message || i18n.t('error.invalid_username_or_password'), ERROR_LEVEL_ERROR),
  1102: (data) => errorAssessment(false, data.message || i18n.t('error.inactive_account'), ERROR_LEVEL_ERROR),
  1103: (data) => errorAssessment(false, data.message || i18n.t('error.phone_or_username_occupied'), ERROR_LEVEL_ERROR),
  1104: (data) => errorAssessment(false, data.message || i18n.t('error.verification_code_too_frequent'), ERROR_LEVEL_ERROR),
  1205: ignore, // 需要先支付之前订单的运费，调用时单独处理报错
  1206: ignore, // 相关支付正在处理中，调用时单独处理报错
  1207: ignore, // 没有需要支付的订单或者运费，调用时单独处理报错
  1208: (data) => errorAssessment(false, data.message || i18n.t('error.you_can_only_collect_coupons_once_a_day'), ERROR_LEVEL_ERROR),
  1209: ignore, // 订单可以用全部用余额，不需要其他支付手段，调用时单独处理报错
  1210: (data) => errorAssessment(false, data.message || i18n.t('error.not_enough_balance'), ERROR_LEVEL_ERROR), // 订单支付余额不足
  1211: (data) => errorAssessment(false, data.message || i18n.t('error.balance_locked'), ERROR_LEVEL_ERROR), // 有余额被锁定不能使用余额支付
  1212: ignore, // 余额未更新，调用时单独处理报错
  1302: ignore, // 拉取商品评论失败，隐藏报错以免影响使用
  1301: ignore, // 提交商品价格超货值限制，调用时单独处理报错
  1304: (data) => errorAssessment(false, data.message, ERROR_LEVEL_ERROR, true), // 无法识别该淘口令域名，出现该错误时需要出发监控报警
  1303: ignore, // 搜索不到合适的商品，正在爬取相关商品，调用时单独处理
  1305: (data) => errorAssessment(false, data.message, ERROR_LEVEL_ERROR), // 所搜商品品类暂不支持
};

function handleAuthFail(data) {
  const invalidPreviousRouteRegex = /^\/(login|register|reset-password)/g;
  const callback = window.location.pathname;
  if (!callback.match(invalidPreviousRouteRegex)) {
    const encodeStr = encodeURIComponent(callback);
    browserHistory.push(`/login?service=${encodeStr}`);
  } else {
    browserHistory.push('/login');
  }
  return errorAssessment(false, data.message || i18n.t('error.please_login_first'), ERROR_LEVEL_WARNING);
}

/**
 * Assessment templates - ignore
 *
 */
function ignore() { // eslint-disable-line
  return errorAssessment(false, '', ERROR_LEVEL_IGNORE); // No messages to delivery, caller will handler this error on his own
}

function defaultHandler(data) { // 原则上一切错误后端都需要返回 message，如果没有的话是需要引起后端注意的，可能是没有考虑到的情况或是其他问题
  return errorAssessment(false, data.message || '无错误信息', ERROR_LEVEL_FATAL);
}

/**
 *  The data structure of handler's return value.
 *
 */

function errorAssessment(success, message, level = ERROR_LEVEL_ERROR, report = false, duration) {
  return {
    success,
    message,
    level,
    report, // 是否触发 telegram 监控报警
    duration, // 错误弹窗显示时长
  };
}

export default function errorCodeHandler(data) {
  const handler = handlerMap[data.errCode || data.errorCode || data.error_code] || defaultHandler; // errCode is standard, but errorCode and error_code might appear sometimes

  return handler(data);
}
