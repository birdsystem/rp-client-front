import 'whatwg-fetch';
import URL from 'url';
import querystring from 'querystring';
import message from 'components/message';
import i18n from 'i18next';
import ProgressModal from 'components/ProgressModal';
import appConfig from 'config';
import {
  localStorageKeys,
} from 'utils/constants';
import errorCodeHandler from './errorCode';

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           An object containing either "data" or "error"
 */
export default function request(url, options) {
  const config = Object.assign({}, options, { credentials: 'include' });

  let fetchUrl = url;

  const systemBaseUrl = `${appConfig.serverHost}`;

  fetchUrl = systemBaseUrl + fetchUrl;

  if (config.method === 'POST' && !config.headers && !config.form) {
    config.headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
    const params = config.body;
    Object.keys(params).forEach((key) => {
      if (typeof params[key] === 'string' || params[key] instanceof String) {
        params[key] = params[key].trim();
      }
    });
    config.body = JSON.stringify(params);
  }

  const urlParams = config.urlParams || {};
  // cache config
  const isGetMethod = config.method === 'GET' || config.method === undefined;
  if (config.cache !== true && isGetMethod) {
    urlParams.ts = new Date().getTime();
  }

  // URL Params
  if (Object.keys(urlParams).length) {
    const urlObject = URL.parse(fetchUrl, true);
    let params = querystring.parse(urlObject.search.slice(1));
    params = Object.assign({}, params, urlParams);
    fetchUrl = URL.resolve(urlObject.href, `?${querystring.stringify(params)}`);
    delete config.urlParams;
  }

  // add token to headers
  const token = localStorage.getItem(localStorageKeys.TOKEN);

  if (token) {
    if (config.headers) {
      config.headers['access-token'] = token;
    } else {
      config.headers = {
        'access-token': token,
      };
    }
  }

  //  Timeout Promise
  handleProgressModal('show', config.feedback);
  const promise = timeoutFetchWithRetries(fetchUrl, config);

  if (config.headers && config.headers.Accept === 'image/png') {
    return promise.then((response) => response.blob());
  }

  if (config.headers && config.headers.Accept === 'text/html') {
    return promise.then((response) => response.text());
  }

  return promise
    .then(checkStatus)
    .then(parseJSON)
    .then((responseJson = {}) => {
      handleProgressModal('hide', config.feedback);
      if (!config.customHandler) { // If customHandler set to true, no error messages will popup, request caller need to handle errors on his own. Do not set customHandler unless you really need to
        const errorData = handleErrorCode(responseJson);
        handleFeedback(errorData);
        if (errorData.report) {
          badRequestReport(errorData.message, url);
        }
      }
      return responseJson;
    }).catch((error) => {
      handleProgressModal('hide', config.feedback);
      const msg = error.toString();
      const errorData = {
        success: false,
        message: msg,
      };
      handleFeedback(errorData);
      badRequestReport(msg, url);
      return errorData;
    });
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  try {
    return response.json();
  } catch (e) {
    throw new Error(i18n.t('error.system_error') + ': Parse JSON failed'); // eslint-disable-line
  }
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {objct} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(i18n.t('error.system_error') + ': Wrong HTTP status ' + response.status); // eslint-disable-line
  error.response = response;
  throw error;
}

/**
 * Handle feedback - global alert
 */
function handleFeedback(errorData) {
  const msg = errorData.message || i18n.t('error.system_error');

  if (!errorData.success) {
    if (errorData.level === 'warning') {
      message.warning(msg, errorData.duration || 2);
    } else if (errorData.level === 'ignore') {
      // do nothing for this kind of insignificant error
    } else {
      message.error(msg, errorData.duration || 3);
    }
  }
}

/**
 * Handle feedback - global progress
 */
function handleProgressModal(action, config = {}) {
  const modalConfig = Object.assign({
    progress: false,
  }, config);

  if (modalConfig.progress && action === 'show') {
    ProgressModal.show(modalConfig.progress || {});
  }

  if (modalConfig.progress && action === 'hide') {
    ProgressModal.hide();
  }
}

/**
 * Timeout Promise
 */
function timeoutPromise(ms, promise) {
  return new Promise((resolve, reject) => {
    const timeoutId = setTimeout(() => {
      const timeoutError = new Error(i18n.t('error.request_timeout'));
      timeoutError.isCustomTimeout = true;
      reject(timeoutError);
    }, ms);
    promise.then(
      (res) => {
        clearTimeout(timeoutId);
        resolve(res);
      },
      (err) => {
        clearTimeout(timeoutId);
        reject(err);
      }
    );
  });
}

function timeoutFetchWithRetries(fetchUrl, config) {
  const ms = config.timeout || 30000;
  const retries = config.retry || 3;

  return new Promise((resolve, reject) => {
    let triedTimes = 1;
    const promiseSuccessHandler = (res) => resolve(res);
    const promiseFailureHanlder = (err) => {
      if (err.isCustomTimeout && triedTimes++ < retries) { // eslint-disable-line
        timeoutPromise(ms, fetch(fetchUrl, config)).then(
          promiseSuccessHandler,
          promiseFailureHanlder
        );
      } else {
        reject(err);
      }
    };

    timeoutPromise(ms, fetch(fetchUrl, config)).then(
      promiseSuccessHandler,
      promiseFailureHanlder
    );
  });
}


/**
 * Handle Error Code
 */
function handleErrorCode(data) {
  if (!data.success) {
    return errorCodeHandler(data);
  }
  return {
    success: true,
  };
}

function badRequestReport(msg, url) {
  const env = window.location.host;
  const fullMsg = `------ REQUEST ERROR ------\n[LOCATION] ${env}\n[ERROR] ${msg}\n[API] ${url}\n-------------------------------`;
  console.log(fullMsg); // eslint-disable-line
  if (/localhost/i.test(env)) return;
  fetch(`https://api.telegram.org/bot569739033:AAHtHuoIAYgcLxHz8CZoc-s-0Mfmo2NO5yk/sendMessage?text=${encodeURI(fullMsg)}&chat_id=-1001123482008`);
}
