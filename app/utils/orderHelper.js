import {
  // ORDER_STATUS_CANCELLED,
  // ORDER_STATUS_GROUP_ACTIVE,
  // ORDER_STATUS_GROUP_FAILED,
  // ORDER_STATUS_GROUP_SUCCESS,
  // ORDER_STATUS_WAIT_WAREHOUSE_PURCHASE,
  // ORDER_STATUS_SUPPLIER_SHIPPED,
  // ORDER_STATUS_ORDER_SHIPPED,
  // ORDER_STATUS_PAYMENT_PENDING,
  // ORDER_STATUS_PROBLEM,
  // ORDER_STATUS_REFUNDED,
  // ORDER_STATUS_USER_RECEIVED,
  // ORDER_STATUS_WAREHOUSE_PURCHASED,
  // ORDER_STATUS_WAREHOUSE_RECEIVED,
  // ORDER_STATUS_SUPPLEMENTARY_PAYMENT_PENDING
  ORDER_ACTION_COMPLETE_PAYMENT,
  ORDER_ACTION_CONFIRM_RECEIPT,
  ORDER_ACTION_CANCEL_ORDER,
  ORDER_ACTION_TRACK_ORDER,
  ORDER_ACTION_COMPLETE_SUPPLEMENTARY_PAYMENT,
} from './constants';

const actionAndStatusTable = {
  mightShowCompletePayment: {
    ORDER_STATUS_PAYMENT_PENDING: true,
  },
  mightShowConfirmReceipt: {
    ORDER_STATUS_ORDER_SHIPPED: true,
  },
  mightShowCancelOrder: {
    ORDER_STATUS_PAYMENT_PENDING: true,
  },
  mightShowTrackOrder: {
    ORDER_STATUS_GROUP_SUCCESS: true,
    ORDER_STATUS_WAIT_WAREHOUSE_PURCHASE: true,
    ORDER_STATUS_WAREHOUSE_PURCHASED: true,
    ORDER_STATUS_SUPPLIER_SHIPPED: true,
    ORDER_STATUS_WAREHOUSE_RECEIVED: true,
    ORDER_STATUS_ORDER_SHIPPED: true,
    ORDER_STATUS_USER_RECEIVED: true,
  },
  mightShowCompleteSupplementaryPayment: {
    ORDER_STATUS_SUPPLEMENTARY_PAYMENT_PENDING: true,
  },
};

const canShow = (detail, action) => {
  if (!detail) return false;
  const status = `ORDER_STATUS_${detail.status}`;
  switch (action) {
    case ORDER_ACTION_COMPLETE_PAYMENT:
      return actionAndStatusTable.mightShowCompletePayment[status];
    case ORDER_ACTION_CONFIRM_RECEIPT:
      return actionAndStatusTable.mightShowConfirmReceipt[status];
    case ORDER_ACTION_CANCEL_ORDER:
      return actionAndStatusTable.mightShowCancelOrder[status];
    case ORDER_ACTION_TRACK_ORDER:
      return actionAndStatusTable.mightShowTrackOrder[status];
    case ORDER_ACTION_COMPLETE_SUPPLEMENTARY_PAYMENT:
      return actionAndStatusTable.mightShowCompleteSupplementaryPayment[status];
    default:
      return false;
  }
};

const canPay = (detail) => canShow(detail, ORDER_ACTION_COMPLETE_PAYMENT);

const canCancel = (detail) => canShow(detail, ORDER_ACTION_CANCEL_ORDER);

const canConfirmReceipt = (detail) => canShow(detail, ORDER_ACTION_CONFIRM_RECEIPT);

const canTrack = (detail) => canShow(detail, ORDER_ACTION_TRACK_ORDER);

const canCompleteSupplementaryPayment = (detail) => canShow(detail, ORDER_ACTION_COMPLETE_SUPPLEMENTARY_PAYMENT);

export default {
  actionAndStatusTable,
  canShow,
  canPay,
  canCancel,
  canConfirmReceipt,
  canTrack,
  canCompleteSupplementaryPayment,
};
