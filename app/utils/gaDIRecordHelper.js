
export const constantsValue = {
  COUPON_START_GROUP: '1001',
  COUPON_FOLLOW_GROUP: '1001',
};

export const eventCategory = {
  PAGE_ENTER: 'page_enter',
  GLOBAL: 'global',
  USER_ACTION: 'user_action',
  USER_EVENT: 'user_event',
  ADVERTISEMENT: 'advertisement',
};

export const customMap = {
  dimension1: 'user_id',
  dimension2: 'time',
};

export const actions = {
  ENTER_HOME_PAGE: 'enter_home_page',
  ENTER_PRODUCT_PAGE: 'enter_product_page',
  ENTER_REGISTER_PAGE: 'enter_register_page',
  ENTER_LOGIN_PAGE: 'enter_login_page',
  ENTER_SEARCH_PAGE: 'enter_search_page',
  ENTER_SEARCH_RESULT_PAGE: 'enter_search_result_page',
  ENTER_FOLLOW_GROUP_PAGE: 'enter_follow_group_page',
  ENTER_RECOMMEND_PAGE: 'enter_recommend_page',
  ENTER_MY_SUBMIT_PRODUCT_LIST_PAGE: 'enter_my_submit_product_list_page',
  ENTER_MY_ORDER_LIST_PAGE: 'enter_my_order_list_page',
  ENTER_MY_INFO_PAGE: 'enter_my_info_page',
  ENTER_COMPLETE_ADDRESS_PAGE: 'enter_complete_address_page',
  ENTER_PAYMENT_PAGE: 'enter_payment_page',
  ENTER_PAY_PRE_SHIPMENT_PAGE: 'enter_pay_pre_shipment_page',
  ENTER_PAYMENT_RESULT_PAGE: 'enter_payment_result_page',
  ENTER_SUBMIT_PRODUCT_PAGE: 'enter_submit_product_page',
  ENTER_CATEGORY_PRODUCT_PAGE: 'enter_category_product_page',
  ENTER_RESET_PASSWORD_PAGE: 'enter_reset_password_page',
  CLICK_PRODUCT_FROM_HOME: 'click_product_from_home',
  CLICK_PRODUCT_FROM_PRODUCT_LIST: 'click_product_from_product_list',
  CLICK_PRODUCT_FROM_CATEGORY_PRODUCT_LIST: 'click_product_from_category_product_list',
  CLICK_BANNER: 'click_banner',
  CLICK_SINGLE_BUY: 'click_single_buy',
  CLICK_START_GROUP: 'click_start_group',
  CLICK_FOLLOW_GROUP: 'click_follow_group', // 产品页面的follow
  CLICK_PAY: 'click_pay',
  CLICK_SUBMIT_PRODUCT_LINK: 'click_submit_product_link',
  CLICK_HOME_CATEGORY: 'click_home_category',
  CLICK_HOME_SEARCH: 'click_home_search',
  CLICK_HOME_PRODUCT_ITEM: 'click_home_product_item',
  CLICK_HOME_NEWS: 'click_home_news',
  CLICK_PRODUCT_BACK_HOME: 'click_product_back_home',
  CLICK_PRODUCT_PROPS_START: 'click_product_props_start',
  CLICK_PRODUCT_PROPS_FOLLOW: 'click_product_props_follow',
  CLICK_PRODUCT_PROPS_SINGLE: 'click_product_props_single',
  CLICK_PRODUCT_BUY_START: 'click_product_buy_start',
  CLICK_PRODUCT_BUY_FOLLOW: 'click_product_buy_follow',
  CLICK_PRODUCT_BUY_SINGLE: 'click_product_buy_single',
  CLICK_FOLLOW_GROUP_FOLLOW: 'click_follow_group_follow', // 分享页面的follow
  CLICK_FOLLOW_GROUP_PRODUCT_PROPS_FOLLOW: 'click_follow_group_product_props_follow', // 分享页面的商品属性
  CLICK_FOLLOW_GROUP_PRODUCT_BUY_FOLLOW: 'click_follow_group_product_buy_follow',
  CLICK_RECOMMEND_PRODUCT_ITEM: 'click_recommend_product_item',
  CLICK_RECOMMEND_BACK_HOME: 'click_recommend_back_home',
  CLICK_RECOMMEND_BANNER: 'click_recommend_banner',
  REGISTER_SUCCESS: 'register_success',
  LOGIN: 'login',
  LOGIN_SUCCESS: 'login_success',
  VIEW_TUTORIAL: 'view_tutorial',

  VIEW_ITEM: 'view_item', // start with ecommerce track https://developers.google.com/analytics/devguides/collection/gtagjs/enhanced-ecommerce
  BEGIN_CHECKOUT: 'begin_checkout',
  SET_CHECKOUT_OPTION: 'set_checkout_option',
  CHECKOUT_PROGRESS: 'checkout_progress',
  PURCHASE: 'purchase',
  REFUND: 'refund',

  FROM_DNVOD_VIDEO: 'from_dnvod_video',
  TAB_HOME: 'tab_home',
  TAB_SUBMIT_PRODUCT: 'tab_submit_product',
  TAB_ORDER: 'tab_order',
  TAB_ME: 'tab_me',
};

export const eventParams = {
  USER_ID: 'user_id',
  TIME: 'time',
  ID: 'id',
  LINK: 'link',
  GROUP_ID: 'group_id',
  SEARCH: 'search',
};

export function createFunctionWithTimeout(callback, optTimeout) {
  let called = false;
  function fn() {
    if (!called) {
      called = true;
      callback();
    }
  }
  setTimeout(fn, optTimeout || 1000);
  return fn;
}

export function record(action, _params = {}, category, label, value) {
  if (window.gtag) {
    const mParams = Object.assign(_params);
    if (category) {
      mParams.event_category = category;
    }
    if (label) {
      mParams.event_label = label;
    }
    if (value) {
      mParams.value = value;
    }
    if (window.user) {
      mParams[eventParams.USER_ID] = window.user.id;
    }
    if (mParams.event_callback) {
      mParams.event_callback = createFunctionWithTimeout(mParams.event_callback, 1000);
    }
    mParams[eventParams.TIME] = (new Date()).getTime();
    // console.log('log event ------------------');
    // console.log(action);
    // console.log(mParams);
    try {
      window.gtag('event', action, mParams);
    } catch (e) {
      console.log(e);
    }
  }
}
