import { injectAll } from 'utils/asyncInjectors';
import ProgressModal from 'components/ProgressModal';
import wechatHelper from 'utils/wechatHelper';
const loadingModal = {
  show: () => ProgressModal.show({ mask: true }),
  hide: () => ProgressModal.hide(),
};

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

function createBaseRoutes(store) {
  return [
    {
      path: '/',
      name: 'home',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/Home'),
          System.import('containers/Home/reducer'),
          System.import('containers/Home/sagas'),
        ]).then(injectAll(store, 'home', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/healthz',
      name: 'healthz',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/Home'),
          System.import('containers/Home/reducer'),
          System.import('containers/Home/sagas'),
        ]).then(injectAll(store, 'home', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/login',
      name: 'login',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Login')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/register',
      name: 'register',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Register')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/reset-password',
      name: 'resetPassword',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/ResetPassword')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/change-password',
      name: 'changePassword',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/ChangePassword')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/payment/success',
      name: 'paymentSuccess',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Payment/Result')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/payment/:orderId',
      name: 'payment',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/Payment'),
          System.import('containers/Payment/reducer'),
          System.import('containers/Payment/sagas'),
        ]).then(injectAll(store, 'payment', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/tracking/:orderId',
      name: 'tracking',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/Tracking'),
          System.import('containers/Tracking/reducer'),
          System.import('containers/Tracking/sagas'),
        ]).then(injectAll(store, 'tracking', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/mysubmitproductlist',
      name: 'mysubmitproductlist',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/MySubmitProductList'),
          System.import('containers/MySubmitProductList/reducer'),
          System.import('containers/MySubmitProductList/sagas'),
        ]).then(injectAll(store, 'mysubmitproductlist', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/myinfo',
      name: 'myinfo',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/MyInfo')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/product-browse-history',
      name: 'productBrowseHistory',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/ProductBrowseHistory'),
          System.import('containers/ProductBrowseHistory/reducer'),
          System.import('containers/ProductBrowseHistory/sagas'),
        ]).then(injectAll(store, 'productBrowseHistory', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/receiveaddress',
      name: 'receiveaddress',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/ReceiveAddress')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/productdetail/:productId',
      name: 'productdetail',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/ProductDetail'),
          System.import('containers/ProductDetail/reducer'),
          System.import('containers/ProductDetail/sagas'),
        ]).then(injectAll(store, 'productdetail', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/orderlist/:type',
      name: 'orderlist',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/OrderList'),
          System.import('containers/OrderList/reducer'),
          System.import('containers/OrderList/sagas'),
        ]).then(injectAll(store, 'orderlist', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/myorderlist',
      name: 'myorderlist',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/MyOrderList'),
          System.import('containers/MyOrderList/reducer'),
          System.import('containers/MyOrderList/sagas'),
        ]).then(injectAll(store, 'myorderlist', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/authcallback',
      name: 'authcallback',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/AuthCallback')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/search',
      name: 'search',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/Search'),
          System.import('containers/Search/reducer'),
          System.import('containers/Search/sagas'),
        ]).then(injectAll(store, 'search', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/searchresult',
      name: 'searchresult',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/SearchResult'),
          System.import('containers/SearchResult/reducer'),
          System.import('containers/SearchResult/sagas'),
        ]).then(injectAll(store, 'searchresult', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/address',
      name: 'address',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Address')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/categories',
      name: 'categories',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Categories')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/coupons',
      name: 'coupons',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Coupons')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/collect-coupon',
      name: 'collectCoupon',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/CollectCoupon')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/categoryproductlist/:cid',
      name: 'categoryproductlist',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/CategoryProductList'),
          System.import('containers/CategoryProductList/reducer'),
          System.import('containers/CategoryProductList/sagas'),
        ]).then(injectAll(store, 'categoryproductlist', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/groupdetail/:groupId',
      name: 'groupdetail',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/GroupDetail'),
          System.import('containers/GroupDetail/reducer'),
          System.import('containers/GroupDetail/sagas'),
        ]).then(injectAll(store, 'groupdetail', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/orderdetail/:orderId',
      name: 'orderdetail',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/OrderDetail'),
          System.import('containers/OrderDetail/reducer'),
          System.import('containers/OrderDetail/sagas'),
        ]).then(injectAll(store, 'orderdetail', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/followgroup/:groupId/:productId',
      name: 'followgroup',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/FollowGroup'),
          System.import('containers/FollowGroup/reducer'),
          System.import('containers/FollowGroup/sagas'),
        ]).then(injectAll(store, 'followgroup', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/submit-product',
      name: 'submitProduct',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/SubmitProduct')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/payment-records',
      name: 'paymentRecords',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/PaymentRecords')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/follow-official-account',
      name: 'followOfficialAccount',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/FollowOfficialAccount')
        .then((component) => cb(null, component.default)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/notice',
      name: 'notice',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/Notice')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/logga',
      name: 'logga',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/LogGA')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/couponpackage',
      name: 'couponpackage',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/CouponPackage')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '/recommend',
      name: 'recommend',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        Promise.all([
          System.import('containers/Recommend'),
          System.import('containers/Recommend/reducer'),
          System.import('containers/Recommend/sagas'),
        ]).then(injectAll(store, 'recommend', cb)).then(loadingModal.hide()).catch(errorLoading);
      },
    },
    {
      path: '/weekly-recommendation',
      name: 'weeklyRecommendationPost',
      getComponent: (nextState, cb) => {
        loadingModal.show();
        System.import('containers/WeeklyRecommendationPost')
        .then((component) => cb(null, component.default))
        .then(loadingModal.hide())
        .catch(errorLoading);
      },
    },
    {
      path: '*',
      name: 'notFound',
      getComponent(nextState, cb) {
        loadingModal.show();
        System.import('components/NotFoundPage')
          .then((component) => cb(null, component.default))
          .then(loadingModal.hide())
          .catch(errorLoading);
      },
    },
  ];
}

export default function createRoutes(store) {
  return {
    onEnter: (() => {
      wechatHelper.detectAndRedirect();
    })(),
    getComponent: (nextState, cb) => {
      loadingModal.show();
      Promise.all([
        System.import('containers/App'),
        System.import('containers/App/reducer'),
        System.import('containers/App/sagas'),
      ]).then(injectAll(store, 'global', cb)).then(loadingModal.hide()).catch(errorLoading);
    },
    childRoutes: createBaseRoutes(store),
  };
}
