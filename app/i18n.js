import i18n from 'i18next';
import enGB from 'locale/en_GB';
import zhCN from 'locale/zh_CN';

const localeResources = {
  en_GB: { translation: enGB },
  zh_CN: { translation: zhCN },
};

i18n.init({
  lng: 'zh_CN',
  resources: localeResources,
  fallbackLng: 'en_GB',
  interpolation: {
    escapeValue: false, // not needed for react!!
  },
});

export default i18n;
